-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-08-2020 a las 16:39:25
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sisalmacen`
--
CREATE DATABASE IF NOT EXISTS `sisalmacen` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci;
USE `sisalmacen`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area`
--

DROP TABLE IF EXISTS `area`;
CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(30) COLLATE utf8mb4_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `area`
--

INSERT INTO `area` (`id`, `descripcion`) VALUES
(1, 'DIRECTIVOS'),
(2, 'COORDINADORES'),
(3, 'ASESOR JURIDICO'),
(4, 'PERSONAL ADMINISTRATIVO'),
(5, 'PERSONAL DE SERVICIO'),
(6, 'PERSONAL DE PROFOCOM');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cant_aut`
--

DROP TABLE IF EXISTS `cant_aut`;
CREATE TABLE `cant_aut` (
  `id` int(11) NOT NULL,
  `ca` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `material_id` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `cant_aut`
--

INSERT INTO `cant_aut` (`id`, `ca`, `area_id`, `material_id`, `fecha_creacion`) VALUES
(1, 10, 1, 1, '2018-08-01 17:38:54'),
(2, 10, 1, 2, '2018-08-01 17:38:54'),
(3, 10, 1, 4, '2018-08-01 17:38:54'),
(4, 10, 1, 6, '2018-08-01 17:38:55'),
(5, 10, 1, 3, '2018-08-01 17:38:55'),
(6, 10, 1, 5, '2018-08-01 17:38:55'),
(7, 10, 1, 8, '2018-08-01 17:38:55'),
(8, 10, 1, 7, '2018-08-01 17:38:55'),
(9, 10, 1, 9, '2018-08-01 17:38:55'),
(10, 10, 1, 10, '2018-08-01 17:38:55'),
(11, 10, 4, 1, '2018-08-28 23:27:04'),
(12, 5, 4, 2, '2018-08-28 23:27:04'),
(13, 10, 4, 8, '2018-08-28 23:27:04'),
(14, 10, 4, 6, '2018-08-28 23:27:05'),
(15, 10, 4, 3, '2018-08-28 23:27:05'),
(16, 10, 4, 7, '2018-08-28 23:27:05'),
(17, 10, 4, 4, '2018-08-28 23:27:05'),
(18, 10, 4, 5, '2018-08-28 23:27:05'),
(19, 10, 4, 9, '2018-08-28 23:27:05'),
(20, 10, 4, 10, '2018-08-28 23:27:05'),
(21, 5, 2, 1, '2018-08-28 23:36:54'),
(22, 3, 1, 11, '2020-08-18 19:10:27'),
(23, 3, 1, 13, '2020-08-18 19:11:00'),
(24, 3, 1, 15, '2020-08-18 19:11:35'),
(25, 4, 1, 16, '2020-08-18 19:12:11'),
(26, 4, 1, 18, '2020-08-18 19:12:57'),
(27, 2, 1, 31, '2020-08-18 19:13:44'),
(28, 4, 1, 33, '2020-08-18 19:15:14'),
(29, 5, 1, 36, '2020-08-18 19:15:52'),
(30, 5, 2, 2, '2020-08-18 19:16:59'),
(31, 5, 3, 1, '2020-08-18 19:18:50'),
(32, 4, 2, 6, '2020-08-18 19:19:37'),
(33, 10, 2, 11, '2020-08-18 19:19:51'),
(34, 10, 2, 13, '2020-08-18 19:20:29'),
(35, 1, 2, 26, '2020-08-18 19:23:29'),
(36, 5, 5, 40, '2020-08-18 19:23:47'),
(37, 2, 5, 41, '2020-08-18 19:24:05'),
(38, 5, 6, 8, '2020-08-18 19:30:05'),
(39, 20, 1, 12, '2020-08-20 10:00:23'),
(40, 10, 1, 20, '2020-08-22 08:23:26'),
(41, 10, 1, 21, '2020-08-22 08:23:26'),
(42, 3, 1, 23, '2020-08-22 08:23:26'),
(43, 5, 1, 17, '2020-08-22 08:23:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

DROP TABLE IF EXISTS `cargo`;
CREATE TABLE `cargo` (
  `id` int(11) NOT NULL,
  `nomcargo` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`id`, `nomcargo`, `area_id`) VALUES
(1, 'DIRECTOR GENERAL', 1),
(2, 'DIRECTOR ACADEMICO', 1),
(3, 'DIRECTOR ADMINISTRATIVO-FINANCIERO', 1),
(4, 'COORDINADOR IEPC-PEC', 2),
(5, 'CONTADOR GENERAL', 4),
(6, 'TECNICO SISTEMA INFORMATICO', 4),
(7, 'TECNICO CAJERO', 4),
(8, 'SECRETARIA ACADEMICA', 4),
(9, 'RESPONSABLE DE BIENES Y SERVICIOS', 4),
(10, 'BIBLIOTECARIA', 4),
(11, 'RESPONSABLE DE SALUD PREVENTIVA', 4),
(12, 'SECRETARIA ADMINISTRATIVA-FINANCIERA', 4),
(13, 'RESPONSABLE DE ARCHIVO Y KARDEX', 4),
(14, 'MENSAJERO', 5),
(15, 'PORTERA', 5),
(16, 'FACILITADOR PROFOCOM', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

DROP TABLE IF EXISTS `categoria`;
CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `tipo` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `nombre_cat` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `tipo`, `nombre_cat`, `fecha_creacion`) VALUES
(1, 'PE', 'Papeleria de Escritorio', '2018-08-01 17:12:51'),
(2, 'EO', 'Material de Escritorio y Oficina', '2020-08-18 14:54:32'),
(3, 'PAG', 'Productos de Artes Grafiza Papel y Carton', '2020-08-18 14:57:33'),
(4, 'ML', 'MATERIAL DE LIMPIEZA', '2020-08-18 15:00:10'),
(5, 'MMP', 'PRODUCTOS MINERALES NO METALICOS Y PLASTICOS', '2020-08-18 15:01:31'),
(6, 'LAB', 'MATERIAL DE LABORATORIO', '2020-08-18 15:17:19'),
(7, 'UEC', 'UTILES DE EDUCACION CULTURALES', '2020-08-18 15:18:29'),
(8, 'MD', 'MATERIAL DEPORTIVO', '2020-08-18 15:19:10'),
(9, 'ELC', 'MATERIAL ELECTRICO', '2020-08-18 19:27:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_pedido`
--

DROP TABLE IF EXISTS `estado_pedido`;
CREATE TABLE `estado_pedido` (
  `id` int(11) NOT NULL,
  `estado` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `estado_pedido`
--

INSERT INTO `estado_pedido` (`id`, `estado`) VALUES
(1, 'derivado'),
(2, 'entregado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funcionario`
--

DROP TABLE IF EXISTS `funcionario`;
CREATE TABLE `funcionario` (
  `id` int(11) NOT NULL,
  `ci` int(11) DEFAULT NULL,
  `exp` varchar(20) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `nombres` varchar(35) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `appaterno` varchar(40) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `apmaterno` varchar(40) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `sexo` varchar(10) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `direccion` varchar(30) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `telefono` varchar(24) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `correo` varchar(24) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `cargo_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `funcionario`
--

INSERT INTO `funcionario` (`id`, `ci`, `exp`, `nombres`, `appaterno`, `apmaterno`, `sexo`, `direccion`, `telefono`, `correo`, `cargo_id`) VALUES
(0, 123456, '', 'Nolia', 'chamairo', 'Espejo', 'mujer', 'La Paz-El Alto', '76568197', 'nolia@gmail.com', 1),
(1, 2799344, 'OR', 'GERARDO', 'BUENO', 'QUISPE', 'hombre', 'NORMAL-EL ALTO', '77739304', 'gerardoB@gmail.com', 1),
(2, 4622210, 'LP', 'LUCAS ABDON', 'LINCON', 'HEREDIA', 'hombre', 'NORMAL-EL ALTO', '71272278', 'linconLuc@gmail.com', 2),
(3, 3326424, 'LP', 'SILVERIO', 'ALARCON', 'CRUZ', 'hombre', 'NORMAL-EL ALTO', '72278130', 'nocuenta@gmail.com', 3),
(4, 3719315, 'LP', 'RUBEN', 'QUISPE', 'MAMANI', 'hombre', 'NORMAL-EL ALTO', '76745261', 'rubenQ@gmail.com', 4),
(5, 46238740, 'CBBA', 'HUGO REMBERTO', 'ESPEJO', 'LOPEZ', 'hombre', 'NORMAL-EL ALTO', '76821005', 'hugoE@gmail.com', 4),
(6, 4777547, 'LP', 'ROSA MARIA', 'ATAHUACHI', 'ROJAS', 'mujer', 'NORMAL-EL ALTO', '00000000', 'nocuenta@gmail.com', 5),
(7, 9225178, 'LP', 'BRAYAN CRISTIAN', 'FLORES', 'MONRROY', 'hombre', 'NORMAL-EL ALTO', '00000000', 'nocuenta@gmail.com', 6),
(8, 0, 'LP', 'DENIS', 'MAMANI', 'GOMEZ', 'mujer', 'NORMAL-EL ALTO', '00000000', 'nocuenta@gmail.com', 7),
(9, 6877226, 'LP', 'FABIOLA CINTHYA', 'MAMANI', 'QUISPE', 'mujer', 'NORMAL-EL ALTO', '00000000', 'nocuenta@gmail.com', 8),
(10, 6019571, 'LP', 'KAREN ROCIO', 'MATIAS', 'FLORES', 'mujer', 'NORMAL-EL ALTO', '00000000', 'nocuenta@gmail.com', 9),
(11, 6951471, 'LP', 'ROSSE MARY', 'MORALES', 'VILLCA', 'mujer', 'NORMAL-EL ALTO', '00000000', 'nocuenta@gmail.com', 10),
(12, 8363830, 'LP', 'Nolia', 'Chamairo', 'Espejo', 'mujer', 'el alto av.jaimendoza#031', '76568185', 'noe_chamairo@gmail.com', 6),
(13, 8363831, 'LP', 'lucia', 'peñas', 'lopez', 'mujer', 'av. juan pablo II', '76568185', 'luci@gmail.com', 15);

--
-- Disparadores `funcionario`
--
DROP TRIGGER IF EXISTS `deletefuncionario`;
DELIMITER $$
CREATE TRIGGER `deletefuncionario` AFTER DELETE ON `funcionario` FOR EACH ROW BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |ci=',ifnull(old.ci,''),
    ' |exp=',ifnull(old.exp,''),
    ' |nombres=',ifnull(old.nombres,''),
    ' |appaterno=',ifnull(old.appaterno,''),
    ' |apmaterno=',ifnull(old.apmaterno,''),
    ' |sexo=',ifnull(old.sexo,''),
    ' |direccion=',ifnull(old.direccion,''),
    ' |telefono=',ifnull(old.telefono,''),
    ' |correo=',ifnull(old.correo,''),
    ' |cargo_id=',ifnull(old.cargo_id,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"funcionario");
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `insertfuncionario`;
DELIMITER $$
CREATE TRIGGER `insertfuncionario` BEFORE INSERT ON `funcionario` FOR EACH ROW BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |ci=',ifnull(new.ci,''),
    ' |exp=',ifnull(new.exp,''),
    ' |nombres=',ifnull(new.nombres,''),
    ' |appaterno=',ifnull(new.appaterno,''),
    ' |apmaterno=',ifnull(new.apmaterno,''),
    ' |sexo=',ifnull(new.sexo,''),
    ' |direccion=',ifnull(new.direccion,''),
    ' |telefono=',ifnull(new.telefono,''),
    ' |correo=',ifnull(new.correo,''),
    ' |cargo_id=',ifnull(new.cargo_id,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"funcionario");
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `updatefuncionario`;
DELIMITER $$
CREATE TRIGGER `updatefuncionario` BEFORE UPDATE ON `funcionario` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN
  #concatenamos los campos de la tabla a auditar y verificamos que no sean null, en caso de que los campos sean null agregamos un espacio
  #las variables (new,old)son de mysql, el valor old es el que ya se tenia en la tabla y el new es el valor que se modifico

  #Valores viejos
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |ci=',ifnull(old.ci,''),
    ' |exp=',ifnull(old.exp,''),
    ' |nombres=',ifnull(old.nombres,''),
    ' |appaterno=',ifnull(old.appaterno,''),
    ' |apmaterno=',ifnull(old.apmaterno,''),
    ' |sexo=',ifnull(old.sexo,''),
    ' |direccion=',ifnull(old.direccion,''),
    ' |telefono=',ifnull(old.telefono,''),
    ' |correo=',ifnull(old.correo,''),
    ' |cargo_id=',ifnull(old.cargo_id,'')
  );
  #Valores nuevos
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |ci=',ifnull(new.ci,''),
    ' |exp=',ifnull(new.exp,''),
    ' |nombres=',ifnull(new.nombres,''),
    ' |appaterno=',ifnull(new.appaterno,''),
    ' |apmaterno=',ifnull(new.apmaterno,''),
    ' |sexo=',ifnull(new.sexo,''),
    ' |direccion=',ifnull(new.direccion,''),
    ' |telefono=',ifnull(new.telefono,''),
    ' |correo=',ifnull(new.correo,''),
    ' |cargo_id=',ifnull(new.cargo_id,'')
  );

	#guardamos en una variable los valores que unicamente cambiaron
  set @res = '';
	IF OLD.ci <> new.ci THEN set @res = CONCAT (@res, ' |Cambió el valor de ci: ',ifnull(OLD.ci,''), ' a: ',ifnull(new.ci,'')); END IF;
	IF OLD.exp <> new.exp THEN set @res = CONCAT (@res, ' |Cambió el valor de exp: ',ifnull(OLD.exp,''), ' a: ',ifnull(new.exp,'')); END IF;
	IF OLD.nombres <> new.nombres THEN set @res = CONCAT (@res, ' |Cambió el valor de nombres: ',ifnull(OLD.nombres,''), ' a: ',ifnull(new.nombres,'')); END IF;
	IF OLD.appaterno <> new.appaterno THEN set @res = CONCAT (@res, ' |Cambió el valor de appaterno: ',ifnull(OLD.appaterno,''), ' a: ',ifnull(new.appaterno,'')); END IF;
	IF OLD.apmaterno <> new.apmaterno THEN set @res = CONCAT (@res, ' |Cambió el valor de apmaterno: ',ifnull(OLD.apmaterno,''), ' a: ',ifnull(new.apmaterno,'')); END IF;
	IF OLD.sexo <> new.sexo THEN set @res = CONCAT (@res, ' |Cambió el valor de sexo: ',ifnull(OLD.sexo,''), ' a: ',ifnull(new.sexo,'')); END IF;
	IF OLD.direccion <> new.direccion THEN set @res = CONCAT (@res, ' |Cambió el valor de direccion: ',ifnull(OLD.direccion,''), ' a: ',ifnull(new.direccion,'')); END IF;
	IF OLD.telefono <> new.telefono THEN set @res = CONCAT (@res, ' |Cambió el valor de telefono: ',ifnull(OLD.telefono,''), ' a: ',ifnull(new.telefono,'')); END IF;
	IF OLD.correo <> new.correo THEN set @res = CONCAT (@res, ' |Cambió el valor de correo: ',ifnull(OLD.correo,''), ' a: ',ifnull(new.correo,'')); END IF;
	IF OLD.cargo_id <> new.cargo_id THEN set @res = CONCAT (@res, ' |Cambió el valor de cargo_id: ',ifnull(OLD.cargo_id,''), ' a: ',ifnull(new.cargo_id,'')); END IF;

	#insertamos en nuestra tabla de log la informacion
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla,valor_alterado)
	VALUES (@oldq ,@newq,CURRENT_USER,"UPDATE",NOW(),"funcionario",ifnull(@res,'No cambio nada'));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `tabla` varchar(255) DEFAULT NULL,
  `old` text,
  `new` text,
  `valor_alterado` text,
  `usuario` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `fecha` timestamp(6) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(6)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `logs`
--

INSERT INTO `logs` (`id`, `tipo`, `tabla`, `old`, `new`, `valor_alterado`, `usuario`, `ip`, `fecha`) VALUES
(1379, 'INSERT', 'usuario', '', ' |id=0 |nombre=LUCAS |estado=0 |funcionario_id=2 |rol_id=1', NULL, 'root@localhost', NULL, '2020-08-17 22:46:22.000000'),
(1380, 'UPDATE', 'funcionario', ' |id=1 |ci=2799344 |exp=OR |nombres=GERARDO |appaterno=BUENO |apmaterno=QUISPE |sexo=hombre |direccion=NORMAL-EL ALTO |telefono=00000000 |correo=nocuenta@gmail.com |cargo_id=1', ' |id=1 |ci=2799344 |exp=OR |nombres=GERARDO |appaterno=BUENO |apmaterno=QUISPE |sexo=hombre |direccion=NORMAL-EL ALTO |telefono=77739304 |correo=gerardoB@gmail.com |cargo_id=1', ' |Cambió el valor de telefono: 00000000 a: 77739304 |Cambió el valor de correo: nocuenta@gmail.com a: gerardoB@gmail.com', 'root@localhost', NULL, '2020-08-17 22:53:48.000000'),
(1381, 'UPDATE', 'funcionario', ' |id=2 |ci=0 |exp=LP |nombres=LUCAS ABDON |appaterno=LINCON |apmaterno=HEREDIA |sexo=hombre |direccion=NORMAL-EL ALTO |telefono=00000000 |correo=nocuenta@gmail.com |cargo_id=2', ' |id=2 |ci=0 |exp=LP |nombres=LUCAS ABDON |appaterno=LINCON |apmaterno=HEREDIA |sexo=hombre |direccion=NORMAL-EL ALTO |telefono=71272278 |correo=linconLuc@gmail.com |cargo_id=2', ' |Cambió el valor de telefono: 00000000 a: 71272278 |Cambió el valor de correo: nocuenta@gmail.com a: linconLuc@gmail.com', 'root@localhost', NULL, '2020-08-17 22:57:45.000000'),
(1382, 'UPDATE', 'funcionario', ' |id=2 |ci=0 |exp=LP |nombres=LUCAS ABDON |appaterno=LINCON |apmaterno=HEREDIA |sexo=hombre |direccion=NORMAL-EL ALTO |telefono=71272278 |correo=linconLuc@gmail.com |cargo_id=2', ' |id=2 |ci=4622210 |exp=LP |nombres=LUCAS ABDON |appaterno=LINCON |apmaterno=HEREDIA |sexo=hombre |direccion=NORMAL-EL ALTO |telefono=71272278 |correo=linconLuc@gmail.com |cargo_id=2', ' |Cambió el valor de ci: 0 a: 4622210', 'root@localhost', NULL, '2020-08-17 22:57:59.000000'),
(1383, 'UPDATE', 'funcionario', ' |id=3 |ci=3326424 |exp=LP |nombres=SILVERIO |appaterno=ALARCON |apmaterno=CRUZ |sexo=hombre |direccion=NORMAL-EL ALTO |telefono=00000000 |correo=nocuenta@gmail.com |cargo_id=3', ' |id=3 |ci=3326424 |exp=LP |nombres=SILVERIO |appaterno=ALARCON |apmaterno=CRUZ |sexo=hombre |direccion=NORMAL-EL ALTO |telefono=72278130 |correo=nocuenta@gmail.com |cargo_id=3', ' |Cambió el valor de telefono: 00000000 a: 72278130', 'root@localhost', NULL, '2020-08-18 17:48:05.000000'),
(1384, 'UPDATE', 'funcionario', ' |id=4 |ci=3719315 |exp=LP |nombres=RUBEN |appaterno=QUISPE |apmaterno=MAMANI |sexo=hombre |direccion=NORMAL-EL ALTO |telefono=00000000 |correo=nocuenta@gmail.com |cargo_id=4', ' |id=4 |ci=3719315 |exp=LP |nombres=RUBEN |appaterno=QUISPE |apmaterno=MAMANI |sexo=hombre |direccion=NORMAL-EL ALTO |telefono=76745261 |correo=rubenQ@gmail.com |cargo_id=4', ' |Cambió el valor de telefono: 00000000 a: 76745261 |Cambió el valor de correo: nocuenta@gmail.com a: rubenQ@gmail.com', 'root@localhost', NULL, '2020-08-18 17:49:20.000000'),
(1385, 'UPDATE', 'funcionario', ' |id=5 |ci=0 |exp=LP |nombres=HUGO REMBERTO |appaterno=ESPEJO |apmaterno=LOPEZ |sexo=hombre |direccion=NORMAL-EL ALTO |telefono=00000000 |correo=nocuenta@gmail.com |cargo_id=4', ' |id=5 |ci=46238740 |exp=CBBA |nombres=HUGO REMBERTO |appaterno=ESPEJO |apmaterno=LOPEZ |sexo=hombre |direccion=NORMAL-EL ALTO |telefono=76821005 |correo=hugoE@gmail.com |cargo_id=4', ' |Cambió el valor de ci: 0 a: 46238740 |Cambió el valor de exp: LP a: CBBA |Cambió el valor de telefono: 00000000 a: 76821005 |Cambió el valor de correo: nocuenta@gmail.com a: hugoE@gmail.com', 'root@localhost', NULL, '2020-08-18 17:51:00.000000'),
(1386, 'INSERT', 'funcionario', '', ' |id=0 |ci=8363830 |exp=LP |nombres=Nolia |appaterno=Chamairo |apmaterno=Espejo |sexo=mujer |direccion=el alto av.jaimendoza#031 |telefono=76568185 |correo=noe_chamairo@gmail.com |cargo_id=6', NULL, 'root@localhost', NULL, '2020-08-18 18:39:24.000000'),
(1387, 'INSERT', 'usuario', '', ' |id=0 |nombre=noe |estado=1 |funcionario_id=12 |rol_id=2', NULL, 'root@localhost', NULL, '2020-08-18 18:43:12.000000'),
(1388, 'INSERT', 'proveedor', '', ' |id=0 |Nit=22001084 |empresa=libreria JL SRL |nombres=Lucia |apellidos=Quisbert |direccion=El Alto Av. Juan Pablo II N3201 |telefono=2204611 |correo=JL@gmail.com |fecha_creacion=2020-08-18 14:50:02', NULL, 'root@localhost', NULL, '2020-08-18 18:50:02.000000'),
(1389, 'INSERT', 'pedido', '', ' |id= |estado_pedido_id=1 |funcionario_id=10 |proveedor_id= |usuario_id=1 |transaccion_tipo_id=2 |Nro_factura= |partida_presup= |fuente= |fecha_creacion=2020-08-18 15:02:10 |fecha_entrega=', NULL, 'root@localhost', NULL, '2020-08-18 19:02:10.000000'),
(1390, 'INSERT', 'transaccion', '', ' |id= |material_id=1 |precio_unitario= |q=2 |qe= |transaccion_tipo_id=2 |pedido_id=14 |obs= |fecha_creacion=2020-08-18 15:02:10', NULL, 'root@localhost', NULL, '2020-08-18 19:02:10.000000'),
(1391, 'UPDATE', 'material', ' |id=3 |codigo=321005 |nom_material=PAPEL BOND |descripcion=TAMAÑO CARTA DE COLOR CELESTE |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=1 |fecha_creacion=2018-08-28 13:11:10', ' |id=3 |codigo=321003 |nom_material=PAPEL BOND |descripcion=TAMAÑO CARTA DE COLOR CELESTE |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=1 |fecha_creacion=2020-08-18 15:22:27', ' |Cambió el valor de codigo: 321005 a: 321003 |Cambió el valor de fecha_creacion: 2018-08-28 13:11:10 a: 2020-08-18 15:22:27', 'root@localhost', NULL, '2020-08-18 19:22:27.000000'),
(1392, 'UPDATE', 'material', ' |id=4 |codigo=321008 |nom_material=PAPEL BOND |descripcion=TAMAÑO CARTA DE COLOR FUXIA |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=1 |fecha_creacion=2018-08-28 13:12:21', ' |id=4 |codigo=321004 |nom_material=PAPEL BOND |descripcion=TAMAÑO CARTA DE COLOR FUXIA |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=1 |fecha_creacion=2020-08-18 15:22:36', ' |Cambió el valor de codigo: 321008 a: 321004 |Cambió el valor de fecha_creacion: 2018-08-28 13:12:21 a: 2020-08-18 15:22:36', 'root@localhost', NULL, '2020-08-18 19:22:36.000000'),
(1393, 'UPDATE', 'material', ' |id=5 |codigo=321012 |nom_material=PAPEL |descripcion=PARA FAX |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=1 |fecha_creacion=2018-08-28 13:13:01', ' |id=5 |codigo=321005 |nom_material=PAPEL |descripcion=PARA FAX |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=1 |fecha_creacion=2020-08-18 15:22:47', ' |Cambió el valor de codigo: 321012 a: 321005 |Cambió el valor de fecha_creacion: 2018-08-28 13:13:01 a: 2020-08-18 15:22:47', 'root@localhost', NULL, '2020-08-18 19:22:47.000000'),
(1394, 'UPDATE', 'material', ' |id=6 |codigo=321013 |nom_material=PAPEL |descripcion=COUCHE TAMAÑO OFICIO |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=1 |fecha_creacion=2018-08-28 13:13:44', ' |id=6 |codigo=321006 |nom_material=PAPEL |descripcion=COUCHE TAMAÑO OFICIO |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=1 |fecha_creacion=2020-08-18 15:23:09', ' |Cambió el valor de codigo: 321013 a: 321006 |Cambió el valor de fecha_creacion: 2018-08-28 13:13:44 a: 2020-08-18 15:23:09', 'root@localhost', NULL, '2020-08-18 19:23:09.000000'),
(1395, 'UPDATE', 'material', ' |id=2 |codigo=321002 |nom_material=PAPEL BOND |descripcion=TAMAÑO OFICIO |min_inventario=10 |presentacion=Pqte. |activo=1 |categoria_id=1 |fecha_creacion=2018-08-28 13:10:27', ' |id=2 |codigo=321006 |nom_material=PAPEL BOND |descripcion=TAMAÑO OFICIO |min_inventario=10 |presentacion=Pqte. |activo=1 |categoria_id=1 |fecha_creacion=2020-08-18 15:23:20', ' |Cambió el valor de codigo: 321002 a: 321006 |Cambió el valor de fecha_creacion: 2018-08-28 13:10:27 a: 2020-08-18 15:23:20', 'root@localhost', NULL, '2020-08-18 19:23:20.000000'),
(1396, 'UPDATE', 'material', ' |id=7 |codigo=321015 |nom_material=PAPEL HILADO |descripcion=TAMAÑO OFICIO COLOR CREMA |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=1 |fecha_creacion=2018-08-28 13:14:33', ' |id=7 |codigo=321006 |nom_material=PAPEL HILADO |descripcion=TAMAÑO OFICIO COLOR CREMA |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=1 |fecha_creacion=2020-08-18 15:23:44', ' |Cambió el valor de codigo: 321015 a: 321006 |Cambió el valor de fecha_creacion: 2018-08-28 13:14:33 a: 2020-08-18 15:23:44', 'root@localhost', NULL, '2020-08-18 19:23:44.000000'),
(1397, 'UPDATE', 'material', ' |id=7 |codigo=321006 |nom_material=PAPEL HILADO |descripcion=TAMAÑO OFICIO COLOR CREMA |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=1 |fecha_creacion=2020-08-18 15:23:44', ' |id=7 |codigo=321007 |nom_material=PAPEL HILADO |descripcion=TAMAÑO OFICIO COLOR CREMA |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=1 |fecha_creacion=2020-08-18 15:24:30', ' |Cambió el valor de codigo: 321006 a: 321007 |Cambió el valor de fecha_creacion: 2020-08-18 15:23:44 a: 2020-08-18 15:24:30', 'root@localhost', NULL, '2020-08-18 19:24:30.000000'),
(1398, 'UPDATE', 'material', ' |id=8 |codigo=321017 |nom_material=PAPEL BOND |descripcion=TAMAÑO RESMA |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=1 |fecha_creacion=2018-08-28 13:15:13', ' |id=8 |codigo=321008 |nom_material=PAPEL BOND |descripcion=TAMAÑO RESMA |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=1 |fecha_creacion=2020-08-18 15:24:45', ' |Cambió el valor de codigo: 321017 a: 321008 |Cambió el valor de fecha_creacion: 2018-08-28 13:15:13 a: 2020-08-18 15:24:45', 'root@localhost', NULL, '2020-08-18 19:24:45.000000'),
(1399, 'UPDATE', 'material', ' |id=9 |codigo=321018 |nom_material=PAPEL  ESPECIAL |descripcion=TAMAÑO CARTA (CASCARA DE HUEVO) |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=1 |fecha_creacion=2018-08-28 13:16:01', ' |id=9 |codigo=321009 |nom_material=PAPEL  ESPECIAL |descripcion=TAMAÑO CARTA (CASCARA DE HUEVO) |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=1 |fecha_creacion=2020-08-18 15:25:01', ' |Cambió el valor de codigo: 321018 a: 321009 |Cambió el valor de fecha_creacion: 2018-08-28 13:16:01 a: 2020-08-18 15:25:01', 'root@localhost', NULL, '2020-08-18 19:25:01.000000'),
(1400, 'UPDATE', 'material', ' |id=10 |codigo=321019 |nom_material=PAPEL |descripcion=TAMAÑO CARTA DE VARIOS COLORES |min_inventario=10 |presentacion=Pqte. |activo=1 |categoria_id=1 |fecha_creacion=2018-08-28 13:16:45', ' |id=10 |codigo=321010 |nom_material=PAPEL |descripcion=TAMAÑO CARTA DE VARIOS COLORES |min_inventario=10 |presentacion=Pqte. |activo=1 |categoria_id=1 |fecha_creacion=2020-08-18 15:25:18', ' |Cambió el valor de codigo: 321019 a: 321010 |Cambió el valor de fecha_creacion: 2018-08-28 13:16:45 a: 2020-08-18 15:25:18', 'root@localhost', NULL, '2020-08-18 19:25:18.000000'),
(1401, 'INSERT', 'material', '', ' |id= |codigo=331001 |nom_material=BOLIGRAFO  ROJO |descripcion=PILOT |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 15:59:19', NULL, 'root@localhost', NULL, '2020-08-18 19:59:19.000000'),
(1402, 'INSERT', 'material', '', ' |id= |codigo=331003 |nom_material=BOLIGRAFO AZUL |descripcion=PILOT |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 16:00:31', NULL, 'root@localhost', NULL, '2020-08-18 20:00:31.000000'),
(1403, 'UPDATE', 'material', ' |id=12 |codigo=331003 |nom_material=BOLIGRAFO AZUL |descripcion=PILOT |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 16:00:31', ' |id=12 |codigo=331002 |nom_material=BOLIGRAFO AZUL |descripcion=PILOT |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 16:04:26', ' |Cambió el valor de codigo: 331003 a: 331002 |Cambió el valor de fecha_creacion: 2020-08-18 16:00:31 a: 2020-08-18 16:04:26', 'root@localhost', NULL, '2020-08-18 20:04:26.000000'),
(1404, 'INSERT', 'material', '', ' |id= |codigo=331003 |nom_material=BOLIGRAFO NEGRO |descripcion=PILOT |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 16:07:31', NULL, 'root@localhost', NULL, '2020-08-18 20:07:31.000000'),
(1405, 'INSERT', 'material', '', ' |id= |codigo=331004 |nom_material=TINTA DE TAMPO AZUL |descripcion=PEQUEÑO |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 16:17:16', NULL, 'root@localhost', NULL, '2020-08-18 20:17:16.000000'),
(1406, 'INSERT', 'material', '', ' |id= |codigo=331005 |nom_material=TINTA DE TAMPO ROJO |descripcion=NEUTRO |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 17:14:40', NULL, 'root@localhost', NULL, '2020-08-18 21:14:40.000000'),
(1407, 'INSERT', 'material', '', ' |id= |codigo=331006 |nom_material=CUADERNO TAMAÑO MEDIO OFICIO |descripcion=CON ASPIRAL |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 17:15:50', NULL, 'root@localhost', NULL, '2020-08-18 21:15:50.000000'),
(1408, 'INSERT', 'material', '', ' |id= |codigo=331007 |nom_material=CUADERNO TAMAÑO OFICIO |descripcion=CON ESPIRAL |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 17:20:09', NULL, 'root@localhost', NULL, '2020-08-18 21:20:09.000000'),
(1409, 'INSERT', 'material', '', ' |id= |codigo=331008 |nom_material=CUADERNO TAMAÑO CARTA |descripcion=CON ASPIRAL |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 17:29:24', NULL, 'root@localhost', NULL, '2020-08-18 21:29:24.000000'),
(1410, 'INSERT', 'material', '', ' |id= |codigo=331009 |nom_material=PORTAL BLOCK DE NOTAS |descripcion=ARTESCO |min_inventario=3 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 17:31:08', NULL, 'root@localhost', NULL, '2020-08-18 21:31:08.000000'),
(1411, 'INSERT', 'material', '', ' |id= |codigo=331010 |nom_material=RADEX |descripcion=LIQUIDO |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 17:31:49', NULL, 'root@localhost', NULL, '2020-08-18 21:31:49.000000'),
(1412, 'INSERT', 'material', '', ' |id= |codigo=331011 |nom_material=LAPIZ NEGRO |descripcion=STABILO |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 17:35:11', NULL, 'root@localhost', NULL, '2020-08-18 21:35:11.000000'),
(1413, 'INSERT', 'material', '', ' |id= |codigo=331012 |nom_material=LAPIZ ROJO |descripcion=STABILO |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 17:37:06', NULL, 'root@localhost', NULL, '2020-08-18 21:37:06.000000'),
(1414, 'INSERT', 'material', '', ' |id= |codigo=331014 |nom_material=BLOK DE NOTAS |descripcion=ADESIVO POSIT |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 17:41:22', NULL, 'root@localhost', NULL, '2020-08-18 21:41:22.000000'),
(1415, 'UPDATE', 'material', ' |id=23 |codigo=331014 |nom_material=BLOK DE NOTAS |descripcion=ADESIVO POSIT |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 17:41:22', ' |id=23 |codigo=331013 |nom_material=BLOK DE NOTAS |descripcion=ADESIVO POSIT |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 17:41:56', ' |Cambió el valor de codigo: 331014 a: 331013 |Cambió el valor de fecha_creacion: 2020-08-18 17:41:22 a: 2020-08-18 17:41:56', 'root@localhost', NULL, '2020-08-18 21:41:56.000000'),
(1416, 'INSERT', 'material', '', ' |id= |codigo=331014 |nom_material=CDs |descripcion=PRINCO |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 17:42:33', NULL, 'root@localhost', NULL, '2020-08-18 21:42:33.000000'),
(1417, 'INSERT', 'material', '', ' |id= |codigo=331015 |nom_material=FUNDAS PARA CDs |descripcion=PLASTICAS |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 17:43:53', NULL, 'root@localhost', NULL, '2020-08-18 21:43:53.000000'),
(1418, 'INSERT', 'material', '', ' |id= |codigo=331016 |nom_material=ENGRAMPADORA |descripcion=MEDIANA |min_inventario=2 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 17:44:57', NULL, 'root@localhost', NULL, '2020-08-18 21:44:57.000000'),
(1419, 'INSERT', 'material', '', ' |id= |codigo=331017 |nom_material=CLIPS |descripcion=PEQUEÑO |min_inventario=2 |presentacion=Pqte. |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 17:58:56', NULL, 'root@localhost', NULL, '2020-08-18 21:58:56.000000'),
(1420, 'INSERT', 'material', '', ' |id= |codigo=331018 |nom_material=CLIPS |descripcion=MEDIANO |min_inventario=2 |presentacion=Pqte. |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 18:00:49', NULL, 'root@localhost', NULL, '2020-08-18 22:00:49.000000'),
(1421, 'INSERT', 'material', '', ' |id= |codigo=331019 |nom_material=GRAPAS |descripcion=MADISON |min_inventario=5 |presentacion=Pqte. |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 18:02:04', NULL, 'root@localhost', NULL, '2020-08-18 22:02:04.000000'),
(1422, 'INSERT', 'material', '', ' |id= |codigo=331020 |nom_material=TIJERA MUNDIAÑ |descripcion=MEDIANO |min_inventario=3 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 18:23:08', NULL, 'root@localhost', NULL, '2020-08-18 22:23:08.000000'),
(1423, 'INSERT', 'material', '', ' |id= |codigo=331021 |nom_material=MARCADOR COLOR NEGRO |descripcion=ACRILICO |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 18:24:55', NULL, 'root@localhost', NULL, '2020-08-18 22:24:55.000000'),
(1424, 'INSERT', 'material', '', ' |id= |codigo=331022 |nom_material=MARCADOR COLOR AZUL |descripcion=ACRILICO |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 18:25:40', NULL, 'root@localhost', NULL, '2020-08-18 22:25:40.000000'),
(1425, 'INSERT', 'material', '', ' |id= |codigo=331023 |nom_material=MARCADOR COLOR ROJO |descripcion=ACRILICO |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 18:46:02', NULL, 'root@localhost', NULL, '2020-08-18 22:46:02.000000'),
(1426, 'INSERT', 'material', '', ' |id= |codigo=331024 |nom_material=MARCADOR COLOR VERDE |descripcion=ACRILICO |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 18:46:41', NULL, 'root@localhost', NULL, '2020-08-18 22:46:41.000000'),
(1427, 'INSERT', 'material', '', ' |id= |codigo=331025 |nom_material=ARCHIVADOR |descripcion=TAMAÑO OFICIO |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=2 |fecha_creacion=2020-08-18 18:50:46', NULL, 'root@localhost', NULL, '2020-08-18 22:50:46.000000'),
(1428, 'INSERT', 'material', '', ' |id= |codigo=341001 |nom_material=SOBRE MANILA TAMAÑO CARTA |descripcion=MATERIAL DE CARTON |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=3 |fecha_creacion=2020-08-18 18:57:07', NULL, 'root@localhost', NULL, '2020-08-18 22:57:07.000000'),
(1429, 'INSERT', 'material', '', ' |id= |codigo=341002 |nom_material=SOBRE MANILA TAMAÑO OFICIO |descripcion=MATERIAL DE CARTON |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=3 |fecha_creacion=2020-08-18 18:57:43', NULL, 'root@localhost', NULL, '2020-08-18 22:57:43.000000'),
(1430, 'INSERT', 'material', '', ' |id= |codigo=341003 |nom_material=SOBRE MANILA TAMAÑO DOBLE OFICIO |descripcion=MATERIAL DE CARTON |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=3 |fecha_creacion=2020-08-18 18:58:32', NULL, 'root@localhost', NULL, '2020-08-18 22:58:32.000000'),
(1431, 'INSERT', 'material', '', ' |id= |codigo=341004 |nom_material=ARCHIVADORES DE PALANCA |descripcion=MATERIAL DE CARTON |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=3 |fecha_creacion=2020-08-18 18:59:39', NULL, 'root@localhost', NULL, '2020-08-18 22:59:39.000000'),
(1432, 'INSERT', 'material', '', ' |id= |codigo=351001 |nom_material=DETERGENTE EN POLVO |descripcion=ACE OMO |min_inventario=5 |presentacion=unidad |activo=1 |categoria_id=4 |fecha_creacion=2020-08-18 19:00:45', NULL, 'root@localhost', NULL, '2020-08-18 23:00:45.000000'),
(1433, 'INSERT', 'material', '', ' |id= |codigo=351002 |nom_material=ESCOBAS |descripcion=PLASTICAS DOBLES |min_inventario=3 |presentacion=unidad |activo=1 |categoria_id=4 |fecha_creacion=2020-08-18 19:02:56', NULL, 'root@localhost', NULL, '2020-08-18 23:02:56.000000'),
(1434, 'INSERT', 'pedido', '', ' |id= |estado_pedido_id=1 |funcionario_id=10 |proveedor_id= |usuario_id=1 |transaccion_tipo_id=2 |Nro_factura= |partida_presup= |fuente= |fecha_creacion=2020-08-18 19:32:30 |fecha_entrega=', NULL, 'root@localhost', NULL, '2020-08-18 23:32:30.000000'),
(1435, 'INSERT', 'transaccion', '', ' |id= |material_id=1 |precio_unitario= |q=1 |qe= |transaccion_tipo_id=2 |pedido_id=15 |obs= |fecha_creacion=2020-08-18 19:32:30', NULL, 'root@localhost', NULL, '2020-08-18 23:32:30.000000'),
(1436, 'INSERT', 'transaccion', '', ' |id= |material_id=3 |precio_unitario= |q=1 |qe= |transaccion_tipo_id=2 |pedido_id=15 |obs= |fecha_creacion=2020-08-18 19:32:30', NULL, 'root@localhost', NULL, '2020-08-18 23:32:30.000000'),
(1437, 'INSERT', 'pedido', '', ' |id= |estado_pedido_id= |funcionario_id=10 |proveedor_id=2 |usuario_id=1 |transaccion_tipo_id=1 |Nro_factura=2210 |partida_presup=321006 |fuente=01/20 |fecha_creacion=2020-08-18 19:53:50 |fecha_entrega=', NULL, 'root@localhost', NULL, '2020-08-18 23:53:50.000000'),
(1438, 'INSERT', 'transaccion', '', ' |id= |material_id=6 |precio_unitario=30.00 |q=100 |qe= |transaccion_tipo_id=1 |pedido_id=16 |obs= |fecha_creacion=2020-08-18 19:53:50', NULL, 'root@localhost', NULL, '2020-08-18 23:53:50.000000'),
(1439, 'INSERT', 'pedido', '', ' |id= |estado_pedido_id= |funcionario_id=10 |proveedor_id=2 |usuario_id=1 |transaccion_tipo_id=1 |Nro_factura=35101 |partida_presup=331001 |fuente=02/0820 |fecha_creacion=2020-08-18 20:02:05 |fecha_entrega=', NULL, 'root@localhost', NULL, '2020-08-19 00:02:05.000000'),
(1440, 'INSERT', 'transaccion', '', ' |id= |material_id=11 |precio_unitario=3.00 |q=100 |qe= |transaccion_tipo_id=1 |pedido_id=17 |obs= |fecha_creacion=2020-08-18 20:02:05', NULL, 'root@localhost', NULL, '2020-08-19 00:02:05.000000'),
(1441, 'INSERT', 'transaccion', '', ' |id= |material_id=12 |precio_unitario=3.00 |q=100 |qe= |transaccion_tipo_id=1 |pedido_id=17 |obs= |fecha_creacion=2020-08-18 20:02:05', NULL, 'root@localhost', NULL, '2020-08-19 00:02:05.000000'),
(1442, 'INSERT', 'transaccion', '', ' |id= |material_id=17 |precio_unitario=15.00 |q=50 |qe= |transaccion_tipo_id=1 |pedido_id=17 |obs= |fecha_creacion=2020-08-18 20:02:05', NULL, 'root@localhost', NULL, '2020-08-19 00:02:05.000000'),
(1443, 'INSERT', 'transaccion', '', ' |id= |material_id=16 |precio_unitario=10.00 |q=100 |qe= |transaccion_tipo_id=1 |pedido_id=17 |obs= |fecha_creacion=2020-08-18 20:02:05', NULL, 'root@localhost', NULL, '2020-08-19 00:02:05.000000'),
(1444, 'INSERT', 'transaccion', '', ' |id= |material_id=21 |precio_unitario=1.00 |q=100 |qe= |transaccion_tipo_id=1 |pedido_id=17 |obs= |fecha_creacion=2020-08-18 20:02:05', NULL, 'root@localhost', NULL, '2020-08-19 00:02:05.000000'),
(1445, 'INSERT', 'transaccion', '', ' |id= |material_id=22 |precio_unitario=1.00 |q=50 |qe= |transaccion_tipo_id=1 |pedido_id=17 |obs= |fecha_creacion=2020-08-18 20:02:05', NULL, 'root@localhost', NULL, '2020-08-19 00:02:05.000000'),
(1446, 'INSERT', 'transaccion', '', ' |id= |material_id=31 |precio_unitario=2.50 |q=100 |qe= |transaccion_tipo_id=1 |pedido_id=17 |obs= |fecha_creacion=2020-08-18 20:02:05', NULL, 'root@localhost', NULL, '2020-08-19 00:02:05.000000'),
(1447, 'INSERT', 'transaccion', '', ' |id= |material_id=32 |precio_unitario=2.50 |q=100 |qe= |transaccion_tipo_id=1 |pedido_id=17 |obs= |fecha_creacion=2020-08-18 20:02:05', NULL, 'root@localhost', NULL, '2020-08-19 00:02:05.000000'),
(1448, 'INSERT', 'transaccion', '', ' |id= |material_id=33 |precio_unitario=2.50 |q=100 |qe= |transaccion_tipo_id=1 |pedido_id=17 |obs= |fecha_creacion=2020-08-18 20:02:05', NULL, 'root@localhost', NULL, '2020-08-19 00:02:05.000000'),
(1449, 'INSERT', 'pedido', '', ' |id= |estado_pedido_id= |funcionario_id=10 |proveedor_id=2 |usuario_id=1 |transaccion_tipo_id=1 |Nro_factura=22 |partida_presup=351002 |fuente=03/0820 |fecha_creacion=2020-08-18 20:06:55 |fecha_entrega=', NULL, 'root@localhost', NULL, '2020-08-19 00:06:55.000000'),
(1450, 'INSERT', 'transaccion', '', ' |id= |material_id=41 |precio_unitario=15.00 |q=10 |qe= |transaccion_tipo_id=1 |pedido_id=18 |obs= |fecha_creacion=2020-08-18 20:06:55', NULL, 'root@localhost', NULL, '2020-08-19 00:06:55.000000'),
(1451, 'INSERT', 'transaccion', '', ' |id= |material_id=40 |precio_unitario=3.00 |q=30 |qe= |transaccion_tipo_id=1 |pedido_id=18 |obs= |fecha_creacion=2020-08-18 20:06:55', NULL, 'root@localhost', NULL, '2020-08-19 00:06:55.000000'),
(1452, 'INSERT', 'proveedor', '', ' |id=0 |Nit=6045011010 |empresa=Productos en General |nombres=Juiana Mery |apellidos=Apaza Calisaya |direccion=z/12 de octubre Av.  6 de marzo N50 |telefono=60500693 |correo=julia@gmail.com |fecha_creacion=2020-08-18 20:38:53', NULL, 'root@localhost', NULL, '2020-08-19 00:38:53.000000'),
(1453, 'INSERT', 'pedido', '', ' |id= |estado_pedido_id= |funcionario_id=10 |proveedor_id=3 |usuario_id=1 |transaccion_tipo_id=1 |Nro_factura=245 |partida_presup=2452020 |fuente=recursos propios |fecha_creacion=2020-08-18 21:18:49 |fecha_entrega=', NULL, 'root@localhost', NULL, '2020-08-19 01:18:49.000000'),
(1454, 'INSERT', 'transaccion', '', ' |id= |material_id=14 |precio_unitario=20.00 |q=20 |qe= |transaccion_tipo_id=1 |pedido_id=19 |obs= |fecha_creacion=2020-08-18 21:18:49', NULL, 'root@localhost', NULL, '2020-08-19 01:18:49.000000'),
(1455, 'INSERT', 'transaccion', '', ' |id= |material_id=12 |precio_unitario=2.00 |q=10 |qe= |transaccion_tipo_id=1 |pedido_id=19 |obs= |fecha_creacion=2020-08-18 21:18:49', NULL, 'root@localhost', NULL, '2020-08-19 01:18:49.000000'),
(1456, 'INSERT', 'funcionario', '', ' |id=0 |ci=8363831 |exp=LP |nombres=lucia |appaterno=peñas |apmaterno=lopez |sexo=mujer |direccion=av. juan pablo II |telefono=76568185 |correo=luci@gmail.com |cargo_id=15', NULL, 'root@localhost', NULL, '2020-08-20 13:53:02.000000'),
(1457, 'INSERT', 'usuario', '', ' |id=0 |nombre=LUCIA |estado=1 |funcionario_id=13 |rol_id=2', NULL, 'root@localhost', NULL, '2020-08-20 13:53:59.000000'),
(1458, 'UPDATE', 'transaccion', ' |id=14 |material_id=1 |precio_unitario= |q=2 |qe= |transaccion_tipo_id=2 |pedido_id=14 |obs= |fecha_creacion=2020-08-18 15:02:10', ' |id=14 |material_id=1 |precio_unitario= |q=2 |qe=1 |transaccion_tipo_id=2 |pedido_id=14 |obs=POR MOTIVOS DE STOCK NO SE LE ENTREGA LA CANTIDAD SOLICITADA |fecha_creacion=2020-08-18 15:02:10', '', 'root@localhost', NULL, '2020-08-20 14:03:28.000000'),
(1459, 'UPDATE', 'pedido', ' |id=14 |estado_pedido_id=1 |funcionario_id=10 |proveedor_id= |usuario_id=1 |transaccion_tipo_id=2 |Nro_factura= |partida_presup= |fuente= |fecha_creacion=2020-08-18 15:02:10 |fecha_entrega=', ' |id=14 |estado_pedido_id=2 |funcionario_id=10 |proveedor_id= |usuario_id=1 |transaccion_tipo_id=2 |Nro_factura= |partida_presup= |fuente= |fecha_creacion=2020-08-18 15:02:10 |fecha_entrega=2020-08-20 10:03:32', ' |Cambió el valor de estado_pedido_id: 1 a: 2', 'root@localhost', NULL, '2020-08-20 14:03:32.000000'),
(1460, 'INSERT', 'pedido', '', ' |id= |estado_pedido_id=1 |funcionario_id=1 |proveedor_id= |usuario_id=1 |transaccion_tipo_id=2 |Nro_factura= |partida_presup= |fuente= |fecha_creacion=2020-08-20 10:08:48 |fecha_entrega=', NULL, 'root@localhost', NULL, '2020-08-20 14:08:48.000000'),
(1461, 'INSERT', 'transaccion', '', ' |id= |material_id=1 |precio_unitario= |q=2 |qe= |transaccion_tipo_id=2 |pedido_id=20 |obs= |fecha_creacion=2020-08-20 10:08:48', NULL, 'root@localhost', NULL, '2020-08-20 14:08:48.000000'),
(1462, 'INSERT', 'pedido', '', ' |id= |estado_pedido_id= |funcionario_id=10 |proveedor_id=3 |usuario_id=1 |transaccion_tipo_id=1 |Nro_factura=2147483647 |partida_presup=350 |fuente=RECURSOS PROPIOS |fecha_creacion=2020-08-20 10:11:17 |fecha_entrega=', NULL, 'root@localhost', NULL, '2020-08-20 14:11:17.000000'),
(1463, 'INSERT', 'transaccion', '', ' |id= |material_id=18 |precio_unitario=15.00 |q=50 |qe= |transaccion_tipo_id=1 |pedido_id=21 |obs= |fecha_creacion=2020-08-20 10:11:17', NULL, 'root@localhost', NULL, '2020-08-20 14:11:17.000000'),
(1464, 'INSERT', 'transaccion', '', ' |id= |material_id=13 |precio_unitario=3.00 |q=20 |qe= |transaccion_tipo_id=1 |pedido_id=21 |obs= |fecha_creacion=2020-08-20 10:11:17', NULL, 'root@localhost', NULL, '2020-08-20 14:11:17.000000'),
(1465, 'INSERT', 'pedido', '', ' |id= |estado_pedido_id=1 |funcionario_id=1 |proveedor_id= |usuario_id=2 |transaccion_tipo_id=2 |Nro_factura= |partida_presup= |fuente= |fecha_creacion=2020-08-21 17:23:48 |fecha_entrega=', NULL, 'root@localhost', NULL, '2020-08-21 21:23:48.000000'),
(1466, 'INSERT', 'transaccion', '', ' |id= |material_id=3 |precio_unitario= |q=1 |qe= |transaccion_tipo_id=2 |pedido_id=22 |obs= |fecha_creacion=2020-08-21 17:23:48', NULL, 'root@localhost', NULL, '2020-08-21 21:23:48.000000'),
(1467, 'INSERT', 'usuario', '', ' |id=0 |nombre=mary |estado=1 |funcionario_id=11 |rol_id=2', NULL, 'root@localhost', NULL, '2020-08-22 12:03:10.000000'),
(1468, 'INSERT', 'proveedor', '', ' |id=0 |Nit=329838025 |empresa=Evolution Forever SRL. |nombres=Rut |apellidos=Poma |direccion=Av. Buenos Aires N 201 |telefono=77805622 |correo=evolution@gmail.com |fecha_creacion=2020-08-22 08:15:11', NULL, 'root@localhost', NULL, '2020-08-22 12:15:11.000000'),
(1469, 'INSERT', 'material', '', ' |id= |codigo=351003 |nom_material=JABON LIQUIDO |descripcion=DE 3/4 lt |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=4 |fecha_creacion=2020-08-22 08:19:46', NULL, 'root@localhost', NULL, '2020-08-22 12:19:46.000000'),
(1470, 'INSERT', 'pedido', '', ' |id= |estado_pedido_id= |funcionario_id=10 |proveedor_id=3 |usuario_id=1 |transaccion_tipo_id=1 |Nro_factura=370224561 |partida_presup=236 |fuente=RECURSOS PROPIOS |fecha_creacion=2020-08-22 08:31:02 |fecha_entrega=', NULL, 'root@localhost', NULL, '2020-08-22 12:31:02.000000'),
(1471, 'INSERT', 'transaccion', '', ' |id= |material_id=12 |precio_unitario=3.00 |q=100 |qe= |transaccion_tipo_id=1 |pedido_id=23 |obs= |fecha_creacion=2020-08-22 08:31:02', NULL, 'root@localhost', NULL, '2020-08-22 12:31:02.000000'),
(1472, 'INSERT', 'material', '', ' |id= |codigo=351006 |nom_material=ola limpia Vidrio |descripcion=Maximus |min_inventario=10 |presentacion=unidad |activo=1 |categoria_id=4 |fecha_creacion=2020-08-22 16:40:29', NULL, 'root@localhost', NULL, '2020-08-22 20:40:29.000000'),
(1473, 'INSERT', 'pedido', '', ' |id= |estado_pedido_id=1 |funcionario_id=1 |proveedor_id= |usuario_id=2 |transaccion_tipo_id=2 |Nro_factura= |partida_presup= |fuente= |fecha_creacion=2020-08-22 18:05:01 |fecha_entrega=', NULL, 'root@localhost', NULL, '2020-08-22 22:05:01.000000'),
(1474, 'INSERT', 'transaccion', '', ' |id= |material_id=1 |precio_unitario= |q=1 |qe= |transaccion_tipo_id=2 |pedido_id=24 |obs= |fecha_creacion=2020-08-22 18:05:01', NULL, 'root@localhost', NULL, '2020-08-22 22:05:01.000000'),
(1475, 'INSERT', 'pedido', '', ' |id= |estado_pedido_id=1 |funcionario_id=1 |proveedor_id= |usuario_id=2 |transaccion_tipo_id=2 |Nro_factura= |partida_presup= |fuente= |fecha_creacion=2020-08-22 18:17:37 |fecha_entrega=', NULL, 'root@localhost', NULL, '2020-08-22 22:17:37.000000'),
(1476, 'INSERT', 'transaccion', '', ' |id= |material_id=11 |precio_unitario= |q=1 |qe= |transaccion_tipo_id=2 |pedido_id=25 |obs= |fecha_creacion=2020-08-22 18:17:37', NULL, 'root@localhost', NULL, '2020-08-22 22:17:37.000000'),
(1477, 'INSERT', 'transaccion', '', ' |id= |material_id=13 |precio_unitario= |q=2 |qe= |transaccion_tipo_id=2 |pedido_id=25 |obs= |fecha_creacion=2020-08-22 18:17:37', NULL, 'root@localhost', NULL, '2020-08-22 22:17:37.000000'),
(1478, 'INSERT', 'pedido', '', ' |id= |estado_pedido_id=1 |funcionario_id=1 |proveedor_id= |usuario_id=2 |transaccion_tipo_id=2 |Nro_factura= |partida_presup= |fuente= |fecha_creacion=2020-08-22 18:19:38 |fecha_entrega=', NULL, 'root@localhost', NULL, '2020-08-22 22:19:38.000000'),
(1479, 'INSERT', 'transaccion', '', ' |id= |material_id=21 |precio_unitario= |q=10 |qe= |transaccion_tipo_id=2 |pedido_id=26 |obs= |fecha_creacion=2020-08-22 18:19:38', NULL, 'root@localhost', NULL, '2020-08-22 22:19:38.000000'),
(1480, 'UPDATE', 'transaccion', ' |id=38 |material_id=13 |precio_unitario= |q=2 |qe= |transaccion_tipo_id=2 |pedido_id=25 |obs= |fecha_creacion=2020-08-22 18:17:37', ' |id=38 |material_id=13 |precio_unitario= |q=2 |qe=1 |transaccion_tipo_id=2 |pedido_id=25 |obs=por falta de material solo se le entrego uno |fecha_creacion=2020-08-22 18:17:37', '', 'root@localhost', NULL, '2020-08-22 22:53:08.000000'),
(1481, 'UPDATE', 'transaccion', ' |id=37 |material_id=11 |precio_unitario= |q=1 |qe= |transaccion_tipo_id=2 |pedido_id=25 |obs= |fecha_creacion=2020-08-22 18:17:37', ' |id=37 |material_id=11 |precio_unitario= |q=1 |qe=1 |transaccion_tipo_id=2 |pedido_id=25 |obs= |fecha_creacion=2020-08-22 18:17:37', '', 'root@localhost', NULL, '2020-08-22 22:53:13.000000'),
(1482, 'UPDATE', 'transaccion', ' |id=37 |material_id=11 |precio_unitario= |q=1 |qe=1 |transaccion_tipo_id=2 |pedido_id=25 |obs= |fecha_creacion=2020-08-22 18:17:37', ' |id=37 |material_id=11 |precio_unitario= |q=1 |qe=1 |transaccion_tipo_id=2 |pedido_id=25 |obs= |fecha_creacion=2020-08-22 18:17:37', '', 'root@localhost', NULL, '2020-08-22 22:53:13.000000'),
(1483, 'UPDATE', 'pedido', ' |id=25 |estado_pedido_id=1 |funcionario_id=1 |proveedor_id= |usuario_id=2 |transaccion_tipo_id=2 |Nro_factura= |partida_presup= |fuente= |fecha_creacion=2020-08-22 18:17:37 |fecha_entrega=', ' |id=25 |estado_pedido_id=2 |funcionario_id=1 |proveedor_id= |usuario_id=2 |transaccion_tipo_id=2 |Nro_factura= |partida_presup= |fuente= |fecha_creacion=2020-08-22 18:17:37 |fecha_entrega=2020-08-22 18:53:13', ' |Cambió el valor de estado_pedido_id: 1 a: 2', 'root@localhost', NULL, '2020-08-22 22:53:13.000000'),
(1484, 'INSERT', 'proveedor', '', ' |id=0 |Nit=8363830016 |empresa=quisbert SRL. |nombres=Andres |apellidos=Quisbet |direccion=av. 6 de marzo calle 1 |telefono=2206514 |correo=quisbert@gmail.com |fecha_creacion=2020-08-23 09:38:52', NULL, 'root@localhost', NULL, '2020-08-23 13:38:52.000000'),
(1485, 'INSERT', 'pedido', '', ' |id= |estado_pedido_id=1 |funcionario_id=1 |proveedor_id= |usuario_id=2 |transaccion_tipo_id=2 |Nro_factura= |partida_presup= |fuente= |fecha_creacion=2020-08-23 15:27:45 |fecha_entrega=', NULL, 'root@localhost', NULL, '2020-08-23 19:27:45.000000'),
(1486, 'INSERT', 'transaccion', '', ' |id= |material_id=3 |precio_unitario= |q=5 |qe= |transaccion_tipo_id=2 |pedido_id=27 |obs= |fecha_creacion=2020-08-23 15:27:45', NULL, 'root@localhost', NULL, '2020-08-23 19:27:45.000000'),
(1487, 'INSERT', 'transaccion', '', ' |id= |material_id=12 |precio_unitario= |q=4 |qe= |transaccion_tipo_id=2 |pedido_id=27 |obs= |fecha_creacion=2020-08-23 15:27:45', NULL, 'root@localhost', NULL, '2020-08-23 19:27:45.000000'),
(1488, 'INSERT', 'pedido', '', ' |id= |estado_pedido_id=1 |funcionario_id=10 |proveedor_id= |usuario_id=1 |transaccion_tipo_id=2 |Nro_factura= |partida_presup= |fuente= |fecha_creacion=2020-08-25 23:03:31 |fecha_entrega=', NULL, 'root@localhost', NULL, '2020-08-26 03:03:31.000000'),
(1489, 'INSERT', 'transaccion', '', ' |id= |material_id=1 |precio_unitario= |q=1 |qe= |transaccion_tipo_id=2 |pedido_id=28 |obs= |fecha_creacion=2020-08-25 23:03:31', NULL, 'root@localhost', NULL, '2020-08-26 03:03:31.000000'),
(1490, 'INSERT', 'pedido', '', ' |id= |estado_pedido_id= |funcionario_id=10 |proveedor_id=5 |usuario_id=1 |transaccion_tipo_id=1 |Nro_factura=12 |partida_presup=23 |fuente=recursos propios |fecha_creacion=2020-08-25 23:05:38 |fecha_entrega=', NULL, 'root@localhost', NULL, '2020-08-26 03:05:38.000000'),
(1491, 'INSERT', 'transaccion', '', ' |id= |material_id=11 |precio_unitario=3.00 |q=12 |qe= |transaccion_tipo_id=1 |pedido_id=29 |obs= |fecha_creacion=2020-08-25 23:05:38', NULL, 'root@localhost', NULL, '2020-08-26 03:05:38.000000'),
(1492, 'INSERT', 'pedido', '', ' |id= |estado_pedido_id=1 |funcionario_id=1 |proveedor_id= |usuario_id=2 |transaccion_tipo_id=2 |Nro_factura= |partida_presup= |fuente= |fecha_creacion=2020-08-25 23:11:45 |fecha_entrega=', NULL, 'root@localhost', NULL, '2020-08-26 03:11:45.000000'),
(1493, 'INSERT', 'transaccion', '', ' |id= |material_id=3 |precio_unitario= |q=1 |qe= |transaccion_tipo_id=2 |pedido_id=30 |obs= |fecha_creacion=2020-08-25 23:11:45', NULL, 'root@localhost', NULL, '2020-08-26 03:11:45.000000'),
(1494, 'INSERT', 'pedido', '', ' |id= |estado_pedido_id=1 |funcionario_id=1 |proveedor_id= |usuario_id=2 |transaccion_tipo_id=2 |Nro_factura= |partida_presup= |fuente= |fecha_creacion=2020-08-25 23:20:41 |fecha_entrega=', NULL, 'root@localhost', NULL, '2020-08-26 03:20:41.000000'),
(1495, 'INSERT', 'transaccion', '', ' |id= |material_id=1 |precio_unitario= |q=1 |qe= |transaccion_tipo_id=2 |pedido_id=31 |obs= |fecha_creacion=2020-08-25 23:20:41', NULL, 'root@localhost', NULL, '2020-08-26 03:20:41.000000'),
(1496, 'INSERT', 'pedido', '', ' |id= |estado_pedido_id=1 |funcionario_id=1 |proveedor_id= |usuario_id=2 |transaccion_tipo_id=2 |Nro_factura= |partida_presup= |fuente= |fecha_creacion=2020-08-25 23:22:07 |fecha_entrega=', NULL, 'root@localhost', NULL, '2020-08-26 03:22:07.000000'),
(1497, 'INSERT', 'transaccion', '', ' |id= |material_id=1 |precio_unitario= |q=1 |qe= |transaccion_tipo_id=2 |pedido_id=32 |obs= |fecha_creacion=2020-08-25 23:22:07', NULL, 'root@localhost', NULL, '2020-08-26 03:22:07.000000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `material`
--

DROP TABLE IF EXISTS `material`;
CREATE TABLE `material` (
  `id` int(11) NOT NULL,
  `codigo` int(11) DEFAULT NULL,
  `nom_material` varchar(250) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `descripcion` varchar(500) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `min_inventario` int(11) DEFAULT NULL,
  `presentacion` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `material`
--

INSERT INTO `material` (`id`, `codigo`, `nom_material`, `descripcion`, `min_inventario`, `presentacion`, `activo`, `categoria_id`, `fecha_creacion`) VALUES
(1, 321001, 'PAPEL BOND', 'TAMAÑO CARTA', 10, 'Pqte.', 1, 1, '2018-08-28 13:09:50'),
(2, 321006, 'PAPEL BOND', 'TAMAÑO OFICIO', 10, 'Pqte.', 1, 1, '2020-08-18 15:23:20'),
(3, 321003, 'PAPEL BOND', 'TAMAÑO CARTA DE COLOR CELESTE', 10, 'unidad', 1, 1, '2020-08-18 15:22:27'),
(4, 321004, 'PAPEL BOND', 'TAMAÑO CARTA DE COLOR FUXIA', 10, 'unidad', 1, 1, '2020-08-18 15:22:36'),
(5, 321005, 'PAPEL', 'PARA FAX', 10, 'unidad', 1, 1, '2020-08-18 15:22:47'),
(6, 321006, 'PAPEL', 'COUCHE TAMAÑO OFICIO', 10, 'unidad', 1, 1, '2020-08-18 15:23:09'),
(7, 321007, 'PAPEL HILADO', 'TAMAÑO OFICIO COLOR CREMA', 5, 'unidad', 1, 1, '2020-08-18 15:24:30'),
(8, 321008, 'PAPEL BOND', 'TAMAÑO RESMA', 10, 'unidad', 1, 1, '2020-08-18 15:24:45'),
(9, 321009, 'PAPEL  ESPECIAL', 'TAMAÑO CARTA (CASCARA DE HUEVO)', 10, 'unidad', 1, 1, '2020-08-18 15:25:01'),
(10, 321010, 'PAPEL', 'TAMAÑO CARTA DE VARIOS COLORES', 10, 'Pqte.', 1, 1, '2020-08-18 15:25:18'),
(11, 331001, 'BOLIGRAFO  ROJO', 'PILOT', 10, 'unidad', 1, 2, '2020-08-18 15:59:19'),
(12, 331002, 'BOLIGRAFO AZUL', 'PILOT', 10, 'unidad', 1, 2, '2020-08-18 16:04:26'),
(13, 331003, 'BOLIGRAFO NEGRO', 'PILOT', 10, 'unidad', 1, 2, '2020-08-18 16:07:31'),
(14, 331004, 'TINTA DE TAMPO AZUL', 'PEQUEÑO', 5, 'unidad', 1, 2, '2020-08-18 16:17:16'),
(15, 331005, 'TINTA DE TAMPO ROJO', 'NEUTRO', 5, 'unidad', 1, 2, '2020-08-18 17:14:40'),
(16, 331006, 'CUADERNO TAMAÑO MEDIO OFICIO', 'CON ASPIRAL', 5, 'unidad', 1, 2, '2020-08-18 17:15:50'),
(17, 331007, 'CUADERNO TAMAÑO OFICIO', 'CON ESPIRAL', 5, 'unidad', 1, 2, '2020-08-18 17:20:09'),
(18, 331008, 'CUADERNO TAMAÑO CARTA', 'CON ASPIRAL', 5, 'unidad', 1, 2, '2020-08-18 17:29:24'),
(19, 331009, 'PORTAL BLOCK DE NOTAS', 'ARTESCO', 3, 'unidad', 1, 2, '2020-08-18 17:31:08'),
(20, 331010, 'RADEX', 'LIQUIDO', 5, 'unidad', 1, 2, '2020-08-18 17:31:49'),
(21, 331011, 'LAPIZ NEGRO', 'STABILO', 10, 'unidad', 1, 2, '2020-08-18 17:35:11'),
(22, 331012, 'LAPIZ ROJO', 'STABILO', 10, 'unidad', 1, 2, '2020-08-18 17:37:06'),
(23, 331013, 'BLOK DE NOTAS', 'ADESIVO POSIT', 5, 'unidad', 1, 2, '2020-08-18 17:41:56'),
(24, 331014, 'CDs', 'PRINCO', 5, 'unidad', 1, 2, '2020-08-18 17:42:33'),
(25, 331015, 'FUNDAS PARA CDs', 'PLASTICAS', 5, 'unidad', 1, 2, '2020-08-18 17:43:53'),
(26, 331016, 'ENGRAMPADORA', 'MEDIANA', 2, 'unidad', 1, 2, '2020-08-18 17:44:57'),
(27, 331017, 'CLIPS', 'PEQUEÑO', 2, 'Pqte.', 1, 2, '2020-08-18 17:58:56'),
(28, 331018, 'CLIPS', 'MEDIANO', 2, 'Pqte.', 1, 2, '2020-08-18 18:00:49'),
(29, 331019, 'GRAPAS', 'MADISON', 5, 'Pqte.', 1, 2, '2020-08-18 18:02:04'),
(30, 331020, 'TIJERA MUNDIAÑ', 'MEDIANO', 3, 'unidad', 1, 2, '2020-08-18 18:23:08'),
(31, 331021, 'MARCADOR COLOR NEGRO', 'ACRILICO', 5, 'unidad', 1, 2, '2020-08-18 18:24:55'),
(32, 331022, 'MARCADOR COLOR AZUL', 'ACRILICO', 5, 'unidad', 1, 2, '2020-08-18 18:25:40'),
(33, 331023, 'MARCADOR COLOR ROJO', 'ACRILICO', 5, 'unidad', 1, 2, '2020-08-18 18:46:02'),
(34, 331024, 'MARCADOR COLOR VERDE', 'ACRILICO', 5, 'unidad', 1, 2, '2020-08-18 18:46:41'),
(35, 331025, 'ARCHIVADOR', 'TAMAÑO OFICIO', 5, 'unidad', 1, 2, '2020-08-18 18:50:46'),
(36, 341001, 'SOBRE MANILA TAMAÑO CARTA', 'MATERIAL DE CARTON', 5, 'unidad', 1, 3, '2020-08-18 18:57:07'),
(37, 341002, 'SOBRE MANILA TAMAÑO OFICIO', 'MATERIAL DE CARTON', 5, 'unidad', 1, 3, '2020-08-18 18:57:43'),
(38, 341003, 'SOBRE MANILA TAMAÑO DOBLE OFICIO', 'MATERIAL DE CARTON', 5, 'unidad', 1, 3, '2020-08-18 18:58:32'),
(39, 341004, 'ARCHIVADORES DE PALANCA', 'MATERIAL DE CARTON', 5, 'unidad', 1, 3, '2020-08-18 18:59:39'),
(40, 351001, 'DETERGENTE EN POLVO', 'ACE OMO', 5, 'unidad', 1, 4, '2020-08-18 19:00:45'),
(41, 351002, 'ESCOBAS', 'PLASTICAS DOBLES', 3, 'unidad', 1, 4, '2020-08-18 19:02:56'),
(42, 351003, 'JABON LIQUIDO', 'DE 3/4 lt', 10, 'unidad', 1, 4, '2020-08-22 08:19:46'),
(43, 351006, 'ola limpia Vidrio', 'Maximus', 10, 'unidad', 1, 4, '2020-08-22 16:40:29');

--
-- Disparadores `material`
--
DROP TRIGGER IF EXISTS `deletematerial`;
DELIMITER $$
CREATE TRIGGER `deletematerial` AFTER DELETE ON `material` FOR EACH ROW BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |codigo=',ifnull(old.codigo,''),
    ' |nom_material=',ifnull(old.nom_material,''),
    ' |descripcion=',ifnull(old.descripcion,''),
    ' |min_inventario=',ifnull(old.min_inventario,''),
    ' |presentacion=',ifnull(old.presentacion,''),
    ' |activo=',ifnull(old.activo,''),
    ' |categoria_id=',ifnull(old.categoria_id,''),
    ' |fecha_creacion=',ifnull(old.fecha_creacion,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"material");
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `insertmaterial`;
DELIMITER $$
CREATE TRIGGER `insertmaterial` BEFORE INSERT ON `material` FOR EACH ROW BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |codigo=',ifnull(new.codigo,''),
    ' |nom_material=',ifnull(new.nom_material,''),
    ' |descripcion=',ifnull(new.descripcion,''),
    ' |min_inventario=',ifnull(new.min_inventario,''),
    ' |presentacion=',ifnull(new.presentacion,''),
    ' |activo=',ifnull(new.activo,''),
    ' |categoria_id=',ifnull(new.categoria_id,''),
    ' |fecha_creacion=',ifnull(new.fecha_creacion,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"material");
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `updatematerial`;
DELIMITER $$
CREATE TRIGGER `updatematerial` BEFORE UPDATE ON `material` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN
  #concatenamos los campos de la tabla a auditar y verificamos que no sean null, en caso de que los campos sean null agregamos un espacio
  #las variables (new,old)son de mysql, el valor old es el que ya se tenia en la tabla y el new es el valor que se modifico

  #Valores viejos
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |codigo=',ifnull(old.codigo,''),
    ' |nom_material=',ifnull(old.nom_material,''),
    ' |descripcion=',ifnull(old.descripcion,''),
    ' |min_inventario=',ifnull(old.min_inventario,''),
    ' |presentacion=',ifnull(old.presentacion,''),
    ' |activo=',ifnull(old.activo,''),
    ' |categoria_id=',ifnull(old.categoria_id,''),
    ' |fecha_creacion=',ifnull(old.fecha_creacion,'')
  );
  #Valores nuevos
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |codigo=',ifnull(new.codigo,''),
    ' |nom_material=',ifnull(new.nom_material,''),
    ' |descripcion=',ifnull(new.descripcion,''),
    ' |min_inventario=',ifnull(new.min_inventario,''),
    ' |presentacion=',ifnull(new.presentacion,''),
    ' |activo=',ifnull(new.activo,''),
    ' |categoria_id=',ifnull(new.categoria_id,''),
    ' |fecha_creacion=',ifnull(new.fecha_creacion,'')
  );

	#guardamos en una variable los valores que unicamente cambiaron
  set @res = '';
	IF OLD.codigo <> new.codigo THEN set @res = CONCAT (@res, ' |Cambió el valor de codigo: ',ifnull(OLD.codigo,''), ' a: ',ifnull(new.codigo,'')); END IF;
	IF OLD.nom_material <> new.nom_material THEN set @res = CONCAT (@res, ' |Cambió el valor de nom_material: ',ifnull(OLD.nom_material,''), ' a: ',ifnull(new.nom_material,'')); END IF;
	IF OLD.descripcion <> new.descripcion THEN set @res = CONCAT (@res, ' |Cambió el valor de descripcion: ',ifnull(OLD.descripcion,''), ' a: ',ifnull(new.descripcion,'')); END IF;
	IF OLD.min_inventario <> new.min_inventario THEN set @res = CONCAT (@res, ' |Cambió el valor de min_inventario: ',ifnull(OLD.min_inventario,''), ' a: ',ifnull(new.min_inventario,'')); END IF;
	IF OLD.presentacion <> new.presentacion THEN set @res = CONCAT (@res, ' |Cambió el valor de presentacion: ',ifnull(OLD.presentacion,''), ' a: ',ifnull(new.presentacion,'')); END IF;
	IF OLD.activo <> new.activo THEN set @res = CONCAT (@res, ' |Cambió el valor de activo: ',ifnull(OLD.activo,''), ' a: ',ifnull(new.activo,'')); END IF;
	IF OLD.categoria_id <> new.categoria_id THEN set @res = CONCAT (@res, ' |Cambió el valor de categoria_id: ',ifnull(OLD.categoria_id,''), ' a: ',ifnull(new.categoria_id,'')); END IF;
	IF OLD.fecha_creacion <> new.fecha_creacion THEN set @res = CONCAT (@res, ' |Cambió el valor de fecha_creacion: ',ifnull(OLD.fecha_creacion,''), ' a: ',ifnull(new.fecha_creacion,'')); END IF;

	#insertamos en nuestra tabla de log la informacion
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla,valor_alterado)
	VALUES (@oldq ,@newq,CURRENT_USER,"UPDATE",NOW(),"material",ifnull(@res,'No cambio nada'));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

DROP TABLE IF EXISTS `pedido`;
CREATE TABLE `pedido` (
  `id` int(11) NOT NULL,
  `estado_pedido_id` int(11) DEFAULT NULL,
  `funcionario_id` int(11) DEFAULT NULL,
  `proveedor_id` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `transaccion_tipo_id` int(11) DEFAULT NULL,
  `Nro_factura` int(11) DEFAULT NULL,
  `partida_presup` int(11) DEFAULT NULL,
  `fuente` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_entrega` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`id`, `estado_pedido_id`, `funcionario_id`, `proveedor_id`, `usuario_id`, `transaccion_tipo_id`, `Nro_factura`, `partida_presup`, `fuente`, `fecha_creacion`, `fecha_entrega`) VALUES
(1, NULL, 10, 1, 1, 1, 10240152, 100450, 'Recursos Propios', '2018-11-10 20:52:19', NULL),
(2, 2, 10, NULL, 1, 2, NULL, NULL, NULL, '2018-11-10 21:51:57', '2018-11-10 21:52:18'),
(3, 2, 1, NULL, 2, 2, NULL, NULL, NULL, '2018-11-10 22:12:26', '2018-11-10 22:13:14'),
(4, NULL, 10, 1, 1, 1, 1010102223, 22222222, 'Recursos Propios', '2018-11-11 12:11:29', NULL),
(5, NULL, 10, 1, 1, 1, 858585, 121212, 'Recursos Propios', '2018-11-12 10:50:15', NULL),
(6, 2, 10, NULL, 1, 2, NULL, NULL, NULL, '2018-11-12 17:03:12', '2018-11-12 17:03:23'),
(7, NULL, 10, 1, 1, 1, 505041, 66666, 'Recursos Propios', '2018-11-14 00:00:34', NULL),
(8, 2, 10, NULL, 1, 2, NULL, NULL, NULL, '2018-11-21 09:23:28', '2018-11-21 09:23:36'),
(9, 2, 1, NULL, 2, 2, NULL, NULL, NULL, '2018-11-29 19:07:27', '2018-11-29 19:16:55'),
(10, 2, 6, NULL, 1, 2, NULL, NULL, NULL, '2018-11-29 19:53:17', '2018-11-29 20:10:13'),
(11, 2, 6, NULL, 1, 2, NULL, NULL, NULL, '2018-11-29 20:28:30', '2018-11-29 20:30:37'),
(12, 2, 7, NULL, 1, 2, NULL, NULL, NULL, '2018-11-29 20:37:43', '2018-11-29 20:54:43'),
(13, 2, 8, NULL, 1, 2, NULL, NULL, NULL, '2018-11-29 21:05:20', '2018-11-29 21:05:36'),
(14, 2, 10, NULL, 1, 2, NULL, NULL, NULL, '2020-08-18 15:02:10', '2020-08-20 10:03:32'),
(15, 1, 10, NULL, 1, 2, NULL, NULL, NULL, '2020-08-18 19:32:30', NULL),
(16, NULL, 10, 2, 1, 1, 2210, 321006, '01/20', '2020-08-18 19:53:50', NULL),
(17, NULL, 10, 2, 1, 1, 35101, 331001, '02/0820', '2020-08-18 20:02:05', NULL),
(18, NULL, 10, 2, 1, 1, 22, 351002, '03/0820', '2020-08-18 20:06:55', NULL),
(19, NULL, 10, 3, 1, 1, 245, 2452020, 'recursos propios', '2020-08-18 21:18:49', NULL),
(20, 1, 1, NULL, 1, 2, NULL, NULL, NULL, '2020-08-20 10:08:48', NULL),
(21, NULL, 10, 3, 1, 1, 2147483647, 350, 'RECURSOS PROPIOS', '2020-08-20 10:11:17', NULL),
(22, 1, 1, NULL, 2, 2, NULL, NULL, NULL, '2020-08-21 17:23:48', NULL),
(23, NULL, 10, 3, 1, 1, 370224561, 236, 'RECURSOS PROPIOS', '2020-08-22 08:31:02', NULL),
(24, 1, 1, NULL, 2, 2, NULL, NULL, NULL, '2020-08-22 18:05:01', NULL),
(25, 2, 1, NULL, 2, 2, NULL, NULL, NULL, '2020-08-22 18:17:37', '2020-08-22 18:53:13'),
(26, 1, 1, NULL, 2, 2, NULL, NULL, NULL, '2020-08-22 18:19:38', NULL),
(27, 1, 1, NULL, 2, 2, NULL, NULL, NULL, '2020-08-23 15:27:45', NULL),
(28, 1, 10, NULL, 1, 2, NULL, NULL, NULL, '2020-08-25 23:03:31', NULL),
(29, NULL, 10, 5, 1, 1, 12, 23, 'recursos propios', '2020-08-25 23:05:38', NULL),
(30, 1, 1, NULL, 2, 2, NULL, NULL, NULL, '2020-08-25 23:11:45', NULL),
(31, 1, 1, NULL, 2, 2, NULL, NULL, NULL, '2020-08-25 23:20:41', NULL),
(32, 1, 1, NULL, 2, 2, NULL, NULL, NULL, '2020-08-25 23:22:07', NULL);

--
-- Disparadores `pedido`
--
DROP TRIGGER IF EXISTS `deletepedido`;
DELIMITER $$
CREATE TRIGGER `deletepedido` AFTER DELETE ON `pedido` FOR EACH ROW BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |estado_pedido_id=',ifnull(old.estado_pedido_id,''),
    ' |funcionario_id=',ifnull(old.funcionario_id,''),
    ' |proveedor_id=',ifnull(old.proveedor_id,''),
    ' |usuario_id=',ifnull(old.usuario_id,''),
    ' |transaccion_tipo_id=',ifnull(old.transaccion_tipo_id,''),
    ' |Nro_factura=',ifnull(old.Nro_factura,''),
    ' |partida_presup=',ifnull(old.partida_presup,''),
    ' |fuente=',ifnull(old.fuente,''),
    ' |fecha_creacion=',ifnull(old.fecha_creacion,''),
    ' |fecha_entrega=',ifnull(old.fecha_entrega,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"pedido");
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `insertpedido`;
DELIMITER $$
CREATE TRIGGER `insertpedido` BEFORE INSERT ON `pedido` FOR EACH ROW BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |estado_pedido_id=',ifnull(new.estado_pedido_id,''),
    ' |funcionario_id=',ifnull(new.funcionario_id,''),
    ' |proveedor_id=',ifnull(new.proveedor_id,''),
    ' |usuario_id=',ifnull(new.usuario_id,''),
    ' |transaccion_tipo_id=',ifnull(new.transaccion_tipo_id,''),
    ' |Nro_factura=',ifnull(new.Nro_factura,''),
    ' |partida_presup=',ifnull(new.partida_presup,''),
    ' |fuente=',ifnull(new.fuente,''),
    ' |fecha_creacion=',ifnull(new.fecha_creacion,''),
    ' |fecha_entrega=',ifnull(new.fecha_entrega,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"pedido");
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `updatepedido`;
DELIMITER $$
CREATE TRIGGER `updatepedido` BEFORE UPDATE ON `pedido` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN
  #Valores viejos
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |estado_pedido_id=',ifnull(old.estado_pedido_id,''),
    ' |funcionario_id=',ifnull(old.funcionario_id,''),
    ' |proveedor_id=',ifnull(old.proveedor_id,''),
    ' |usuario_id=',ifnull(old.usuario_id,''),
    ' |transaccion_tipo_id=',ifnull(old.transaccion_tipo_id,''),
    ' |Nro_factura=',ifnull(old.Nro_factura,''),
    ' |partida_presup=',ifnull(old.partida_presup,''),
    ' |fuente=',ifnull(old.fuente,''),
    ' |fecha_creacion=',ifnull(old.fecha_creacion,''),
    ' |fecha_entrega=',ifnull(old.fecha_entrega,'')
  );
  #Valores nuevos
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |estado_pedido_id=',ifnull(new.estado_pedido_id,''),
    ' |funcionario_id=',ifnull(new.funcionario_id,''),
    ' |proveedor_id=',ifnull(new.proveedor_id,''),
    ' |usuario_id=',ifnull(new.usuario_id,''),
    ' |transaccion_tipo_id=',ifnull(new.transaccion_tipo_id,''),
    ' |Nro_factura=',ifnull(new.Nro_factura,''),
    ' |partida_presup=',ifnull(new.partida_presup,''),
    ' |fuente=',ifnull(new.fuente,''),
    ' |fecha_creacion=',ifnull(new.fecha_creacion,''),
    ' |fecha_entrega=',ifnull(new.fecha_entrega,'')
  );

	#guardamos en una variable los valores que unicamente cambiaron
  set @res = '';
	IF OLD.estado_pedido_id <> new.estado_pedido_id THEN set @res = CONCAT (@res, ' |Cambió el valor de estado_pedido_id: ',ifnull(OLD.estado_pedido_id,''), ' a: ',ifnull(new.estado_pedido_id,'')); END IF;
	IF OLD.funcionario_id <> new.funcionario_id THEN set @res = CONCAT (@res, ' |Cambió el valor de funcionario_id: ',ifnull(OLD.funcionario_id,''), ' a: ',ifnull(new.funcionario_id,'')); END IF;
	IF OLD.proveedor_id <> new.proveedor_id THEN set @res = CONCAT (@res, ' |Cambió el valor de proveedor_id: ',ifnull(OLD.proveedor_id,''), ' a: ',ifnull(new.proveedor_id,'')); END IF;
	IF OLD.usuario_id <> new.usuario_id THEN set @res = CONCAT (@res, ' |Cambió el valor de usuario_id: ',ifnull(OLD.usuario_id,''), ' a: ',ifnull(new.usuario_id,'')); END IF;
	IF OLD.transaccion_tipo_id <> new.transaccion_tipo_id THEN set @res = CONCAT (@res, ' |Cambió el valor de transaccion_tipo_id: ',ifnull(OLD.transaccion_tipo_id,''), ' a: ',ifnull(new.transaccion_tipo_id,'')); END IF;
	IF OLD.Nro_factura <> new.Nro_factura THEN set @res = CONCAT (@res, ' |Cambió el valor de Nro_factura: ',ifnull(OLD.Nro_factura,''), ' a: ',ifnull(new.Nro_factura,'')); END IF;
	IF OLD.partida_presup <> new.partida_presup THEN set @res = CONCAT (@res, ' |Cambió el valor de partida_presup: ',ifnull(OLD.partida_presup,''), ' a: ',ifnull(new.partida_presup,'')); END IF;
	IF OLD.fuente <> new.fuente THEN set @res = CONCAT (@res, ' |Cambió el valor de fuente: ',ifnull(OLD.fuente,''), ' a: ',ifnull(new.fuente,'')); END IF;
	IF OLD.fecha_creacion <> new.fecha_creacion THEN set @res = CONCAT (@res, ' |Cambió el valor de fecha_creacion: ',ifnull(OLD.fecha_creacion,''), ' a: ',ifnull(new.fecha_creacion,'')); END IF;
	IF OLD.fecha_entrega <> new.fecha_entrega THEN set @res = CONCAT (@res, ' |Cambió el valor de fecha_entrega: ',ifnull(OLD.fecha_entrega,''), ' a: ',ifnull(new.fecha_entrega,'')); END IF;

	#insertamos en nuestra tabla de log la informacion
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla,valor_alterado)
	VALUES (@oldq ,@newq,CURRENT_USER,"UPDATE",NOW(),"pedido",ifnull(@res,'No cambio nada'));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
CREATE TABLE `proveedor` (
  `id` int(11) NOT NULL,
  `Nit` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `empresa` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `nombres` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `apellidos` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `direccion` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `telefono` varchar(25) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `correo` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`id`, `Nit`, `empresa`, `nombres`, `apellidos`, `direccion`, `telefono`, `correo`, `fecha_creacion`) VALUES
(1, '1007173022', 'La Papelera S.A', 'Juan Carlos', 'Yujra', 'Carretera a Viacha', '52416352', 'lapapelera@gmail.com', '2018-09-01 17:16:55'),
(2, '22001084', 'libreria JL SRL', 'Lucia', 'Quisbert', 'El Alto Av. Juan Pablo II N3201', '2204611', 'JL@gmail.com', '2020-08-18 14:50:02'),
(3, '6045011010', 'Productos en General', 'Juiana Mery', 'Apaza Calisaya', 'z/12 de octubre Av.  6 de marzo N50', '60500693', 'julia@gmail.com', '2020-08-18 20:38:53'),
(4, '329838025', 'Evolution Forever SRL.', 'Rut', 'Poma', 'Av. Buenos Aires N 201', '77805622', 'evolution@gmail.com', '2020-08-22 08:15:11'),
(5, '8363830016', 'quisbert SRL.', 'Andres', 'Quisbet', 'av. 6 de marzo calle 1', '2206514', 'quisbert@gmail.com', '2020-08-23 09:38:52');

--
-- Disparadores `proveedor`
--
DROP TRIGGER IF EXISTS `deleteproveedor`;
DELIMITER $$
CREATE TRIGGER `deleteproveedor` AFTER DELETE ON `proveedor` FOR EACH ROW BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |Nit=',ifnull(old.Nit,''),
    ' |empresa=',ifnull(old.empresa,''),
    ' |nombres=',ifnull(old.nombres,''),
    ' |apellidos=',ifnull(old.apellidos,''),
    ' |direccion=',ifnull(old.direccion,''),
    ' |telefono=',ifnull(old.telefono,''),
    ' |correo=',ifnull(old.correo,''),
    ' |fecha_creacion=',ifnull(old.fecha_creacion,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"proveedor");
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `insertproveedor`;
DELIMITER $$
CREATE TRIGGER `insertproveedor` BEFORE INSERT ON `proveedor` FOR EACH ROW BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |Nit=',ifnull(new.Nit,''),
    ' |empresa=',ifnull(new.empresa,''),
    ' |nombres=',ifnull(new.nombres,''),
    ' |apellidos=',ifnull(new.apellidos,''),
    ' |direccion=',ifnull(new.direccion,''),
    ' |telefono=',ifnull(new.telefono,''),
    ' |correo=',ifnull(new.correo,''),
    ' |fecha_creacion=',ifnull(new.fecha_creacion,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"proveedor");
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `updateproveedor`;
DELIMITER $$
CREATE TRIGGER `updateproveedor` BEFORE UPDATE ON `proveedor` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN
  #Valores viejos
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |Nit=',ifnull(old.Nit,''),
    ' |empresa=',ifnull(old.empresa,''),
    ' |nombres=',ifnull(old.nombres,''),
    ' |apellidos=',ifnull(old.apellidos,''),
    ' |direccion=',ifnull(old.direccion,''),
    ' |telefono=',ifnull(old.telefono,''),
    ' |correo=',ifnull(old.correo,''),
    ' |fecha_creacion=',ifnull(old.fecha_creacion,'')
  );
  #Valores nuevos
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |Nit=',ifnull(new.Nit,''),
    ' |empresa=',ifnull(new.empresa,''),
    ' |nombres=',ifnull(new.nombres,''),
    ' |apellidos=',ifnull(new.apellidos,''),
    ' |direccion=',ifnull(new.direccion,''),
    ' |telefono=',ifnull(new.telefono,''),
    ' |correo=',ifnull(new.correo,''),
    ' |fecha_creacion=',ifnull(new.fecha_creacion,'')
  );

	#guardamos en una variable los valores que unicamente cambiaron
  set @res = '';
	IF OLD.Nit <> new.Nit THEN set @res = CONCAT (@res, ' |Cambió el valor de Nit: ',ifnull(OLD.Nit,''), ' a: ',ifnull(new.Nit,'')); END IF;
	IF OLD.empresa <> new.empresa THEN set @res = CONCAT (@res, ' |Cambió el valor de empresa: ',ifnull(OLD.empresa,''), ' a: ',ifnull(new.empresa,'')); END IF;
	IF OLD.nombres <> new.nombres THEN set @res = CONCAT (@res, ' |Cambió el valor de nombres: ',ifnull(OLD.nombres,''), ' a: ',ifnull(new.nombres,'')); END IF;
	IF OLD.apellidos <> new.apellidos THEN set @res = CONCAT (@res, ' |Cambió el valor de apellidos: ',ifnull(OLD.apellidos,''), ' a: ',ifnull(new.apellidos,'')); END IF;
	IF OLD.direccion <> new.direccion THEN set @res = CONCAT (@res, ' |Cambió el valor de direccion: ',ifnull(OLD.direccion,''), ' a: ',ifnull(new.direccion,'')); END IF;
	IF OLD.telefono <> new.telefono THEN set @res = CONCAT (@res, ' |Cambió el valor de telefono: ',ifnull(OLD.telefono,''), ' a: ',ifnull(new.telefono,'')); END IF;
	IF OLD.correo <> new.correo THEN set @res = CONCAT (@res, ' |Cambió el valor de correo: ',ifnull(OLD.correo,''), ' a: ',ifnull(new.correo,'')); END IF;
	IF OLD.fecha_creacion <> new.fecha_creacion THEN set @res = CONCAT (@res, ' |Cambió el valor de fecha_creacion: ',ifnull(OLD.fecha_creacion,''), ' a: ',ifnull(new.fecha_creacion,'')); END IF;

	#insertamos en nuestra tabla de log la informacion
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla,valor_alterado)
	VALUES (@oldq ,@newq,CURRENT_USER,"UPDATE",NOW(),"proveedor",ifnull(@res,'No cambio nada'));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

DROP TABLE IF EXISTS `rol`;
CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `leer` tinyint(1) DEFAULT NULL,
  `editar` tinyint(1) DEFAULT NULL,
  `eliminar` tinyint(1) DEFAULT NULL,
  `rol_usuario` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `leer`, `editar`, `eliminar`, `rol_usuario`) VALUES
(1, 1, 1, 1, 'administrador'),
(2, 1, 1, NULL, 'avanzado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transaccion`
--

DROP TABLE IF EXISTS `transaccion`;
CREATE TABLE `transaccion` (
  `id` int(11) NOT NULL,
  `material_id` int(11) DEFAULT NULL,
  `precio_unitario` double(11,2) DEFAULT NULL,
  `q` int(11) DEFAULT NULL,
  `qe` int(11) DEFAULT NULL,
  `transaccion_tipo_id` int(11) DEFAULT NULL,
  `pedido_id` int(11) DEFAULT NULL,
  `obs` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `transaccion`
--

INSERT INTO `transaccion` (`id`, `material_id`, `precio_unitario`, `q`, `qe`, `transaccion_tipo_id`, `pedido_id`, `obs`, `fecha_creacion`) VALUES
(1, 1, 20.00, 100, NULL, 1, 1, NULL, '2018-11-10 20:52:19'),
(2, 1, NULL, 5, 5, 2, 2, NULL, '2018-11-10 21:51:57'),
(3, 1, NULL, 10, 10, 2, 3, NULL, '2018-11-10 22:12:26'),
(4, 1, 50.00, 22, NULL, 1, 4, NULL, '2018-11-11 12:11:29'),
(5, 3, 1.50, 50, NULL, 1, 5, NULL, '2018-11-12 10:50:15'),
(6, 3, NULL, 2, 2, 2, 6, NULL, '2018-11-12 17:03:12'),
(7, 1, 25.00, 50, NULL, 1, 7, NULL, '2018-11-14 00:00:34'),
(8, 1, NULL, 10, 10, 2, 8, NULL, '2018-11-21 09:23:28'),
(9, 1, NULL, 5, 5, 2, 9, NULL, '2018-11-29 19:07:27'),
(10, 1, NULL, 2, 2, 2, 10, NULL, '2018-11-29 19:53:17'),
(11, 1, NULL, 5, 5, 2, 11, NULL, '2018-11-29 20:28:30'),
(12, 3, NULL, 1, 1, 2, 12, NULL, '2018-11-29 20:37:43'),
(13, 1, NULL, 3, 3, 2, 13, NULL, '2018-11-29 21:05:20'),
(14, 1, NULL, 2, 1, 2, 14, 'POR MOTIVOS DE STOCK NO SE LE ENTREGA LA CANTIDAD SOLICITADA', '2020-08-18 15:02:10'),
(15, 1, NULL, 1, NULL, 2, 15, NULL, '2020-08-18 19:32:30'),
(16, 3, NULL, 1, NULL, 2, 15, NULL, '2020-08-18 19:32:30'),
(17, 6, 30.00, 100, NULL, 1, 16, NULL, '2020-08-18 19:53:50'),
(18, 11, 3.00, 100, NULL, 1, 17, NULL, '2020-08-18 20:02:05'),
(19, 12, 3.00, 100, NULL, 1, 17, NULL, '2020-08-18 20:02:05'),
(20, 17, 15.00, 50, NULL, 1, 17, NULL, '2020-08-18 20:02:05'),
(21, 16, 10.00, 100, NULL, 1, 17, NULL, '2020-08-18 20:02:05'),
(22, 21, 1.00, 100, NULL, 1, 17, NULL, '2020-08-18 20:02:05'),
(23, 22, 1.00, 50, NULL, 1, 17, NULL, '2020-08-18 20:02:05'),
(24, 31, 2.50, 100, NULL, 1, 17, NULL, '2020-08-18 20:02:05'),
(25, 32, 2.50, 100, NULL, 1, 17, NULL, '2020-08-18 20:02:05'),
(26, 33, 2.50, 100, NULL, 1, 17, NULL, '2020-08-18 20:02:05'),
(27, 41, 15.00, 10, NULL, 1, 18, NULL, '2020-08-18 20:06:55'),
(28, 40, 3.00, 30, NULL, 1, 18, NULL, '2020-08-18 20:06:55'),
(29, 14, 20.00, 20, NULL, 1, 19, NULL, '2020-08-18 21:18:49'),
(30, 12, 2.00, 10, NULL, 1, 19, NULL, '2020-08-18 21:18:49'),
(31, 1, NULL, 2, NULL, 2, 20, NULL, '2020-08-20 10:08:48'),
(32, 18, 15.00, 50, NULL, 1, 21, NULL, '2020-08-20 10:11:17'),
(33, 13, 3.00, 20, NULL, 1, 21, NULL, '2020-08-20 10:11:17'),
(34, 3, NULL, 1, NULL, 2, 22, NULL, '2020-08-21 17:23:48'),
(35, 12, 3.00, 100, NULL, 1, 23, NULL, '2020-08-22 08:31:02'),
(36, 1, NULL, 1, NULL, 2, 24, NULL, '2020-08-22 18:05:01'),
(37, 11, NULL, 1, 1, 2, 25, NULL, '2020-08-22 18:17:37'),
(38, 13, NULL, 2, 1, 2, 25, 'por falta de material solo se le entrego uno', '2020-08-22 18:17:37'),
(39, 21, NULL, 10, NULL, 2, 26, NULL, '2020-08-22 18:19:38'),
(40, 3, NULL, 5, NULL, 2, 27, NULL, '2020-08-23 15:27:45'),
(41, 12, NULL, 4, NULL, 2, 27, NULL, '2020-08-23 15:27:45'),
(42, 1, NULL, 1, NULL, 2, 28, NULL, '2020-08-25 23:03:31'),
(43, 11, 3.00, 12, NULL, 1, 29, NULL, '2020-08-25 23:05:38'),
(44, 3, NULL, 1, NULL, 2, 30, NULL, '2020-08-25 23:11:45'),
(45, 1, NULL, 1, NULL, 2, 31, NULL, '2020-08-25 23:20:41'),
(46, 1, NULL, 1, NULL, 2, 32, NULL, '2020-08-25 23:22:07');

--
-- Disparadores `transaccion`
--
DROP TRIGGER IF EXISTS `deletetransaccion`;
DELIMITER $$
CREATE TRIGGER `deletetransaccion` AFTER DELETE ON `transaccion` FOR EACH ROW BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |material_id=',ifnull(old.material_id,''),
    ' |precio_unitario=',ifnull(old.precio_unitario,''),
    ' |q=',ifnull(old.q,''),
    ' |qe=',ifnull(old.qe,''),
    ' |transaccion_tipo_id=',ifnull(old.transaccion_tipo_id,''),
    ' |pedido_id=',ifnull(old.pedido_id,''),
    ' |obs=',ifnull(old.obs,''),
    ' |fecha_creacion=',ifnull(old.fecha_creacion,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"transaccion");
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `inserttransaccion`;
DELIMITER $$
CREATE TRIGGER `inserttransaccion` BEFORE INSERT ON `transaccion` FOR EACH ROW BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |material_id=',ifnull(new.material_id,''),
    ' |precio_unitario=',ifnull(new.precio_unitario,''),
    ' |q=',ifnull(new.q,''),
    ' |qe=',ifnull(new.qe,''),
    ' |transaccion_tipo_id=',ifnull(new.transaccion_tipo_id,''),
    ' |pedido_id=',ifnull(new.pedido_id,''),
    ' |obs=',ifnull(new.obs,''),
    ' |fecha_creacion=',ifnull(new.fecha_creacion,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"transaccion");
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `updatetransaccion`;
DELIMITER $$
CREATE TRIGGER `updatetransaccion` BEFORE UPDATE ON `transaccion` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN
  #Valores viejos
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |material_id=',ifnull(old.material_id,''),
    ' |precio_unitario=',ifnull(old.precio_unitario,''),
    ' |q=',ifnull(old.q,''),
    ' |qe=',ifnull(old.qe,''),
    ' |transaccion_tipo_id=',ifnull(old.transaccion_tipo_id,''),
    ' |pedido_id=',ifnull(old.pedido_id,''),
    ' |obs=',ifnull(old.obs,''),
    ' |fecha_creacion=',ifnull(old.fecha_creacion,'')
  );
  #Valores nuevos
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |material_id=',ifnull(new.material_id,''),
    ' |precio_unitario=',ifnull(new.precio_unitario,''),
    ' |q=',ifnull(new.q,''),
    ' |qe=',ifnull(new.qe,''),
    ' |transaccion_tipo_id=',ifnull(new.transaccion_tipo_id,''),
    ' |pedido_id=',ifnull(new.pedido_id,''),
    ' |obs=',ifnull(new.obs,''),
    ' |fecha_creacion=',ifnull(new.fecha_creacion,'')
  );

	#guardamos en una variable los valores que unicamente cambiaron
  set @res = '';
	IF OLD.material_id <> new.material_id THEN set @res = CONCAT (@res, ' |Cambió el valor de material_id: ',ifnull(OLD.material_id,''), ' a: ',ifnull(new.material_id,'')); END IF;
	IF OLD.precio_unitario <> new.precio_unitario THEN set @res = CONCAT (@res, ' |Cambió el valor de precio_unitario: ',ifnull(OLD.precio_unitario,''), ' a: ',ifnull(new.precio_unitario,'')); END IF;
	IF OLD.q <> new.q THEN set @res = CONCAT (@res, ' |Cambió el valor de q: ',ifnull(OLD.q,''), ' a: ',ifnull(new.q,'')); END IF;
	IF OLD.qe <> new.qe THEN set @res = CONCAT (@res, ' |Cambió el valor de qe: ',ifnull(OLD.qe,''), ' a: ',ifnull(new.qe,'')); END IF;
	IF OLD.transaccion_tipo_id <> new.transaccion_tipo_id THEN set @res = CONCAT (@res, ' |Cambió el valor de transaccion_tipo_id: ',ifnull(OLD.transaccion_tipo_id,''), ' a: ',ifnull(new.transaccion_tipo_id,'')); END IF;
	IF OLD.pedido_id <> new.pedido_id THEN set @res = CONCAT (@res, ' |Cambió el valor de pedido_id: ',ifnull(OLD.pedido_id,''), ' a: ',ifnull(new.pedido_id,'')); END IF;
	IF OLD.obs <> new.obs THEN set @res = CONCAT (@res, ' |Cambió el valor de obs: ',ifnull(OLD.obs,''), ' a: ',ifnull(new.obs,'')); END IF;
	IF OLD.fecha_creacion <> new.fecha_creacion THEN set @res = CONCAT (@res, ' |Cambió el valor de fecha_creacion: ',ifnull(OLD.fecha_creacion,''), ' a: ',ifnull(new.fecha_creacion,'')); END IF;

	#insertamos en nuestra tabla de log la informacion
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla,valor_alterado)
	VALUES (@oldq ,@newq,CURRENT_USER,"UPDATE",NOW(),"transaccion",ifnull(@res,'No cambio nada'));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transaccion_tipo`
--

DROP TABLE IF EXISTS `transaccion_tipo`;
CREATE TABLE `transaccion_tipo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `transaccion_tipo`
--

INSERT INTO `transaccion_tipo` (`id`, `nombre`) VALUES
(1, 'entrada'),
(2, 'salida');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  `funcionario_id` int(11) DEFAULT NULL,
  `rol_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `password`, `estado`, `funcionario_id`, `rol_id`) VALUES
(0, 'adminnoe2018', 'f865b53623b121fd34ee5426c792e5c33af8c227', 1, 0, 1),
(1, 'admin', 'f865b53623b121fd34ee5426c792e5c33af8c227', 1, 10, 1),
(2, 'admin1', '7ea4912022463b0c1342562015f114768c41c1ec', 1, 1, 2),
(3, 'admin2', 'f865b53623b121fd34ee5426c792e5c33af8c227', 1, 3, 2),
(4, 'LUCAS', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 0, 2, 1),
(5, 'noe', 'cfdbe72ce1072c7dd9418e150a4ac157464cf676', 1, 12, 2),
(6, 'LUCIA', 'b521caa6e1db82e5a01c924a419870cb72b81635', 1, 13, 2),
(7, 'mary', 'f865b53623b121fd34ee5426c792e5c33af8c227', 1, 11, 2);

--
-- Disparadores `usuario`
--
DROP TRIGGER IF EXISTS `deleteusuario`;
DELIMITER $$
CREATE TRIGGER `deleteusuario` AFTER DELETE ON `usuario` FOR EACH ROW BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |nombre=',ifnull(old.nombre,''),
    ' |estado=',ifnull(old.estado,''),
    ' |funcionario_id=',ifnull(old.funcionario_id,''),
    ' |rol_id=',ifnull(old.rol_id,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"usuario");
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `insertusuario`;
DELIMITER $$
CREATE TRIGGER `insertusuario` BEFORE INSERT ON `usuario` FOR EACH ROW BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |nombre=',ifnull(new.nombre,''),
    ' |estado=',ifnull(new.estado,''),
    ' |funcionario_id=',ifnull(new.funcionario_id,''),
    ' |rol_id=',ifnull(new.rol_id,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"usuario");
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `updateusuario`;
DELIMITER $$
CREATE TRIGGER `updateusuario` BEFORE UPDATE ON `usuario` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN
  #Valores viejos
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |nombre=',ifnull(old.nombre,''),
    ' |estado=',ifnull(old.estado,''),
    ' |funcionario_id=',ifnull(old.funcionario_id,''),
    ' |rol_id=',ifnull(old.rol_id,'')
  );
  #Valores nuevos
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |nombre=',ifnull(new.nombre,''),
    ' |estado=',ifnull(new.estado,''),
    ' |funcionario_id=',ifnull(new.funcionario_id,''),
    ' |rol_id=',ifnull(new.rol_id,'')
  );

	#guardamos en una variable los valores que unicamente cambiaron
  set @res = '';
	IF OLD.nombre <> new.nombre THEN set @res = CONCAT (@res, ' |Cambió el valor de nombre: ',ifnull(OLD.nombre,''), ' a: ',ifnull(new.nombre,'')); END IF;
	IF OLD.password <> new.password THEN set @res = CONCAT (@res, ' |El password a cambiado!'); END IF;
	IF OLD.estado <> new.estado THEN set @res = CONCAT (@res, ' |Cambió el valor de estado: ',ifnull(OLD.estado,''), ' a: ',ifnull(new.estado,'')); END IF;
	IF OLD.funcionario_id <> new.funcionario_id THEN set @res = CONCAT (@res, ' |Cambió el valor de funcionario_id: ',ifnull(OLD.funcionario_id,''), ' a: ',ifnull(new.funcionario_id,'')); END IF;
	IF OLD.rol_id <> new.rol_id THEN set @res = CONCAT (@res, ' |Cambió el valor de rol_id: ',ifnull(OLD.rol_id,''), ' a: ',ifnull(new.rol_id,'')); END IF;

	#insertamos en nuestra tabla de log la informacion
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla,valor_alterado)
	VALUES (@oldq ,@newq,CURRENT_USER,"UPDATE",NOW(),"usuario",ifnull(@res,'No cambio nada'));
END
$$
DELIMITER ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cant_aut`
--
ALTER TABLE `cant_aut`
  ADD PRIMARY KEY (`id`),
  ADD KEY `area_id` (`area_id`),
  ADD KEY `material_id` (`material_id`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `area_id` (`area_id`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado_pedido`
--
ALTER TABLE `estado_pedido`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `funcionario`
--
ALTER TABLE `funcionario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cargo_id` (`cargo_id`);

--
-- Indices de la tabla `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoria_id` (`categoria_id`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `estado_pedido_id` (`estado_pedido_id`),
  ADD KEY `funcionario_id` (`funcionario_id`),
  ADD KEY `proveedor_id` (`proveedor_id`),
  ADD KEY `usuario_id` (`usuario_id`),
  ADD KEY `transaccion_tipo_id` (`transaccion_tipo_id`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `transaccion`
--
ALTER TABLE `transaccion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `material_id` (`material_id`),
  ADD KEY `transaccion_tipo_id` (`transaccion_tipo_id`),
  ADD KEY `pedido_id` (`pedido_id`);

--
-- Indices de la tabla `transaccion_tipo`
--
ALTER TABLE `transaccion_tipo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rol_id` (`rol_id`),
  ADD KEY `funcionario_id` (`funcionario_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `cant_aut`
--
ALTER TABLE `cant_aut`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `estado_pedido`
--
ALTER TABLE `estado_pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `funcionario`
--
ALTER TABLE `funcionario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1498;

--
-- AUTO_INCREMENT de la tabla `material`
--
ALTER TABLE `material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT de la tabla `pedido`
--
ALTER TABLE `pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `transaccion`
--
ALTER TABLE `transaccion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT de la tabla `transaccion_tipo`
--
ALTER TABLE `transaccion_tipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cant_aut`
--
ALTER TABLE `cant_aut`
  ADD CONSTRAINT `cant_aut_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `area` (`id`),
  ADD CONSTRAINT `cant_aut_ibfk_2` FOREIGN KEY (`material_id`) REFERENCES `material` (`id`);

--
-- Filtros para la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD CONSTRAINT `cargo_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `area` (`id`);

--
-- Filtros para la tabla `funcionario`
--
ALTER TABLE `funcionario`
  ADD CONSTRAINT `funcionario_ibfk_1` FOREIGN KEY (`cargo_id`) REFERENCES `cargo` (`id`);

--
-- Filtros para la tabla `material`
--
ALTER TABLE `material`
  ADD CONSTRAINT `material_ibfk_1` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`);

--
-- Filtros para la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `pedido_ibfk_1` FOREIGN KEY (`estado_pedido_id`) REFERENCES `estado_pedido` (`id`),
  ADD CONSTRAINT `pedido_ibfk_2` FOREIGN KEY (`funcionario_id`) REFERENCES `funcionario` (`id`),
  ADD CONSTRAINT `pedido_ibfk_3` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`),
  ADD CONSTRAINT `pedido_ibfk_4` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `pedido_ibfk_5` FOREIGN KEY (`transaccion_tipo_id`) REFERENCES `transaccion_tipo` (`id`);

--
-- Filtros para la tabla `transaccion`
--
ALTER TABLE `transaccion`
  ADD CONSTRAINT `transaccion_ibfk_1` FOREIGN KEY (`material_id`) REFERENCES `material` (`id`),
  ADD CONSTRAINT `transaccion_ibfk_2` FOREIGN KEY (`transaccion_tipo_id`) REFERENCES `transaccion_tipo` (`id`),
  ADD CONSTRAINT `transaccion_ibfk_3` FOREIGN KEY (`pedido_id`) REFERENCES `pedido` (`id`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`id`),
  ADD CONSTRAINT `usuario_ibfk_2` FOREIGN KEY (`funcionario_id`) REFERENCES `funcionario` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
