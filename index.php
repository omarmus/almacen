<?php
/*
AGREGARA CONTROLADOR POR DEFECTO POR SI NO ESTA ASIGNADO O DEFINIDO
*/
require_once('config/config.php');
require_once('core/FrontController.php');
$view=isset($_GET['view']) ? $_GET['view']:CONTROLADOR_DEFAULT;
$view=ucwords($view);
if (file_exists('controller/'.$view.'_Controller.php')) {
	$controllerObj=cargarControlador($view);
	lanzarAccion($controllerObj);
 }else{
 	///pagina default de error
 	require_once('views/error404.php');
 }
?>