app.controller('ctrlrepGeneral', function ($scope, $http, $sce) {
  $('.button-collapse').sideNav('hide');
  $('.collapsible').collapsible('close', 0);
  $scope.buscadatos = function () {
    $scope.repgral = [];
    $http.post('index.php?view=estadistica&action=buscarDatos', {
      'inicial': $scope.fechaini,
      'final': $scope.fechafin
    })
      .then(function (response) {
        if (response.data != 0) {
          $scope.repgral = response.data;
        }
      });
  };
  $scope.pdf = function () {
   		// window.open('index.php?view=estadistica&action=pdf&inicial='+$scope.fechaini+'&final='+$scope.fechafin);
    // alert($scope.fechaini);

    $http({
      method: 'GET',
      url: 'index.php?view=estadistica&action=pdf',
      params: { 'inicial': $scope.fechaini, 'final': $scope.fechafin },
      responseType: 'arraybuffer'
    })
      .then(function (response) {
        var file = new Blob([response.data], { type: 'application/pdf' });
        var fileURL = URL.createObjectURL(file);
        $scope.facturapdf = $sce.trustAsResourceUrl(fileURL);
        $('#modalpdf').modal('open');
      });
  };
  $scope.excel = function () {
    $http.post('index.php?view=estadistica&action=excel', {
      'inicial': $scope.fechaini,
      'final': $scope.fechafin
    })
      .then(function (response) {

      });
  };
});

app.controller('ctrlrepPersona', function ($scope, $http, $sce) {
  $('.button-collapse').sideNav('hide');
  $('.collapsible').collapsible('close', 0);
  $scope.conreg = false;
  $scope.datosfun = [];
  $scope.datosgral = [];
  $http.post('index.php?view=reppersona&action=datosfuncionarios')
    .then(function (response) {
      $scope.datosfun = response.data;
    });
  $scope.buscadatos = function () {
    $scope.datosgral = [];
    $http.post('index.php?view=reppersona&action=buscarDatos', {
      'inicial': $scope.fechaini,
      'final': $scope.fechafin,
      'idfun': $scope.idfun
    })
      .then(function (response) {
        if (response.data != 0) {
          $scope.datosgral = response.data;
          $scope.conreg = true;
        }
      });
  };
  $scope.cambiardatos = function (idfun) {
    if (idfun != null) {
      $scope.buscadatos();
    }
  };
  $scope.pdf = function () {
    // window.open('index.php?view=reppersona&action=pdf&inicial='+$scope.fechaini+'&final='+$scope.fechafin+'&idfun='+$scope.idfun);
    /* alert("alert"); */
    $http({
      method: 'GET',
      url: 'index.php?view=reppersona&action=pdf',
      params: { 'inicial': $scope.fechaini, 'final': $scope.fechafin, 'idfun': $scope.idfun },
      responseType: 'arraybuffer'
    })
      .then(function (response) {
        var file = new Blob([response.data], { type: 'application/pdf' });
        var fileURL = URL.createObjectURL(file);
        $scope.facturapdf = $sce.trustAsResourceUrl(fileURL);
        $('#modalpdf').modal('open');
      });
  };
});

app.controller('ctrlrepMaterial', function ($scope, $http, $sce) {
  $('.button-collapse').sideNav('hide');
  $('.collapsible').collapsible('close', 0);

  $scope.listaexiste = true;
  $scope.listanoexiste = true;
  $scope.currentPage = 0;
  $scope.pageSize = 5;
  $scope.pages = [];

  $scope.regAllmate = function () {
    $scope.datosalmacen = [];
    $http.post('index.php?view=almacen&action=listardisponible')
      .then(function (response) {
        if (response.data != 0) {
          $scope.datosalmacen = response.data;
          $scope.listaexiste = false;
        } else {
          $scope.listanoexiste = false;
        }
      });
  };

  $scope.pdf = function () {
    $http({
      method: 'GET',
      url: 'index.php?view=almacen&action=pdf',
      params: {
        'material': 'si',
        nombre: name_completo
      },
      responseType: 'arraybuffer'
    })
      .then(function (response) {
        var file = new Blob([response.data], { type: 'application/pdf' });
        var fileURL = URL.createObjectURL(file);
        $scope.facturapdf = $sce.trustAsResourceUrl(fileURL);
        $('#modalpdf').modal('open');
      });
  };

  $scope.configPages = function () {
    $scope.regAllmate();
    // alert(datosalmacen.length);
    $scope.pages.length = 0;
    var ini = $scope.currentPage - 4;
    var fin = $scope.currentPage + 5;

    if (ini < 1) {
      ini = 1;
      if (Math.ceil($scope.datosalmacen.length / $scope.pageSize) > 10) { fin = 10; } else { fin = Math.ceil($scope.datosalmacen.length / $scope.pageSize); }
    } else {
      if (ini >= Math.ceil($scope.datosalmacen.length / $scope.pageSize) - 10) {
        ini = Math.ceil($scope.datosalmacen.length / $scope.pageSize) - 10;
        fin = Math.ceil($scope.datosalmacen.length / $scope.pageSize);
      }
    }
    if (ini < 1) { ini = 1; }
    for (var i = ini; i <= fin; i++) {
      $scope.pages.push({ no: i });
    }

    if ($scope.currentPage >= $scope.pages.length) {
      $scope.currentPage = $scope.pages.length - 1;
    }
  };

  $scope.setPage = function (page) {
    $scope.currentPage = page - 1;
  };
});

app.controller('ctrlrepMaterialNo', function ($scope, $http, $sce) {
  $('.button-collapse').sideNav('hide');
  $('.collapsible').collapsible('close', 0);

  $scope.listaexiste = true;
  $scope.listanoexiste = true;
  $scope.currentPage = 0;
  $scope.pageSize = 5;
  $scope.pages = [];

  $scope.regAllmate = function () {
    $scope.datosalmacen = [];
    $http.post('index.php?view=almacen&action=listarnodisponible')
      .then(function (response) {
        if (response.data != 0) {
          $scope.datosalmacen = response.data;
          $scope.listaexiste = false;
        } else {
          $scope.listanoexiste = false;
        }
      });
  };

  $scope.pdf = function () {
    $http({
      method: 'GET',
      url: 'index.php?view=almacen&action=pdf',
      params: {
        'material': 'no',
        nombre: name_completo
      },
      responseType: 'arraybuffer'
    })
      .then(function (response) {
        var file = new Blob([response.data], { type: 'application/pdf' });
        var fileURL = URL.createObjectURL(file);
        $scope.facturapdf = $sce.trustAsResourceUrl(fileURL);
        $('#modalpdfno').modal('open');
      });
  };

  $scope.configPages = function () {
    $scope.regAllmate();
    // alert(datosalmacen.length);
    $scope.pages.length = 0;
    var ini = $scope.currentPage - 4;
    var fin = $scope.currentPage + 5;

    if (ini < 1) {
      ini = 1;
      if (Math.ceil($scope.datosalmacen.length / $scope.pageSize) > 10) { fin = 10; } else { fin = Math.ceil($scope.datosalmacen.length / $scope.pageSize); }
    } else {
      if (ini >= Math.ceil($scope.datosalmacen.length / $scope.pageSize) - 10) {
        ini = Math.ceil($scope.datosalmacen.length / $scope.pageSize) - 10;
        fin = Math.ceil($scope.datosalmacen.length / $scope.pageSize);
      }
    }
    if (ini < 1) { ini = 1; }
    for (var i = ini; i <= fin; i++) {
      $scope.pages.push({ no: i });
    }

    if ($scope.currentPage >= $scope.pages.length) {
      $scope.currentPage = $scope.pages.length - 1;
    }
  };

  $scope.setPage = function (page) {
    $scope.currentPage = page - 1;
  };
});

app.filter('startFromGrid', function () {
  return function (input, start) {
    start = +start;
    return input.slice(start);
  };
});
