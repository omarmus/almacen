app.controller('ctrladmin', function($scope, $http, $interval, $location,$rootScope,$notify) {
  //$('#fompedido').modal('close');
    //var total = '';
    var areaid=''; 
    var idpedido='';
    var vm = this;
    $scope.listaver= false;
  	$scope.mostrarpedidos = function(){
  		$scope.pendientes = '';
    var promise = $interval(function(){$http.post("index.php?view=admin&action=listarpendientes")
    .then(function (response) {
       $scope.pendientes = response.data[0].num;
    });}, 7000); 
  	}
    $scope.$on('destroy', function()
    {
      $interval.cancel(promise);
    });
  	$scope.abrirpendientes = function(){
      if ($scope.pendientes != 0) {
        $scope.todopendientes=[];
        $http.post("index.php?view=admin&action=listartodopendientes")
        .then(function (response) {
          $scope.todopendientes = response.data;
          $location.path("/despendientes"); 
        });
      }else{
        $location.path("/");
      }
  	}
    $scope.detallependiente = function(){ 
        $scope.detalle = [];
        $scope.verificado=[];
      $http.post("index.php?view=admin&action=generarlistadetallada",{
        'id': idpedido,
        'aid': areaid
      })
      .then(function (response) {
        if (response.data != 0) {
          $scope.detalle = response.data;
          } 
          $location.path("/despendientes"); 
          //location.reload("/despendientes");      
      });
    }
    $scope.undetalle = function(id,aid){
      idpedido = id;
      areaid = aid;
      //alert(areaid);
      $scope.detallependiente();
      $scope.listaver= true;
    }
    $scope.ocultadetalle = function(){
      $scope.listaver= false;
    }
    $scope.entregarpedido = function(){
      //alert(idpedido);
      $scope.unpedido = [];
      $http.post("index.php?view=admin&action=detalleunpedido",{
        'id': idpedido
      }).then(function (response){
        $scope.unpedido = response.data;
        for (var i = 0; i <= $scope.unpedido.length-1; i++) {
          if ($scope.unpedido[i].qe == null && $scope.unpedido[i].obs == null){
            $http.post("index.php?view=admin&action=nocambioMatPedido",{
              'tid': $scope.unpedido[i].tid,
              'ce': $scope.unpedido[i].q
            }).then(function (response){});
          }
        }
        $http.post("index.php?view=admin&action=estadoPedido",{
          'id': idpedido
        }).then(function (response){
          if(response.data[0]==1){
            $scope.abrirpendientes();
            $scope.ocultadetalle();
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.success('Ok','Se entrego El material!');
            $scope.generarfomentregaPDF();
          }else{
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.error('Error','Nose se Entrego Material!');
          }
        });
      });
      //alert(idpedido);
    }
    $scope.generarfomentregaPDF = function(){
        window.open('index.php?view=reportes&action=generarPDFentrega&id='+idpedido);
    }
    $scope.editarmatTransaccion = function(index){
      //alert(index);
      $scope.codigo = $scope.detalle[index].codigo;
      $scope.material = $scope.detalle[index].nom_material+' '+$scope.detalle[index].descripcion;
      $scope.ca = $scope.detalle[index].ca;
      $scope.cs = $scope.detalle[index].q;
      $scope.tid = $scope.detalle[index].tid;
      $('#modalentrega').modal('open');
    }
    $scope.confirmarcambioPedido = function(){
      $http.post("index.php?view=admin&action=cambioMatPedido",{
        'tid': $scope.tid,
        'ce': $scope.ce,
        'obs':$scope.obs
      }).then(function (response){
        if (response.data[0]==1) {
          $notify.setTime(3).showCloseButton(false).showProgressBar(true);
          $notify.setPosition('bottom-left');
          $notify.success('Ok','Registro Correcto!');
          $scope.detallependiente();
          $('#modalentrega').modal('close');
        }else{
          $notify.setTime(3).showCloseButton(false).showProgressBar(true);
          $notify.setPosition('bottom-left');
          $notify.error('Error','Nose se Registro!');
          $('#modalentrega').modal('close');
        }
      });
    }
});