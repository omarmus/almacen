app.controller('ctrlcategorias', function($scope, $http, $notify, $filter) {
	$('.button-collapse').sideNav('hide');
   	$('.collapsible').collapsible('close',0);
   	$scope.listaexiste=true;
   	$scope.listanoexiste=true;
   	var idelimina = '';
   	$scope.currentPage = 0;
   	$scope.pageSize = 5;
   	$scope.pages = [];

   	$scope.openformulario = function(){
  		$scope.limpiarForm();
      	$("#titulo-modal").text("Registrar Nueva  Categoria");
   	  	$('#btnModificar').hide();
      	$('#btnCrear').show();
      	$('#formCategoria').modal('open');   
  	}
  	$scope.limpiarForm = function(){
  		$scope.Formcate.tipo = "";
  		$scope.Formcate.nomcat = "";
  	}
  	$scope.crearcategoria = function(){
  		$http.post("index.php?view=categoria&action=crearcategoria",{
                  "tipo":$scope.Formcate.tipo,
                  "cate":$scope.Formcate.nomcat   
        })
        .then(function (response){
            if(response.data[0]==1){
                $notify.setTime(3).showCloseButton(false).showProgressBar(true);
                $notify.setPosition('bottom-left');
                $notify.success('Ok','Registro Correcto!');
                $('#formCategoria').modal('close');
                $scope.listanoexiste=true;
                $scope.setPage(1);
                $scope.configPages();
            }else{
                $notify.setTime(3).showCloseButton(false).showProgressBar(true);
                $notify.setPosition('bottom-left');
                $notify.error('Error','Nose se Registro!');
                $('#formCategoria').modal('close');
                $scope.listanoexiste=true;  
            }
        });	
  	}
  	$scope.regAllcate = function(){
  		$scope.datoscat=[];
  		$http.post("index.php?view=categoria&action=listarcategorias")
      	.then(function (response) {
        if(response.data != 0){
          $scope.datoscat = response.data;
          $scope.listaexiste=false;
        }else{
          $scope.listanoexiste=false;
        }
      });
  	}
  	$scope.abrirformelimina = function(id){
    idelimina = id;
    $('#formeliminar').modal('open');
  	}
  	$scope.eliminarcategoria = function(){
    $http.post("index.php?view=categoria&action=eliminarcategoria" ,{
      'id': idelimina
      }).then(function(response){
          if(response.data[0]==1){
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.success('Ok','Eliminacion Correcta!');
            $('#formeliminar').modal('close');
            $scope.listaexiste=true;
            $scope.setPage(1);
            $scope.configPages();
          }else{
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.error('Error','No se Elimino!');
            $('#formeliminar').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }
      });
  	}
  	$scope.abrirformeditacate = function(id){
    	$("#titulo-modal").text("Editar Categoria");
    	$('#btnCrear').hide();
    	$('#btnModificar').show();
    	$scope.limpiarForm();
    	$scope.datouncate=[];
    	$http.post("index.php?view=categoria&action=buscarunacate", {
      	"id" : id
    	}).then(function(response){
      	$scope.datouncate = response.data;
      	$scope.id = $scope.datouncate[0].id;
      	$scope.Formcate.tipo = $scope.datouncate[0].tipo;
      	$scope.Formcate.nomcat = $scope.datouncate[0].nombre_cat;
      	$('#formCategoria').modal('open');
    	});
  	}
  	$scope.editarCate = function(id){
    $http.post("index.php?view=categoria&action=editarcate", {
      "id" : $scope.id,
      "tipo":$scope.Formcate.tipo,
      "nomcat": $scope.Formcate.nomcat
    }).then(function(response){
        if(response.data[0]==1){
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.success('Ok','Edición Correcta!');
            $('#formCategoria').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }else{
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.error('Error','No se Edito!');
            $('#formCategoria').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }
    });
  	}
  	$scope.configPages = function() {
        $scope.regAllcate();
        alert(datoscat.length);/*no quitar me sirve para paginar mis registros*/
        $scope.pages.length = 0;
        var ini = $scope.currentPage - 4;
        var fin = $scope.currentPage + 5;
      
        if (ini < 1) {
            ini = 1;
            if (Math.ceil($scope.datoscat.length / $scope.pageSize) > 10)
                fin = 10;
            else
                fin = Math.ceil($scope.datoscat.length / $scope.pageSize);
        }
        else {
            if (ini >= Math.ceil($scope.datoscat.length / $scope.pageSize) - 10) {
                ini = Math.ceil($scope.datoscat.length / $scope.pageSize) - 10;
                fin = Math.ceil($scope.datoscat.length / $scope.pageSize);
            }
        }
        if (ini < 1) 
          ini = 1;
        for (var i = ini; i <= fin; i++) {
            $scope.pages.push({no: i});
        }

        if ($scope.currentPage >= $scope.pages.length)
            $scope.currentPage = $scope.pages.length - 1;

  	};
  	$scope.setPage = function(page) {
        $scope.currentPage = page - 1;
  	};
})
app.filter('startFromGrid', function() {
    return function(input, start) {
        start =+ start;
        return input.slice(start);
    }
});