app.controller('ctrlregEntrada', function($scope, $http, $notify,$sce) {
  
   	$('.button-collapse').sideNav('hide');
   	$('.collapsible').collapsible('close',0);
   	var idmat = '';
   	$scope.mostrarlista = true;
   	$scope.agregarca=[];
  	$scope.aux1=[];
  	var idpedido = '';
  	var iding = '';
  	var idprov = '';
  	$scope.mostrardetalle = false;
  	$scope.currentPage = 0;
  	$scope.pageSize = 5;
  	$scope.pages = [];
   	$scope.openformulario = function(){
   		$scope.limpiarForm();
   		$scope.listarprov();
   		$scope.listarmaterial(); 
   		$('#btnModificar').hide();
		$('#scope.btnCrear').show();
    	$('#modal3').modal('open');     
  	}
  	$scope.listarprov = function(){
  		$scope.datosprov=[];
  		$http.post("index.php?view=proveedor&action=listarproveedor")
        .then(function (response) {
          $scope.datosprov = response.data;
    	});
  	}
  	$scope.listarmaterial = function(){
  		$scope.datosmate=[];
  		$http.post("index.php?view=material&action=listarmaterial")
        .then(function (response) {
          $scope.datosmate = response.data;
    	});
  	}
  	$scope.llenadatosprov = function(index){
  		$scope.nitprov = $scope.datosprov[index].Nit;
  	}
  	$scope.limpiarForm = function(){
  		$scope.nitprov="";
  		$scope.nrofact="";
  		$scope.parpresup="";
		$scope.fuente ="";
		$scope.cantidad="";
		$scope.preciounit="";
		$scope.agregarca=[];
  	}
  	$scope.llenarsolicitud = function(){
  		$scope.datounmate=[];
  		$scope.datounmatetotal=[];
  		$scope.aux1 = $scope.agregarca.filter(c => c.id === $scope.selectmat);
  		if($scope.aux1 == 0){
  		$http.post("index.php?view=material&action=buscarunmate",{'id':$scope.selectmat})
        .then(function (response) {
          	$scope.datounmate = response.data;
  			$http.post("index.php?view=almacen&action=detalleunmaterial",{'id':$scope.selectmat})
        	.then(function (response) {
          		$scope.datounmatetotal = response.data;
          		$scope.agregarca.push({id:$scope.selectmat, codigo: $scope.datounmate[0].codigo, nommate: $scope.datounmate[0].nom_material,
          		desc: $scope.datounmate[0].descripcion,presentacion: $scope.datounmate[0].presentacion, tipo: $scope.datounmate[0].tipo,
          		q: $scope.cantidad, pu: $scope.preciounit, nrofact: $scope.nrofact, partida_presup: $scope.parpresup, fuente: $scope.fuente, 
          		id_prov: $scope.selectprov,tot: $scope.datounmatetotal});
          		//console.log($scope.agregarca);
          		$scope.cantidad="";
				$scope.preciounit="";
          		$scope.mostrarlista = false;
    		});
    	});
    	}
  	}
  	$scope.eliminaritem = function(index){
    	//$scope.aux2 = $scope.agregarca.lastIndexOf(index); //obtenemos el indice de un array
    	$scope.agregarca.splice(index, 1);//borramos el elemnto de un array
    	$scope.agregarca.sort();//reordeno el array despues de borrar
  	}
  	$scope.confirmarasigancion = function(){
      //alert(id_fun);
      //alert(id_usuario);
      /*$timeout($scope.generarfompedidoPDF(),8);*/
      $http.post("index.php?view=regEntrada&action=crearingreso",{
        'user_id': id_usuario,
        'fun_id': id_fun,
        'trans':{array1:$scope.agregarca}
      }).then(function (response) {
      		idpedido = response.data[1];
      		idprov = response.data[2];
          if(response.data[0]==1){
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.success('Ok','Registro Correcto!');
            $('#modal3').modal('close');
            $scope.generarfomingresoPDF(idpedido,idprov);
            $scope.setPage(1);
            $scope.configPages();
          }else{
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.error('Error','Nose se Registro!');
            $('#modal3').modal('close');
        }
      });
      $scope.agregarca=[];
      $scope.cantidad="";
	  $scope.preciounit="";
      $scope.mostrarlista = true;
    }
    $scope.listarIngresos = function(){
    	$scope.datosingresos=[];
  		$http.post("index.php?view=regEntrada&action=listarIngresos")
      	.then(function (response) {
          	$scope.datosingresos = response.data;
      	});	
    }
    $scope.datospedidoingresos=[];
    $scope.listarpedidoIngreso = function(){
    	
  		$http.post("index.php?view=regEntrada&action=listarpedidoIngreso")
      	.then(function (response) {
          	$scope.datospedidoingresos = response.data;
      	});
    }
    $scope.detalleuningreso = function(id){
    	$scope.undetalle=[];
  		$http.post("index.php?view=regEntrada&action=detalleuningreso",{'id':id})
      	.then(function (response) {
          	$scope.undetalle = response.data;
          	$scope.mostrardetalle = true;
      	});
    }
    $scope.ocultadetalle = function(){
      $scope.mostrardetalle= false;
    }
    $scope.generarfomingresoPDF = function(idpedido,idprov){
    	//alert(idpedido);/*aqui empiezo a generar el pdf*/
    	window.open('index.php?view=reportes&action=generarPDFingresos&id='+idpedido+'&idprov='+idprov);
    }
    $scope.imprimir = function(id,proveedor_id){
      //window.open('index.php?view=reportes&action=generarPDFingresos&id='+id+'&idprov='+proveedor_id);
      //$('#modalpdf').modal('open');
      $http({
        method:'GET',
        url:'index.php?view=reportes&action=generarPDFingresos',
        params: {'id':id,'idprov':proveedor_id},
        responseType:'arraybuffer'
      })
      .then(function (response) {
        var file = new Blob([response.data], {type: 'application/pdf'});
        var fileURL = URL.createObjectURL(file);
        $scope.facturapdf = $sce.trustAsResourceUrl(fileURL);
        $('#modalpdf').modal('open');
      });

    }
    $scope.configPages = function() {
        $scope.listarpedidoIngreso();
        alert(datospedidoingresos.length);/*no quitar me sirve para paginar mis registros*/
        $scope.pages.length = 0;
        var ini = $scope.currentPage - 4;
        var fin = $scope.currentPage + 5;
      
        if (ini < 1) {
            ini = 1;
            if (Math.ceil($scope.datospedidoingresos.length / $scope.pageSize) > 10)
                fin = 10;
            else
                fin = Math.ceil($scope.datospedidoingresos.length / $scope.pageSize);
        }
        else {
            if (ini >= Math.ceil($scope.datospedidoingresos.length / $scope.pageSize) - 10) {
                ini = Math.ceil($scope.datospedidoingresos.length / $scope.pageSize) - 10;
                fin = Math.ceil($scope.datospedidoingresos.length / $scope.pageSize);
            }
        }
        if (ini < 1) 
          ini = 1;
        for (var i = ini; i <= fin; i++) {
            $scope.pages.push({no: i});
        }
        if ($scope.currentPage >= $scope.pages.length)
            $scope.currentPage = $scope.pages.length - 1;
  	};
  	$scope.setPage = function(page) {
        $scope.currentPage = page - 1;
  	}; 
})
app.filter('startFromGrid', function() {
    return function(input, start) {
        start =+ start;
        return input.slice(start);
    }
});