app.controller('ctrlusuario', function($scope, $http,$notify, $filter) {

  $('.button-collapse').sideNav('hide');
  $('.collapsible').collapsible('close',0);
  $scope.listaexiste=true;
  $scope.listanoexiste=true;
  var idelimina = '';
  var idedita = '';
  var passcambio = '';
  var nuevopassword = '';
  $scope.funnoedita = true;
  $scope.funedita = true;
  $scope.checkeditapass = true;
  $scope.editacontra = true;
  $scope.currentPage = 0;
  $scope.pageSize = 5;
  $scope.pages = [];

  $scope.limpiarform = function () {
    $scope.Formuser.nomusuario="";
    $scope.Formuser.password1 ="";
    $scope.Formuser.password2 ="";
    $scope.Formuser.tipouser ="";
    $scope.useractivo ="";
  }
  $scope.openformulario = function(){
    $scope.funnoedita = false;
    $scope.funedita = true;
    $scope.checkeditapass = true;
    $scope.editacontra = false;
    $scope.limpiarform();
    $scope.buscarfunuser();

    $("#titulo-modal").text("Registrar Nuevo Usuario");
    $('#btnModificar').hide();
    $('#btnCrear').show();
    $('#formUsuario').modal('open');
  }

  $scope.regAlluser = function(){
      $scope.datosuser=[];
      $http.post("index.php?view=usuario&action=listarusuario")
      .then(function (response){
        if(response.data != 0){
          $scope.datosuser = response.data;
          $scope.listaexiste=false;
        }else{
          $scope.listanoexiste=false;
        }
      });
  }
  $scope.buscarfunuser = function(){
    $scope.listatodos=[];
    $scope.sololistaselect=[];
    $scope.res = [];
    $http.post("index.php?view=usuario&action=listatodosci")
        .then(function (response) {
          $scope.listatodos = response.data;
        $http.post("index.php?view=usuario&action=buscarcifun")
        .then(function (response) {
          $scope.sololistaselect = response.data;
          /*En esta parte comparo los dos array obtenidos para solo mostrar los funcionarios q no tienen registrado su usuario*/
          $scope.res = $scope.listatodos.filter(obj => {const exists = $scope.sololistaselect.some(obj2 => (obj2.funcionario_id === obj.id
          ));
          if (!exists) { return obj;}
          });/*fin de comparacion de array*/
        });
    });

  }

  $scope.crearusuario = function(){

    $http.post("index.php?view=usuario&action=crearusuario",{
      'fun_id':$scope.Formuser.cifun,
      'nomuser':$scope.Formuser.nomusuario,
      'passw1':$scope.Formuser.password1,
      'rol_id':$scope.Formuser.tipouser,
      'useractivo':$scope.useractivo
    })
    .then(function (response){
      $toastContent = response.data;
      if(response.data[0]==1){
                $notify.setTime(3).showCloseButton(false).showProgressBar(true);
                $notify.setPosition('bottom-left');
                $notify.success('Ok','Registro Correcto!');
                $('#formUsuario').modal('close');
                $scope.listanoexiste=true;
                $scope.setPage(1);
                $scope.configPages();
            }else{
                $notify.setTime(3).showCloseButton(false).showProgressBar(true);
                $notify.setPosition('bottom-left');
                $notify.error('Error',$toastContent);
                $('#formUsuario').modal('close');
                $scope.listanoexiste=true;
            }
    });
  }
  $scope.abrirformelimina = function(id){
    idelimina = id;
    $('#formeliminar').modal('open');
  }
  $scope.eliminarusuario = function(){
    $http.post("index.php?view=usuario&action=eliminarusuario",{
      'id': idelimina
      }).then(function(response){
          if(response.data[0]==1){
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.success('Ok','Eliminacion Correcta!');
            $('#formeliminar').modal('close');
            $scope.listaexiste=true;
            $scope.setPage(1);
            $scope.configPages();
          }else{
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.error('Error','No se Elimino!');
            $('#formeliminar').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }
      });
  }
  $scope.abrirformeditausu = function(id){
      $scope.funnoedita = true;
      $scope.funedita = false;
      $scope.checkeditapass = false;
      $scope.editacontra = true;
      $scope.confirmarcambio = "";
      $('#btnModificar').removeClass('disabled');
      $("#titulo-modal").text("Editar Usuario");
      $('#btnCrear').hide();
      $('#btnModificar').show();
      $scope.limpiarform();
      $scope.datounusuario=[];
      $http.post("index.php?view=usuario&action=buscarunusuario", {
        "id" : id
      }).then(function(response){
        $scope.datounusuario = response.data;
        idedita = $scope.datounusuario[0].id;
        $scope.Formuser.nomusuario = $scope.datounusuario[0].nombre;
        passcambio = $scope.datounusuario[0].password;
        $scope.Formuser.tipouser = $scope.datounusuario[0].rol_id;

        if($scope.datounusuario[0].estado == 1){
          $scope.useractivo = true;
        }
        $scope.dato1 = $scope.datounusuario[0].funcionario_id;
        $scope.dato2 = $scope.datounusuario[0].ci;
        $scope.dato3 = $scope.datounusuario[0].nombres;
        $scope.data = {
        availableOptions: [
        {id: $scope.dato1, ci: $scope.dato2, nombres: $scope.dato3}
        ],
        selectedOption: {id: $scope.dato1, ci: $scope.dato2, nombres: $scope.dato3}
        };
        $('#formUsuario').modal('open');
      });
  }
  $scope.activa = function(){
    //alert($scope.confirmed);
      if($scope.confirmarcambio == true){
        $scope.editacontra = false;
        $scope.Formuser.password1 ="";
        $scope.Formuser.password2 ="";
      }else{
        $scope.editacontra = true;
      }
  }
  $scope.editarUsuario = function(){
    if($scope.confirmarcambio == true){
      nuevopassword = $scope.Formuser.password1;
    }else{
      nuevopassword = passcambio;
      $scope.confirmarcambio = 0;
    }
    if($scope.useractivo == false){
      $scope.useractivo = 0;
    }
    $http.post("index.php?view=usuario&action=editarusuario", {
      'id':idedita,
      'fun_id':$scope.data.selectedOption.id,
      'nomuser':$scope.Formuser.nomusuario,
      'passw1':nuevopassword,
      'rol_id':$scope.Formuser.tipouser,
      'activo':$scope.useractivo,
      'editapassword':$scope.confirmarcambio
    }).then(function(response){
        $toastContent = response.data;
        if(response.data[0]==1){
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.success('Ok','Edición Correcta!');
            $('#formUsuario').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }else{
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.error('Error',$toastContent);
            $('#formUsuario').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }
    });
  }
  $scope.configPages = function() {
        $scope.regAlluser();
        alert(datosuser.length);
        $scope.pages.length = 0;
        var ini = $scope.currentPage - 4;
        var fin = $scope.currentPage + 5;

        if (ini < 1) {
            ini = 1;
            if (Math.ceil($scope.datosuser.length / $scope.pageSize) > 10)
                fin = 10;
            else
                fin = Math.ceil($scope.datosuser.length / $scope.pageSize);
        }
        else {
            if (ini >= Math.ceil($scope.datosuser.length / $scope.pageSize) - 10) {
                ini = Math.ceil($scope.datosuser.length / $scope.pageSize) - 10;
                fin = Math.ceil($scope.datosuser.length / $scope.pageSize);
            }
        }
        if (ini < 1)
          ini = 1;
        for (var i = ini; i <= fin; i++) {
            $scope.pages.push({no: i});
        }

        if ($scope.currentPage >= $scope.pages.length)
            $scope.currentPage = $scope.pages.length - 1;

  };
  $scope.setPage = function(page) {
    $scope.currentPage = page - 1;
  };
})
app.filter('startFromGrid', function() {
    return function(input, start) {
        start =+ start;
        return input.slice(start);
    }
});