app.controller('ctrlcargos', function($scope, $http, $notify, $filter) {

   $('.button-collapse').sideNav('hide');
   $('.collapsible').collapsible('close',0);
   var idelimina = '';
   var idedita = '';
   var cambioedita = '';
   $scope.cargonuevo=true;
   $scope.cargoedita=true;
   $scope.activaedicion=true;
   $scope.listaexiste=true;
   $scope.listanoexiste=true;
   $scope.currentPage = 0;
   $scope.pageSize = 5;
   $scope.pages = [];
      
   $scope.openformulario = function(){
  	  $scope.cargonuevo=false;
      $scope.cargoedita=true;
      $scope.limpiarForm();
      $("#titulo-modal").text("Registrar Nuevo Cargo");
   	  $('#btnModificar').hide();
      $('#btnCrear').show();
      $scope.listarareas();
      $('#formCargo').modal('open');
      $('#cargo').focus();
      $('#cargo').addClass('invalid');    
  }
  $scope.limpiarForm = function(){
      $scope.Formcargos.nomcargo = "";
      $scope.Formcargos.nomarea = "";
      $('#cargo').removeClass('invalid');
  }
  $scope.listarareas = function(){
      $scope.datosarea=[];
      $http.post("index.php?view=area&action=listarareas")
      .then(function (response) {
        if(response.data != 0){
          $scope.datosarea = response.data;
        }
      });
  }
  $scope.createnuevoreg = function(){
  	$scope.nomcargo = $scope.Formcargos.nomcargo;
  	$scope.nomarea = $scope.Formcargos.nomarea;

    $http.post("index.php?view=cargo&action=crearnuevocargo",{
                  "nomcargo":$scope.nomcargo,
                  "nomarea":$scope.Formcargos.nomarea  
                })
                .then(function (response){
                if(response.data[0]==1){
                      $notify.setTime(3).showCloseButton(false).showProgressBar(true);
                      $notify.setPosition('bottom-left');
                      $notify.success('Ok','Registro Correcto!');
                      $('#formCargo').modal('close');
                      $scope.listanoexiste=true;
                      $scope.setPage(1);
                      $scope.configPages();
                }else{
                      $notify.setTime(3).showCloseButton(false).showProgressBar(true);
                      $notify.setPosition('bottom-left');
                      $notify.error('Error','Nose se Registro!');
                      $('#formCargo').modal('close');
                      $scope.listanoexiste=true;  
                }
                });

  }
  $scope.abrirformelimina = function(id){
    idelimina = id;
    $('#formeliminar').modal('open');
  }
  $scope.eliminarcargo = function(){
    $http.post("index.php?view=cargo&action=eliminarcargo" ,{
      'id': idelimina
      }).then(function(response){
          if(response.data[0]==1){
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.success('Ok','Eliminacion Correcta!');
            $('#formeliminar').modal('close');
            $scope.listaexiste=true;
            $scope.setPage(1);
            $scope.configPages();
          }else{
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.error('Error','No se Elimino!');
            $('#formeliminar').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }
      });
  }

  $scope.abrirformeditacargo = function(id){
    $scope.cargonuevo=true;
    $scope.cargoedita=false;
    $scope.confirmed=false;
    $("#titulo-modal").text("Editar Cargo");
    $('#btnCrear').hide();
    $('#btnModificar').show();
    $scope.limpiarForm();
    $scope.datouncargo=[];
    $http.post("index.php?view=cargo&action=buscaruncargo", {
      "id" : id
    }).then(function(response){
      $scope.datouncargo = response.data;
      //alert(response.statusText);
      idedita = $scope.datouncargo[0].id;
      $scope.Formcargos.nomcargo = $scope.datouncargo[0].nomcargo;
      $scope.dato1 = $scope.datouncargo[0].area_id;
      $scope.dato2 = $scope.datouncargo[0].descripcion;
      /*inicio para cargar un valor por defecto*/
      $scope.data = {
      availableOptions: [
       {id: $scope.dato1, descripcion: $scope.dato2}
      ],
      selectedOption: {id: $scope.dato1, descripcion: $scope.dato2} //This sets the default value of the select in the ui
      };
      /*fin para cargar un valor por defecto*/
      $scope.listarareas();
      $('#formCargo').modal('open');
    });
  }
  $scope.activa = function(){
    //alert($scope.confirmed);
    $scope.cargonuevo=false;
    $scope.cargoedita=true;
    //alert($scope.Formcargos.nomarea);
  }
  $scope.editarCargo = function(){
    //alert(idedita); Áqui almaceno el id de un registro de cargo
    //alert($scope.data.selectedOption.id); aqui almaceno el id del area que corresponde al cargo
    if($scope.confirmed == true){
      //alert("activo edicion");
      cambioedita = $scope.Formcargos.nomarea;
    }else{
      //alert("no activo edicion");
      cambioedita = $scope.data.selectedOption.id;
    }
    $http.post("index.php?view=cargo&action=editarcargo", {
      "id" : idedita,
      "nomcargo": $scope.Formcargos.nomcargo,
      "area_id": cambioedita
    }).then(function(response){
        if(response.data[0]==1){
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.success('Ok','Edición Correcta!');
            $('#formCargo').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }else{
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.error('Error','No se Edito!');
            $('#formCargo').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }
    });
  }
  $scope.regAllcargo = function(){
      $scope.datoscargo=[];
      $http.post("index.php?view=cargo&action=listarcargos")
      .then(function (response) {
        if(response.data != 0){
          $scope.datoscargo = response.data;
          $scope.listaexiste=false;
        }else{
          $scope.listanoexiste=false;
        }
      });
  }
  $scope.configPages = function() {

        $scope.regAllcargo();
        alert(datoscargo.length);
        $scope.pages.length = 0;
        var ini = $scope.currentPage - 4;
        var fin = $scope.currentPage + 5;
      
        if (ini < 1) {
            ini = 1;
            if (Math.ceil($scope.datoscargo.length / $scope.pageSize) > 10)
                fin = 10;
            else
                fin = Math.ceil($scope.datoscargo.length / $scope.pageSize);
        }
        else {
            if (ini >= Math.ceil($scope.datoscargo.length / $scope.pageSize) - 10) {
                ini = Math.ceil($scope.datoscargo.length / $scope.pageSize) - 10;
                fin = Math.ceil($scope.datoscargo.length / $scope.pageSize);
            }
        }
        if (ini < 1) 
          ini = 1;
        for (var i = ini; i <= fin; i++) {
            $scope.pages.push({no: i});
        }

        if ($scope.currentPage >= $scope.pages.length)
            $scope.currentPage = $scope.pages.length - 1;

  };
  $scope.setPage = function(page) {
        $scope.currentPage = page - 1;

  };

})
app.filter('startFromGrid', function() {
    return function(input, start) {
        start =+ start;
        return input.slice(start);
    }
});