app.controller('ctrllogs', function ($scope, $http, $notify, $filter) {
  $('.button-collapse').sideNav('hide');
  $('.collapsible').collapsible('close', 0);
  $scope.listaexiste = true;
  $scope.listanoexiste = true;
  $scope.funnoedita = true;
  $scope.funedita = true;
  $scope.checkeditapass = true;
  $scope.editacontra = true;
  $scope.currentPage = 0;
  $scope.pageSize = 5;
  $scope.pages = [];
  $scope.tipos = {
    'UPDATE': 'ACTUALIZACIÓN DE REGISTRO',
    'INSERT': 'NUEVO REGISTRO',
    'DELETE': 'ELIMINACIÓN DEL REGISTRO'
  };
  $scope.tiposB = {
    'UPDATE': 'ACTUALIZADO',
    'INSERT': 'REGISTRADO',
    'DELETE': 'ELIMINADO'
  };

  $scope.formatDate = function (date) {
    if (date.split('.').length) {
      return date.split('.')[0];
    }
    return date;
  };

  $scope.getID = function (data) {
    if (data.split('|').length > 1) {
      data = data.split('|')[1];
      if (data.indexOf('id=') !== -1) {
        return data.toUpperCase();
      } else {
        return '';
      }
    }
    return '';
  };

  $scope.regAlluser = function () {
    $scope.datoslogs = [];
    $http.post('index.php?view=logs&action=listarlogs')
      .then(function (response) {
        if (response.data != 0) {
          $scope.datoslogs = response.data;
          $scope.listaexiste = false;
        } else {
          $scope.listanoexiste = false;
        }
      });
  };
  $scope.buscarfunuser = function () {
    $scope.listatodos = [];
    $scope.sololistaselect = [];
    $scope.res = [];
    $http.post('index.php?view=logs&action=listatodosci')
      .then(function (response) {
        $scope.listatodos = response.data;
        $http.post('index.php?view=logs&action=buscarcifun')
          .then(function (response) {
            $scope.sololistaselect = response.data;
            /* En esta parte comparo los dos array obtenidos para solo mostrar los funcionarios q no tienen registrado su usuario */
            $scope.res = $scope.listatodos.filter(obj => {
              const exists = $scope.sololistaselect.some(obj2 => (obj2.funcionario_id === obj.id
              ));
              if (!exists) { return obj; }
            });/* fin de comparacion de array */
          });
      });
  };

  $scope.configPages = function () {
    $scope.regAlluser();
    // alert(datoslogs.length);
    $scope.pages.length = 0;
    var ini = $scope.currentPage - 4;
    var fin = $scope.currentPage + 5;

    if (ini < 1) {
      ini = 1;
      if (Math.ceil($scope.datoslogs.length / $scope.pageSize) > 10) {
        fin = 10;
      } else {
        fin = Math.ceil($scope.datoslogs.length / $scope.pageSize);
      }
    } else {
      if (ini >= Math.ceil($scope.datoslogs.length / $scope.pageSize) - 10) {
        ini = Math.ceil($scope.datoslogs.length / $scope.pageSize) - 10;
        fin = Math.ceil($scope.datoslogs.length / $scope.pageSize);
      }
    }
    if (ini < 1) {
      ini = 1;
    }
    for (var i = ini; i <= fin; i++) {
      $scope.pages.push({ no: i });
    }

    if ($scope.currentPage >= $scope.pages.length) {
      $scope.currentPage = $scope.pages.length - 1;
    }
  };
  $scope.setPage = function (page) {
    $scope.currentPage = page - 1;
  };
});

app.filter('startFromGrid', function () {
  return function (input, start) {
    start = +start;
    return input.slice(start);
  };
});
