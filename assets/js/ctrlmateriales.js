app.controller('ctrlmateriales', function($scope, $http, $notify, $filter) {
	  $('.button-collapse').sideNav('hide');
   	$('.collapsible').collapsible('close',0);
   	$scope.listaexiste=true;
    $scope.listanoexiste=true;
   	$scope.aleditarmaterial=true;
   	var idelimina = '';
    var idedita = '';
    var cambioedita = '';
    $scope.materialactivo=true;
    $scope.tipoedita = true;
    $scope.tiponoedita = true;
    $scope.activaedita = true;
   	$scope.currentPage = 0;
   	$scope.pageSize = 5;
   	$scope.pages = [];

   	$scope.openformulario = function(){
        $scope.materialactivo=true;
        $scope.aleditarmaterial=false;
        $scope.tipoedita = true;
        $scope.tiponoedita = false;
        $scope.activaedita = true;
  		  $scope.limpiarForm();
      	$("#titulo-modal").text("Registrar Nuevo Material");
   	  	$('#btnModificar').hide();
      	$('#btnCrear').show();
        $scope.listarcates();
      	$('#formMaterial').modal('open');   
  	}
    $scope.listarcates = function(){
      $scope.datoscat=[];
      $http.post("index.php?view=categoria&action=listarcategorias")
        .then(function (response) {
          $scope.datoscat = response.data;
      });
    }
  	$scope.limpiarForm = function(){
  		$scope.Formmate.matcod = "";
  		$scope.Formmate.mattipo = "";
      $scope.Formmate.nommat="";
      $scope.Formmate.desmat="";
      $scope.Formmate.minmat="";
      /*$scope.Formmate.invinicial="";*/
      $scope.Formmate.presmat="";
      $scope.Formmate.matactivo="";
  	}
  	$scope.crearmaterial = function(){
      $scope.Formmate.matactivo = true;
  		$http.post("index.php?view=material&action=crearmaterial",{
                  "matcod":$scope.Formmate.matcod,
                  "mattipo":$scope.Formmate.mattipo,
                  "nommat":$scope.Formmate.nommat,
                  "desmat":$scope.Formmate.desmat,
                  "minmat":$scope.Formmate.minmat,
                  /*"invinicial": $scope.Formmate.invinicial,*/
                  "presmat": $scope.Formmate.presmat,
                  "matactivo": $scope.Formmate.matactivo
        })
        .then(function (response){
            if(response.data[0]==1){
                $notify.setTime(3).showCloseButton(false).showProgressBar(true);
                $notify.setPosition('bottom-left');
                $notify.success('Ok','Registro Correcto!');
                $('#formMaterial').modal('close');
                $scope.listanoexiste=true;
                $scope.setPage(1);
                $scope.configPages();
            }else{
                $notify.setTime(3).showCloseButton(false).showProgressBar(true);
                $notify.setPosition('bottom-left');
                $notify.error('Error','Nose se Registro!');
                $('#formMaterial').modal('close');
                $scope.listanoexiste=true;  
            }
        });	
  	}
  	$scope.regAllmate = function(){
  		$scope.datosmat=[];
  		$http.post("index.php?view=material&action=listarmaterial")
      	.then(function (response) {
        if(response.data != 0){
          $scope.datosmat = response.data;
          $scope.listaexiste=false;
        }else{
          $scope.listanoexiste=false;
        }
      });
  	}
  	$scope.abrirformelimina = function(id){
    idelimina = id;
    $('#formeliminar').modal('open');
  	}
  	$scope.eliminarmaterial = function(){
    $http.post("index.php?view=material&action=eliminarmaterial" ,{
      'id': idelimina
      }).then(function(response){
          if(response.data[0]==1){
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.success('Ok','Eliminacion Correcta!');
            $('#formeliminar').modal('close');
            $scope.listaexiste=true;
            $scope.setPage(1);
            $scope.configPages();
          }else{
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.error('Error','No se Elimino!');
            $('#formeliminar').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }
      });
  	}
  	$scope.abrirformeditamate = function(id){
      $scope.materialactivo=false;
      $scope.aleditarmaterial=true;
      $scope.tipoedita = false;
      $scope.tiponoedita = true;
      $scope.activaedita = false;
      $scope.confirmarcambio = "";
    	$("#titulo-modal").text("Editar Material");
    	$('#btnCrear').hide();
    	$('#btnModificar').show();
    	$scope.limpiarForm();
    	$scope.datounmate=[];
    	$http.post("index.php?view=material&action=buscarunmate", {
      	"id" : id
    	}).then(function(response){
      	$scope.datounmate = response.data;
      	idedita = $scope.datounmate[0].id;
      	$scope.Formmate.matcod = $scope.datounmate[0].codigo;
      	$scope.Formmate.nommat = $scope.datounmate[0].nom_material;
        $scope.Formmate.desmat = $scope.datounmate[0].descripcion;
        $scope.Formmate.minmat = $scope.datounmate[0].min_inventario;
        $scope.Formmate.invinicial = $scope.datounmate[0].init_inventario;
        if($scope.datounmate[0].activo == 1){
          $scope.Formmate.matactivo = true;
        }
        $scope.dato1 = $scope.datounmate[0].categoria_id;
        $scope.dato2 = $scope.datounmate[0].tipo;
        $scope.dato3 = $scope.datounmate[0].nombre_cat;
        $scope.data = {
        availableOptions: [
        {id: $scope.dato1, tipo: $scope.dato2, nombre_cat: $scope.dato3}
        ],
        selectedOption: {id: $scope.dato1, tipo: $scope.dato2, nombre_cat: $scope.dato3} //This sets the default value of the select in the ui
        };
        $scope.Formmate.presmat = $scope.datounmate[0].presentacion;
        $scope.listarcates();
      	$('#formMaterial').modal('open');
    	});
  	}
    $scope.activa = function(){
    //alert($scope.confirmed);
      if($scope.confirmarcambio == true){
        $scope.tiponoedita = false;
        $scope.tipoedita = true;
      }else{
        $scope.tiponoedita = true;
        $scope.tipoedita = false;
      }
    }
  	$scope.editarMate = function(){
      
    if($scope.confirmarcambio == true){
      cambioedita = $scope.Formmate.mattipo;
    }else{
      cambioedita = $scope.data.selectedOption.id;
    }
    if($scope.Formmate.matactivo == false){
      $scope.Formmate.matactivo = 0;
    }
    $http.post("index.php?view=material&action=editarmate", {
      "id" : idedita,
      "matcod":$scope.Formmate.matcod,
      "nommat":$scope.Formmate.nommat,
      "desmat":$scope.Formmate.desmat,
      "minmat":$scope.Formmate.minmat,
      "invinicial": $scope.Formmate.invinicial,
      "presmat": $scope.Formmate.presmat,
      "matactivo": $scope.Formmate.matactivo,
      "mattipo": cambioedita
    }).then(function(response){
        if(response.data[0]==1){
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.success('Ok','Edición Correcta!');
            $('#formMaterial').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }else{
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.error('Error','No se Edito!');
            $('#formMaterial').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }
    });
  	}
  	$scope.configPages = function() {
        $scope.regAllmate();
        alert(datosmat.length);
        $scope.pages.length = 0;
        var ini = $scope.currentPage - 4;
        var fin = $scope.currentPage + 5;
      
        if (ini < 1) {
            ini = 1;
            if (Math.ceil($scope.datosmat.length / $scope.pageSize) > 10)
                fin = 10;
            else
                fin = Math.ceil($scope.datosmat.length / $scope.pageSize);
        }
        else {
            if (ini >= Math.ceil($scope.datosmat.length / $scope.pageSize) - 10) {
                ini = Math.ceil($scope.datosmat.length / $scope.pageSize) - 10;
                fin = Math.ceil($scope.datosmat.length / $scope.pageSize);
            }
        }
        if (ini < 1) 
          ini = 1;
        for (var i = ini; i <= fin; i++) {
            $scope.pages.push({no: i});
        }

        if ($scope.currentPage >= $scope.pages.length)
            $scope.currentPage = $scope.pages.length - 1;

  	};
  	$scope.setPage = function(page) {
        $scope.currentPage = page - 1;
  	};
})
app.filter('startFromGrid', function() {
    return function(input, start) {
        start =+ start;
        return input.slice(start);
    }
});