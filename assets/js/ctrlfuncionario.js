app.controller('ctrlfuncionario', function($scope, $http, $notify, $filter) {
  
   $('.button-collapse').sideNav('hide');
   $('.collapsible').collapsible('close',0);
   var idelimina = '';
   var idedita = '';
   var cambioedita = '';
   $scope.cargonuevo=true;
   $scope.cargoedita=true;
   $scope.activaedicion=true;
   $scope.listaexiste=true;
   $scope.listanoexiste=true;
   $scope.currentPage = 0;
   $scope.pageSize = 5;
   $scope.pages = [];

  $scope.openformulario = function(){
      $scope.cargonuevo=false;
      $scope.cargoedita=true;
      $scope.limpiarForm();
      $("#titulo-modal").text("Registrar Nuevo Funcionario");
      $('#btnModificar').hide();
      $('#btnCrear').show(); 		
   		$scope.consultacargo();
      $('#Formfuncionario').modal('open');

  }
  $scope.limpiarForm = function(){
      $scope.Formfunc.ci = "";
      $scope.Formfunc.expedido ="";
      $scope.Formfunc.nom = "";
      $scope.Formfunc.appaterno = "";
      $scope.Formfunc.apmaterno = "";
      $scope.Formfunc.direccion = "";
      $scope.Formfunc.correo = "";
      $scope.Formfunc.telefono = "";
      $scope.Formfunc.sexo = "";
  }
  $scope.regAllfun= function(){
      $scope.datosfun=[];
      $http.post("index.php?view=funcionario&action=listarfuncionario")
      .then(function (response) {
        if (response.data != 0) {
          $scope.datosfun = response.data;
          $scope.listaexiste=false;
          $scope.listanoexiste=true;
        }else{
          $scope.listanoexiste=false;
        }  
      });    
  }
  
  $scope.consultacargo = function(){  
    $http.post("index.php?view=funcionario&action=listarcargo")
      .then(function (response) {$scope.cargo = response.data;});
  }
  $scope.createnuevoreg = function(){
    $http.post("index.php?view=funcionario&action=registrarfuncionario",{
      "ci":$scope.Formfunc.ci,
      "exp":$scope.Formfunc.expedido,
      "nombres":$scope.Formfunc.nom,
      "appaterno":$scope.Formfunc.appaterno,
      "apmaterno":$scope.Formfunc.apmaterno,
      "selectcargo":$scope.Formfunc.selectcargo,
      "direccion":$scope.Formfunc.direccion,
      "telefono":$scope.Formfunc.telefono,
      "correo":$scope.Formfunc.correo,
      "sexo":$scope.Formfunc.sexo             
    })
    .then(function (response) {
    if(response.data[0]==1){
          $notify.setTime(3).showCloseButton(false).showProgressBar(true);
          $notify.setPosition('bottom-left');
          $notify.success('Ok:','Registro Correcto!');
          $('#Formfuncionario').modal('close');
          $scope.setPage(1);
          $scope.configPages();
    }else{
          $notify.setTime(3).showCloseButton(false).showProgressBar(true);
          $notify.setPosition('bottom-left');
          $notify.error('Error:','Nose se Registro!');
          $('#Formfuncionario').modal('close');
          $scope.setPage(1);
          $scope.configPages();
    }
    }); 
  }
  $scope.abrirformelimina = function(id){
    idelimina = id;
    $('#formeliminar').modal('open');
  }
  $scope.eliminarfuncionario = function(){
    $http.post("index.php?view=funcionario&action=eliminarfuncionario" ,{
      'id': idelimina
      }).then(function(response){
          if(response.data[0]==1){
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.success('Ok','Eliminacion Correcta!');
            $('#formeliminar').modal('close');
            $scope.listaexiste=true;
            $scope.setPage(1);
            $scope.configPages();
          }else{
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.error('Error','No se Elimino!');
            $('#formeliminar').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }
      });
  }
  $scope.abrirformeditafun = function(id){
    $scope.cargonuevo=true;
    $scope.cargoedita=false;
    $scope.confirmed=false;
    $("#titulo-modal").text("Editar Funcionario");
    $('#btnCrear').hide();
    $('#btnModificar').show();
    $scope.limpiarForm();
    $scope.datounfun=[];
    $http.post("index.php?view=funcionario&action=buscarunfuncionario", {
      "id" : id
    }).then(function(response){
      $scope.datounfun = response.data;
      idedita = $scope.datounfun[0].id;
      $scope.Formfunc.ci = $scope.datounfun[0].ci;
      $scope.Formfunc.expedido = $scope.datounfun[0].exp;
      $scope.Formfunc.nom = $scope.datounfun[0].nombres;
      $scope.Formfunc.appaterno = $scope.datounfun[0].appaterno;
      $scope.Formfunc.apmaterno = $scope.datounfun[0].apmaterno;
      $scope.Formfunc.direccion = $scope.datounfun[0].direccion;
      $scope.Formfunc.correo = $scope.datounfun[0].correo;
      $scope.Formfunc.telefono = $scope.datounfun[0].telefono;
      $scope.Formfunc.sexo = $scope.datounfun[0].sexo;
      $scope.dato1 = $scope.datounfun[0].cargo_id;
      $scope.dato2 = $scope.datounfun[0].nomcargo;
      /*inicio para cargar un valor por defecto*/
      $scope.data = {
      availableOptions: [
       {id: $scope.dato1, nomcargo: $scope.dato2}
      ],
      selectedOption: {id: $scope.dato1, nomcargo: $scope.dato2} //This sets the default value of the select in the ui
      };
      /*fin para cargar un valor por defecto*/
      $scope.consultacargo();
      $('#Formfuncionario').modal('open');
    });
  }
  $scope.activa = function(){
    //alert($scope.confirmed);
    $scope.cargonuevo=false;
    $scope.cargoedita=true;
    //alert($scope.Formcargos.nomarea);
  }
  $scope.editarFuncionario = function(){
    //alert(idedita); Áqui almaceno el id de un registro de cargo
    //alert($scope.data.selectedOption.id); aqui almaceno el id del area que corresponde al cargo
    if($scope.confirmed == true){
      //alert("activo edicion");
      cambioedita = $scope.Formfunc.selectcargo;
    }else{
      //alert("no activo edicion");
      cambioedita = $scope.data.selectedOption.id;
    }
    $http.post("index.php?view=funcionario&action=editarfuncionario", {
      "id" : idedita,
      "ci" : $scope.Formfunc.ci,
      "exp": $scope.Formfunc.expedido,
      "nombres":$scope.Formfunc.nom,
      "appaterno":$scope.Formfunc.appaterno,
      "apmaterno":$scope.Formfunc.apmaterno,
      "direccion":$scope.Formfunc.direccion,
      "telefono":$scope.Formfunc.telefono,
      "correo":$scope.Formfunc.correo,
      "sexo":$scope.Formfunc.sexo,
      "cargo_id": cambioedita
    }).then(function(response){
        if(response.data[0]==1){
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.success('Ok','Edición Correcta!');
            $('#Formfuncionario').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }else{
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.error('Error','No se Edito!');
            $('#Formfuncionario').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }
    });
  }
  $scope.configPages = function() {

        $scope.regAllfun();
        alert(datosfun.length);
        $scope.pages.length = 0;
        var ini = $scope.currentPage - 4;
        var fin = $scope.currentPage + 5;
      
        if (ini < 1) {
            ini = 1;
            if (Math.ceil($scope.datosfun.length / $scope.pageSize) > 10)
                fin = 10;
            else
                fin = Math.ceil($scope.datosfun.length / $scope.pageSize);
        }
        else {
            if (ini >= Math.ceil($scope.datosfun.length / $scope.pageSize) - 10) {
                ini = Math.ceil($scope.datosfun.length / $scope.pageSize) - 10;
                fin = Math.ceil($scope.datosfun.length / $scope.pageSize);
            }
        }
        if (ini < 1) 
          ini = 1;
        for (var i = ini; i <= fin; i++) {
            $scope.pages.push({no: i});
        }

        if ($scope.currentPage >= $scope.pages.length)
            $scope.currentPage = $scope.pages.length - 1;

  };
  $scope.setPage = function(page) {
        $scope.currentPage = page - 1;

  };

})
app.filter('startFromGrid', function() {
    return function(input, start) {
        start =+ start;
        return input.slice(start);
    }
});