app.controller('ctrlproveedores', function($scope, $http, $notify, $filter) {
  
    $('.button-collapse').sideNav('hide');
    $('.collapsible').collapsible('close',0);
    $scope.listaexiste=true;
    $scope.listanoexiste=true;
    var idedita = '';
    var idelimina = '';
    $scope.currentPage = 0;
    $scope.pageSize = 5;
    $scope.pages = [];

  $scope.openformulario = function(){
  	$scope.limpiarform();
    $("#titulo-modal").text("Registrar Nuevo Proveedor");
   	$('#btnModificar').hide();
    $('#btnCrear').show();
    $('#formProveedor').modal('open');   
  }
  $scope.limpiarform = function(){
    $scope.Formprov.nitprov="";
    $scope.Formprov.emp="";
    $scope.Formprov.nom ="";
    $scope.Formprov.approv ="";
    $scope.Formprov.dir ="";
    $scope.Formprov.fono ="";
    $scope.Formprov.email ="";
  }
  $scope.regAllprov = function(){
      $scope.datosprov=[];
      $http.post("index.php?view=proveedor&action=listarproveedor")
      .then(function (response){
        if(response.data != 0){
          $scope.datosprov = response.data;
          $scope.listaexiste=false;
        }else{
          $scope.listanoexiste=false;
        }
      });
  }
  $scope.crearproveedor = function(){ 
    $http.post("index.php?view=proveedor&action=crearproveedor",{
      'nit':$scope.Formprov.nitprov,
      'emp':$scope.Formprov.emp,
      'nom':$scope.Formprov.nom,
      'ap':$scope.Formprov.approv,
      'dir':$scope.Formprov.dir,
      'fono':$scope.Formprov.fono,
      'email':$scope.Formprov.email
    })
    .then(function (response){
      if(response.data[0]==1){
                $notify.setTime(3).showCloseButton(false).showProgressBar(true);
                $notify.setPosition('bottom-left');
                $notify.success('Ok','Registro Correcto!');
                $('#formProveedor').modal('close');
                $scope.listanoexiste=true;
                $scope.setPage(1);
                $scope.configPages();
            }else{
                $notify.setTime(3).showCloseButton(false).showProgressBar(true);
                $notify.setPosition('bottom-left');
                $notify.error('Error','No se Registro!');
                $('#formProveedor').modal('close');
                $scope.listanoexiste=true;  
            }
    });
  }
  $scope.abrirformeditaprov = function(id){
    $("#titulo-modal").text("Editar Proveedor");
    $('#btnCrear').hide();
    $('#btnModificar').show();
    $scope.limpiarform();
    $scope.datounproveedor=[];
    $http.post("index.php?view=proveedor&action=buscarunproveedor", {
        "id" : id
    }).then(function(response){
        $scope.datounproveedor = response.data;
        idedita = $scope.datounproveedor[0].id;
        $scope.Formprov.nitprov = $scope.datounproveedor[0].Nit;
        $scope.Formprov.emp = $scope.datounproveedor[0].empresa;
        $scope.Formprov.nom = $scope.datounproveedor[0].nombres;
        $scope.Formprov.approv = $scope.datounproveedor[0].apellidos;
        $scope.Formprov.dir = $scope.datounproveedor[0].direccion;
        $scope.Formprov.fono = $scope.datounproveedor[0].telefono;
        $scope.Formprov.email = $scope.datounproveedor[0].correo;
        $('#formProveedor').modal('open');
    });
  }
  $scope.editarProveedor = function(){
    $http.post("index.php?view=proveedor&action=editarproveedor", {
      'id':idedita,
      'nit':$scope.Formprov.nitprov,
      'emp':$scope.Formprov.emp,
      'nom':$scope.Formprov.nom,
      'ap':$scope.Formprov.approv,
      'dir':$scope.Formprov.dir,
      'fono':$scope.Formprov.fono,
      'email':$scope.Formprov.email
    }).then(function(response){
        if(response.data[0]==1){
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.success('Ok','Edición Correcta!');
            $('#formProveedor').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }else{
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.error('Error','No se Edito!');
            $('#formProveedor').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }
    });
  }
  $scope.abrirformelimina = function(id){
    idelimina = id;
    $('#formeliminar').modal('open');
  }
  $scope.eliminarproveedor = function(){
    $http.post("index.php?view=proveedor&action=eliminarproveedor",{
      'id': idelimina
      }).then(function(response){
          if(response.data[0]==1){
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.success('Ok','Eliminacion Correcta!');
            $('#formeliminar').modal('close');
            $scope.listaexiste=true;
            $scope.setPage(1);
            $scope.configPages();
          }else{
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.error('Error','No se Elimino!');
            $('#formeliminar').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }
      });
  }
  $scope.configPages = function() {
        $scope.regAllprov();
        alert(datosprov.length);
        $scope.pages.length = 0;
        var ini = $scope.currentPage - 4;
        var fin = $scope.currentPage + 5;
      
        if (ini < 1) {
            ini = 1;
            if (Math.ceil($scope.datosprov.length / $scope.pageSize) > 10)
                fin = 10;
            else
                fin = Math.ceil($scope.datosprov.length / $scope.pageSize);
        }
        else {
            if (ini >= Math.ceil($scope.datosprov.length / $scope.pageSize) - 10) {
                ini = Math.ceil($scope.datosprov.length / $scope.pageSize) - 10;
                fin = Math.ceil($scope.datosprov.length / $scope.pageSize);
            }
        }
        if (ini < 1) 
          ini = 1;
        for (var i = ini; i <= fin; i++) {
            $scope.pages.push({no: i});
        }

        if ($scope.currentPage >= $scope.pages.length)
            $scope.currentPage = $scope.pages.length - 1;

  };
  $scope.setPage = function(page) {
    $scope.currentPage = page - 1;
  };
})
app.filter('startFromGrid', function() {
    return function(input, start) {
        start =+ start;
        return input.slice(start);
    }
});