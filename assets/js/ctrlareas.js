app.controller('ctrlareas', function($scope, $http, $notify, $filter) {
  
   $('.button-collapse').sideNav('hide');
   $('.collapsible').collapsible('close',0);
   $scope.listaexiste=true;
   $scope.listanoexiste=true;
   //$scope.limpiarForm();
   var idelimina = '';
   $scope.currentPage = 0;
   $scope.pageSize = 5;

   $scope.pages = [];
   
  $scope.openformulario = function(){
  		$scope.limpiarForm();
      $("#titulo-modal").text("Registrar Nueva  Área");
   		$('#btnModificar').hide();
      $('#btnCrear').show();
      $('#formArea').modal('open');
      $('#area').focus();
      $('#area').addClass('invalid');    
  }
  $scope.limpiarForm = function(){
      $scope.Formareas.nomarea = "";
      $('#area').removeClass('invalid');
  }
  $scope.createnuevoreg = function(){
    
    $http.post("index.php?view=area&action=crearnuevaarea",{
                  "nomarea":$scope.Formareas.nomarea  
                })
                .then(function (response){
                if(response.data[0]==1){
                      $notify.setTime(3).showCloseButton(false).showProgressBar(true);
                      $notify.setPosition('bottom-left');
                      $notify.success('Ok','Registro Correcto!');
                      $('#formArea').modal('close');
                      $scope.listanoexiste=true;
                      $scope.setPage(1);
                      $scope.configPages();
                }else{
                      $notify.setTime(3).showCloseButton(false).showProgressBar(true);
                      $notify.setPosition('bottom-left');
                      $notify.error('Error','Nose se Registro!');
                      $('#formArea').modal('close');
                      $scope.listanoexiste=true;  
                }
                });
    
  }
  $scope.regAllarea = function(){
      $scope.datosarea=[];
      $http.post("index.php?view=area&action=listarareas")
      .then(function (response) {
        if(response.data != 0){
          $scope.datosarea = response.data;
          $scope.listaexiste=false;
        }else{
          $scope.listanoexiste=false;
        }
      });
  }
  $scope.abrirformelimina = function(id){
    idelimina = id;
    $('#formeliminar').modal('open');
  }
  $scope.eliminararea = function(){
    $http.post("index.php?view=area&action=eliminararea" ,{
      'id': idelimina
      }).then(function(response){
          if(response.data[0]==1){
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.success('Ok','Eliminacion Correcta!');
            $('#formeliminar').modal('close');
            $scope.listaexiste=true;
            $scope.setPage(1);
            $scope.configPages();
          }else{
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.error('Error','No se Elimino!');
            $('#formeliminar').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }
      });
  }
  $scope.abrirformeditaarea = function(id){
    $("#titulo-modal").text("Editar Área");
    $('#btnCrear').hide();
    $('#btnModificar').show();
    $scope.limpiarForm();
    $scope.datounarea=[];
    $http.post("index.php?view=area&action=buscarunarea", {
      "id" : id
    }).then(function(response){
      $scope.datounarea = response.data;
      //alert(response.statusText);
      $scope.id = $scope.datounarea[0].id;
      $scope.Formareas.nomarea = $scope.datounarea[0].descripcion;
      $('#formArea').modal('open');
    });
  }
  $scope.activabusqueda = function(){
    $scope.currentPage = 0;
    $scope.page = 1;
  }
  $scope.editarArea = function(id){
    $http.post("index.php?view=area&action=editararea", {
      "id" : $scope.id,
      "descripcion":$scope.Formareas.nomarea 
    }).then(function(response){
        if(response.data[0]==1){
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.success('Ok','Edición Correcta!');
            $('#formArea').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }else{
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.error('Error','No se Edito!');
            $('#formArea').modal('close');
            $scope.setPage(1);
            $scope.configPages();
          }
    });
  }
  $scope.configPages = function() {
        $scope.regAllarea();
        alert(datosarea.length);/*no quitar me sirve para paginar mis registros*/
        $scope.pages.length = 0;
        var ini = $scope.currentPage - 4;
        var fin = $scope.currentPage + 5;
      
        if (ini < 1) {
            ini = 1;
            if (Math.ceil($scope.datosarea.length / $scope.pageSize) > 10)
                fin = 10;
            else
                fin = Math.ceil($scope.datosarea.length / $scope.pageSize);
        }
        else {
            if (ini >= Math.ceil($scope.datosarea.length / $scope.pageSize) - 10) {
                ini = Math.ceil($scope.datosarea.length / $scope.pageSize) - 10;
                fin = Math.ceil($scope.datosarea.length / $scope.pageSize);
            }
        }
        if (ini < 1) 
          ini = 1;
        for (var i = ini; i <= fin; i++) {
            $scope.pages.push({no: i});
        }

        if ($scope.currentPage >= $scope.pages.length)
            $scope.currentPage = $scope.pages.length - 1;

  };

  $scope.setPage = function(page) {
        $scope.currentPage = page - 1;
  };
  
})
app.filter('startFromGrid', function() {
    return function(input, start) {
        start =+ start;
        return input.slice(start);
    }
});