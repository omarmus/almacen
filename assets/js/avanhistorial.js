app.controller('avanhistorial', function($scope, $http) {
   $('.button-collapse').sideNav('hide');
   $('.collapsible').collapsible('close',0);
   $scope.listaexiste = true;
   $scope.listanoexiste = true;
   $scope.currentPage = 0;
   $scope.pageSize = 5;
   $scope.pages = [];
   $scope.listaver= false;
   $scope.listarhistorial = function(){
   		$scope.datoshist = [];
   		$http.post("index.php?view=avanhistorial&action=listarhistorial"
      	).then(function (response) {
          	if(response.data != 0){
          		$scope.datoshist = response.data;
          		$scope.listaexiste = false;
          		$scope.listanoexiste=true;
        	}else{
          		$scope.listanoexiste=false;
          		$scope.listaexiste = true;
        	}
      	});
   }
   $scope.detallependiente = function(){ 
        $scope.detalle = [];
        $scope.verificado=[];
      $http.post("index.php?view=admin&action=generarlistadetallada",{
        'id': idpedido,
        'aid': areaid
      })
      .then(function (response) {
        if (response.data != 0) {
          $scope.detalle = response.data;
          } 
          $location.path("/despendientes"); 
          //location.reload("/despendientes");      
      });
    }
    $scope.undetalle = function(id,aid){
      idpedido = id;
      areaid = aid;
      //alert(areaid);
      $scope.detallependiente();
      $scope.listaver= true;
    }
    $scope.ocultadetalle = function(){
      $scope.listaver= false;
    }
   $scope.configPages = function() {
   		$scope.datoshist = [];
   		$http.post("index.php?view=avanhistorial&action=listarhistorial"
      	).then(function (response) {
          	if(response.data != 0){
          		$scope.datoshist = response.data;
          		$scope.listaexiste = false;
          		$scope.listanoexiste=true;
        	}else{
          		$scope.listanoexiste=false;
          		$scope.listaexiste = true;
        	}
      	});
        //$scope.listarhistorial();
        //alert($scope.datoshist.length);
        $scope.pages.length = 0;
        var ini = $scope.currentPage - 4;
        var fin = $scope.currentPage + 5;
      
        if (ini < 1) {
            ini = 1;
            if (Math.ceil($scope.datoshist.length / $scope.pageSize) > 10)
                fin = 10;
            else
                fin = Math.ceil($scope.datoshist.length / $scope.pageSize);
        }
        else {
            if (ini >= Math.ceil($scope.datoshist.length / $scope.pageSize) - 10) {
                ini = Math.ceil($scope.datoshist.length / $scope.pageSize) - 10;
                fin = Math.ceil($scope.datoshist.length / $scope.pageSize);
            }
        }
        if (ini < 1) 
          ini = 1;
        for (var i = ini; i <= fin; i++) {
            $scope.pages.push({no: i});
        }
        if ($scope.currentPage >= $scope.pages.length)
            $scope.currentPage = $scope.pages.length - 1;
    };
    $scope.setPage = function(page) {
        $scope.currentPage = page - 1;
    };
});
app.filter('startFromGrid', function() {
    return function(input, start) {
        start =+ start;
        return input.slice(start);
    }
});