app.controller('ctrlsalidas', function($scope, $http, $notify) {
  
  $('.button-collapse').sideNav('hide');
  $('.collapsible').collapsible('close',0);
  $scope.currentPage = 0;
  $scope.pageSize = 5;
  $scope.pages = [];
  $scope.listarSalidas = function(){
      $scope.datossalidas=[];
      $http.post("index.php?view=salidas&action=listarSalidas")
        .then(function (response) {
            $scope.datossalidas = response.data;
      });
  }
  $scope.detalleunasalida = function(id){
    $scope.undetalle=[];
      $http.post("index.php?view=salidas&action=detalleunasalida",{'id':id})
        .then(function (response) {
            $scope.undetalle = response.data;
            $scope.mostrardetalle = true;
      });
  }
  $scope.ocultadetalle = function(){
      $scope.mostrardetalle= false;
  }
  $scope.imprimir = function(id){
    window.open('index.php?view=reportes&action=generarPDFentrega&id='+id);
  }
  $scope.configPages = function() {
        $scope.listarSalidas();
        alert(datossalidas.length);/*no quitar me sirve para paginar mis registros*/
        $scope.pages.length = 0;
        var ini = $scope.currentPage - 4;
        var fin = $scope.currentPage + 5;
      
        if (ini < 1) {
            ini = 1;
            if (Math.ceil($scope.datossalidas.length / $scope.pageSize) > 10)
                fin = 10;
            else
                fin = Math.ceil($scope.datossalidas.length / $scope.pageSize);
        }
        else {
            if (ini >= Math.ceil($scope.datossalidas.length / $scope.pageSize) - 10) {
                ini = Math.ceil($scope.datossalidas.length / $scope.pageSize) - 10;
                fin = Math.ceil($scope.datossalidas.length / $scope.pageSize);
            }
        }
        if (ini < 1) 
          ini = 1;
        for (var i = ini; i <= fin; i++) {
            $scope.pages.push({no: i});
        }
        if ($scope.currentPage >= $scope.pages.length)
            $scope.currentPage = $scope.pages.length - 1;
  };
  $scope.setPage = function(page) {
    $scope.currentPage = page - 1;
  };  	
})
app.filter('startFromGrid', function() {
    return function(input, start) {
        start =+ start;
        return input.slice(start);
    }
});
