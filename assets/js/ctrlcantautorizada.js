app.controller('ctrlcantautorizada', function($scope, $http, $notify, $filter) {
  
  $('.button-collapse').sideNav('hide');
  $('.collapsible').collapsible('close',0);
  $scope.mostrarasignacion = true;
  $scope.mostraritems =true;
  $scope.agregarca=[];
  $scope.aux1=[];
  var idelimina ='';
  var idedita ='';
  var cod_area ='';
  var cod_mat ='';
  var cod_aux='';
  $scope.currentPage = 0;
  $scope.pageSize = 5;
  $scope.pages = [];
  $scope.listarAreas = function(){
    $scope.datosarea=[];
      $http.post("index.php?view=area&action=listarareas")
      .then(function (response) {
        if(response.data != 0){
          $scope.datosarea = response.data;
        }
    });
  }
  $scope.nuevaasignacion = function(){
    $scope.listarAreas();
    $scope.agregarca=[];
    $scope.usuavanzado=false;
    $scope.FormCA.cantaut='';
  }
  $scope.activa = function(id){ 
    $scope.res = [];
    $scope.listatodos =[];
    $scope.sololistaselect=[];
    $http.post("index.php?view=material&action=listarmaterial")
    .then(function (response) {
      $scope.listatodos = response.data;
      $http.post("index.php?view=cantidadautorizada&action=buscarmatasig",{'id':id})
      .then(function (response) {
        $scope.sololistaselect = response.data;
        $scope.res = $scope.listatodos.filter(obj => {const exists = $scope.sololistaselect.some(obj2 => (obj2.material_id === obj.id
        ));
        if (!exists) { return obj;}
        });
        if ($scope.res != 0) {$scope.usuavanzado=true;}
      });
    });
  }
  $scope.asignarcantaut = function(){
    $scope.datounmate=[];
    $scope.datounarea=[];
    $scope.aux1 = $scope.agregarca.filter(c => c.material_id === $scope.FormCA.selectmatenew);
    if($scope.aux1 == 0){
      $http.post("index.php?view=material&action=buscarunmate", {
        "id" : $scope.FormCA.selectmatenew
      }).then(function(response){$scope.datounmate = response.data;
        $http.post("index.php?view=area&action=buscarunarea", {
        "id" : $scope.FormCA.selectareanew
        }).then(function(response){$scope.datounarea = response.data;
          $scope.agregarca.push({area_id:$scope.FormCA.selectareanew, nomarea: $scope.datounarea[0].descripcion, 
            codigo: $scope.datounmate[0].codigo,material_id: $scope.FormCA.selectmatenew, 
            nommate: $scope.datounmate[0].nom_material,desc: $scope.datounmate[0].descripcion,ca: $scope.FormCA.cantaut});
          $scope.mostraritems =false;
          //console.log($scope.agregarca);
        });
      });
      
    } 
  }
  $scope.bucarmatasignados = function(id){
    cod_aux = id;
    $scope.mostrarasignacion = true;
    if (id != null) {
      $scope.datoscanaut = [];
      $http.post("index.php?view=cantidadautorizada&action=listarcantaut",{
        'id':id
      }).then(function (response) {
        if(response.data != 0){
          $scope.datoscanaut = response.data;
          $scope.mostrarasignacion = false;
        }else{
          $scope.mostrarasignacion = true;
        }
        $scope.setPage(1);
      });
    }
  }
  $scope.eliminaritem = function(index){
    //$scope.aux2 = $scope.agregarca.lastIndexOf(index); //obtenemos el indice de un array
    $scope.agregarca.splice(index, 1);//borramos el elemnto de un array
    $scope.agregarca.sort();//reordeno el array despues de borrar
  }
  $scope.confirmarasigancion = function(){
    $scope.area_id = '';
    $scope.mat_id = '';
    $scope.cant_aut = '';
    $scope.usuavanzado=false;
    for (var i = 0; i <= $scope.agregarca.length-1; i++) {
      $scope.area_id = $scope.agregarca[i].area_id;
      $scope.mat_id = $scope.agregarca[i].material_id;
      $scope.cant_aut = $scope.agregarca[i].ca;
      $http.post("index.php?view=cantidadautorizada&action=crearcantidadautorizada",{
        'area_id':$scope.area_id,
        'mat_id': $scope.mat_id,
        'cant_aut': $scope.cant_aut
      }).then(function (response) {
        if(response.data[0]==1){
          $notify.setTime(3).showCloseButton(false).showProgressBar(true);
          $notify.setPosition('bottom-left');
          $notify.success('Ok','Registro Correcto!');
          $scope.bucarmatasignados(cod_aux);
          $scope.setPage(1);
          $scope.configPages();
        }else{
          $notify.setTime(3).showCloseButton(false).showProgressBar(true);
          $notify.setPosition('bottom-left');
          $notify.error('Error','Nose se Registro!');
          $scope.bucarmatasignados(cod_aux);
          $scope.setPage(1);
          $scope.configPages();
        }
      });
    }
    $scope.agregarca=[];
    $scope.FormCA.cantaut='';
    $scope.FormCA.selectareanew='';
    $scope.FormCA.selectmatenew='';
    $scope.mostraritems =true;
    $scope.setPage(1);
  }
  $scope.abrirformelimina = function(id){
    idelimina = id;
    $('#formeliminar').modal('open');
  }
  $scope.eliminarcant_aut = function(){
    $http.post("index.php?view=cantidadautorizada&action=eliminarca" ,{
      'id': idelimina
      }).then(function(response){
          if(response.data[0]==1){
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.success('Ok','Eliminacion Correcta!');
            $('#formeliminar').modal('close');
            $scope.bucarmatasignados(cod_aux);
            $scope.setPage(1);
            $scope.configPages();
          }else{
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.error('Error','No se Elimino!');
            $('#formeliminar').modal('close');
            $scope.bucarmatasignados(cod_aux);
            $scope.setPage(1);
            $scope.configPages();
          }
      });
  }
  $scope.abrirformeditacant = function(id){
    $("#titulo-modal").text("Editar C/A");
    $('#btnModificar').show();
    $scope.datounaca=[];
      $http.post("index.php?view=cantidadautorizada&action=buscarunaca", {
        "id" : id
      }).then(function(response){
        $scope.datounaca = response.data;
        idedita = $scope.datounaca[0].id;
        cod_area = $scope.datounaca[0].area_id;
        cod_mat = $scope.datounaca[0].material_id;
        $scope.materialca = $scope.datounaca[0].descripcion;
        $scope.codigomatca = $scope.datounaca[0].codigo;
        $scope.Formcantaut.nroca = $scope.datounaca[0].ca;
        $('#formCantidad').modal('open');
      });  
  }
  $scope.editarcant_aut = function(){
    $http.post("index.php?view=cantidadautorizada&action=editarca", {
      "id" : idedita,
      "cod_area":cod_area,
      "cod_mat":cod_mat,
      "ca":$scope.Formcantaut.nroca
    }).then(function(response){
        if(response.data[0]==1){
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.success('Ok','Edición Correcta!');
            $('#formCantidad').modal('close');
            
            $scope.bucarmatasignados(cod_aux);
            $scope.setPage(1);
            $scope.configPages();
          }else{
            $notify.setTime(3).showCloseButton(false).showProgressBar(true);
            $notify.setPosition('bottom-left');
            $notify.error('Error','No se Edito!');
            $('#formCantidad').modal('close');
            
            $scope.bucarmatasignados(cod_aux);
            $scope.setPage(1);
            $scope.configPages();
          }
    });
    
  }
  $scope.configPages = function() {
        $scope.bucarmatasignados();
        
        alert(datoscanaut.length);/*no quitar me sirve para paginar mis registros*/
        $scope.pages.length = 0;
        var ini = $scope.currentPage - 4;
        var fin = $scope.currentPage + 5;
      
        if (ini < 1) {
            ini = 1;
            if (Math.ceil($scope.datoscanaut.length / $scope.pageSize) > 10)
                fin = 10;
            else
                fin = Math.ceil($scope.datoscanaut.length / $scope.pageSize);
        }
        else {
            if (ini >= Math.ceil($scope.datoscanaut.length / $scope.pageSize) - 10) {
                ini = Math.ceil($scope.datoscanaut.length / $scope.pageSize) - 10;
                fin = Math.ceil($scope.datoscanaut.length / $scope.pageSize);
            }
        }
        if (ini < 1) 
          ini = 1;
        for (var i = ini; i <= fin; i++) {
            $scope.pages.push({no: i});
        }
        if ($scope.currentPage >= $scope.pages.length)
            $scope.currentPage = $scope.pages.length - 1;
  };
  $scope.setPage = function(page) {
        $scope.currentPage = page - 1;
  }; 
})
app.filter('startFromGrid', function() {
    return function(input, start) {
        start =+ start;
        return input.slice(start);
    }
});