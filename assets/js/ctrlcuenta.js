app.controller('ctrlcuenta', function ($scope, $http, $notify, $filter) {
  $scope.nombre = name_completo;
  $scope.id_usuario = id_usuario;

  $scope.Formuser = {
    oldPass: '',
    password1: '',
    password2: ''
  };

  $scope.cambiarContrasena = function () {
    $http.post('index.php?view=usuario&action=editarpass', {
      'id': $scope.id_usuario,
      'passw1': $scope.Formuser.password1
    }).then(function (response) {
      $notify.setTime(3).showCloseButton(false).showProgressBar(true);
      $notify.setPosition('bottom-left');
      if (parseInt(response.data[0]) === 1) {
        $notify.success('Ok', 'Edición Correcta!');
      } else {
        $notify.error('Error', response.data);
      }
    });
  };
});
