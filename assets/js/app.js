var app = angular.module('myApp', ['ui.materialize', 'ngRoute', 'ngNotify']);/* 'pdf' */

$( document ).ready(function () {
  $('#collapse-btn').click(function () {
    $('#body').toggleClass('active');
  });
});

app.config(['$routeProvider',
  function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/admin/bienvenida.php'
        // controler: 'ctrlpersonal'
      })
      .when('/areas', {
        templateUrl: 'views/admin/areas.php'
      })
      .when('/cargos', {
        templateUrl: 'views/admin/cargos.php'
      })
      .when('/funcionario', {
        templateUrl: 'views/admin/funcionario.php'
      })
      .when('/usuario', {
        templateUrl: 'views/admin/usuario.php'
      })
      .when('/logs', {
        templateUrl: 'views/admin/logs.php'
      })
      .when('/cuenta', {
        templateUrl: 'views/admin/cuenta.php'
      })
      .when('/proveedores', {
        templateUrl: 'views/admin/proveedores.php'
      })
      .when('/formularios', {
        templateUrl: 'views/admin/formularios.php'
      })
      .when('/regEntrada', {
        templateUrl: 'views/admin/regEntrada.php'
      })
      .when('/regSalida', {
        templateUrl: 'views/admin/regSalida.php'
      })
      .when('/categoria', {
        templateUrl: 'views/admin/categoria.php'
      })
      .when('/material', {
        templateUrl: 'views/admin/material.php'
      })
      .when('/cant_aut', {
        templateUrl: 'views/admin/cant_aut.php'
      })
      .when('/almacen', {
        templateUrl: 'views/admin/almacen.php'
      })
      .when('/avanzadomateriales', {
        templateUrl: 'views/avanzado/material.php'
      })
      .when('/avanzadosolicitar', {
        templateUrl: 'views/avanzado/solicitud.php'
      })
      .when('/avanzadosolicitudes', {
        templateUrl: 'views/avanzado/historial.php'
      })
      .when('/despendientes', {
        templateUrl: 'views/admin/pendiente.php'
      })
      .when('/salidas', {
        templateUrl: 'views/admin/salidas.php'
      })
      .when('/repGeneral', {
        templateUrl: 'views/admin/repGeneral.php'
      })
      .when('/repPersona', {
        templateUrl: 'views/admin/repPersona.php'
      })
      .when('/repMaterial', {
        templateUrl: 'views/admin/repMaterial.php'
      })
      .when('/repMaterialNo', {
        templateUrl: 'views/admin/repMaterialNo.php'
      })
      .when('/regSalidaFun', {
        templateUrl: 'views/admin/regSalidaFun.php'
      })
      .otherwise({
        redirectTo: '/'
      });
  }]);
