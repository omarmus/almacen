$(document).ready(function(){
      
    $(".button-collapse").sideNav();
    $('.modal').modal();
    $('.collapsible').collapsible();  
    $('ul.tabs').tabs();
    $('select').material_select();
    $('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrainWidth: false, // Does not change width of dropdown to that of the activator
      hover: true, // Activate on hover
      gutter: 2, // Spacing from edge
      belowOrigin: true, // Displays dropdown below the button
      alignment: 'left', // Displays dropdown with edge aligned to the left of button
      stopPropagation: true // Stops event propagation
    }
  	);

    $('.slider').slider();
         
});