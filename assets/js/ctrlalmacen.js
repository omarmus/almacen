app.controller('ctrlalmacen', function ($scope, $http) {
  $('.button-collapse').sideNav('hide');
  $('.collapsible').collapsible('close', 0);

  $scope.listaexiste = true;
  $scope.listanoexiste = true;
  $scope.currentPage = 0;
  $scope.pageSize = 5;
  $scope.pages = [];
  $scope.meses = [
    { value: '1', text: 'ENERO' },
    { value: '2', text: 'FEBRERO' },
    { value: '3', text: 'MARZO' },
    { value: '4', text: 'ABRIL' },
    { value: '5', text: 'MAYO' },
    { value: '6', text: 'JUNIO' },
    { value: '7', text: 'JULIO' },
    { value: '8', text: 'AGOSTO' },
    { value: '9', text: 'SEPTIEMBRE' },
    { value: '10', text: 'OCTUBRE' },
    { value: '11', text: 'NOVIEMBRE' },
    { value: '12', text: 'DICIEMBRE' }
  ];
  let gestion = new Date().getFullYear();
  let gestiones = [];
  for (let i = 0; i < 3; i++, gestion--) {
    gestiones.push({ value: gestion, text: gestion });
  }
  $scope.gestiones = gestiones;
  $scope.datosalmacen = [];

  $scope.fTipo = '-';
  $scope.fMes = '-';
  $scope.fGestion = '-';
  $scope.showMes = false;
  $scope.showGestion = false;

  $scope.id = null;

  $scope.changeMes = function () {
    console.log('MES', $scope.fMes);
    if ($scope.fMes !== '-') {
      getHistorial();
    }
  };
  $scope.changeGestion = function () {
    console.log('GESTION', $scope.fGestion);
    if ($scope.fGestion !== '-') {
      getHistorial();
    }
  };

  $scope.changeTipo = function (val) {
    console.log($scope.fTipo);
    if ($scope.fTipo === 'MES') {
      $scope.showMes = true;
      $scope.showGestion = false;
    } else if ($scope.fTipo === 'GESTION') {
      $scope.showMes = false;
      $scope.showGestion = true;
    } else {
      $scope.showMes = false;
      $scope.showGestion = false;
      getHistorial();
    }
  };

  $scope.regAllmate = function () {
    $scope.datosalmacen = [];
    $http.post('index.php?view=almacen&action=listarinventario')
      .then(function (response) {
        if (response.data != 0) {
          $scope.datosalmacen = response.data;
          $scope.listaexiste = false;
        } else {
          $scope.listanoexiste = false;
        }
      });
  };
  $scope.historial = function (id) {
    $scope.fTipo = '-';
    $scope.fMes = '-';
    $scope.fGestion = '-';
    $scope.showMes = false;
    $scope.showGestion = false;
    $('#formhistorial').modal('open');
    $('select').material_select();
    $scope.id = id;
    getHistorial();
  };

  function getHistorial () {
    $http.post('index.php?view=almacen&action=historial', {
      id: $scope.id,
      mes: $scope.fMes,
      gestion: $scope.fGestion,
      tipo: $scope.fTipo
    }).then(function (response) {
      $scope.entradas = response.data[0];
      $scope.dispon = response.data[1];
      $scope.salidas = response.data[2];
    });
  }

  $scope.configPages = function () {
    $scope.regAllmate();
    // alert(datosalmacen.length);
    $scope.pages.length = 0;
    var ini = $scope.currentPage - 4;
    var fin = $scope.currentPage + 5;

    if (ini < 1) {
      ini = 1;
      if (Math.ceil($scope.datosalmacen.length / $scope.pageSize) > 10) { fin = 10; } else { fin = Math.ceil($scope.datosalmacen.length / $scope.pageSize); }
    } else {
      if (ini >= Math.ceil($scope.datosalmacen.length / $scope.pageSize) - 10) {
        ini = Math.ceil($scope.datosalmacen.length / $scope.pageSize) - 10;
        fin = Math.ceil($scope.datosalmacen.length / $scope.pageSize);
      }
    }
    if (ini < 1) { ini = 1; }
    for (var i = ini; i <= fin; i++) {
      $scope.pages.push({ no: i });
    }

    if ($scope.currentPage >= $scope.pages.length) { $scope.currentPage = $scope.pages.length - 1; }
  	};
  	$scope.setPage = function (page) {
    $scope.currentPage = page - 1;
  	};
});
app.filter('startFromGrid', function () {
  return function (input, start) {
    start = +start;
    return input.slice(start);
  };
});
