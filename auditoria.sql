DROP TRIGGER IF EXISTS `insertfuncionario`;
DROP TRIGGER IF EXISTS `deletefuncionario`;
DROP TRIGGER IF EXISTS `updatefuncionario`;
DROP TRIGGER IF EXISTS `insertusuario`;
DROP TRIGGER IF EXISTS `deleteusuario`;
DROP TRIGGER IF EXISTS `updateusuario`;
DROP TRIGGER IF EXISTS `insertmaterial`;
DROP TRIGGER IF EXISTS `deletematerial`;
DROP TRIGGER IF EXISTS `updatematerial`;
DROP TRIGGER IF EXISTS `insertpedido`;
DROP TRIGGER IF EXISTS `deletepedido`;
DROP TRIGGER IF EXISTS `updatepedido`;
DROP TRIGGER IF EXISTS `insertproveedor`;
DROP TRIGGER IF EXISTS `deleteproveedor`;
DROP TRIGGER IF EXISTS `updateproveedor`;
DROP TRIGGER IF EXISTS `inserttransaccion`;
DROP TRIGGER IF EXISTS `deletetransaccion`;
DROP TRIGGER IF EXISTS `updatetransaccion`;

DROP TABLE IF EXISTS `sisalmacen`.`logs`;
CREATE TABLE `sisalmacen`.`logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(255) DEFAULT NULL,
  `tabla` varchar(255) DEFAULT NULL,
  `old` text,
  `new` text,
  `valor_alterado` text,
  `usuario` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `fecha` timestamp(6) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1379 DEFAULT CHARSET=utf8;

# TRIGGERS FUNCIONARIO

#Log de nuevo registro
DELIMITER $$
USE `sisalmacen`$$
CREATE TRIGGER `insertfuncionario`
BEFORE INSERT ON `funcionario` FOR EACH ROW
BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |ci=',ifnull(new.ci,''),
    ' |exp=',ifnull(new.exp,''),
    ' |nombres=',ifnull(new.nombres,''),
    ' |appaterno=',ifnull(new.appaterno,''),
    ' |apmaterno=',ifnull(new.apmaterno,''),
    ' |sexo=',ifnull(new.sexo,''),
    ' |direccion=',ifnull(new.direccion,''),
    ' |telefono=',ifnull(new.telefono,''),
    ' |correo=',ifnull(new.correo,''),
    ' |cargo_id=',ifnull(new.cargo_id,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"funcionario");
END$$

#Log de eliminación funcionario
DELIMITER $$
USE `sisalmacen`$$
CREATE TRIGGER `deletefuncionario`
AFTER DELETE ON `funcionario` FOR EACH ROW
BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |ci=',ifnull(old.ci,''),
    ' |exp=',ifnull(old.exp,''),
    ' |nombres=',ifnull(old.nombres,''),
    ' |appaterno=',ifnull(old.appaterno,''),
    ' |apmaterno=',ifnull(old.apmaterno,''),
    ' |sexo=',ifnull(old.sexo,''),
    ' |direccion=',ifnull(old.direccion,''),
    ' |telefono=',ifnull(old.telefono,''),
    ' |correo=',ifnull(old.correo,''),
    ' |cargo_id=',ifnull(old.cargo_id,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"funcionario");
END$$

# Log Actualización funcionario
DELIMITER $$
USE `sisalmacen`$$
CREATE TRIGGER `updatefuncionario`
BEFORE UPDATE ON `funcionario` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN
  #concatenamos los campos de la tabla a auditar y verificamos que no sean null, en caso de que los campos sean null agregamos un espacio
  #las variables (new,old)son de mysql, el valor old es el que ya se tenia en la tabla y el new es el valor que se modifico

  #Valores viejos
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |ci=',ifnull(old.ci,''),
    ' |exp=',ifnull(old.exp,''),
    ' |nombres=',ifnull(old.nombres,''),
    ' |appaterno=',ifnull(old.appaterno,''),
    ' |apmaterno=',ifnull(old.apmaterno,''),
    ' |sexo=',ifnull(old.sexo,''),
    ' |direccion=',ifnull(old.direccion,''),
    ' |telefono=',ifnull(old.telefono,''),
    ' |correo=',ifnull(old.correo,''),
    ' |cargo_id=',ifnull(old.cargo_id,'')
  );
  #Valores nuevos
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |ci=',ifnull(new.ci,''),
    ' |exp=',ifnull(new.exp,''),
    ' |nombres=',ifnull(new.nombres,''),
    ' |appaterno=',ifnull(new.appaterno,''),
    ' |apmaterno=',ifnull(new.apmaterno,''),
    ' |sexo=',ifnull(new.sexo,''),
    ' |direccion=',ifnull(new.direccion,''),
    ' |telefono=',ifnull(new.telefono,''),
    ' |correo=',ifnull(new.correo,''),
    ' |cargo_id=',ifnull(new.cargo_id,'')
  );

	#guardamos en una variable los valores que unicamente cambiaron
  set @res = '';
	IF OLD.ci <> new.ci THEN set @res = CONCAT (@res, ' |Cambió el valor de ci: ',ifnull(OLD.ci,''), ' a: ',ifnull(new.ci,'')); END IF;
	IF OLD.exp <> new.exp THEN set @res = CONCAT (@res, ' |Cambió el valor de exp: ',ifnull(OLD.exp,''), ' a: ',ifnull(new.exp,'')); END IF;
	IF OLD.nombres <> new.nombres THEN set @res = CONCAT (@res, ' |Cambió el valor de nombres: ',ifnull(OLD.nombres,''), ' a: ',ifnull(new.nombres,'')); END IF;
	IF OLD.appaterno <> new.appaterno THEN set @res = CONCAT (@res, ' |Cambió el valor de appaterno: ',ifnull(OLD.appaterno,''), ' a: ',ifnull(new.appaterno,'')); END IF;
	IF OLD.apmaterno <> new.apmaterno THEN set @res = CONCAT (@res, ' |Cambió el valor de apmaterno: ',ifnull(OLD.apmaterno,''), ' a: ',ifnull(new.apmaterno,'')); END IF;
	IF OLD.sexo <> new.sexo THEN set @res = CONCAT (@res, ' |Cambió el valor de sexo: ',ifnull(OLD.sexo,''), ' a: ',ifnull(new.sexo,'')); END IF;
	IF OLD.direccion <> new.direccion THEN set @res = CONCAT (@res, ' |Cambió el valor de direccion: ',ifnull(OLD.direccion,''), ' a: ',ifnull(new.direccion,'')); END IF;
	IF OLD.telefono <> new.telefono THEN set @res = CONCAT (@res, ' |Cambió el valor de telefono: ',ifnull(OLD.telefono,''), ' a: ',ifnull(new.telefono,'')); END IF;
	IF OLD.correo <> new.correo THEN set @res = CONCAT (@res, ' |Cambió el valor de correo: ',ifnull(OLD.correo,''), ' a: ',ifnull(new.correo,'')); END IF;
	IF OLD.cargo_id <> new.cargo_id THEN set @res = CONCAT (@res, ' |Cambió el valor de cargo_id: ',ifnull(OLD.cargo_id,''), ' a: ',ifnull(new.cargo_id,'')); END IF;

	#insertamos en nuestra tabla de log la informacion
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla,valor_alterado)
	VALUES (@oldq ,@newq,CURRENT_USER,"UPDATE",NOW(),"funcionario",ifnull(@res,'No cambio nada'));
END$$

# TRIGGERS USUARIOS

#Log de nuevo registro
DELIMITER $$
USE `sisalmacen`$$
CREATE TRIGGER `insertusuario`
BEFORE INSERT ON `usuario` FOR EACH ROW
BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |nombre=',ifnull(new.nombre,''),
    ' |estado=',ifnull(new.estado,''),
    ' |funcionario_id=',ifnull(new.funcionario_id,''),
    ' |rol_id=',ifnull(new.rol_id,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"usuario");
END$$

#Log de eliminación usuario
DELIMITER $$
USE `sisalmacen`$$
CREATE TRIGGER `deleteusuario`
AFTER DELETE ON `usuario` FOR EACH ROW
BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |nombre=',ifnull(old.nombre,''),
    ' |estado=',ifnull(old.estado,''),
    ' |funcionario_id=',ifnull(old.funcionario_id,''),
    ' |rol_id=',ifnull(old.rol_id,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"usuario");
END$$

# Log Actualización usuario
DELIMITER $$
USE `sisalmacen`$$
CREATE TRIGGER `updateusuario`
BEFORE UPDATE ON `usuario` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN
  #Valores viejos
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |nombre=',ifnull(old.nombre,''),
    ' |estado=',ifnull(old.estado,''),
    ' |funcionario_id=',ifnull(old.funcionario_id,''),
    ' |rol_id=',ifnull(old.rol_id,'')
  );
  #Valores nuevos
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |nombre=',ifnull(new.nombre,''),
    ' |estado=',ifnull(new.estado,''),
    ' |funcionario_id=',ifnull(new.funcionario_id,''),
    ' |rol_id=',ifnull(new.rol_id,'')
  );

	#guardamos en una variable los valores que unicamente cambiaron
  set @res = '';
	IF OLD.nombre <> new.nombre THEN set @res = CONCAT (@res, ' |Cambió el valor de nombre: ',ifnull(OLD.nombre,''), ' a: ',ifnull(new.nombre,'')); END IF;
	IF OLD.password <> new.password THEN set @res = CONCAT (@res, ' |El password a cambiado!'); END IF;
	IF OLD.estado <> new.estado THEN set @res = CONCAT (@res, ' |Cambió el valor de estado: ',ifnull(OLD.estado,''), ' a: ',ifnull(new.estado,'')); END IF;
	IF OLD.funcionario_id <> new.funcionario_id THEN set @res = CONCAT (@res, ' |Cambió el valor de funcionario_id: ',ifnull(OLD.funcionario_id,''), ' a: ',ifnull(new.funcionario_id,'')); END IF;
	IF OLD.rol_id <> new.rol_id THEN set @res = CONCAT (@res, ' |Cambió el valor de rol_id: ',ifnull(OLD.rol_id,''), ' a: ',ifnull(new.rol_id,'')); END IF;

	#insertamos en nuestra tabla de log la informacion
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla,valor_alterado)
	VALUES (@oldq ,@newq,CURRENT_USER,"UPDATE",NOW(),"usuario",ifnull(@res,'No cambio nada'));
END$$

# TRIGGERS MATERIAL

#Log de nuevo registro
DELIMITER $$
USE `sisalmacen`$$
CREATE TRIGGER `insertmaterial`
BEFORE INSERT ON `material` FOR EACH ROW
BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |codigo=',ifnull(new.codigo,''),
    ' |nom_material=',ifnull(new.nom_material,''),
    ' |descripcion=',ifnull(new.descripcion,''),
    ' |min_inventario=',ifnull(new.min_inventario,''),
    ' |presentacion=',ifnull(new.presentacion,''),
    ' |activo=',ifnull(new.activo,''),
    ' |categoria_id=',ifnull(new.categoria_id,''),
    ' |fecha_creacion=',ifnull(new.fecha_creacion,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"material");
END$$

#Log de eliminación material
DELIMITER $$
USE `sisalmacen`$$
CREATE TRIGGER `deletematerial`
AFTER DELETE ON `material` FOR EACH ROW
BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |codigo=',ifnull(old.codigo,''),
    ' |nom_material=',ifnull(old.nom_material,''),
    ' |descripcion=',ifnull(old.descripcion,''),
    ' |min_inventario=',ifnull(old.min_inventario,''),
    ' |presentacion=',ifnull(old.presentacion,''),
    ' |activo=',ifnull(old.activo,''),
    ' |categoria_id=',ifnull(old.categoria_id,''),
    ' |fecha_creacion=',ifnull(old.fecha_creacion,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"material");
END$$

# Log Actualización material
DELIMITER $$
USE `sisalmacen`$$
CREATE TRIGGER `updatematerial`
BEFORE UPDATE ON `material` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN
  #concatenamos los campos de la tabla a auditar y verificamos que no sean null, en caso de que los campos sean null agregamos un espacio
  #las variables (new,old)son de mysql, el valor old es el que ya se tenia en la tabla y el new es el valor que se modifico

  #Valores viejos
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |codigo=',ifnull(old.codigo,''),
    ' |nom_material=',ifnull(old.nom_material,''),
    ' |descripcion=',ifnull(old.descripcion,''),
    ' |min_inventario=',ifnull(old.min_inventario,''),
    ' |presentacion=',ifnull(old.presentacion,''),
    ' |activo=',ifnull(old.activo,''),
    ' |categoria_id=',ifnull(old.categoria_id,''),
    ' |fecha_creacion=',ifnull(old.fecha_creacion,'')
  );
  #Valores nuevos
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |codigo=',ifnull(new.codigo,''),
    ' |nom_material=',ifnull(new.nom_material,''),
    ' |descripcion=',ifnull(new.descripcion,''),
    ' |min_inventario=',ifnull(new.min_inventario,''),
    ' |presentacion=',ifnull(new.presentacion,''),
    ' |activo=',ifnull(new.activo,''),
    ' |categoria_id=',ifnull(new.categoria_id,''),
    ' |fecha_creacion=',ifnull(new.fecha_creacion,'')
  );

	#guardamos en una variable los valores que unicamente cambiaron
  set @res = '';
	IF OLD.codigo <> new.codigo THEN set @res = CONCAT (@res, ' |Cambió el valor de codigo: ',ifnull(OLD.codigo,''), ' a: ',ifnull(new.codigo,'')); END IF;
	IF OLD.nom_material <> new.nom_material THEN set @res = CONCAT (@res, ' |Cambió el valor de nom_material: ',ifnull(OLD.nom_material,''), ' a: ',ifnull(new.nom_material,'')); END IF;
	IF OLD.descripcion <> new.descripcion THEN set @res = CONCAT (@res, ' |Cambió el valor de descripcion: ',ifnull(OLD.descripcion,''), ' a: ',ifnull(new.descripcion,'')); END IF;
	IF OLD.min_inventario <> new.min_inventario THEN set @res = CONCAT (@res, ' |Cambió el valor de min_inventario: ',ifnull(OLD.min_inventario,''), ' a: ',ifnull(new.min_inventario,'')); END IF;
	IF OLD.presentacion <> new.presentacion THEN set @res = CONCAT (@res, ' |Cambió el valor de presentacion: ',ifnull(OLD.presentacion,''), ' a: ',ifnull(new.presentacion,'')); END IF;
	IF OLD.activo <> new.activo THEN set @res = CONCAT (@res, ' |Cambió el valor de activo: ',ifnull(OLD.activo,''), ' a: ',ifnull(new.activo,'')); END IF;
	IF OLD.categoria_id <> new.categoria_id THEN set @res = CONCAT (@res, ' |Cambió el valor de categoria_id: ',ifnull(OLD.categoria_id,''), ' a: ',ifnull(new.categoria_id,'')); END IF;
	IF OLD.fecha_creacion <> new.fecha_creacion THEN set @res = CONCAT (@res, ' |Cambió el valor de fecha_creacion: ',ifnull(OLD.fecha_creacion,''), ' a: ',ifnull(new.fecha_creacion,'')); END IF;

	#insertamos en nuestra tabla de log la informacion
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla,valor_alterado)
	VALUES (@oldq ,@newq,CURRENT_USER,"UPDATE",NOW(),"material",ifnull(@res,'No cambio nada'));
END$$v

# TRIGGERS PEDIDO

#Log de nuevo registro
DELIMITER $$
USE `sisalmacen`$$
CREATE TRIGGER `insertpedido`
BEFORE INSERT ON `pedido` FOR EACH ROW
BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |estado_pedido_id=',ifnull(new.estado_pedido_id,''),
    ' |funcionario_id=',ifnull(new.funcionario_id,''),
    ' |proveedor_id=',ifnull(new.proveedor_id,''),
    ' |usuario_id=',ifnull(new.usuario_id,''),
    ' |transaccion_tipo_id=',ifnull(new.transaccion_tipo_id,''),
    ' |Nro_factura=',ifnull(new.Nro_factura,''),
    ' |partida_presup=',ifnull(new.partida_presup,''),
    ' |fuente=',ifnull(new.fuente,''),
    ' |fecha_creacion=',ifnull(new.fecha_creacion,''),
    ' |fecha_entrega=',ifnull(new.fecha_entrega,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"pedido");
END$$

#Log de eliminación funcionario
DELIMITER $$
USE `sisalmacen`$$
CREATE TRIGGER `deletepedido`
AFTER DELETE ON `pedido` FOR EACH ROW
BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |estado_pedido_id=',ifnull(old.estado_pedido_id,''),
    ' |funcionario_id=',ifnull(old.funcionario_id,''),
    ' |proveedor_id=',ifnull(old.proveedor_id,''),
    ' |usuario_id=',ifnull(old.usuario_id,''),
    ' |transaccion_tipo_id=',ifnull(old.transaccion_tipo_id,''),
    ' |Nro_factura=',ifnull(old.Nro_factura,''),
    ' |partida_presup=',ifnull(old.partida_presup,''),
    ' |fuente=',ifnull(old.fuente,''),
    ' |fecha_creacion=',ifnull(old.fecha_creacion,''),
    ' |fecha_entrega=',ifnull(old.fecha_entrega,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"pedido");
END$$

# Log Actualización funcionario
DELIMITER $$
USE `sisalmacen`$$
CREATE TRIGGER `updatepedido`
BEFORE UPDATE ON `pedido` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN
  #Valores viejos
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |estado_pedido_id=',ifnull(old.estado_pedido_id,''),
    ' |funcionario_id=',ifnull(old.funcionario_id,''),
    ' |proveedor_id=',ifnull(old.proveedor_id,''),
    ' |usuario_id=',ifnull(old.usuario_id,''),
    ' |transaccion_tipo_id=',ifnull(old.transaccion_tipo_id,''),
    ' |Nro_factura=',ifnull(old.Nro_factura,''),
    ' |partida_presup=',ifnull(old.partida_presup,''),
    ' |fuente=',ifnull(old.fuente,''),
    ' |fecha_creacion=',ifnull(old.fecha_creacion,''),
    ' |fecha_entrega=',ifnull(old.fecha_entrega,'')
  );
  #Valores nuevos
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |estado_pedido_id=',ifnull(new.estado_pedido_id,''),
    ' |funcionario_id=',ifnull(new.funcionario_id,''),
    ' |proveedor_id=',ifnull(new.proveedor_id,''),
    ' |usuario_id=',ifnull(new.usuario_id,''),
    ' |transaccion_tipo_id=',ifnull(new.transaccion_tipo_id,''),
    ' |Nro_factura=',ifnull(new.Nro_factura,''),
    ' |partida_presup=',ifnull(new.partida_presup,''),
    ' |fuente=',ifnull(new.fuente,''),
    ' |fecha_creacion=',ifnull(new.fecha_creacion,''),
    ' |fecha_entrega=',ifnull(new.fecha_entrega,'')
  );

	#guardamos en una variable los valores que unicamente cambiaron
  set @res = '';
	IF OLD.estado_pedido_id <> new.estado_pedido_id THEN set @res = CONCAT (@res, ' |Cambió el valor de estado_pedido_id: ',ifnull(OLD.estado_pedido_id,''), ' a: ',ifnull(new.estado_pedido_id,'')); END IF;
	IF OLD.funcionario_id <> new.funcionario_id THEN set @res = CONCAT (@res, ' |Cambió el valor de funcionario_id: ',ifnull(OLD.funcionario_id,''), ' a: ',ifnull(new.funcionario_id,'')); END IF;
	IF OLD.proveedor_id <> new.proveedor_id THEN set @res = CONCAT (@res, ' |Cambió el valor de proveedor_id: ',ifnull(OLD.proveedor_id,''), ' a: ',ifnull(new.proveedor_id,'')); END IF;
	IF OLD.usuario_id <> new.usuario_id THEN set @res = CONCAT (@res, ' |Cambió el valor de usuario_id: ',ifnull(OLD.usuario_id,''), ' a: ',ifnull(new.usuario_id,'')); END IF;
	IF OLD.transaccion_tipo_id <> new.transaccion_tipo_id THEN set @res = CONCAT (@res, ' |Cambió el valor de transaccion_tipo_id: ',ifnull(OLD.transaccion_tipo_id,''), ' a: ',ifnull(new.transaccion_tipo_id,'')); END IF;
	IF OLD.Nro_factura <> new.Nro_factura THEN set @res = CONCAT (@res, ' |Cambió el valor de Nro_factura: ',ifnull(OLD.Nro_factura,''), ' a: ',ifnull(new.Nro_factura,'')); END IF;
	IF OLD.partida_presup <> new.partida_presup THEN set @res = CONCAT (@res, ' |Cambió el valor de partida_presup: ',ifnull(OLD.partida_presup,''), ' a: ',ifnull(new.partida_presup,'')); END IF;
	IF OLD.fuente <> new.fuente THEN set @res = CONCAT (@res, ' |Cambió el valor de fuente: ',ifnull(OLD.fuente,''), ' a: ',ifnull(new.fuente,'')); END IF;
	IF OLD.fecha_creacion <> new.fecha_creacion THEN set @res = CONCAT (@res, ' |Cambió el valor de fecha_creacion: ',ifnull(OLD.fecha_creacion,''), ' a: ',ifnull(new.fecha_creacion,'')); END IF;
	IF OLD.fecha_entrega <> new.fecha_entrega THEN set @res = CONCAT (@res, ' |Cambió el valor de fecha_entrega: ',ifnull(OLD.fecha_entrega,''), ' a: ',ifnull(new.fecha_entrega,'')); END IF;

	#insertamos en nuestra tabla de log la informacion
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla,valor_alterado)
	VALUES (@oldq ,@newq,CURRENT_USER,"UPDATE",NOW(),"pedido",ifnull(@res,'No cambio nada'));
END$$

# TRIGGERS PROVEEDOR

#Log de nuevo registro
DELIMITER $$
USE `sisalmacen`$$
CREATE TRIGGER `insertproveedor`
BEFORE INSERT ON `proveedor` FOR EACH ROW
BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |Nit=',ifnull(new.Nit,''),
    ' |empresa=',ifnull(new.empresa,''),
    ' |nombres=',ifnull(new.nombres,''),
    ' |apellidos=',ifnull(new.apellidos,''),
    ' |direccion=',ifnull(new.direccion,''),
    ' |telefono=',ifnull(new.telefono,''),
    ' |correo=',ifnull(new.correo,''),
    ' |fecha_creacion=',ifnull(new.fecha_creacion,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"proveedor");
END$$

#Log de eliminación funcionario
DELIMITER $$
USE `sisalmacen`$$
CREATE TRIGGER `deleteproveedor`
AFTER DELETE ON `proveedor` FOR EACH ROW
BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |Nit=',ifnull(old.Nit,''),
    ' |empresa=',ifnull(old.empresa,''),
    ' |nombres=',ifnull(old.nombres,''),
    ' |apellidos=',ifnull(old.apellidos,''),
    ' |direccion=',ifnull(old.direccion,''),
    ' |telefono=',ifnull(old.telefono,''),
    ' |correo=',ifnull(old.correo,''),
    ' |fecha_creacion=',ifnull(old.fecha_creacion,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"proveedor");
END$$

# Log Actualización funcionario
DELIMITER $$
USE `sisalmacen`$$
CREATE TRIGGER `updateproveedor`
BEFORE UPDATE ON `proveedor` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN
  #Valores viejos
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |Nit=',ifnull(old.Nit,''),
    ' |empresa=',ifnull(old.empresa,''),
    ' |nombres=',ifnull(old.nombres,''),
    ' |apellidos=',ifnull(old.apellidos,''),
    ' |direccion=',ifnull(old.direccion,''),
    ' |telefono=',ifnull(old.telefono,''),
    ' |correo=',ifnull(old.correo,''),
    ' |fecha_creacion=',ifnull(old.fecha_creacion,'')
  );
  #Valores nuevos
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |Nit=',ifnull(new.Nit,''),
    ' |empresa=',ifnull(new.empresa,''),
    ' |nombres=',ifnull(new.nombres,''),
    ' |apellidos=',ifnull(new.apellidos,''),
    ' |direccion=',ifnull(new.direccion,''),
    ' |telefono=',ifnull(new.telefono,''),
    ' |correo=',ifnull(new.correo,''),
    ' |fecha_creacion=',ifnull(new.fecha_creacion,'')
  );

	#guardamos en una variable los valores que unicamente cambiaron
  set @res = '';
	IF OLD.Nit <> new.Nit THEN set @res = CONCAT (@res, ' |Cambió el valor de Nit: ',ifnull(OLD.Nit,''), ' a: ',ifnull(new.Nit,'')); END IF;
	IF OLD.empresa <> new.empresa THEN set @res = CONCAT (@res, ' |Cambió el valor de empresa: ',ifnull(OLD.empresa,''), ' a: ',ifnull(new.empresa,'')); END IF;
	IF OLD.nombres <> new.nombres THEN set @res = CONCAT (@res, ' |Cambió el valor de nombres: ',ifnull(OLD.nombres,''), ' a: ',ifnull(new.nombres,'')); END IF;
	IF OLD.apellidos <> new.apellidos THEN set @res = CONCAT (@res, ' |Cambió el valor de apellidos: ',ifnull(OLD.apellidos,''), ' a: ',ifnull(new.apellidos,'')); END IF;
	IF OLD.direccion <> new.direccion THEN set @res = CONCAT (@res, ' |Cambió el valor de direccion: ',ifnull(OLD.direccion,''), ' a: ',ifnull(new.direccion,'')); END IF;
	IF OLD.telefono <> new.telefono THEN set @res = CONCAT (@res, ' |Cambió el valor de telefono: ',ifnull(OLD.telefono,''), ' a: ',ifnull(new.telefono,'')); END IF;
	IF OLD.correo <> new.correo THEN set @res = CONCAT (@res, ' |Cambió el valor de correo: ',ifnull(OLD.correo,''), ' a: ',ifnull(new.correo,'')); END IF;
	IF OLD.fecha_creacion <> new.fecha_creacion THEN set @res = CONCAT (@res, ' |Cambió el valor de fecha_creacion: ',ifnull(OLD.fecha_creacion,''), ' a: ',ifnull(new.fecha_creacion,'')); END IF;

	#insertamos en nuestra tabla de log la informacion
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla,valor_alterado)
	VALUES (@oldq ,@newq,CURRENT_USER,"UPDATE",NOW(),"proveedor",ifnull(@res,'No cambio nada'));
END$$

# TRIGGERS TRANSACCION

#Log de nuevo registro
DELIMITER $$
USE `sisalmacen`$$
CREATE TRIGGER `inserttransaccion`
BEFORE INSERT ON `transaccion` FOR EACH ROW
BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |material_id=',ifnull(new.material_id,''),
    ' |precio_unitario=',ifnull(new.precio_unitario,''),
    ' |q=',ifnull(new.q,''),
    ' |qe=',ifnull(new.qe,''),
    ' |transaccion_tipo_id=',ifnull(new.transaccion_tipo_id,''),
    ' |pedido_id=',ifnull(new.pedido_id,''),
    ' |obs=',ifnull(new.obs,''),
    ' |fecha_creacion=',ifnull(new.fecha_creacion,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"transaccion");
END$$

#Log de eliminación funcionario
DELIMITER $$
USE `sisalmacen`$$
CREATE TRIGGER `deletetransaccion`
AFTER DELETE ON `transaccion` FOR EACH ROW
BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |material_id=',ifnull(old.material_id,''),
    ' |precio_unitario=',ifnull(old.precio_unitario,''),
    ' |q=',ifnull(old.q,''),
    ' |qe=',ifnull(old.qe,''),
    ' |transaccion_tipo_id=',ifnull(old.transaccion_tipo_id,''),
    ' |pedido_id=',ifnull(old.pedido_id,''),
    ' |obs=',ifnull(old.obs,''),
    ' |fecha_creacion=',ifnull(old.fecha_creacion,'')
  );
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"transaccion");
END$$

# Log Actualización funcionario
DELIMITER $$
USE `sisalmacen`$$
CREATE TRIGGER `updatetransaccion`
BEFORE UPDATE ON `transaccion` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN
  #Valores viejos
  SET @oldq = CONCAT (
    ' |id=',ifnull(old.id,''),
    ' |material_id=',ifnull(old.material_id,''),
    ' |precio_unitario=',ifnull(old.precio_unitario,''),
    ' |q=',ifnull(old.q,''),
    ' |qe=',ifnull(old.qe,''),
    ' |transaccion_tipo_id=',ifnull(old.transaccion_tipo_id,''),
    ' |pedido_id=',ifnull(old.pedido_id,''),
    ' |obs=',ifnull(old.obs,''),
    ' |fecha_creacion=',ifnull(old.fecha_creacion,'')
  );
  #Valores nuevos
  SET @newq = CONCAT (
    ' |id=',ifnull(new.id,''),
    ' |material_id=',ifnull(new.material_id,''),
    ' |precio_unitario=',ifnull(new.precio_unitario,''),
    ' |q=',ifnull(new.q,''),
    ' |qe=',ifnull(new.qe,''),
    ' |transaccion_tipo_id=',ifnull(new.transaccion_tipo_id,''),
    ' |pedido_id=',ifnull(new.pedido_id,''),
    ' |obs=',ifnull(new.obs,''),
    ' |fecha_creacion=',ifnull(new.fecha_creacion,'')
  );

	#guardamos en una variable los valores que unicamente cambiaron
  set @res = '';
	IF OLD.material_id <> new.material_id THEN set @res = CONCAT (@res, ' |Cambió el valor de material_id: ',ifnull(OLD.material_id,''), ' a: ',ifnull(new.material_id,'')); END IF;
	IF OLD.precio_unitario <> new.precio_unitario THEN set @res = CONCAT (@res, ' |Cambió el valor de precio_unitario: ',ifnull(OLD.precio_unitario,''), ' a: ',ifnull(new.precio_unitario,'')); END IF;
	IF OLD.q <> new.q THEN set @res = CONCAT (@res, ' |Cambió el valor de q: ',ifnull(OLD.q,''), ' a: ',ifnull(new.q,'')); END IF;
	IF OLD.qe <> new.qe THEN set @res = CONCAT (@res, ' |Cambió el valor de qe: ',ifnull(OLD.qe,''), ' a: ',ifnull(new.qe,'')); END IF;
	IF OLD.transaccion_tipo_id <> new.transaccion_tipo_id THEN set @res = CONCAT (@res, ' |Cambió el valor de transaccion_tipo_id: ',ifnull(OLD.transaccion_tipo_id,''), ' a: ',ifnull(new.transaccion_tipo_id,'')); END IF;
	IF OLD.pedido_id <> new.pedido_id THEN set @res = CONCAT (@res, ' |Cambió el valor de pedido_id: ',ifnull(OLD.pedido_id,''), ' a: ',ifnull(new.pedido_id,'')); END IF;
	IF OLD.obs <> new.obs THEN set @res = CONCAT (@res, ' |Cambió el valor de obs: ',ifnull(OLD.obs,''), ' a: ',ifnull(new.obs,'')); END IF;
	IF OLD.fecha_creacion <> new.fecha_creacion THEN set @res = CONCAT (@res, ' |Cambió el valor de fecha_creacion: ',ifnull(OLD.fecha_creacion,''), ' a: ',ifnull(new.fecha_creacion,'')); END IF;

	#insertamos en nuestra tabla de log la informacion
	INSERT INTO logs (old,new,usuario,tipo,fecha,tabla,valor_alterado)
	VALUES (@oldq ,@newq,CURRENT_USER,"UPDATE",NOW(),"transaccion",ifnull(@res,'No cambio nada'));
END$$
