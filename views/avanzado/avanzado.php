<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Usuario Avanzado</title>
  <link rel="stylesheet" href="assets/css/normalize.css?rev=<?php echo time(); ?>">
  <link rel="stylesheet" href="assets/css/materialize.min.css?rev=<?php echo time(); ?>">
  <link rel="stylesheet" href="assets/css/fonts.css?rev=<?php echo time(); ?>">
  <link rel="stylesheet" href="assets/css/sweetalert.css?rev=<?php echo time(); ?>">
  <link rel="stylesheet" href="assets/css/admin.css?rev=<?php echo time(); ?>">
  <script type="text/javascript">
    var id_usuario = '<?php echo $_SESSION['id']; ?>';
    var id_fun = '<?php echo $_SESSION['fun_id']; ?>';
    var name_completo = '<?php echo $_SESSION['name_complet']; ?>';
  </script>
</head>

<body ng-app="myApp" id="body">
  <div class="wrapper">
    <header>
      <!--este es la cabeza -->
      <nav>
        <div class="nav-wrapper">
        <a href="" id="collapse-btn" class="collapse-btn show-on-large"><i class="material-icons">menu</i></a>
          <!--referencia de iconos-->
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <!--cabezera-->
            <!--inicio cerrar secion-->
            <ul class="right">
              <li><img src="assets/img/jc.jpg" alt="" class="circle" width="60px" id="usuario"></li>
              <li>
                <a class='dropdown-button' href='' data-activates='useravanzado'><?php echo $_SESSION['name_complet']; ?></a>
              </li>
              <!-- Dropdown Structure -->
              <ul id='useravanzado' class='dropdown-content'>
                <li><a href="#!cuenta"><i class="material-icons left">person_pin</i>Mi Perfil</a></li>
                <li class="divider"></li>
                <li><a href="index.php?view=admin&action=logout"><i class="material-icons left">lock_outline</i>Cerrar Session</a>
                </li>
                <li class="divider"></li>
              </ul>
              <!--creando secion y nos encia a carpeta controller-->
            </ul>
            <!--fin cerrar secion-->
          </ul>
        </div>
      </nav>
    </header>

    <div class="side-nav">
      <div class="side-nav-logo">
        <span id="nomlog">ESMFTHEA </span><span id="nomlog1">ALMACEN</span>
      </div>
      <div class="side-nav-user">
        <img src="assets/img/logo.jpg" alt="" class="circle" width="60px" id="logo ">
        <span class="subheader">MENU</span>
      </div>
      <!-- amburgeza-->
      <ul>
        <li><a href="#!inicio" class="waves-effect waves-teal"><i class="material-icons">home</i>Inicio</a></li>
        <li><a href="#!avanzadomateriales" class="waves-effect waves-teal"><i class="material-icons">list</i>Materiales</a></li>
        <li><a href="#!avanzadosolicitar" class="waves-effect waves-teal"><i class="material-icons">credit_card</i>Solicitar Material</a></li>
        <li><a href="#!avanzadosolicitudes" class="waves-effect waves-teal"><i class="material-icons">info</i>Solicitudes</a></li>
      </ul>
      <!--fin hamburgesa-->
    </div>

    <!--contenido-->
    <div class="row" id="">
      <div class="col s12 m12 l12 cuerpo-wrapper">
        <div data-ng-view="" id="ng-view"></div>
      </div>
    </div>
  </div>

  <footer class="app-footer">
    <!--  <div class="container">
      <div class="row" id="pie">
        <div class="col l6 s12">
          <h5 class="white-text">Unidad de Bienes y Servicios</h5>
          <p class="grey-text text-lighten-4">Sistema de Inventario de Alamacenes</p>
        </div>
        <div class="col l4 offset-l2 s12">
          <h5 class="white-text">Referencias</h5>
          <ul>
            <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
          </ul>
        </div>
      </div>
    </div> -->
    <div>
      <span>
        Sistema de Inventario en Almacen © 2020
      </span>
      <a class="grey-text text-lighten-4" href="#!">esfmthea "Escuela Superior de Formacion de Maestros El Alto"</a>
    </div>
  </footer>

  <script src="assets/js/sweetalert.min.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/jquery-3.1.1.min.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/angular.min.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/angular-notify.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/materialize.min.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/angular-materialize.min.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/angular-route.min.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/init.js" ?rev=<?php echo time(); ?>></script>
  <script src="assets/js/app.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/avanmateriales.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/avansolicitud.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/avanhistorial.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/ctrlhome.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/ctrlcuenta.js?rev=<?php echo time(); ?>"></script>

</body>
</html>