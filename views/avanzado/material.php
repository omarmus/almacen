<div class="row" ng-controller="avanmateriales">
	  <nav>
    <div class="nav-wrapper" id="contenido">
      <a class="brand-logo left" id="logopersona"><i class="material-icons left">recent_actors</i>Materiales</a>
    </div>
  </nav>
  <form class="col s12 m12 l12 full" id="btnbusqueda">
      <div class="row">
        <div class="input-field col s12 m12 l12">
          <input id="search" type="text" class="validate" ng-model="q" ng-change="currentPage = 0">
          <label for="search">Busqueda</label>
        </div>
      </div>
  </form>

<div class="container-full">
    <div class="row" ng-hide="listaexiste">
      <div class="col s12 m12 l12">
        <table class="striped" ng-init='configPages()'>
          <thead>
            <tr>
              <th data-field="tipo">Codigo</th>
              <th data-field="name">Nombre de Material</th>
              <th data-field="name">Descripcion</th>
              <th data-field="name">Presentación</th>
              <th data-field="name">C/A</th>
              <th data-field="name">Disponible</th>
            </tr>
          </thead>
          <tbody >
            <tr ng-repeat="x in datoscanaut | filter:q | startFromGrid: currentPage * pageSize | limitTo: pageSize">
              <td>{{x.codigo}}</td>
              <td>{{x.nom_material}}</td>
              <td>{{x.descripcion}}</td>
              <td>{{x.presentacion}}</td>
              <td>{{x.ca}}</td>
              <td>
                <i ng-show="x.q >= {{x.ca}}" class="accent-3 material-icons">check</i>
                <a href="" id="btn2" tooltipped data-position="top" data-delay="40" data-tooltip="Este material no esta disponible !"><i ng-show="x.q < {{x.ca}}" class="accent-2 material-icons">info</i></a>
              </td>
            </tr> 
          </tbody>
        </table>
      </div> 
    </div>
    <!--inicio de paginacion-->
    <div class="row" ng-hide="listaexiste">
      <div class="col s12 m12 l12 center">
        <pagination page="1" page-size="pageSize" total="datoscanaut.length" show-prev-next="true" use-simple-prev-next="false" dots="..." hide-if-empty="false" adjacente="2" scroll-top="false"  pagination-action="setPage(page)"/>
      </div>
    </div>
    <!--fin de paginacion-->        
    <div class="row" ng-hide="listanoexiste">
      <div id="advertencia" class="col s12 m12 l12">
        <p>No hay registros que mostrar!!! <i class="tiny material-icons">error</i></p> 
      </div>
    </div>
</div> 
</div>