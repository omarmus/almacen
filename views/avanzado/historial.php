<div class="row" ng-controller="avanhistorial">
	  <nav>
    <div class="nav-wrapper" id="contenido">
      <a class="brand-logo left" id="logopersona"><i class="material-icons left">recent_actors</i>Historial de Pedidos</a>
    </div>
  </nav>
  <form class="col s12 m12 l12 full" id="btnbusqueda">
      <div class="row">
        <div class="input-field col s12 m12 l12">
          <input id="search" type="text" class="validate" ng-model="q" ng-change="currentPage = 0">
          <label for="search">Busqueda</label>
        </div>
      </div>
  </form>

<div class="container-full">
    <div class="row" ng-hide="listaexiste">
      <div class="col s12 m12 l12">
        <table class="striped" ng-init='configPages()'>
          <thead>
            <tr>
              <th data-field="tipo">Nº</th>
              <th data-field="name">Solicitante</th>
              <th data-field="name">Cargo</th>
              <th data-field="name">Fecha pedido</th>
              <th data-field="name">Fecha entrgado</th>
              <th data-field="name">Estado pedido</th>
              <th>Detalle</th>
            </tr>
          </thead>
          <tbody >
            <tr ng-repeat="x in datoshist | filter:q | startFromGrid: currentPage * pageSize | limitTo: pageSize">
              <td>{{$index+1}}</td>
              <td>{{x.nombres}} {{x.appeterno}} {{x.apmaterno}}</td>
              <td>{{x.nomcargo}}</td>
              <td>{{x.fecha_creacion}}</td>
              <td>{{x.fecha_entrega}}</td>
              <td ng-if="x.estado_pedido_id==2">
                <a href="" id="btn1" tooltipped data-position="left" data-delay="40" data-tooltip="Este de material se entrego"><i  class="accent-2 material-icons">assignment_turned_in</i></a>
              </td>
              <td ng-if="x.estado_pedido_id==1">
                <a href="" id="btn2" tooltipped data-position="top" data-delay="40" data-tooltip="Este de material no se entrego"><i  class="accent-2 material-icons">info</i></a>
              </td>
              <td>
                <a href="" id="btn" tooltipped data-position="top" data-delay="40" data-tooltip="Ver detalle del pedido" ng-click="undetalle(x.id,x.aid)"><i  class="accent-2 material-icons">visibility</i></a>
              </td>
            </tr> 
          </tbody>
        </table>
      </div> 
    </div>
    <!--inicio de paginacion-->
    <div class="row" ng-hide="listaexiste">
      <div class="col s12 m12 l12 center">
        <pagination page="1" page-size="pageSize" total="datoshist.length" show-prev-next="true" use-simple-prev-next="false" dots="..." hide-if-empty="false" adjacente="2" scroll-top="false"  pagination-action="setPage(page)"/>
      </div>
    </div>
    <!--fin de paginacion-->        
    <div class="row" ng-hide="listanoexiste">
      <div id="advertencia" class="col s12 m12 l12">
        <p>No hay registros que mostrar!!! <i class="tiny material-icons">error</i></p> 
      </div>
    </div>
<div class="col s12" ng-show="listaver">
    <div class="row" style="background-color: grey; color: #f4f4f9; border-radius: 5px 0px 5px 0px;box-shadow: 1px 3px 3px #677474;">
      <div class="col s11 center-align">
        <h5>Detalle de Materiales Solicitados</h5>
      </div>
      <div class="col s1 right-align">
        <a href=""  ng-click="ocultadetalle()" tooltipped data-position="left" data-delay="40" data-tooltip="Cerrar Detalle"><i class="small material-icons" style="margin-top: 10px; color: #f4f4f9;">close</i></a>
      </div>  
    </div>
    <table class="striped"><!---->
            <thead>
              <tr>
                <th>Item</th>
                <th>Codigo</th>
                <th>Tipo</th>
                <th>Material Suministro</th>
                <th>Unidad</th>
                <th>C/A</th>
                <th>C/S</th>
                <th>C/E</th>
                <th>Obs</th>
                <!--th>Disponible</th-->
                
              </tr>
            </thead>
            <tbody >
              <tr ng-repeat="a in detalle">
                <td>{{$index+1}}</td>
                <td>{{a.codigo}}</td>
                <td>{{a.tipo}}</td>
                <td>{{a.nom_material}} {{a.descripcion}}</td>
                <td>{{a.presentacion}}</td>
                <td>{{a.ca}}</td>
                <td>{{a.q}}</td>
                <td ng-show="a.qe == null">{{a.q}}</td>
                <td ng-show="a.qe != null">{{a.qe}}</td>
                <td>{{a.obs}}</td>
                <!--td>{{a.tot}}</td-->
                
              </tr>
              
            </tbody>
        </table>
  </div>
</div> 
</div>