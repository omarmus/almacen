<!DOCTYPE html>
<html lang="es" ng-app="mylogin">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>ESMFTHEA</title>
  
     <!-- Normalize CSS -->
  <link rel="stylesheet" href="assets/css/normalize.css"> 
     <!-- Materialize CSS -->
  <link rel="stylesheet" href="assets/css/materialize.min.css">
     <!-- Material Design Iconic Font CSS -->
  <link rel="stylesheet" href="assets/css/fonts.css">
    <!-- Malihu jQuery custom content scroller CSS -->
  <link rel="stylesheet" href="assets/css/jquery.mCustomScrollbar.css">
    <!-- Sweet Alert CSS -->
  <link rel="stylesheet" href="assets/css/sweetalert.css">  
    <!-- MaterialDark CSS -->
  <link rel="stylesheet" href="assets/css/style.css">
</head>
<body ng-controller="myCtrl" class="font-cover" id="login">
    <div class="container-login center-align">
        <div style="margin:15px 0;">
            <i class="large material-icons ">account_circle</i>
            <p>Inicia sesión con tu cuenta</p>   
        </div>
        <form ng-submit="iniciar_session()">
            <div class="input-field col s12">
                <input id="UserName" type="text" class="validate" name="user" ng-model="user">
                <label for="UserName"><i class="material-icons left">person</i>&nbsp;Nombre</label>
            </div>
            <div class="input-field col s12">
                <input id="Password" type="password" class="validate" name="pass" ng-model="pass">
                <label for="Password"><i class="material-icons left">lock</i>&nbsp;Contraseña</label>
            </div>
            <button class="waves-effect waves-teal btn-flat"  type="submit">Ingresar &nbsp; <i class="zmdi zmdi-mail-send"></i></button>
        </form>
        <div class="divider" style="margin: 20px 0;"></div>
        <!-- <a href="home.html">Crear cuenta</a> -->
    </div> 
  <script src="assets/js/sweetalert.min.js"></script>
  <script src="assets/js/jquery-3.1.1.min.js"></script>
    <!-- Materialize JS -->
  <script src="assets/js/angular.min.js"></script>
  <script src="assets/js/angular-animate.min.js"></script>
  <script src="assets/js/angular-messages.min.js"></script>
  <script src="assets/js/angular-materialize.min.js"></script>
  <script src="assets/js/materialize.min.js"></script>  
    <!-- Malihu jQuery custom content scroller JS -->
  <script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    
    <!-- MaterialDark JS-->
  <script src="assets/js/main.js"></script> 
  

  <script>
  var app = angular.module('mylogin', ['ngMessages','ngAnimate']);
  app.controller('myCtrl', function($scope, $http) {
    $scope.user="";
    $scope.pass="";
    $scope.iniciar_session=function(){
      if($scope.user==""){
        $('#UserName').focus();
        $('#UserName').addClass('invalid');
      }else{
        $('#UserName').removeClass('invalid');
        $('#UserName').addClass('valid');
        if($scope.pass==""){
          $('#Password').focus();
          $('#Password').addClass('invalid');
        }else{
          $http.post("index.php?view=login&action=session",{
            'user':$scope.user,
            'pass':$scope.pass
          }).then(function(response) {
            if(response.data[1]=='1'){
              location.href=response.data[0];
            }else{
              alert("la contraseña o el nombre de usuario no son correctos!!");
            }
          });
        }
      } 
    }
  });
  </script> 
</body>
</html>