<div class="row" ng-controller="ctrlrepMaterialNo">
  <nav>
    <div class="nav-wrapper" id="contenido">
      <a class="brand-logo left" id="logopersona"><i class="material-icons left">recent_actors</i> Material no disponible</a>
    </div>
  </nav>
  <form class="col s12 m12 l12 full" id="btnbusqueda">
    <div class="row">
      <div class="input-field col s10 m10 l10">
        <input id="search" type="text" class="validate" ng-model="q1" ng-change="currentPage = 0">
        <label for="search">Búsqueda</label>
      </div>
      <div class="col s2 m2 l2">
        <br>
        <a class=" btn" data-target='modalpdfno' modal ng-click="pdf()">Pdf<i class="small material-icons left">print</i></a>
      </div>
    </div>
  </form>
  <div class="">
    <div class="row" ng-hide="listaexiste">
      <div class="col s12">
        <table class="striped" ng-init='configPages()'>
          <thead>
            <tr>
              <th data-field="id">Id</th>
              <th data-field="tipo">Codigo</th>
              <th data-field="name">Nombre de Material</th>
              <th>Disponible</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="x in datosalmacen | filter:q1 | startFromGrid: currentPage * pageSize | limitTo: pageSize">
              <td>{{x.id}}</td>
              <td>{{x.codigo}}</td>
              <td>{{x.nom_material}} {{x.descripcion}}</td>
              <td>{{x.tot}}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <!--inicio de paginacion-->
    <div class="row" ng-hide="listaexiste">
      <div class="col s12 m12 l12 center">
        <pagination page="1" page-size="pageSize" total="datosalmacen.length" show-prev-next="true" use-simple-prev-next="false" dots="..." hide-if-empty="false" adjacente="2" scroll-top="false" pagination-action="setPage(page)" />
      </div>
    </div>
    <!--fin de paginacion-->
    <div class="row" ng-hide="listanoexiste">
      <div id="advertencia" class="col s12 m12 l12">
        <p>No hay registros que mostrar!!! <i class="tiny material-icons">error</i></p>
      </div>
    </div>
  </div>
  <div id="modalpdfno" class="modal" style="width:100%;height:600px !important;">
    <div class="modal-content" >
      <object ng-show="facturapdf" width="100%" height="1000px" data="{{facturapdf}}" type="application/pdf"></object>
    </div>
  </div>
</div>