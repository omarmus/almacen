<div class="row" ng-controller="ctrlcategorias">
	  <nav>
    <div class="nav-wrapper" id="contenido">
      <a class="brand-logo left" id="logopersona"><i class="material-icons left">recent_actors</i>Categorias de Materiales</a>
      <ul id="nav-mobile" class="right">
        <li><a id="btnNuevo" class="waves-light btn" data-target='formCategoria' modal ng-click="openformulario()">Nueva Categoria</a></li>
      </ul>
    </div>
  </nav>
  <form class="col s12 m12 l12 full" id="btnbusqueda">
      <div class="row">
        <div class="input-field col s12 m12 l12">
          <input id="search" type="text" class="validate" ng-model="q" ng-change="currentPage = 0">
          <label for="search">Busqueda</label>
        </div>
      </div>
  </form>

<div class="">
    <div class="row" ng-hide="listaexiste">
      <div class="col s12 m12 l12">
        <table class="striped" ng-init='configPages()'>
          <thead>
            <tr>
              <th data-field="id">Id</th>
              <th data-field="tipo">Tipo</th>
              <th data-field="name">Nombre de Categoria</th>
              <th data-field="opciones" colspan="2">Opciones</th>
            </tr>
          </thead>
          <tbody >
            <tr ng-repeat="x in datoscat | filter:q | startFromGrid: currentPage * pageSize | limitTo: pageSize">
              <td>{{x.id}}</td>
              <td>{{x.tipo}}</td>
              <td>{{x.nombre_cat}}</td>
              <td id="col1"><a href="" ng-click="abrirformeditacate(x.id)" id="btn1" tooltipped data-position="left" data-delay="40" data-tooltip="Editar"><i class="small material-icons">edit</i></a></td>
              <td id="col1"><a href="" ng-click="abrirformelimina(x.id)" id="btn2" data-target='formeliminar' modal tooltipped data-position="right" data-delay="40" data-tooltip="Eliminar"><i class="small material-icons">delete</i></a></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <!--inicio de paginacion-->
    <div class="row" ng-hide="listaexiste">
      <div class="col s12 m12 l12 center">
        <pagination page="1" page-size="pageSize" total="datoscat.length" show-prev-next="true" use-simple-prev-next="false" dots="..." hide-if-empty="false" adjacente="2" scroll-top="false"  pagination-action="setPage(page)"/>
      </div>
    </div>
    <!--fin de paginacion-->
    <div class="row" ng-hide="listanoexiste">
      <div id="advertencia" class="col s12 m12 l12">
        <p>No hay registros que mostrar!!!.Haga Click en <i class="tiny material-icons">done</i>  Nueva Categoria</p>
      </div>
    </div>
</div>
<!-- Modal Structure -->
<div id="formCategoria" class="modal">
  <div class="modal-content">
      <h4 id="titulo-modal"></h4>
      <!--FORMULARIO-->
  <form name="Formcate">
  <div class="row" >
    <div class="input-field col s4 m4 l4">
      <input class="txterror" type="text" name="cattipo" ng-model="Formcate.tipo" required ng-pattern="/^[a-zA-ZÁ-ÿ\_\-\.\s\xF1\xD1]*$/" ng-class='{ error: !Formcate.$pristine, warning: Formcate.cattipo.$error.pattern}' >
      <label>Tipo:</label>
      <span  class="warning" ng-show="Formcate.cattipo.$error.pattern">Ingrese Tipo valido!</span>
    </div>
    <div class="input-field col s8 m8 l8">
      <input type="text" class="txterror" ng-model="Formcate.nomcat" name="nombrecat" required  ng-pattern="/^[a-zA-ZÁ-ÿ\_\-\.\s\xF1\xD1]*$/" ng-class='{ error: !Formcate.$pristine, warning: Formcate.nombrecat.$error.pattern}'>
      <label>Nombre de Categoria:</label>
      <span class="warning" ng-show="Formcate.nombrecat.$error.pattern">Ingrese un nombre de Categoria Valido!</span>
    </div>
  </div>
  </form>
  </div>
    <div class="modal-footer" id="piemodalpersona">
      <a id="btnModificar" class="waves-effect waves-light btn  blue accent-4" ng-click="editarCate()" ><i class="material-icons left">restore</i>Modificar</a>
      <a id="btnCrear" class="waves-effect waves-light btn blue accent-4" ng-click="crearcategoria()" ng-disabled='!Formcate.$valid'><i class="material-icons left">add</i>Crear</a>
      <a class="modal-action modal-close waves-effect waves-light btn red"><i class="material-icons left">close</i>Close</a>
    </div>
</div>
<!--empiezo de modal de eliminacion-->
<div id="formeliminar" class="modal">
  <div class="modal-content">
        <h7>ESTA SEGURO DE ELIMINAR!!!</h7>
  </div>
    <div class="modal-footer">
      <div class="row">
        <div class="col s6">
          <a  class="waves-effect waves-light btn blue" ng-click="eliminarcategoria()" ><i class="material-icons left">add</i>Aceptar</a>
        </div>
        <div class="col s6">
          <a class="modal-action modal-close waves-effect waves-light btn red"><i class="material-icons left">close</i>Cancelar</a>
        </div>
      </div>
    </div>
</div>
<!--fin de modal de eliminacion-->
</div>