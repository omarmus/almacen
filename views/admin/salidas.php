<div class="row" ng-controller="ctrlsalidas">
	  <nav>
    <div class="nav-wrapper" id="contenido">
      <a class="brand-logo left" id="logopersona"><i class="material-icons left">person</i>Registros de Salidas</a>
    </div>
  </nav>
  <form class="col s12 m12 l12 full" id="btnbusqueda">
      <div class="row">
        <div class="input-field col s12 m12 l12">
          <input id="search" type="text" class="validate" ng-model="q" ng-change="currentPage = 0">
          <label for="search">Busqueda</label>
        </div>
      </div>
  </form>
  <div class="container-full">
    <div class="row" ng-show="datossalidas != 0">
      <table class="striped" ng-init='configPages()'>
          <thead>
            <tr>
              <th>Item</th>
              <th>Funcionario</th>
              <th>Cargo</th>
              <th>Area</th>
              <th>Fecha de Entrega</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="x in datossalidas | filter:q | startFromGrid: currentPage * pageSize | limitTo: pageSize">

              <td>{{x.id}}</td>
              <td>{{x.nombres}} {{x.appaterno}} {{x.apmaterno}}</td>
              <td>{{x.nomcargo}}</td>
              <td>{{x.descripcion}}</td>
              <td>{{x.fecha_entrega}}</td>
              <td><a href="" ng-click="detalleunasalida(x.id)" tooltipped data-position="left" data-delay="40" data-tooltip="Detalle"><i class="small material-icons" style="margin-top: 10px; color: #01579b;">info</i></a></td>
              <td><a href="" ng-click="imprimir(x.id)" tooltipped data-position="top" data-delay="40" data-tooltip="Imprimir Informe"><i class="small material-icons" style="margin-top: 10px; color: #01579b;">print</i></a></td>
            </tr>
          </tbody>
      </table>
    </div>
    <div class="row">
      <div class="col s12 m12 l12 center">
        <pagination page="1" page-size="pageSize" total="datossalidas.length" show-prev-next="true" use-simple-prev-next="false" dots="..." hide-if-empty="false" adjacente="2" scroll-top="false"  pagination-action="setPage(page)"/>
      </div>
    </div>
    <div class="row" ng-show="mostrardetalle">
      <div class="col s12 m12"><!--  listarpedidoIngreso()detalleuningreso()-->
        <div class="row" style="background-color: grey; color: #f4f4f9; border-radius: 5px 0px 5px 0px;box-shadow: 1px 3px 3px #677474;">
          <div class="col s11 center-align">
            <h5>Detalle de Entrega de Materiales </h5>
          </div>
          <div class="col s1 right-align">
            <a href=""  ng-click="ocultadetalle()" tooltipped data-position="left" data-delay="40" data-tooltip="Cerrar Detalle"><i class="small material-icons" style="margin-top: 10px; color: #f4f4f9;">close</i></a>
          </div>
        </div>
        <table>
          <thead>
            <tr>
              <th>Item</th>
              <th>Codigo</th>
              <th>Material</th>
              <th>Unidad</th>
              <th>Pedido_id</th>
              <th>Cantidad/solicitada</th>
              <th>Cantidad/entregada</th>
              <th>Observaciones</th>
              <th>Fecha de entrega</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="x in undetalle">
              <td>{{$index+1}}</td>
              <td>{{x.codigo}}</td>
              <td>{{x.nom_material}} {{x.descripcion}}</td>
              <td>{{x.presentacion}}</td>
              <td>{{x.pedido_id}}</td>
              <td>{{x.q}}</td>
              <td>{{x.qe}}</td>
              <td>{{x.obs}}</td>
              <td>{{x.fechped}}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <div class="row" ng-if="datossalidas == 0">
      <div id="advertencia" class="col s12 m12 l12">
        <p>No hay registros que mostrar!!!.Haga Click en <i class="tiny material-icons">done</i>  Nueva Area</p>
      </div>
    </div>
  </div>
</div>