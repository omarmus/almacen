<div class="row" ng-controller="ctrlregEntrada">
	  <nav>
    <div class="nav-wrapper" id="contenido">
      <a class="brand-logo left" id="logopersona"><i class="material-icons left">person</i>Ingreso de Bienes y Materiales</a>

      <ul id="nav-mobile" class="right">
        <a class="waves-effect waves-light btn"  data-target='modal3' modal ng-click="openformulario()">Nuevo Ingreso</a>
      </ul>
    </div>
  </nav>
  <form class="col s12 m12 l12 full" id="btnbusqueda">
      <div class="row">
        <div class="input-field col s12 m12 l12">
          <input id="search" type="text" class="validate" ng-model="q" ng-change="currentPage = 0">
          <label for="search">Busqueda</label>
        </div>
      </div>
  </form>
  <div class="container-full">
    <div class="row" ng-show="datospedidoingresos != 0">
      <table class="striped" ng-init='configPages()'>
          <thead>
            <tr>
              <th>Item</th>
              <th>Partida Presup.</th>
              <th>Empresa</th>
              <th>NIT</th>
              <th>Propietario</th>
              <th>N° Factura</th>
              <th>Fuente</th>
              <th>Fecha</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="x in datospedidoingresos | filter:q | startFromGrid: currentPage * pageSize | limitTo: pageSize">

              <td>{{x.id}}</td>
              <td ng-hide="true">{{x.proveedor_id}}</td>
              <td>{{x.partida_presup}}</td>
              <td>{{x.empresa}}</td>
              <td>{{x.Nit}}</td>
              <td>{{x.nombres}} {{x.apellidos}}</td>
              <td>{{x.Nro_factura}}</td>
              <td>{{x.fuente}}</td>
              <td>{{x.fecha_creacion}}</td>
              <td><a href="" ng-click="detalleuningreso(x.id)" tooltipped data-position="left" data-delay="40" data-tooltip="Detalle"><i class="small material-icons" style="margin-top: 10px; color: #01579b;">info</i></a></td>
              <td><a href="" data-target='modalpdf' modal ng-click="imprimir(x.id,x.proveedor_id)" tooltipped data-position="top" data-delay="40" data-tooltip="Imprimir Informe"><i class="small material-icons" style="margin-top: 10px; color: #01579b;">print</i></a></td>
            </tr>
          </tbody>
      </table>
    </div>
    <div class="row">
      <div class="col s12 m12 l12 center">
        <pagination page="1" page-size="pageSize" total="datospedidoingresos.length" show-prev-next="true" use-simple-prev-next="false" dots="..." hide-if-empty="false" adjacente="2" scroll-top="false"  pagination-action="setPage(page)"/>
      </div>
    </div>
    <div class="row" ng-show="mostrardetalle">
      <div class="col s12 m12"><!--  listarpedidoIngreso()detalleuningreso()-->
        <div class="row" style="background-color: grey; color: #f4f4f9; border-radius: 5px 0px 5px 0px;box-shadow: 1px 3px 3px #677474;">
          <div class="col s11 center-align">
            <h5>Detalle de Ingreso de Bienes y Materiales </h5>
          </div>
          <div class="col s1 right-align">
            <a href=""  ng-click="ocultadetalle()" tooltipped data-position="left" data-delay="40" data-tooltip="Cerrar Detalle"><i class="small material-icons" style="margin-top: 10px; color: #f4f4f9;">close</i></a>
          </div>
        </div>
        <table>
          <thead>
            <tr>
              <th>Item</th>
              <th>Codigo</th>
              <th>Material</th>
              <th>Unidad</th>
              <th>Pedido_id</th>
              <th>N° Factura</th>
              <th>Fuente</th>
              <th>Cantidad</th>
              <th>P/Unitario</th>
              <th>Total</th>
              <th>Fecha</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="x in undetalle">
              <td>{{$index+1}}</td>
              <td>{{x.codigo}}</td>
              <td>{{x.nom_material}} {{x.descripcion}}</td>
              <td>{{x.presentacion}}</td>
              <td>{{x.pedido_id}}</td>
              <td>{{x.Nro_factura}}</td>
              <td>{{x.fuente}}</td>
              <td>{{x.q}}</td>
              <td>{{x.precio_unitario}}</td>
              <td>{{x.q * x.precio_unitario}} Bs.</td>
              <td>{{x.fechped}}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <div class="row" ng-if="datospedidoingresos == 0">
      <div id="advertencia" class="col s12 m12 l12">
        <p>No hay registros que mostrar!!!.Haga Click en <i class="tiny material-icons">done</i>  Nueva Area</p>
      </div>
    </div>
  </div>
  <!-- Modal Structure -->
  <div id="modal3" class="modal bottom-sheet">
  <div class="modal-content">
    <h5 class="center-align">Registro De Ingreso De Bienes y Materiales</h5>
    <form novalidate class="full-width" name="formIB">
      <div class="row">
        <div class="col s6">
          <label>Seleccione Proveedor:</label>
          <select material-select class="browser-default" ng-model="selectprov" required>
            <option value="" disabled selected>Seleccione proveedor...</option>
            <option ng-repeat="x in datosprov" ng-value="{{x.id}}" ng-click="llenadatosprov($index)">{{x.nombres}} {{x.apellidos}} ({{x.empresa}})</option>
          </select>
        </div>
        <div class="input-field col s6">
          <input id="nit" name="nit" type="text" class="validate txterror" ng-model="nitprov" ng-pattern="/^[0-9]*$/" ng-class='{ error: !formIB.$pristine, warning: formIB.nit.$error.pattern}' required>
          <label for="nit">Nit:</label>
          <span class="warning" ng-show="formIB.nit.$error.pattern">Ingrese solo Nros!</span>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s3">
          <input id="presupuesto" name="presupuesto" type="text" class="validate txterror" ng-model="parpresup" ng-pattern="/^[0-9]*$/" ng-class='{ error: !formIB.$pristine, warning: formIB.presupuesto.$error.pattern}' required>
          <label for="presupuesto">Partida Presup.:</label>
          <span class="warning" ng-show="formIB.presupuesto.$error.pattern">Ingrese solo Nros!</span>
        </div>
        <div class="input-field col s3">
          <input id="factura" name="factura" type="text" class="validate txterror" ng-model="nrofact" ng-pattern="/^[0-9]*$/" ng-class='{ error: !formIB.$pristine, warning: formIB.factura.$error.pattern}' required>
          <label for="factura">N° de Factura:</label>
          <span class="warning" ng-show="formIB.factura.$error.pattern">Ingrese solo Nros!</span>
        </div>
        <div class="input-field col s6">
          <input id="fu" type="text" class="validate" ng-model="fuente" required>
          <label for="fu">Fuente:</label>
        </div>
      </div>
      <div class="row divform">
        <div class="col s5">
          <label>Seleccione Material:</label>
          <select material-select class="browser-default" ng-model="selectmat" required>
            <option value="" disabled selected>Seleccione material...</option>
            <option ng-repeat="x in datosmate" ng-value="{{x.id}}">{{x.nom_material}} {{x.descripcion}} ({{x.codigo}})</option>
          </select>
        </div>
        <div class="input-field col s2">
          <input id="cant" name="cant" type="text" class="validate txterror" ng-model="cantidad" ng-pattern="/^[0-9]*$/" ng-class='{ error: !formIB.$pristine, warning: formIB.cant.$error.pattern}' required>
          <label for="cant">cantidad:</label>
          <span class="warning" ng-show="formIB.cant.$error.pattern">Ingrese solo Nros!</span>
        </div>
        <div class="input-field col s2">
          <input id="pu" name="pu" type="text" class="validate txterror" ng-model="preciounit" ng-pattern="/^[0-9/./]*$/" ng-class='{ error: !formIB.$pristine, warning: formIB.pu.$error.pattern}' required>
          <label for="pu">P/U:</label>
          <span class="warning" ng-show="formIB.pu.$error.pattern">Ingrese solo Nros!</span>
        </div>
        <div class="col s3 right-align"><br>
          <button ng-click="llenarsolicitud()" class="waves-effect waves-teal btn-flat" type="submit" ng-disabled="!formIB.$valid"><i class="tiny material-icons left">add_to_photos</i>agregar</button>
        </div>
      </div>
      <div class="row" ng-hide="mostrarlista">
        <div class="col s12 m12 l12">
          <table class="centered  full-width" ng-show="agregarca != 0">
            <thead>
              <tr>
                  <th>Item</th>
                  <th>Codigo</th>
                  <th>Material</th>
                  <th>Presentacion</th>
                  <th>Disponibles</th>
                  <th>Cantidad</th>
                  <th>P/Unitario</th>
                  <th>Total</th>
                  <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="a in agregarca">
                <td>{{$index+1}}</td>
                <td>{{a.codigo}}</td>
                <td>{{a.nommate}} {{a.desc}}</td>
                <td>{{a.presentacion}}</td>
                <td>{{a.tot}}</td>
                <td>{{a.q}}</td>
                <td>{{a.pu}}</td>
                <td>{{a.q * a.pu}} Bs.</td>
                <td><a href="" ng-click="eliminaritem($index)" id="btn2" tooltipped data-position="right" data-delay="40" data-tooltip="Eliminar"><i class="small material-icons">delete</i></a></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </form>
  </div>
  <div class="modal-footer"><!--id="piemodalpersona"-->
      <a id="btnModificar" class="waves-effect waves-light btn  blue accent-4" ng-click="updateRegPer()"><i class="material-icons left">restore</i>Modificar</a>
      <a id="btnCrear" class="waves-effect waves-light btn blue accent-4" ng-click="confirmarasigancion()" ng-disabled="agregarca == 0"><i class="material-icons left">add</i>Crear</a>
      <a class="modal-action modal-close waves-effect waves-light btn red"><i class="material-icons left">close</i>Cancelar</a>
  </div>
  </div>
  <div id="modalpdf" class="modal" style="width:100%;height:600px !important;">
    <div class="modal-content" >
      <object ng-show="facturapdf" width="100%" height="800px" data="{{facturapdf}}" type="application/pdf"></object>
    </div>
  </div>

</div>