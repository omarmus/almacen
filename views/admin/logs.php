<div class="row" ng-controller="ctrllogs">
  <nav>
    <div class="nav-wrapper" id="contenido">
      <a class="brand-logo left" id="logopersona"><i class="material-icons left">update</i>Logs de auditoria</a>
    </div>
  </nav>
  <form class="col s12 m12 l12 full" id="btnbusqueda">
    <div class="row">
      <div class="input-field col s12 m12 l12">
        <input id="search" type="text" class="validate" ng-model="q" ng-change="currentPage = 0">
        <label for="search">Busqueda</label>
      </div>
    </div>
  </form>

  <div class="logs">
    <div class="row" ng-hide="listaexiste">
      <div class="col s12 m12 l12">
        <table class="striped" ng-init='configPages()'>
          <thead>
            <tr>
              <th data-field="id">Id</th>
              <th data-field="tipo">Tipo de operación</th>
              <th data-field="fecha">Fecha y hora de la operación</th>
              <th data-field="tabla">Sección del cambio</th>
              <!-- <th data-field="old">Antiguo valor</th>
              <th data-field="new">Nuevo valor</th> -->
              <th data-field="valor_alterado">Valores afectados en la operación</th>
              <!-- <th data-field="usuario">Usuario</th> -->
              <!-- <th data-field="ip">IP</th> -->
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="x in datoslogs | filter:q | startFromGrid: currentPage * pageSize | limitTo: pageSize">
              <td>{{x.id}}</td>
              <td>{{tipos[x.tipo]}}</td>
              <td>{{formatDate(x.fecha)}}</td>
              <td>En la sección <strong>{{x.tabla}}</strong></td>
              <!-- <td>{{x.old}}</td>
              <td>{{x.new}}</td> -->
              <td>
                <span ng-if="x.tipo === 'UPDATE'">
                  El registro con <strong>{{getID(x.old)}}</strong> fue {{tiposB[x.tipo]}}. <br> <em>Valores modificados:</em> {{x.valor_alterado || 'NINGUNO'}}
                </span>
                <span ng-if="x.tipo === 'DELETE'">
                  El registro con <strong>{{getID(x.old)}}</strong> fue {{tiposB[x.tipo]}}.
                </span>
                <span ng-if="x.tipo === 'INSERT'">
                  Se agregó un nuevo registro con los siguientes datos: <br>{{x.new.replace('|id=0 ', '')}}
                </span>
              </td>
              <!-- <td>{{x.usuario}}</td> -->
              <!-- <td>{{x.ip}}</td> -->
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <!--inicio de paginacion-->
    <div class="row" ng-hide="listaexiste">
      <div class="col s12 m12 l12 center">
        <pagination page="1" page-size="pageSize" total="datoslogs.length" show-prev-next="true" use-simple-prev-next="false" dots="..." hide-if-empty="false" adjacente="2" scroll-top="false" pagination-action="setPage(page)" />
      </div>
    </div>
    <!--fin de paginacion-->
    <div class="row" ng-hide="listanoexiste">
      <div id="advertencia" class="col s12 m12 l12">
        <p>No hay registros que mostrar!!!.</p>
      </div>
    </div>
  </div>
</div>