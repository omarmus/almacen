<div class="row" ng-controller="regsalidafun">
	  <nav>
    <div class="nav-wrapper" id="contenido">
      <a class="brand-logo left" id="logopersona"><i class="material-icons left">recent_actors</i>solicitar Material por Funcionario</a>
    </div>
  </nav>
  <br>
<div class="container-full">
      <div class="row" id="avanzadoform">
      <form name="Formpedido" novalidate>
        <div class="col s4" >
          <label>Seleccione Funcionario:</label>
          <select material-select class="browser-default" ng-model="selectfunnew" ng-change="listarMateriales(selectfunnew)" required>
            <option value="" disabled selected>Seleccione Funcionario...</option>
            <option ng-repeat="x in listfun" ng-value="$index" >{{x.nombres}} {{x.appaterno}} {{x.apmaterno}} Nº CI:({{x.ci}} {{x.exp}})</option>    
          </select>
        </div>
        <div class="col s4" >
          <label>Seleccione Material:</label>
          <select material-select class="browser-default" ng-model="Formpedido.selectmatenew" required>
            <option value="" disabled selected>Seleccione Material...</option>
            <option ng-repeat="x in datoscanaut" ng-value="{{x.material_id}}" ng-show="x.q >= {{x.ca}}" ng-click="obtieneca($index)">{{x.nom_material}} {{x.descripcion}} ({{x.codigo}})</option>    
          </select>
        </div>
        <div class="input-field col s2">
          <input  type="number" class="txterror" name="cant" ng-model="Formpedido.cantaut" required placeholder="ingrese N° cantidad" ng-pattern="/^[0-9]*$/" ng-class='{ error: !Formpedido.$pristine, warning: Formpedido.cant.$error.pattern}' ng-change="validaca()">
          <label  class="active">Cantidad a Pedir:</label>
          <span  class="warning" ng-show="Formpedido.cant.$error.pattern">Ingrese un N° Valido!</span>
          <span class="warning" ng-hide="error1">No puede pedir esa Cantidad!!!</span>
        </div>
        <div class="col s2">
          <br>
          <a ng-disabled="!Formpedido.$valid" id="btnNuevo" class="waves-yellow btn" ng-click="asignarcantaut()">Agregar</a>
        </div>
      </form>
      </div> 
      <div class="row">
        <div class="col s12">
            <table class="responsive-table bordered" ng-hide="mostraritems" ng-init="listarFuncionario()">
              <!---->
              <thead>
                <tr>
                    <th data-field="name">Material</th>
                    <th data-field="name">Descripción</th>
                    <th data-field="name">Codigo</th>
                    <th data-field="price">Cantidad</th>
                    <th>Opcion</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="a in agregarca">
                  <td ng-hide="true">{{a.material_id}}</td>
                  <td>{{a.nommate}}</td>
                  <td>{{a.descrip}}</td>
                  <td>{{a.codigo}}</td>
                  <td>{{a.ca}}</td>
                  <td><a href="" ng-click="eliminaritem($index)" id="btn2" tooltipped data-position="right" data-delay="40" data-tooltip="Eliminar"><i class="small material-icons">delete</i></a></td>
                </tr>
              </tbody>
            </table>  
        </div>
      </div> 
      <div class="row">
        <div class="col s8"></div>
        <div class="col s4">
          <a ng-if="agregarca != 0" id="btnNuevo" class="waves-light btn right" ng-click="confirmarasigancion()">Realizar Pedido</a>
        </div>
        <!--<object data="./pdf/{{filename}}" style="width: 100%; height: 400px;"></object>-->
      </div>
      <!--<div class="row">
        <div class="col s12">
          <ng-pdf template-url="viewer.php" canvasid="pdf" scale="1.5"></ng-pdf>
        </div>
      </div> -->        
</div> 
</div>