<div class="row" ng-controller="ctrlrepPersona">
	<nav>
    <div class="nav-wrapper" id="contenido">
      <a class="brand-logo left" id="logopersona"><i class="material-icons left">trending_up</i>Reporte por Persona</a>
    </div>
  </nav>
  <div class="alert alert-info">Para generar un Reporte Seleccione una fecha inicial y una fecha final</div>
  <!-- <div class="divider"></div> -->
  <div class="">
    <div class="row">
      <div class="col s2">
        <label>Fecha Inicial:</label>
        <input type="date" class="datepicker" ng-model="fechaini" name="inicio">
      </div>
      <div class="col s2">
        <label>Fecha Final:</label>
        <input type="date" class="datepicker" ng-model="fechafin" name="fin">
      </div>
      <div class="col s4">
        <label>Funcionario:</label>
        <select class="browser-default" material-select ng-model="idfun" ng-change="cambiardatos(idfun)" ng-disabled="fechaini == null || fechafin == null"><!-- ng-click="buscarFuncionario()" -->
          <option value="" disabled selected>Seleccione Funcionario...</option>
          <option  ng-repeat="x in datosfun" ng-value="{{x.id }}">({{ x.ci}}) {{x.nombres}} {{x.appaterno}} {{x.apmaterno}}</option>
        </select>
      </div>
      <div class="col s3">
        <br>
        <a ng-show="datosgral != ''" class="waves-effect waves-light btn" data-target='modalpdf' modal ng-click="pdf()">Pdf<i class="small material-icons left">print</i></a>
        <!-- <a class="waves-effect waves-light btn" ng-click="buscadatos()">Explorar</a> -->
      </div>
    </div>
  </div>
  <div class="divider"></div>
  <div class="container-full">
    <div class="row" id="rowreportes" ng-show="datosgral != ''">
      <div class="col s6 left-align">
        <!-- Generar Reporte en Pdf... -->
        &nbsp;
      </div>
      <div class="col s6 right-align" >
        <!-- <a class="btn" data-target='modalpdf' modal ng-click="pdf()">Pdf<i class="small material-icons left">print</i></a> -->
        <!-- <a class="waves-effect waves-light btn" ng-click="excel()">excel</a> -->
      </div>
    </div>
    <div class="row">
      <div class="col s12" ng-show="datosgral != ''">
      <table class="bordered" >
        <thead>
          <tr>
              <th>Item</th>
              <th>Codigo</th>
              <th>Mateial Suministro</th>
              <th>Unidad</th>
              <th>Solicitado</th>
              <th>Entregado</th>
              <th>Fecha de Entrega</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat = "x in datosgral">
            <td>{{$index+1}}</td>
            <td>{{x.codigo}}</td>
            <td>{{x.nom_material}}  {{x.descripcion}}</td>
            <td>{{x.presentacion}}</td>
            <td>{{x.q}}</td>
            <td>{{x.qe}}</td>
            <td>{{x.fecha_entrega}}</td>
          </tr>

        </tbody>
      </table>
      </div>
      <!-- <div class="col s12" ng-s  how="datosgral == 0">No Se encontraron resultados...</div> -->
    </div>
    <!-- <div class="divider"></div>
    <div class="row">
      <div class="col s12 center-align">abcdefghijklmnopqrstuvwxyz</div>
    </div> -->
  </div>
  <!-- <form class="col s12 m12 l12 full" id="btnbusqueda">
      <div class="row">
        <div class="input-field col s12 m12 l12">
          <input id="last_name" type="text" class="validate">
          <label for="last_name">Busqueda</label>
        </div>
      </div>
  </form> -->
<div id="modalpdf" class="modal" style="width:100%;height:600px !important;">
    <div class="modal-content" >
      <object ng-show="facturapdf" width="100%" height="800px" data="{{facturapdf}}" type="application/pdf"></object>
    </div>
</div>
</div>