 <div class="row" ng-controller="ctrlfuncionario" ng-cloack>
	  <nav>
    <div class="nav-wrapper" id="contenido">
      <a class="brand-logo left" id="logopersona"><i class="material-icons left">person</i>Funcionarios</a>
      <ul id="nav-mobile" class="right">
        <a class=" waves-light btn"  data-target='Formfuncionario' modal ng-click="openformulario()">Nuevo Funcionario</a>
      </ul>
    </div>
  </nav>
  <form class="col s12 m12 l12 full" id="btnbusqueda">
      <div class="row">
        <div class="input-field col s12 m12 l12">
          <input id="search" type="text" class="validate" ng-model="q" ng-change="currentPage = 0">
          <label for="search">Busqueda</label>
        </div>
      </div>
  </form>
  <div class="container-full">
    <div class="row" ng-hide="listaexiste">
      <div class="col s12 m12 l12">
        <table class="striped" ng-init = "configPages()">
        <thead>
          <tr>
              <th data-field="nro">id</th>
              <th data-field="ci">N° CI</th>
              <th data-field="exp">Exp</th>
              <th data-field="nombre">Nombres</th>
              <th data-field="appaterno">Paterno</th>
              <th data-field="apmaterno">Materno</th>
              <th data-field="direccion">Direccion</th>
              <th data-field="telefono">Telefono</th>
              <th data-field="correo">Correo</th>
              <th data-field="cargo">Cargo</th>
              <th data-field="opciones" colspan="2">Opciones</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="x in datosfun | filter:q | startFromGrid: currentPage * pageSize | limitTo: pageSize">
            <td>{{x.id}}</td>
            <td>{{x.ci}}</td>
            <td>{{x.exp}}</td>
            <td>{{x.nombres}}</td>
            <td>{{x.appaterno}}</td>
            <td>{{x.apmaterno}}</td>
            <td>{{x.direccion}}</td>
            <td>{{x.telefono}}</td>
            <td>{{x.correo}}</td>
            <td>{{x.nomcargo}}</td>
            <td id="col1"><a href="" ng-click="abrirformeditafun(x.id)" id="btn1" tooltipped data-position="left" data-delay="40" data-tooltip="Editar"><i class="small material-icons">edit</i></a></td>
            <td id="col1"><a href="" ng-click="abrirformelimina(x.id)" id="btn2" data-target='formeliminar' modal tooltipped data-position="top" data-delay="40" data-tooltip="Eliminar"><i class="small material-icons">delete</i></a></td>
          </tr>
        </tbody>
        </table>
      </div>
    </div>
    <!--inicio de paginacion-->
    <div class="row" ng-hide="listaexiste">
      <div class="col s12 m12 l12 center">
        <pagination page="1" page-size="pageSize" total="datosfun.length" show-prev-next="true" use-simple-prev-next="false" dots="..." hide-if-empty="false" adjacente="2" scroll-top="false"  pagination-action="setPage(page)"/>
      </div>
    </div>
    <!--fin de paginacion-->
    <div class="row" ng-hide="listanoexiste">
      <div id="advertencia" class="col s12 m12 l12">
        <p>No hay registros que mostrar!!!.Haga Click en <i class="tiny material-icons">done</i>Nuevo Funcionario</p>
      </div>
    </div>
  </div>
  <!-- Modal Structure -->
<div id="Formfuncionario" class="modal">
  <form name="Formfunc" novalidate>
  <div class="modal-content">
      <h4 id="titulo-modal"></h4>
   <div class="row" >
      <div class="row">
        <div class="input-field col s4">
          <input id="ci" type="text" class="txterror validate" ng-model="Formfunc.ci" name="cedula" required  ng-pattern="/^[0-9]*$/" ng-class='{ error: !Formfunc.$pristine, warning: Formfunc.cedula.$error.pattern}'>
          <label for="ci">Cedula de identidad</label>
          <span  class="warning" ng-show="Formfunc.cedula.$error.pattern">Ingrese un N° de CI valido!</span>
        </div>
        <div class="col s4">
          <label>Expedido:</label>
          <select class="browser-default" ng-model="Formfunc.expedido" material-select required>
            <option value="" disabled selected>Departamento:</option>
            <option value="LP">La Paz</option>
            <option value="BN">Beni</option>
            <option value="CH">Chuquisaca</option>
            <option value="CBBA">Cochabamba</option>
            <option value="OR">Oruro</option>
            <option value="PN">Pando</option>
            <option value="PS">Potosí</option>
            <option value="SC">Santa Cruz</option>
            <option value="TJ">Tarija</option>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s4">
          <label>Nombre</label>
          <input id="nom" type="text" class="txterror validate" ng-model="Formfunc.nom" name="nombrefun" required  ng-pattern="/^[a-zA-ZÁ-ÿ\_\-\.\s\xF1\xD1]*$/" ng-class='{ error: !Formfunc.$pristine, warning: Formfunc.nombrefun.$error.pattern}'>
          <!-- -->
          <span class="warning" ng-show="Formfunc.nombrefun.$error.pattern">Ingrese un nombre Valido!</span>
        </div>

        <div class="input-field col s4">
          <input id="appat" type="text" class="txterror validate" ng-model="Formfunc.appaterno" name="paterno" required  ng-pattern="/^[a-zA-ZÁ-ÿ\_\-\.\s\xF1\xD1]*$/" ng-class='{ error: !Formfunc.$pristine, warning: Formfunc.paterno.$error.pattern}'>
          <label for="appat">Apellido Paterno</label>
          <span class="warning" ng-show="Formfunc.paterno.$error.pattern">Ingrese un Apellido Valido!</span>
        </div>

        <div class="input-field col s4">
          <input id="apmat" type="text" class="txterror validate" ng-model="Formfunc.apmaterno" name="materno" required  ng-pattern="/^[a-zA-ZÁ-ÿ\_\-\.\s\xF1\xD1]*$/" ng-class='{ error: !Formfunc.$pristine, warning: Formfunc.materno.$error.pattern}'>
          <label for="apmat">Apellido Materno</label>
          <span class="warning" ng-show="Formfunc.materno.$error.pattern">Ingrese un Apellido Valido!</span>
        </div>
      </div>
      <div class="row" >
        <div class="col s4">
          <div class="input-field inline">
            <i class="material-icons prefix">email</i>
            <input id="emai" type="email" class="txterror validate" ng-model="Formfunc.correo" name="correofun" placeholder="ejemplo@email.com" required ng-class='{ error: !Formfunc.$pristine, warning: Formfunc.correofun.$error.email}'>
            <label for="emai" class="active">Correo Electronico</label>
            <span class="warning" ng-show="Formfunc.correofun.$error.email">Ingrese un Correo valido!</span>
          </div>
        </div>
        <div class="input-field col s4">
          <i class="material-icons prefix">phone</i>
          <input id="icon_telephone" type="text" class="txterror validate" ng-model="Formfunc.telefono" name="telefonofun" required  ng-pattern="/^[0-9]*$/" ng-class='{ error: !Formfunc.$pristine, warning: Formfunc.telefonofun.$error.pattern}'>
          <label for="icon_telephone">Telefono</label>
          <span class="warning" ng-show="Formfunc.telefonofun.$error.pattern">Ingrese un Nro Telefonico Valido!</span>
        </div>
        <div class="col s4 pt-field">
          <label>Sexo:</label>
          <input name="sexofun" type="radio" id="test1" ng-model="Formfunc.sexo" value="hombre" required/>
          <label for="test1">Hombre</label>
          <input name="sexofun" type="radio" id="test2" ng-model="Formfunc.sexo" value="mujer" required/>
          <label for="test2">Mujer</label>
        </div>
      </div>
      <div class="row">
        <div class="col s12" ng-hide="cargoedita">
        <input type="checkbox" class="filled-in" id="filled-in-box" ng-model="confirmed" ng-change="activa()"/>
        <label for="filled-in-box">Editar Cargo:</label>
        <!--<input type="checkbox" ng-model="confirmed" ng-change="change()" id="ng-change-example1" />-->
        </div>
        <div class="col s5">
            <label>Cargo:</label>
            <select ng-hide="cargonuevo" class="browser-default" ng-model="Formfunc.selectcargo" material-select required>
              <option value="" disabled selected>Seleccione Cargo:</option>
              <option  ng-repeat="x in cargo" ng-value="{{ x. id }}">{{  x.nomcargo }}</option>
            </select>

            <select ng-hide="cargoedita" class="browser-default" material-select ng-options="option.nomcargo for option in data.availableOptions track by option.id" ng-model="data.selectedOption" disabled ng-disabled="activaedicion">
            </select>
        </div>
        <div class="input-field col s7">
          <input id="direccion" type="text" ng-model="Formfunc.direccion" name="direccionfun" required class="validate">
          <label for="direccion">Direccion</label>
        </div>
      </div>
  </div>
  </div>
    <div class="modal-footer" id="piemodalpersona">
      <a id="btnModificar" class="waves-effect waves-light btn  blue accent-4" ng-click="editarFuncionario()"><i class="material-icons left">restore</i>Modificar</a>
      <a id="btnCrear" class="waves-effect waves-light btn blue accent-4" ng-disabled='!Formfunc.$valid' ng-click="createnuevoreg()" ><i class="material-icons left">add</i>Crear</a> <!---->
      <a class="modal-action modal-close waves-effect waves-light btn red"><i class="material-icons left">close</i>Close</a>
    </div>
  </form>
</div>
<!--empiezo de modal de eliminacion-->
<div id="formeliminar" class="modal">
  <div class="modal-content">
        <h7>ESTA SEGURO DE ELIMINAR!!!</h7>
  </div>
    <div class="modal-footer">
      <div class="row">
        <div class="col s6">
          <a  class="waves-effect waves-light btn blue" ng-click="eliminarfuncionario()" ><i class="material-icons left">add</i>Aceptar</a>
        </div>
        <div class="col s6">
          <a class="modal-action modal-close waves-effect waves-light btn red"><i class="material-icons left">close</i>Cancelar</a>
        </div>
      </div>
    </div>
</div>
</div>