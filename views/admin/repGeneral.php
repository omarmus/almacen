<div class="row" ng-controller="ctrlrepGeneral">
	<nav>
    <div class="nav-wrapper" id="contenido">
      <a class="brand-logo left" id="logopersona"><i class="material-icons left">trending_up</i>Reporte General</a>
    </div>
  </nav>
  <div class="alert alert-info">Para generar un Reporte Seleccione una fecha inicial y una fecha final</div>
  <!-- <div class="divider"></div> -->
  <div class="">
    <div class="row">
      <div class="col s3">
        <label>Fecha Inicial:</label>
        <input type="date" ng-model="fechaini">
      </div>
      <!-- <div class="col s2"></div> -->
      <div class="col s3">
        <label>Fecha Final:</label>
        <input type="date" ng-model="fechafin">
      </div>
      <div class="col s6"><br>
        <a class="waves-effect waves-light btn" ng-click="buscadatos()">Explorar</a>
        <a ng-if="repgral > []" class=" btn" data-target='modalpdf' modal ng-click="pdf()">Pdf<i class="small material-icons left">print</i></a>
      </div>
    </div>
  </div>
  <div class="divider"></div>
  <div class="container-full">
    <div class="row" id="rowreportes">
      <div class="col s6 left-align">
        <!-- Generar Reporte en Pdf... -->
        &nbsp;
      </div>
      <div class="col s6 right-align" ng-if="repgral > []">
        <!-- <a class=" btn" data-target='modalpdf' modal ng-click="pdf()">Pdf<i class="small material-icons left">print</i></a> -->
        <!-- <a class="waves-effect waves-light btn" ng-click="excel()">excel</a> -->
      </div>
    </div>
    <div class="row">
      <div class="col s12" ng-if="repgral > []">
        <!-- <div class="divider"></div> -->
      <table class="bordered"><!-- ng-if="repgral != 0"-->
        <thead>
          <tr>
              <th></th>
              <th>Codigo</th>
              <th>Tipo</th>
              <th>Material Suministro</th>
              <th>Unidad</th>
              <th>transaccion</th>
              <th>Fecha</th>
              <th></th>
              <th></th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="x in repgral">
            <td>{{x.id}}</td>
            <td>{{x.codigo}}</td>
            <td>{{x.tipo}}</td>
            <td>{{x.nom_material}}  {{x.descripcion}}</td>
            <td>{{x.presentacion}}</td>
            <td>{{x.nombre}}</td>
            <td>{{x.fecha_creacion}}</td>
            <td>{{x.estado_pedido_id}}</td>
            <td>{{x.fecha_entrega}}</td>
          </tr>
        </tbody>
      </table>
      </div>
      <div class="col s12" ng-if="repgral == 0">No Se encontraron resultados...</div>
    </div>
    <!-- <div class="divider"></div> -->
    <!-- <div class="row">
      <div class="col s12 center-align">abcdefghijklmnopqrstuvwxyz</div>
    </div> -->
  </div>
  <!-- <form class="col s12 m12 l12 full" id="btnbusqueda">
      <div class="row">
        <div class="input-field col s12 m12 l12">
          <input id="last_name" type="text" class="validate">
          <label for="last_name">Busqueda</label>
        </div>
      </div>
  </form> -->
<div id="modalpdf" class="modal" style="width:100%;height:600px !important;">
    <div class="modal-content" >
      <object ng-show="facturapdf" width="100%" height="800px" data="{{facturapdf}}" type="application/pdf"></object>
    </div>
</div>
</div>