<div class="row" ng-controller="ctrlregSalida">
	  <nav>
    <div class="nav-wrapper" id="contenido">
      <a class="brand-logo left" id="logopersona"><i class="material-icons left">add_shopping_cart
      </i>Solicitud de Materiales</a>
    </div>
  </nav>
  <br>
<div class="container-full">
  <div class="row">
    <div class="col s12">
      <ul tabs reload="allTabContentLoaded">
        <li class="tab col s4 m4"><a class="active" href="#test1" ng-click="listarAreas()">Materiales Asignados</a></li>
        <li class="tab col s4 m4"><a href="#test2" ng-click="nuevaasignacion()">SOLICITAR MATERIAL</a></li>
      </ul>
    </div>
    <div id="test1" class="col s12">
      <div class="row" ng-hide="listaexiste">
      <div class="col s12">
        <table class="striped" ng-init='configPages()'>
          <thead>
            <tr>
              <th data-field="tipo">Item</th>
              <th data-field="tipo">Codigo</th>
              <th data-field="name">Nombre de Material</th>
              <th data-field="name">Descripcion</th>
              <th data-field="name">C/A</th>
              <th data-field="name">Disponible</th>
            </tr>
          </thead>
          <tbody >
            <tr ng-repeat="x in datoscanaut | filter:q | startFromGrid: currentPage * pageSize | limitTo: pageSize">
              <td>{{$index+1}}</td>
              <td>{{x.codigo}}</td>
              <td>{{x.nom_material}}</td>
              <td>{{x.descripcion}}</td>
              <td>{{x.ca}}</td>
              <td>
                <i ng-show="x.q >= {{x.ca}}" class="accent-3 material-icons">check</i>
                <a href="" id="btn2" tooltipped data-position="top" data-delay="40" data-tooltip="Este material no esta disponible !"><i ng-show="x.q < {{x.ca}}" class="accent-2 material-icons">info</i></a>
              </td>
            </tr> 
          </tbody>
        </table>
      </div> 
      </div>
      <!--inicio de paginacion-->
      <div class="row" ng-hide="listaexiste">
        <div class="col s12 m12 l12 center">
          <pagination page="1" page-size="pageSize" total="datoscanaut.length" show-prev-next="true" use-simple-prev-next="false" dots="..." hide-if-empty="false" adjacente="2" scroll-top="false"  pagination-action="setPage(page)"/>
        </div>
      </div>
      <!--fin de paginacion-->        
      <div class="row" ng-hide="listanoexiste">
        <div id="advertencia" class="col s12 m12 l12">
          <p>No hay registros que mostrar!!! <i class="tiny material-icons">error</i></p> 
        </div>
      </div>
    </div>
    <div id="test2" class="col s12">
      <br>
      <div class="row" id="avanzadoform">
      <form name="Formpedido" novalidate>
        <div class="col s4" >
          <label>Seleccione Material:</label>
          <select material-select class="browser-default" ng-model="Formpedido.selectmatenew" required>
            <option value="" disabled selected>Seleccione Material...</option>
            <option ng-repeat="x in datoscanaut" ng-value="{{x.material_id}}" ng-show="x.q >= {{x.ca}}" ng-click="obtieneca($index)">{{x.nom_material}} {{x.descripcion}} ({{x.codigo}})</option>    
          </select>
        </div>
        <div class="input-field col s2">
          <input id="cant" type="number" class="txterror" name="cant" ng-model="Formpedido.cantaut" required placeholder="ingrese N° cantidad" ng-pattern="/^[0-9]*$/" ng-class='{ error: !Formpedido.$pristine, warning: Formpedido.cant.$error.pattern}' ng-change="validaca()">
          <label for="cant" class="active">Cantidad a Pedir:</label>
          <span  class="warning" ng-show="Formpedido.cant.$error.pattern">Ingrese un N° Valido!</span>
          <span class="warning" ng-hide="error1">No puede pedir esa Cantidad!!!</span>
        </div>
        <div class="col s2">
          <br>
          <a ng-disabled="!Formpedido.$valid" id="btnNuevo" class="waves-yellow btn" ng-click="asignarcantaut()">Agregar</a>
        </div>
      </form>
      </div> 
      <div class="row">
        <div class="col s12">
            <table class="responsive-table bordered" ng-hide="mostraritems" ng-init="buscarareadefun()">
              <!---->
              <thead>
                <tr>
                    <th data-field="name">Material</th>
                    <th data-field="name">Descripción</th>
                    <th data-field="name">Codigo</th>
                    <th data-field="price">Cantidad</th>
                    <th>Opcion</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="a in agregarca">
                  <td ng-hide="true">{{a.material_id}}</td>
                  <td>{{a.nommate}}</td>
                  <td>{{a.descrip}}</td>
                  <td>{{a.codigo}}</td>
                  <td>{{a.ca}}</td>
                  <td><a href="" ng-click="eliminaritem($index)" id="btn2" tooltipped data-position="right" data-delay="40" data-tooltip="Eliminar"><i class="small material-icons">delete</i></a></td>
                </tr>
              </tbody>
            </table>  
        </div>
      </div> 
      <div class="row">
        <div class="col s8"></div>
        <div class="col s4">
          <a ng-if="agregarca != 0" id="btnNuevo" class="waves-light btn right" ng-click="confirmarasigancion()">Realizar Pedido</a>
        </div>
        <!--<object data="./pdf/{{filename}}" style="width: 100%; height: 400px;"></object>-->
      </div>     
    </div>
    
  </div>
</div>
</div>