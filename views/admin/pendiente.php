<div class="row" ng-controller="ctrladmin">
    <nav>
    <div class="nav-wrapper" id="contenido">
      <a class="brand-logo left" id="logopersona"><i class="material-icons left">trending_up</i>Solicitud de Materiles Pendientes</a>
    </div>
    </nav>
    <div class="row">
    <div class="col s1"></div>
    <div class="col s10"   ng-show="todopendientes.length > 0">
		    <table class="striped" ng-init="abrirpendientes()">
        <thead>
          <tr>
            <th data-field="id">Item</th>
            <th data-field="id">Funcionario</th>
            <th data-field="id">Area</th>
            <th data-field="tipo">Cargo</th>
            <th data-field="tipo">Fecha</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="x in todopendientes">
            <td ng-hide="true">{{x.id}}</td>
            <td ng-hide="true">{{x.aid}}</td>
            <td>{{$index+1}}</td>
            <td>{{x.nombres}} {{x.appaterno}} {{x.apmaterno}}</td>
            <td>{{x.descripcion}}</td>
            <td>{{x.nomcargo}}</td>
            <td>{{x.fecha_creacion}}</td>
            <td class="right-align">
              <!--<a class="waves-effect waves-yellow btn" ng-click="undetalle(x.id)"><i class="material-icons left">attach_file</i>detalle</a>-->
              <a href="" ng-click="undetalle(x.id,x.aid)" tooltipped data-position="left" data-delay="40" data-tooltip="Detalle"><i class="small material-icons" style="margin-top: 10px; color: #01579b;">info</i></a>
            </td>
          </tr> 
        </tbody>
        </table>
	  </div>
	  <div class="col s1"></div>
    </div>
	
	<div class="col s12" ng-show="listaver">
		<div class="row" style="background-color: grey; color: #f4f4f9; border-radius: 5px 0px 5px 0px;box-shadow: 1px 3px 3px #677474;">
			<div class="col s11 center-align">
				<h5>Detalle de Materiales Solicitados</h5>
			</div>
			<div class="col s1 right-align">
				<a href=""  ng-click="ocultadetalle()" tooltipped data-position="left" data-delay="40" data-tooltip="Cerrar Detalle"><i class="small material-icons" style="margin-top: 10px; color: #f4f4f9;">close</i></a>
			</div>	
		</div>
		<table class="striped"><!---->
          	<thead>
            	<tr>
                <th>Item</th>
              	<th>Codigo</th>
              	<th>Tipo</th>
              	<th>Material Suministro</th>
              	<th>Unidad</th>
                <th>C/A</th>
              	<th>C/S</th>
                <th>C/E</th>
                <th>Obs</th>
                <th>Disponible</th>
                <th>Editar</th>
            	</tr>
          	</thead>
          	<tbody >
            	<tr ng-repeat="a in detalle">
                <td>{{$index+1}}</td>
              	<td>{{a.codigo}}</td>
              	<td>{{a.tipo}}</td>
              	<td>{{a.nom_material}} {{a.descripcion}}</td>
              	<td>{{a.presentacion}}</td>
                <td>{{a.ca}}</td>
              	<td>{{a.q}}</td>
                <td ng-show="a.qe == null">{{a.q}}</td>
                <td ng-show="a.qe != null">{{a.qe}}</td>
                <td>{{a.obs}}</td>
                <td>{{a.tot}}</td>
                <td><a href="" data-target='modalentrega' modal ng-click="editarmatTransaccion($index)"><i class="material-icons">edit</i></a></td>
            	</tr>
              <tr>
                <td colspan="11"><a ng-click="entregarpedido()" data-target='modalentrega' modal class="btn waves-green right"><i class="material-icons left">file_upload</i>Entregar</a></td>
              </tr> 
          	</tbody>
        </table>
	</div>
  <!-- Modal Structure -->
  <div id="modalentrega" class="modal">
    <div class="modal-content">
      <h4>Editar Material a Entregar</h4>
      <form name="FomcambiMat">
        <div class="row">
          <div class="input-field col s2">
            <input id="cod" type="text" ng-model="codigo" ng-disabled="true">
            <label for="cod">Codigo:</label>  
          </div>
          <div class="input-field col s8">
            <input id="mat" type="text" ng-model="material" ng-disabled="true">
            <label for="mat">Material Solicitado:</label>
          </div>
          <div class="input-field col s2">
            <input id="canta" type="text" ng-model="ca" ng-disabled="true">
            <label for="canta">C/A</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s2">
            <input id="cants" type="text" ng-model="cs" ng-disabled="true">
            <label for="cants">C/S</label>
          </div>
          <div class="input-field col s2">
            <input id="cante" type="text" class="validate" ng-model="ce" required>
            <label for="cante" class="active">C/E</label>
          </div>
          <div class="input-field col s8">
            <input id="obs" type="text" class="validate" ng-model="obs" required>
            <label for="obs" class="active">Observación</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s4" ng-hide="true"><!---->
            <input id="tid" type="text" class="validate" ng-model="tid">
            <label for="tid" class="active">Observación</label>
          </div>
        </div>
      </form>
    </div>
    <div class="modal-footer">
      <a href="" ng-disabled="!FomcambiMat.$valid" class="waves-effect waves-green btn-flat" ng-click="confirmarcambioPedido()">Confirmar</a>
      <a href="" class=" modal-action modal-close waves-effect waves-green btn-flat">Cancelar</a>
    </div>
  </div>

</div>