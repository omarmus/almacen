<div class="row" ng-controller="ctrlcargos">
	  <nav>
    <div class="nav-wrapper" id="contenido">
      <a class="brand-logo left" id="logopersona"><i class="material-icons left">trending_up</i>Cargos</a>
      <ul id="nav-mobile" class="right">
        <li><a class="waves-light btn" data-target='formCargo' modal ng-click="openformulario()">Nuevo Cargo</a></li>
      </ul>
    </div>
  </nav>
  <form class="col s12 m12 l12 full" id="btnbusqueda">
      <div class="row">
        <div class="input-field col s12 m12 l12">
          <input id="search" type="text" class="validate" ng-model="q" ng-change="currentPage = 0"><!--ng-change="buscararea(datosbus)"-->
          <label for="search">Busqueda</label>
        </div>
      </div>
  </form>
<div class="">
  <div class="row" ng-hide="listaexiste">
    <div class="col s12 m12 l12">
      <table class="striped" ng-init='configPages()'>
          <thead>
            <tr>
                <th data-field="id">Id</th>
                <th data-field="name">Nombre de Cargo</th>
                <th data-field="name">Dependiente del Área</th>
                <th data-field="price" colspan="2">Opciones</th>
            </tr>
          </thead>
          <tbody >
            <tr ng-repeat="x in datoscargo | filter:q | startFromGrid: currentPage * pageSize | limitTo: pageSize">
              <td>{{x.id}}</td>
              <td>{{x.nomcargo}}</td>
              <td>{{x.descripcion}}</td>
              <td id="col1"><a href="" ng-click="abrirformeditacargo(x.id)" id="btn1" tooltipped data-position="left" data-delay="40" data-tooltip="Editar"><i class="small material-icons">edit</i></a></td>
              <td id="col1"><a href="" ng-click="abrirformelimina(x.id)" id="btn2" data-target='formeliminar' modal tooltipped data-position="right" data-delay="40" data-tooltip="Eliminar"><i class="small material-icons">delete</i></a></td>
            </tr>
          </tbody>
        </table>
    </div>
  </div>
  <!--inicio de paginacion-->
    <div class="row" ng-hide="listaexiste">
      <div class="col s12 m12 l12 center">
        <pagination page="1" page-size="pageSize" total="datoscargo.length" show-prev-next="true" use-simple-prev-next="false" dots="..." hide-if-empty="false" adjacente="2" scroll-top="false"  pagination-action="setPage(page)"  />
      </div>
    </div>
    <!--fin de paginacion-->
    <div class="row" ng-hide="listanoexiste">
      <div id="advertencia" class="col s12 m12 l12">
        <p>No hay registros que mostrar!!!.Haga Click en <i class="tiny material-icons">done</i>  Nuevo Cargo</p>
      </div>
    </div>
</div>
   <!-- Modal Structure -->
<div id="formCargo" class="modal">
  <div class="modal-content">
      <h4 id="titulo-modal"></h4>
      <!--FORMULARIO-->
  <div class="row" >
    <form  name="Formcargos" novalidate>
      <div class="input-field col s12">
        <input id="cargo" type="text" class="validate" name="nomcargo" ng-model="Formcargos.nomcargo" required ng-class='{ error: Formcargos.nomcargo.$invalid && !Formcargos.$pristine}' ng-pattern="/^[a-zA-ZÁ-ÿ\_\-\.\s\xF1\xD1]*$/"><!--ng-pattern="/^[a-zA-Z]*$/" $error.pattern-->
        <label for="cargo" data-error="Este Campo es Obligatorio!!!" data-success="valido">Cargo</label>
      </div>
      <div class="col s12" ng-hide="cargoedita">
        <input type="checkbox" class="filled-in" id="filled-in-box" ng-model="confirmed" ng-change="activa()"/>
        <label for="filled-in-box">Editar Area:</label>
        <!--<input type="checkbox" ng-model="confirmed" ng-change="change()" id="ng-change-example1" />-->
      </div>
      <div class="col s12">
        <label>Área:</label>

        <select ng-hide="cargonuevo" class="browser-default" material-select  ng-model='Formcargos.nomarea' required ng-class='{ error: Formcargos.nomarea.$invalid && !Formcargos.$pristine }'>
          <option value="" disabled selected>Seleccione Área:</option>
          <option  ng-repeat="x in datosarea" ng-value="{{x.id }}">{{ x.descripcion}}</option>
        </select>

        <select ng-hide="cargoedita" class="browser-default" material-select ng-options="option.descripcion for option in data.availableOptions track by option.id" ng-model="data.selectedOption" disabled ng-disabled="activaedicion">
        </select>
      </div>
    </form>
  </div>

  </div>
    <div class="modal-footer" id="piemodalpersona">
      <a id="btnModificar" class="waves-effect waves-light btn  blue accent-4" ng-click="editarCargo()"><i class="material-icons left">restore</i>Modificar</a>
      <a id="btnCrear" class="waves-effect waves-light btn blue accent-4" ng-click="createnuevoreg()" ng-disabled='!Formcargos.$valid'><i class="material-icons left">add</i>Crear</a>
      <a class="modal-action modal-close waves-effect waves-light btn red"><i class="material-icons left">close</i>Close</a>
    </div>
</div>
<!--empiezo de modal de eliminacion-->
<div id="formeliminar" class="modal">
  <div class="modal-content">
        <h7>ESTA SEGURO DE ELIMINAR!!!</h7>
  </div>
    <div class="modal-footer">
      <div class="row">
        <div class="col s6">
          <a  class="waves-effect waves-light btn blue" ng-click="eliminarcargo()" ><i class="material-icons left">add</i>Aceptar</a>
        </div>
        <div class="col s6">
          <a class="modal-action modal-close waves-effect waves-light btn red"><i class="material-icons left">close</i>Cancelar</a>
        </div>
      </div>
    </div>
</div>
</div>