<div class="row" ng-controller="ctrlmateriales">
	  <nav>
    <div class="nav-wrapper" id="contenido">
      <a class="brand-logo left" id="logopersona"><i class="material-icons left">recent_actors</i>Materiales</a>
      <ul id="nav-mobile" class="right">
        <li><a id="btnNuevo" class="waves-light btn" data-target='formMaterial' modal ng-click="openformulario()">Nuevo Material</a></li>
      </ul>
    </div>
  </nav>
  <form class="col s12 m12 l12 full" id="btnbusqueda">
      <div class="row">
        <div class="input-field col s12 m12 l12">
          <input id="search" type="text" class="validate" ng-model="q" ng-change="currentPage = 0">
          <label for="search">Busqueda</label>
        </div>
      </div>
  </form>

<div class="container-full">
    <div class="row" ng-hide="listaexiste">
      <div class="col s12 m12 l12">
        <table class="striped" ng-init='configPages()'>
          <thead>
            <tr>
              <th data-field="id">Id</th>
              <th data-field="tipo">Codigo</th>
              <th data-field="name">Nombre de Material</th>
              <th data-field="name">Descripcion</th>
              <th data-field="name">Presentacion</th>
              <th data-field="name">Categoria/tipo</th>
              <th data-field="name">Minimo/Inventario</th>
              <th data-field="name">Activo</th>
              <th data-field="opciones" colspan="2">Opciones</th>
            </tr>
          </thead>
          <tbody >
            <tr ng-repeat="x in datosmat | filter:q | startFromGrid: currentPage * pageSize | limitTo: pageSize">
              <td>{{x.id}}</td>
              <td>{{x.codigo}}</td>
              <td>{{x.nom_material}}</td>
              <td>{{x.descripcion}}</td>
              <td>{{x.presentacion}}</td>
              <td>{{x.tipo}}</td>
              <td>{{x.min_inventario}}</td>
              <td><i class="material-icons" ng-if="x.activo==1" id="activo">check</i></td>
              <td id="col1"><a href="" ng-click="abrirformeditamate(x.id)" id="btn1" tooltipped data-position="left" data-delay="40" data-tooltip="Editar"><i class="small material-icons">edit</i></a></td>
              <td id="col1"><a href="" ng-click="abrirformelimina(x.id)" id="btn2" data-target='formeliminar' modal tooltipped data-position="right" data-delay="40" data-tooltip="Eliminar"><i class="small material-icons">delete</i></a></td>
            </tr> 
          </tbody>
        </table>
      </div> 
    </div>
    <!--inicio de paginacion-->
    <div class="row" ng-hide="listaexiste">
      <div class="col s12 m12 l12 center">
        <pagination page="1" page-size="pageSize" total="datosmat.length" show-prev-next="true" use-simple-prev-next="false" dots="..." hide-if-empty="false" adjacente="2" scroll-top="false"  pagination-action="setPage(page)"/>
      </div>
    </div>
    <!--fin de paginacion-->        
    <div class="row" ng-hide="listanoexiste">
      <div id="advertencia" class="col s12 m12 l12">
        <p>No hay registros que mostrar!!!.Haga Click en <i class="tiny material-icons">done</i>  Nuevo Material</p> 
      </div>
    </div>
</div> 
<!-- Modal Structure -->
<div id="formMaterial" class="modal">
  <div class="modal-content">
      <h4 id="titulo-modal"></h4>
      <!--FORMULARIO-->
  <form name="Formmate" novalidate>
  <div class="row">
    <div class="input-field col s4 m4 l4">
      <input class="txterror" type="text" name="codigo" ng-model="Formmate.matcod" required ng-pattern="/^[0-9]*$/" ng-class='{ error: !Formmate.$pristine, warning: Formmate.codigo.$error.pattern}' placeholder="Ingrese solo números">
      <label class="active">Codigo:</label>
      <span  class="warning" ng-show="Formmate.codigo.$error.pattern">Ingrese N° de Codigo valido!</span>
    </div>
    <div class="col s3">
      <div ng-hide="activaedita">
        <label>Editar Tipo/Categoria:</label>
        <input type="checkbox" class="filled-in" id="filled-in-box" ng-model="confirmarcambio" ng-change="activa()"/>
        <label for="filled-in-box">Editar</label>
      </div>
    </div>
    <div class="col s5">
      <label >Tipo/Categoria:</label>
      <select ng-hide="tiponoedita" class="browser-default" ng-model="Formmate.mattipo" material-select required>
        <option value="" disabled selected>Seleccione</option>
        <option ng-repeat="x in datoscat" ng-value="{{x.id}}">{{x.tipo}}({{x.nombre_cat}})</option>
      </select>
      <select ng-hide="tipoedita" class="browser-default" material-select ng-options="option.tipo for option in data.availableOptions track by option.id" ng-model="data.selectedOption">
      </select>
    </div>
  </div>    
  <div class="row" >
    <div class="input-field col s6 m6 l6">
      <input class="txterror" type="text" name="nombre" ng-model="Formmate.nommat" required ng-pattern="/^[a-zA-ZÁ-ÿ\_\(\)\-\.\s\xF1\xD1]*$/" ng-class='{ error: !Formcate.$pristine, warning: Formmate.nombre.$error.pattern}' >
      <label>Nombre de Material:</label>
      <span  class="warning" ng-show="Formmate.nombre.$error.pattern">Ingrese un Nombre valido!</span>
    </div>
    <div class="input-field col s6 m6 l6">
      <input type="text" class="txterror" ng-model="Formmate.desmat" name="descripcion" required  ng-class='{ error: !Formmate.$pristine, warning: Formmate.descripcion.$error.pattern}'><!--ng-pattern="/^[a-z0-9A-ZÁ-ÿ\_\(\)\-\.\s\xF1\xD1]*$/" -->
      <label>Descripción:</label>
      <span class="warning" ng-show="Formmate.descripcion.$error.pattern">Ingrese descripcion del material Valido!</span>  
    </div>
  </div>
  <div class="row">
    <div class="input-field col s4 m4 l4">
      <input class="txterror" type="text" name="minimo" ng-model="Formmate.minmat" required ng-pattern="/^[0-9]*$/" ng-class='{ error: !Formmate.$pristine, warning: Formmate.minimo.$error.pattern}'>
      <label>Minima en Inventario:</label>
      <span class="warning" ng-show="Formmate.minimo.$error.pattern">Ingrese cantidad Minima en inventario Valido!</span>  
    </div>
    <div class="input-field col s4 m4 l4" ng-hide="aleditarmaterial">
      <!-- <input class="txterror" type="text" name="inicial" ng-model="Formmate.invinicial" required ng-pattern="/^[0-9]*$/" ng-class='{ error: !Formmate.$pristine, warning: Formmate.inicial.$error.pattern}'>
      <label>Inventario inicial:</label>
      <span class="warning" ng-show="Formmate.inicial.$error.pattern">Ingrese cantidad inicial en inventario Valido!</span> -->
    </div>
    <div class="col s4">
      <label>Presentación:</label>
      <select class="browser-default" ng-model="Formmate.presmat" material-select required>
        <option value="" disabled selected>Seleccione</option>
        <option value="Pqte.">Paquete</option>
        <option value="unidad">Unidad</option>
      </select>
    </div>
  </div>
  <div class="row" ng-hide="materialactivo">
    <div class="input-field col s4 m4 l4"></div>
    <div class="input-field col s4 m4 l4"></div>
    <div class="col s4 m4 l4">
      <input type="checkbox" class="filled-in" id="filled" ng-model="Formmate.matactivo"/>
      <label for="filled">Está Activo:</label>
    </div>
  </div>
  </form>
  </div>
    <div class="modal-footer" id="piemodalpersona">
      <a id="btnModificar" class="waves-effect waves-light btn  blue accent-4" ng-click="editarMate()" ><i class="material-icons left">restore</i>Modificar</a>
      <a id="btnCrear" class="waves-effect waves-light btn blue accent-4" ng-click="crearmaterial()" ng-disabled='!Formmate.$valid'><i class="material-icons left">add</i>Crear</a>
      <a class="modal-action modal-close waves-effect waves-light btn red"><i class="material-icons left">close</i>Close</a>
    </div>
</div>
<!--empiezo de modal de eliminacion-->
<div id="formeliminar" class="modal">
  <div class="modal-content">
        <h7>ESTA SEGURO DE ELIMINAR!!!</h7>
  </div>
    <div class="modal-footer">
      <div class="row">
        <div class="col s6">
          <a  class="waves-effect waves-light btn blue" ng-click="eliminarmaterial()" ><i class="material-icons left">add</i>Aceptar</a>
        </div>
        <div class="col s6">
          <a class="modal-action modal-close waves-effect waves-light btn red"><i class="material-icons left">close</i>Cancelar</a>
        </div>
      </div>
    </div>
</div>
<!--fin de modal de eliminacion--> 
</div>