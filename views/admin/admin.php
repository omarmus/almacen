<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Administrador</title>
  <link rel="stylesheet" href="assets/css/normalize.css?rev=<?php echo time(); ?>">
  <link rel="stylesheet" href="assets/css/materialize.min.css?rev=<?php echo time(); ?>">
  <link rel="stylesheet" href="assets/css/fonts.css?rev=<?php echo time(); ?>">
  <link rel="stylesheet" href="assets/css/sweetalert.css?rev=<?php echo time(); ?>">
  <link rel="stylesheet" href="assets/css/admin.css?rev=<?php echo time(); ?>">
  <script type="text/javascript">
    var id_usuario = '<?php echo $_SESSION['id']; ?>';
    var id_fun = '<?php echo $_SESSION['fun_id']; ?>';
    var name_completo = '<?php echo $_SESSION['name_complet']; ?>';
  </script>
</head>

<body ng-app="myApp" id="body">
  <div class="wrapper">

    <header ng-controller="ctrladmin">
      <!--este es la cabeza -->
      <nav>
        <div class="nav-wrapper" ng-init="mostrarpedidos()">
          <!---->
          <a href="" id="collapse-btn" class="collapse-btn show-on-large"><i class="material-icons">menu</i></a>
          <!--referencia de iconos-->


          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <!--cabezera-->
            <li>
              <a href="" ng-click="abrirpendientes()" ng-cloak>
                <i class="material-icons left">notifications</i>
                <span class="new badge blue accent-1" data-badge-caption="nuevos" ng-show="pendientes > 0">{{pendientes}}</span>
              </a>
              <!---->
            </li>
            <!--<div id="fompedido" class="modal bottom-sheet">
              <div class="modal-content">
                <h4>Solicitud de Materiles Pendientes</h4>
                  <table class="striped">
                    <thead>
                      <tr>
                        <th data-field="id">Funcionario</th>
                        <th data-field="tipo">Cargo</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="x in todopendientes">
                        <td ng-hide="true">{{x.id}}</td>
                        <td>{{x.nombres}} {{x.appaterno}} {{x.apmaterno}}</td>
                        <td>{{x.nomcargo}}</td>
                        <td class="right-align">
                          <a class="waves-effect waves-yellow btn" ng-click="undetalle(x.id)"><i class="material-icons left">attach_file</i>detalle</a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
              </div>
            </div>-->

            <!--inicio cerrar secion-->
            <ul class="right">
              <li><img src="assets/img/yuna.jpg" alt="" class="circle" width="60px" id="usuario"></li>
              <li>
                <a href="#!cuenta"><?php echo $_SESSION['name_complet']; ?></a>
              </li>
              <!--creando secion y nos encia a carpeta controller-->
              <li>
                <a href="index.php?view=admin&action=logout"><i class="material-icons left">lock_outline</i>Cerrar Session</a>
              </li>
            </ul>
            <!--fin cerrar secion-->
          </ul>
        </div>
      </nav>
    </header>
    <div class="side-nav">
      <div class="side-nav-logo">
        <span id="nomlog">ESMFTHEA </span><span id="nomlog1">ALMACEN</span>
      </div>
      <div class="side-nav-user">
        <img src="assets/img/logo.jpg" alt="" class="circle" width="60px" id="logo ">
        <span class="subheader">ADMINISTRACION</span>
      </div>
      <ul>
        <!-- amburgeza-->
        <li><a href="#!inicio" class="waves-effect waves-teal"><i class="material-icons">home</i>Inicio</a></li>
        <li><a href="#!areas" class="waves-effect waves-teal"><i class="material-icons">forum</i>Areas</a></li>
        <li><a href="#!cargos" class="waves-effect waves-teal"><i class="material-icons">work</i>Cargos</a></li>
        <li><a href="#!funcionario" class="waves-effect waves-teal"><i class="material-icons">group</i>Funcionarios</a></li>
        <li><a href="#!usuario" class="waves-effect waves-teal"><i class="material-icons">folder_shared</i>Usuarios</a></li>
        <li><a href="#!proveedores" class="waves-effect waves-teal"><i class="material-icons">recent_actors</i>Proveedores</a></li>

        <li>
          <!--unicio de menu formulario de materiales-->
          <ul class="collapsible" data-collapsible="accordion">
            <li>
              <a class="collapsible-header waves-effect waves-teal ">
                <i id="controlM" class="material-icons">description</i>Control de Materiales
                <i class="material-icons icon-right">keyboard_arrow_right</i>
              </a>
              <div class="collapsible-body">
                <ul>
                  <li class="fondo1"><a href="#!categoria" class="collapsible-header waves-effect waves-green">Categorias de Materiales</a></li>
                  <li class="fondo1"><a href="#!material" class="collapsible-header waves-effect waves-green">Registro de Materiales</a></li>
                  <li class="fondo1"><a href="#!cant_aut" class="collapsible-header waves-effect waves-green">Cantidad Autorizada</a></li>
                  <li class="fondo1"><a href="#!almacen" class="collapsible-header waves-effect waves-green"> Stock en Almacen</a></li>
                  <!--<li><div class="divider" id="div"></div></li>
                <li  class="fondo1"><a href="#!regEntrada" class="collapsible-header waves-effect waves-green">Registro de Ingreso</a></li>
                <li><div class="divider" id="div"></div></li>
                <li class="fondo1"><a href="#!regSalida" class="collapsible-header waves-effect waves-green">Stock de Materiales</a></li>-->
                </ul>
              </div>
            </li>
            <li>
              <a class="collapsible-header waves-effect waves-teal">
                <i id="reporteM" class="material-icons">description</i>Entrada/salida Materials
                <i class="material-icons icon-right">keyboard_arrow_right</i>
              </a>
              <div class="collapsible-body">
                <ul>
                  <li class="fondo1"><a href="#!regSalida" class="collapsible-header waves-effect waves-green">Solicitud de Material</a></li>
                  <li class="fondo1"><a href="#!regSalidaFun" class="collapsible-header waves-effect waves-green">Solicitar por Funcionario</a></li>
                  <li class="fondo1"><a href="#!regEntrada" class="collapsible-header waves-effect waves-green">Registro de Ingreso</a></li>
                  <li class="fondo1"><a href="#!salidas" class="collapsible-header waves-effect waves-green">Registro de Salida</a></li>
                </ul>
              </div>
            </li>
            <li>
              <a class="collapsible-header waves-effect waves-teal">
                <i id="reporteM" class="material-icons">trending_up</i>Reportes
                <i class="material-icons icon-right">keyboard_arrow_right</i>
              </a>
              <!--href="#!estadistica" class="waves-effect waves-teal"-->
              <div class="collapsible-body">
                <ul>
                  <li class="fondo1"><a href="#!repGeneral" class="collapsible-header waves-effect waves-green">Reporte General</a></li>
                  <li class="fondo1"><a href="#!repPersona" class="collapsible-header waves-effect waves-green">Reporte por Persona</a></li>
                  <li class="fondo1"><a href="#!repMaterial" class="collapsible-header waves-effect waves-green">Reporte Material disponible</a></li>
                  <li class="fondo1"><a href="#!repMaterialNo" class="collapsible-header waves-effect waves-green">Reporte Mat. NO disponible</a></li>
                </ul>
              </div>
            </li>
          </ul>
        </li>
        <!--fin de menu formulario de materiales-->
        <li><a href="#!logs" class="waves-effect waves-teal"><i class="material-icons">update</i>Logs</a></li>
      </ul>
    </div>
    <!--fin hamburgesa-->
    <!--contenido cuerpo-->

    <div class="row main">
      <div class="col s12 m12 l12 cuerpo-wrapper">
        <div data-ng-view="" id="ng-view"></div>
      </div>
    </div>
    <!--fin contenido-->
  </div>

  <!--pie de pagina-->

  <footer class="app-footer">
    <!--  <div class="container">
      <div class="row" id="pie">
        <div class="col l6 s12">
          <h5 class="white-text">Unidad de Bienes y Servicios</h5>
          <p class="grey-text text-lighten-4">Sistema de Inventario de Alamacenes</p>
        </div>
        <div class="col l4 offset-l2 s12">
          <h5 class="white-text">Referencias</h5>
          <ul>
            <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
          </ul>
        </div>
      </div>
    </div> -->
    <div>
      <span>
        Sistema de Inventario en Almacen © 2020
      </span>
      <a class="grey-text text-lighten-4" href="#!">esfmthea "Escuela Superior de Formacion de Maestros El Alto"</a>
    </div>
  </footer>

  <script src="assets/js/sweetalert.min.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/jquery-3.1.1.min.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/angular.min.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/angular-notify.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/materialize.min.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/angular-materialize.min.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/angular-animate.min.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/angular-route.min.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/init.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/app.js?rev=<?php echo time(); ?>"></script>
  <!--<script src="assets/js/pdf.js"></script>
  <script src="assets/js/angular-pdf.js"></script>-->
  <script src="assets/js/ctrladmin.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/ctrlfuncionario.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/ctrlusuario.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/ctrlproveedores.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/ctrlformularios.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/ctrlestadistica.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/ctrlregEntrada.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/ctrlregSalida.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/ctrlareas.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/ctrllogs.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/ctrlcuenta.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/ctrlcargos.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/ctrlcategorias.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/ctrlmateriales.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/ctrlcantautorizada.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/ctrlalmacen.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/ctrlhome.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/ctrlsalidas.js?rev=<?php echo time(); ?>"></script>
  <script src="assets/js/regsalidafun.js?rev=<?php echo time(); ?>"></script>
  <!--<script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>-->

  <!-- MaterialDark JS -->
  <!--<script src="assets/js/main.js"></script>-->

</body>

</html>