<div class="row cuenta" ng-controller="ctrlcuenta">
  <nav>
    <div class="nav-wrapper" id="contenido">
      <a class="brand-logo left" id="logopersona"><i class="material-icons left">person</i>Mi cuenta</a>
    </div>
  </nav>

  <div class="">
    <div class="row" ng-hide="listaexiste">
      <div class="col s12 m12 l12">
        <div class="card horizontal cuenta-info">
          <div class="card-image">
            <img src="assets/img/yuna.jpg" alt="" class="circle">
          </div>
          <div class="card-stacked">
            <span>{{nombre}}</span>
          </div>
        </div>
        <div class="row">
          <div class="col s12">
            <ul class="tabs">
              <li class="tab col s3"><a class="active" href="#test1">Cambiar contraseña</a></li>
            </ul>
          </div>
          <div id="test1" class="col s12 cuenta-pass">
            <form name="Formuser" novalidate ng-submit="cambiarContrasena()">
              <!-- <div class="input-field col s12 m3 l3">
                <input class="txterror" type="password" name="contra_1" ng-model="Formuser.oldPass" required ng-pattern="/^[0-9a-zA-ZÁ-ÿ\_\-\.\s\xF1\xD1]*$/" ng-class='{ error: !Formcate.$pristine, warning: Formuser.contra_1.$error.pattern}'>
                <label>Antigua Contraseña:</label>
                <span class="warning" ng-show="Formuser.contra_1.$error.pattern">Ingrese una Contraseña valida!</span>
              </div> -->
              <div class="input-field col s12 m3 l3">
                <input class="txterror" type="password" name="contra_1" ng-model="Formuser.password1" required ng-pattern="/^[0-9a-zA-ZÁ-ÿ\_\-\.\s\xF1\xD1]*$/" ng-class='{ error: !Formcate.$pristine, warning: Formuser.contra_1.$error.pattern}'>
                <label>Nueva Contraseña:</label>
                <span class="warning" ng-show="Formuser.contra_1.$error.pattern">Ingrese una Contraseña valida!</span>
              </div>
              <div class="input-field col s12 m3 l3">
                <input type="password" class="txterror" ng-model="Formuser.password2" name="contra_2" required ng-pattern="/^[0-9a-zA-ZÁ-ÿ\_\-\.\s\xF1\xD1]*$/" ng-class='{ error: !Formuser.$pristine, warning: Formuser.contra_2.$error.pattern && Formuser.password1 != Formuser.password2}'>
                <label>Confirmar nueva Contraseña:</label>
                <span class="warning" ng-show="Formuser.contra_2.$error.pattern">Ingrese una Contraseña valida!</span>
                <span class="warning" ng-show="Formuser.password1 != Formuser.password2">Las Contraseñas no son iguales</span>
              </div>
              <div class="input-field col s12 m3 l3">
                <button type="submit" class="waves-effect waves-light btn blue accent-4" ng-disabled='!Formuser.$valid || Formuser.password1 != Formuser.password2'><i class="material-icons left">check</i> Cambiar contraseña</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>