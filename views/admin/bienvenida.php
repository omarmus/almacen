<div class="row" ng-controller="ctrlhome">
	<!--<h2>Aqui estaran las advertencias de poco material o nada en inventarios</h2>-->
    <div class="col s12 m12 l12">
    	<!--<div slider height='500' transition='400'></div>-->
  		<div class="slider" slider >
  			<ul class="slides" >
  				<li>
  					<img src="assets/img/fondo1.jpg">
  					<div class="caption center-align">
  						<h3 style="">E.S.F.M.T.H.A.</h3>
              <h4 style="color:#eff2f8;">Escuela Superior de Formación de Maestros Tecnológico Humanístico El Alto</h4>
  						<h2 class="light grey-text text-lighten-3" style="">Bienvenido</h2>
  					</div>
  				</li>
  				<li>
  					<img src="assets/img/fondo2.jpg">
  					<div class="caption left-align">
  						<h3>MISION</h3>
  						<h5 class="light grey-text text-lighten-3" style="text-align: justify;">Somos la institución formadora de los docentes de educación básica del estado de Sinaloa; orientada a proporcionar a los futuros maestros, los conocimientos, habilidades y actitudes necesarias para que ejerzan plenamente la labor educadora que el Estado Mexicano le encomienda y atender, desde la docencia, los requerimientos educativos y culturales que le plantea la sociedad sinaloense. Para que, en su ejercicio profesional, los niños y jóvenes que son los beneficiarios de los servicios educativos, se formen como ciudadanos capaces de conducir su vida por el camino del trabajo honesto, del progreso personal y social.</h5>
  					</div>
  				</li>
  				<li>
  					<img src="assets/img/fondo3.jpg">
  					<div class="caption right-align">
  						<h3>VISION:</h3>
  						<h5 class="light grey-text text-lighten-3 " style="text-align: justify; color:#0022; ">Somos la institución formadora de docentes del estado de Sinaloa que ha alcanzado y mantiene el reconocimiento social: por la capacidad de sus egresados en el cumplimiento de sus funciones educativas y sociales; por la calidad y pertinencia de sus programas de investigación y formación de recursos humanos de alto nivel para el desarrollo de la docencia; por su sensibilidad para incorporar a su misión educadora la atención de los problemas socioeconómicos, educativos y culturales de nuestro estado, y por la aplicación de una gestión moderna, transparente y de calidad que busca constantemente su mejora e innovación.   </h5>
  					</div>
  				</li>
  				<li>
  					<img src="assets/img/fondo5.jpg">
  					<div class="caption center-align">
  						<h3>Valores:</h3>
  						<h5 class="light grey-text text-lighten-3" style="text-align: justify; "><h5>La educación que se imparte en la Escuela Normal * respeto desarrolla todas las facultades del ser humano de una manera integral promoviendo la calidad basa en la práctica de los siguientes valores:</h5>
 <h5 align="center">
    * Respeto <br>
    * Disiplina <br>
    * Tolerancia <br>
    * Solidarid <br>
    * Madurez <br>
    * Claridad  <br>
    * Perseverancia <br>
    * Reponsabilidad <br>
    
</h5>
  					</div>
  				</li>
  			</ul>
  		</div>

	</div>
</div>