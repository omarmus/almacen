<div class="row" ng-controller="ctrlusuario">
<nav>
  <div class="nav-wrapper" id="contenido">
    <a class="brand-logo left" id="logopersona"><i class="material-icons left">person</i>Usuario</a>
    <ul id="nav-mobile" class="right">
      <a class="waves-light btn"  data-target='formUsuario' modal ng-click="openformulario()">Nuevo Usuario</a>
    </ul>
  </div>
</nav>
<form class="col s12 m12 l12 full" id="btnbusqueda">
  <div class="row">
    <div class="input-field col s12 m12 l12">
      <input id="search" type="text" class="validate" ng-model="q" ng-change="currentPage = 0">
      <label for="search">Busqueda</label>
    </div>
  </div>
</form>

<div class="">
    <div class="row" ng-hide="listaexiste">
      <div class="col s12 m12 l12">
        <table class="striped" ng-init='configPages()'>
          <thead>
            <tr>
              <th data-field="id">Id</th>
              <th data-field="tipo">Nombres</th>
              <th data-field="name">Usuario</th>
              <th data-field="name">Contraseña</th>
              <th data-field="name">Tipo de Usuario</th>
              <th data-field="name">Activo</th>
              <th data-field="opciones" colspan="2">Opciones</th>
            </tr>
          </thead>
          <tbody >
            <tr ng-repeat="x in datosuser | filter:q | startFromGrid: currentPage * pageSize | limitTo: pageSize">
              <td>{{x.id}}</td>
              <td>{{x.nombres}}</td>
              <td>{{x.nombre}}</td>
              <td>{{x.password}}</td>
              <td>{{x.rol_usuario}}</td>
              <td><i class="material-icons" ng-if="x.estado==1" id="activo">check</i></td>
              <td id="col1"><a href="" ng-click="abrirformeditausu(x.id)" id="btn1" tooltipped data-position="left" data-delay="40" data-tooltip="Editar"><i class="small material-icons">edit</i></a></td>
              <td id="col1"><a href="" ng-click="abrirformelimina(x.id)" id="btn2" data-target='formeliminar' modal tooltipped data-position="right" data-delay="40" data-tooltip="Eliminar"><i class="small material-icons">delete</i></a></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <!--inicio de paginacion-->
    <div class="row" ng-hide="listaexiste">
      <div class="col s12 m12 l12 center">
        <pagination page="1" page-size="pageSize" total="datosuser.length" show-prev-next="true" use-simple-prev-next="false" dots="..." hide-if-empty="false" adjacente="2" scroll-top="false"  pagination-action="setPage(page)"/>
      </div>
    </div>
    <!--fin de paginacion-->
    <div class="row" ng-hide="listanoexiste">
      <div id="advertencia" class="col s12 m12 l12">
        <p>No hay registros que mostrar!!!.Haga Click en <i class="tiny material-icons">done</i>  Nuevo Usuario</p>
      </div>
    </div>
</div>
<!-- Modal Structure -->
<div id="formUsuario" class="modal">
  <div class="modal-content">
      <h4 id="titulo-modal"></h4>
      <!--FORMULARIO-->
  <form name="Formuser" novalidate>
  <div class="row">
    <div class="col s5">
      <label >CI/Funcionario:</label>
      <select ng-hide="funnoedita" class="browser-default" ng-model="Formuser.cifun" material-select required>
        <option value="" disabled selected>Seleccione</option>
        <option ng-repeat="x in res" ng-value="{{x.id}}">{{x.ci}}({{x.nombres}})</option>
      </select>
      <select ng-hide="funedita" class="browser-default" material-select ng-options="option.nombres for option in data.availableOptions track by option.id" ng-model="data.selectedOption" disabled>
      </select>
    </div>
    <div class="input-field col s7">
      <input class="txterror" type="text" name="nom_usu" ng-model="Formuser.nomusuario" required ng-pattern="/^[0-9a-zA-ZÁ-ÿ\_\-\.\s\xF1\xD1]*$/" ng-class='{ error: !Formcate.$pristine, warning: Formuser.nom_usu.$error.pattern'>
      <label>Nombre de Usuario:</label>
      <span  class="warning" ng-show="Formuser.nom_usu.$error.pattern">Ingrese un Nombre de Usuario valido!</span>
    </div>
  </div>
  <div class="row" ng-hide="checkeditapass">
    <div class="col s12 m12">
      <label>Cambiar Contraseña:  </label>
      <input type="checkbox" class="filled-in" id="filled1" ng-model="confirmarcambio" ng-change="activa()"/>
      <label for="filled1">Nuevo</label>
    </div>
  </div>
  <div class="row" ng-hide="editacontra">
    <div class="input-field col s6 m6 l6">
      <input class="txterror" type="password" name="contra_1" ng-model="Formuser.password1" required ng-pattern="/^[0-9a-zA-ZÁ-ÿ\_\-\.\s\xF1\xD1]*$/" ng-class='{ error: !Formcate.$pristine, warning: Formuser.contra_1.$error.pattern}' ng-disabled="editapassword">
      <label>Contraseña:</label>
      <span  class="warning" ng-show="Formuser.contra_1.$error.pattern">Ingrese una Contraseña valida!</span>
    </div>
    <div class="input-field col s6 m6 l6">
      <input type="password" class="txterror" ng-model="Formuser.password2" name="contra_2" required  ng-pattern="/^[0-9a-zA-ZÁ-ÿ\_\-\.\s\xF1\xD1]*$/" ng-class='{ error: !Formuser.$pristine, warning: Formuser.contra_2.$error.pattern && Formuser.password1 != Formuser.password2}' ng-disabled="editapassword">
      <label>Confirmar Contraseña:</label>
      <span class="warning" ng-show="Formuser.contra_2.$error.pattern">Ingrese una Contraseña valida!</span>
      <span class="warning" ng-show="Formuser.password1 != Formuser.password2">Las Contraseñas no son iguales</span>
    </div>
  </div>
  <div class="row">
    <div class="col s4">
      <label>Tipo de Usuario:</label>
      <select class="browser-default" ng-model="Formuser.tipouser" material-select required>
        <option value="" disabled selected>Seleccione</option>
        <option value="1">Administrador</option>
        <option value="2">Avanzado</option>
        <!-- <option value="3">Comun</option> -->
      </select>
    </div>
    <div class="col s4 m4 l4"></div>
    <div class="col s4 m4 l4">
      <label>Usuario Activo:</label>
      <input type="checkbox" class="filled-in" id="filled" name="activo" ng-model="useractivo"/>
      <label for="filled">Activo/Inactivo:</label>
    </div>
  </div>
  </form>
  </div>
    <div class="modal-footer" id="piemodalpersona">
      <a id="btnModificar" class="waves-effect waves-light btn  blue accent-4" ng-click="editarUsuario()" ng-disabled='Formuser.password1 != Formuser.password2'><i class="material-icons left">restore</i>Modificar</a><!--Formuser.password1 != Formuser.password2-->
      <a id="btnCrear" class="waves-effect waves-light btn blue accent-4" ng-click="crearusuario()" ng-disabled='!Formuser.$valid'><i class="material-icons left">add</i>Crear</a>
      <a class="modal-action modal-close waves-effect waves-light btn red"><i class="material-icons left">close</i>Close</a>
    </div>
</div>
<!--empiezo de modal de eliminacion-->
<div id="formeliminar" class="modal">
  <div class="modal-content">
        <h7>ESTA SEGURO DE ELIMINAR!!!</h7>
  </div>
    <div class="modal-footer">
      <div class="row">
        <div class="col s6">
          <a  class="waves-effect waves-light btn blue" ng-click="eliminarusuario()" ><i class="material-icons left">add</i>Aceptar</a>
        </div>
        <div class="col s6">
          <a class="modal-action modal-close waves-effect waves-light btn red"><i class="material-icons left">close</i>Cancelar</a>
        </div>
      </div>
    </div>
</div>
</div>