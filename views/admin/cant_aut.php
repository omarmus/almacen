<div class="row" ng-controller="ctrlcantautorizada">
	<nav>
    <div class="nav-wrapper" id="contenido">
      <a class="brand-logo left" id="logopersona"><i class="material-icons left">recent_actors</i>Cantidad Autorizada</a>
    </div>
  </nav>
  <br>
<div class="container-full" ng-init="listarAreas()">
  <div class="row">
    <div class="col s12">
      <ul tabs reload="allTabContentLoaded">
      <!--<li class="tab col s4 m4"><a class="active" href="#test0">Inicio</a></li>  -->
        <li class="tab col s4 m4"><a class="active" href="#test1" ng-click="listarAreas()">Materiales Asignados</a></li>
        <li class="tab col s4 m4"><a href="#test2" ng-click="nuevaasignacion()">Nueva Asignación</a></li>
      </ul>
    </div>
    <div id="test0" class="col s12">
      <br>
      
      
    </div>
    <div id="test1" class="col s12">
      <br>
      <form>
        <div class="col s4" >
          <label>Seleccione Área para Listar Materiales Asignados:</label>
          <select material-select class="browser-default" ng-model="selectarea" ng-change="bucarmatasignados(selectarea)">
            <option value="" disabled selected>Seleccione Área...</option>
            <option ng-repeat="x in datosarea" ng-value="{{x.id}}">{{x.descripcion}}</option>    
          </select>
        </div>
        <div class="input-field col s8">
          <input id="search" type="text" class="validate" ng-model="q" ng-change="currentPage = 0" placeholder="busqueda por nombre ó codigo de material">
          <label for="search" class="active">Busqueda</label>
        </div>
      </form>
      <table class="bordered" ng-hide="mostrarasignacion">
        <thead>
          <tr>
              <th data-field="id">Id</th>
              <th data-field="nomprod">Nombre</th>
              <th data-field="descripcion">Descripción</th>
              <th data-field="">codigo</th>
              <th data-field="">C/A</th>
              <th data-field="" colspan="2">Opciones</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="c in datoscanaut | filter:q | startFromGrid: currentPage * pageSize | limitTo: pageSize">
            <td>{{c.id}}</td>
            <td>{{c.nom_material}}</td>
            <td>{{c.descripcion}}</td>
            <td>{{c.codigo}}</td>
            <td>{{c.ca}}</td>
            <td id="col1"><a href="" data-target='formCantidad' modal ng-click="abrirformeditacant(c.id)" id="btn1" tooltipped data-position="left" data-delay="40" data-tooltip="Editar"><i class="small material-icons">edit</i></a></td>
            <td id="col1"><a href="" ng-click="abrirformelimina(c.id)" id="btn2" data-target='formeliminar' modal tooltipped data-position="top" data-delay="40" data-tooltip="Eliminar"><i class="small material-icons">delete</i></a></td>
          </tr>
        </tbody>
      </table>
      <!--inicio de paginacion-->
      <div class="row" ng-hide="mostrarasignacion">
        <div class="col s12 m12 l12 center">
          <pagination page="1" page-size="pageSize" total="datoscanaut.length" show-prev-next="true" use-simple-prev-next="false" dots="..." hide-if-empty="false" adjacente="2" scroll-top="false"  pagination-action="setPage(page)"/>
        </div>
      </div>
      <!--fin de paginacion-->
    </div>
    <div id="test2" class="col s12">
      <br>
      <form name="FormCA" novalidate>
        <div class="col s4" >
          <label>Seleccione Área:</label>
          <select  material-select class="browser-default" ng-model="FormCA.selectareanew" ng-disabled="usuavanzado" required ng-change="activa(FormCA.selectareanew)">
            <option value="" disabled selected>Seleccione Área...</option>
            <option ng-repeat="x in datosarea" ng-value="{{x.id}}">{{x.descripcion}}</option>    
          </select>
        </div>
        <div class="col s4" >
          <label>Seleccione Material:</label>
          <select material-select class="browser-default" ng-model="FormCA.selectmatenew" required>
            <option value="" disabled selected>Seleccione Material...</option>
            <option ng-repeat="x in res" ng-value="{{x.id}}">{{x.nom_material}} {{x.descripcion}} ({{x.codigo}})</option>    
          </select>
        </div>
        <div class="input-field col s2">
          <input id="cant" type="text" class="txterror" name="cant" ng-model="FormCA.cantaut" required placeholder="ingrese N° cantidad" ng-pattern="/^[0-9]*$/" ng-class='{ error: !FormCA.$pristine, warning: FormCA.cant.$error.pattern}'>
          <label for="cant" class="active">Cantidad a Asignar:</label>
          <span  class="warning" ng-show="FormCA.cant.$error.pattern">Ingrese un N° de C/A Valido!</span>
        </div>
        <div class="col s2">
          <br>
          <a ng-disabled="!FormCA.$valid" id="btnNuevo" class="waves-light btn" ng-click="asignarcantaut()">Agregar</a>
        </div>
      </form>
      <div class="row">
        <div class="col s12">
            <table class="responsive-table bordered" ng-hide="mostraritems">
              <thead>
                <tr>
                    <th data-field="id">N°</th>
                    <th data-field="area">Área</th>
                    <th data-field="name">Material</th>
                    <th data-field="name">Codigo</th>
                    <th data-field="price">Cantidad</th>
                    <th>Opcion</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="a in agregarca">
                  <td>{{$index+1}}</td>
                  <td ng-hide="true">{{a.area_id}}</td>
                  <td>{{a.nomarea}}</td>
                  <td ng-hide="true">{{a.material_id}}</td>
                  <td>{{a.nommate}} {{a.desc}}</td>
                  <td>{{a.codigo}}</td>
                  <td>{{a.ca}}</td>
                  <td><a href="" ng-click="eliminaritem($index)" id="btn2" tooltipped data-position="right" data-delay="40" data-tooltip="Eliminar"><i class="small material-icons">delete</i></a></td>
                </tr>
              </tbody>
            </table>  
        </div>
      </div> 
      <div class="row">
        <div class="col s8"></div>
        <div class="col s4">
          <a ng-if="agregarca != 0" id="btnNuevo" class="waves-light btn right" ng-click="confirmarasigancion()">agregar C/A a área</a>
        </div>
      </div>   
    </div>
    
  </div>
</div>
<!-- Modal Structure -->
<div id="formCantidad" class="modal">
  <div class="modal-content">
      <h4 id="titulo-modal"></h4>
      <!--FORMULARIO-->
  <form name="Formcantaut" novalidate>
    <div class="row">
      <div class="input-field col s12">
        <input  type="text" ng-model="materialca"  disabled>
        <label>MATERIAL:</label>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s6">
        <input  type="text" ng-model="codigomatca"  disabled>
        <label>CODIGO/MATERIAL:</label>
      </div>
      <div class="input-field col s6">
        <input id="ca" type="text" class="txterror" ng-model="Formcantaut.nroca" name="ca" ng-pattern="/^[0-9]*$/" ng-class='{ error: !Formcantaut.$pristine, warning: Formcantaut.ca.$error.pattern}' required>
        <label>C/A:</label>
        <span  class="warning" ng-show="Formcantaut.ca.$error.pattern">Ingrese un N° valido!</span>
      </div>
    </div>
  </form>
  </div>
    <div class="modal-footer" id="piemodalpersona">
      <a id="btnModificar" class="waves-effect waves-light btn  blue accent-4" ng-click="editarcant_aut()" ng-disabled='!Formcantaut.$valid'><i class="material-icons left">restore</i>Modificar</a>
      <a class="modal-action modal-close waves-effect waves-light btn red"><i class="material-icons left">close</i>Close</a>
    </div>
</div>
<!--empiezo de modal de eliminacion-->
<div id="formeliminar" class="modal">
  <div class="modal-content">
    <h7>ESTA SEGURO DE ELIMINAR!!!</h7>
  </div>
    <div class="modal-footer">
      <div class="row">
        <div class="col s6">
          <a  class="waves-effect waves-light btn blue" ng-click="eliminarcant_aut()" ><i class="material-icons left">add</i>Aceptar</a>
        </div>
        <div class="col s6">
          <a class="modal-action modal-close waves-effect waves-light btn red"><i class="material-icons left">close</i>Cancelar</a>
        </div>
      </div>
    </div>
</div>
</div>