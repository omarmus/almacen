 <div class="row" ng-controller="ctrlproveedores">
	  <nav>
    <div class="nav-wrapper" id="contenido">
      <a class="brand-logo left" id="logopersona"><i class="material-icons left">recent_actors</i>Proveedores</a>
      <ul id="nav-mobile" class="right">
        <li><a id="btnNuevo" class="waves-light btn" data-target='formProveedor' modal ng-click="openformulario()">Nuevo Proveedor</a></li>
      </ul>
    </div>
  </nav>
  <form class="col s12 m12 l12 full" id="btnbusqueda">
      <div class="row">
        <div class="input-field col s12 m12 l12">
          <input id="search" type="text" class="validate" ng-model="q" ng-change="currentPage = 0">
          <label for="search">Busqueda</label>
        </div>
      </div>
  </form>
<div class="container-full">
  <div class="row" ng-hide="listaexiste">
    <div class="col s12 m12 l12">
      <table  class="striped" ng-init = "configPages()">
        <thead>
          <tr>
              <th data-field="nro">Id</th>
              <th data-field="nit">NIT</th>
              <th data-field="emp">Empresa</th>
              <th data-field="nombres">Nombres</th>
              <th data-field="apellidos">Apellidos</th>
              <th data-field="direccion">Direccion</th>
              <th data-field="telefono">Telefono</th>
              <th data-field="correo">Correo</th>
              <th data-field="opciones" colspan="2">Opciones</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="x in datosprov | filter:q | startFromGrid: currentPage * pageSize | limitTo: pageSize">
            <td>{{x.id}}</td>
            <td>{{x.Nit}}</td>
            <td>{{x.empresa}}</td>
            <td>{{x.nombres}}</td>
            <td>{{x.apellidos}}</td>
            <td>{{x.direccion}}</td>
            <td>{{x.telefono}}</td>
            <td>{{x.correo}}</td>
            <td id="col1"><a id="btn1" href="" ng-click="abrirformeditaprov(x.id)" tooltipped data-position="left" data-delay="40" data-tooltip="Editar"><i class="small material-icons">edit</i></a></td>
            <td id="col1"><a id="btn2" href="" ng-click="abrirformelimina(x.id)" data-target='formeliminar' modal tooltipped data-position="top" data-delay="40" data-tooltip="Eliminar"><i class="small material-icons">delete</i></a></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <!--inicio de paginacion-->
  <div class="row" ng-hide="listaexiste">
    <div class="col s12 m12 l12 center">
      <pagination page="1" page-size="pageSize" total="datosprov.length" show-prev-next="true" use-simple-prev-next="false" dots="..." hide-if-empty="false" adjacente="2" scroll-top="false"  pagination-action="setPage(page)"/>
    </div>
  </div>
  <!--fin de paginacion-->        
  <div class="row" ng-hide="listanoexiste">
    <div id="advertencia" class="col s12 m12 l12">
      <p>No hay registros que mostrar!!!.Haga Click en <i class="tiny material-icons">done</i>Nuevo Proveedor</p> 
    </div>
  </div>  
</div>

<!-- Modal Structure -->
<div id="formProveedor" class="modal">
  <div class="modal-content">
      <h4 id="titulo-modal"></h4>
      <!--FORMULARIO-->
  <form name="Formprov" novalidate>
  <div class="row">
    <div class="input-field col s3 m3 l3">
      <input class="txterror" type="text" name="nit" ng-model="Formprov.nitprov" required ng-pattern="/^[0-9]*$/" ng-class='{ error: !Formprov.$pristine, warning: Formprov.nit.$error.pattern}' placeholder="Ingrese N° de NIT.">
      <label class="active">N° de NIT.:</label>
      <span  class="warning" ng-show="Formprov.nit.$error.pattern">Ingrese un NIT valido!</span>
    </div>
    <div class="input-field col s4 m4 l4">
      <input class="txterror" type="text" name="empresa" ng-model="Formprov.emp" required ng-pattern="/^[a-zA-ZÁ-ÿ\_\-\.\s\xF1\xD1]*$/" ng-class='{ error: !Formprov.$pristine, warning: Formprov.empresa.$error.pattern}' placeholder="Ingrese Nombre de Empresa">
      <label class="active">Empresa:</label>
      <span  class="warning" ng-show="Formprov.empresa.$error.pattern">Ingrese un Nombre valido!</span>
    </div>
    <div class="input-field col s5">
      <input class="txterror" type="text" name="nombres" ng-model="Formprov.nom" required ng-pattern="/^[a-zA-ZÁ-ÿ\_\-\.\s\xF1\xD1]*$/" ng-class='{ error: !Formprov.$pristine, warning: Formprov.nombres.$error.pattern}' placeholder="Ingrese Nombres">
      <label class="active">Nombres:</label>
      <span  class="warning" ng-show="Formprov.nombres.$error.pattern">Ingrese un Nombre valido!</span>
    </div>
  </div>    
  <div class="row" >
    <div class="input-field col s5 m5 l5">
      <input class="txterror" type="text" name="apellidos" ng-model="Formprov.approv" required ng-pattern="/^[a-zA-ZÁ-ÿ\_\-\.\s\xF1\xD1]*$/" ng-class='{ error: !Formcate.$pristine, warning: Formprov.apellidos.$error.pattern}' placeholder="Ingrese Apellidos">
      <label class="active">Apellidos:</label>
      <span  class="warning" ng-show="Formprov.apellidos.$error.pattern">Ingrese apellidos validos!</span>
    </div>
    <div class="input-field col s7 m7 l7">
      <input type="text" class="txterror" ng-model="Formprov.dir" name="direccion" required  ng-pattern="/^[0-9a-zA-ZÁ-ÿ\_\-\.\/\°\s\xF1\xD1]*$/" ng-class='{ error: !Formprov.$pristine, warning: Formprov.direccion.$error.pattern}' placeholder="El Alto/z. villa Esperanza/av. Santa Fe/N° 11111">
      <label class="active">Dirección:</label>
      <span class="warning" ng-show="Formprov.direccion.$error.pattern">Ingrese Dirección Valida!</span>  
    </div>
  </div>
  <div class="row">
    <div class="input-field col s4 m4 l4">
      <input class="txterror" type="text" name="telefono" ng-model="Formprov.fono" required ng-pattern="/^[0-9]*$/" ng-class='{ error: !Formprov.$pristine, warning: Formprov.telefono.$error.pattern}' placeholder="Ingrese N° telefonico">
      <label class="active">Telefono:</label>
      <span class="warning" ng-show="Formprov.telefono.$error.pattern">Ingrese N° telefonico Valido!</span>  
    </div>
    <div class="input-field col s8 m8 l8">
      <input class="txterror" type="email" name="correoprov" ng-model="Formprov.email" required ng-class='{ error: !Formprov.$pristine, warning: Formprov.correoprov.$error.email}' placeholder="ejemplo@email.com">
      <label class="active">Correo:</label>
      <span class="warning" ng-show="Formprov.correoprov.$error.email">Ingrese un Correo valido!</span>
    </div>
  </div>
  </form>
  </div>
    <div class="modal-footer" id="piemodalpersona">
      <a id="btnModificar" class="waves-effect waves-light btn  blue accent-4" ng-click="editarProveedor()" ng-disabled='!Formprov.$valid'><i class="material-icons left">restore</i>Modificar</a>
      <a id="btnCrear" class="waves-effect waves-light btn blue accent-4" ng-click="crearproveedor()" ng-disabled='!Formprov.$valid'><i class="material-icons left">add</i>Crear</a>
      <a class="modal-action modal-close waves-effect waves-light btn red"><i class="material-icons left">close</i>Close</a>
    </div>
</div>
<!--empiezo de modal de eliminacion-->
<div id="formeliminar" class="modal">
  <div class="modal-content">
        <h7>ESTA SEGURO DE ELIMINAR!!!</h7>
  </div>
    <div class="modal-footer">
      <div class="row">
        <div class="col s6">
          <a  class="waves-effect waves-light btn blue" ng-click="eliminarproveedor()" ><i class="material-icons left">add</i>Aceptar</a>
        </div>
        <div class="col s6">
          <a class="modal-action modal-close waves-effect waves-light btn red"><i class="material-icons left">close</i>Cancelar</a>
        </div>
      </div>
    </div>
</div>
</div>