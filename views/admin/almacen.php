<div class="row" ng-controller="ctrlalmacen">
	<nav>
    <div class="nav-wrapper" id="contenido">
      <a class="brand-logo left" id="logopersona"><i class="material-icons left">recent_actors</i>Almacen</a>
    </div>
  	</nav>
  	<form class="col s12 m12 l12 full" id="btnbusqueda">
      <div class="row">
        <div class="input-field col s12 m12 l12">
          <input id="search" type="text" class="validate" ng-model="q1" ng-change="currentPage = 0">
          <label for="search">Busqueda</label>
        </div>
      </div>
  	</form>
	<div class="">
		<div class="row" ng-hide="listaexiste">
			<div class="col s12">
				<table class="striped" ng-init='configPages()'>
          			<thead>
            			<tr>
              			<th data-field="id">Id</th>
              			<th data-field="tipo">Codigo</th>
              			<th data-field="name">Nombre de Material</th>
              			<th>Disponible</th>
              			<th data-field="opciones">Opciones</th>
            			</tr>
          			</thead>
          			<tbody >
            		<tr ng-repeat="x in datosalmacen | filter:q1 | startFromGrid: currentPage * pageSize | limitTo: pageSize">
              			<td>{{x.id}}</td>
              			<td>{{x.codigo}}</td>
              			<td>{{x.nom_material}} {{x.descripcion}}</td>
              			<td>{{x.tot}}</td>
              			<td><a href="" ng-click="historial(x.id)" data-target='formhistorial' modal tooltipped data-position="right" data-delay="40" data-tooltip="Historial"><i class="small material-icons  lime accent-3">history</i></a></td>
            		</tr>
          			</tbody>
        		</table>
			</div>
		</div>
		<!--inicio de paginacion-->
    	<div class="row" ng-hide="listaexiste">
      	<div class="col s12 m12 l12 center">
        	<pagination page="1" page-size="pageSize" total="datosalmacen.length" show-prev-next="true" use-simple-prev-next="false" dots="..." hide-if-empty="false" adjacente="2" scroll-top="false"  pagination-action="setPage(page)"/>
      	</div>
    	</div>
    	<!--fin de paginacion-->
    	<div class="row" ng-hide="listanoexiste">
      	<div id="advertencia" class="col s12 m12 l12">
        	<p>No hay registros que mostrar!!! <i class="tiny material-icons">error</i></p>
      	</div>
    	</div>
	</div>
  <div id="formhistorial" class="modal">
    <div class="modal-content">
      <h4>Historial del Material</h4>
      <div class="row">
        <div class="col s6">
          <select ng-model="fTipo" ng-change="changeTipo()">
            <option value="-">TODO</option>
            <option value="MES">POR MES</option>
            <option value="GESTION">POR GESTION</option>
          </select>
        </div>
        <div class="col s6">
          <div ng-style="{ 'display' : showMes ? 'block' : 'none' }">
            <select ng-model="fMes" ng-change="changeMes()">
              <option value="-">Seleccione el mes</option>
              <option ng-repeat="mes in meses" value="{{ mes.value }}">{{ mes.text }}</option>
            </select>
          </div>
          <div ng-style="{ 'display' : showGestion ? 'block' : 'none' }">
            <select ng-model="fGestion" ng-change="changeGestion()">
              <option value="-">Seleccione la gestión</option>
              <option ng-repeat="gestion in gestiones" value="{{ gestion.value }}">{{ gestion.text }}</option>
            </select>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col s4">
          <div class="tile">
            <div class="tile-icon"><i class="material-icons">send</i></div>
            <div class="tile-caption">
              <span class="center-align"><input  ng-model="entradas" class="center-align" ng-disabled="true"></span>
              <p class="center-align">Entradas</p>
            </div>
            <a href="" class="tile-link waves-effect waves-light">Ver Detalle &nbsp; <i class="material-icons right">arrow_drop_down_circle</i></a>
          </div>
        </div>
        <div class="col s4">
          <div class="tile">
            <div class="tile-icon"><i class="material-icons">check_box</i></div>
            <div class="tile-caption">
              <span class="center-align"><input  ng-model="dispon" class="center-align" ng-disabled="true"></span>
              <p class="center-align">Disponibles</p>
            </div>
            <a href="" class="tile-link waves-effect waves-light">Ver Detalle &nbsp; <i class="material-icons right">arrow_drop_down_circle</i></a>
          </div>
        </div>
        <div class="col s4">
          <div class="tile">
            <div class="tile-icon"><i class="material-icons">add_shopping_cart</i></div>
            <div class="tile-caption">
              <span class="center-align"><input  ng-model="salidas" class="center-align" ng-disabled="true"></span>
              <p class="center-align">Salidas</p>
            </div>
            <a href="" class="tile-link waves-effect waves-light">Ver Detalle &nbsp; <i class="material-icons right">arrow_drop_down_circle</i></a>
          </div>
        </div>
      </div>

    </div>
    <div class="modal-footer">
      <a href="" class="modal-close waves-effect waves-green btn-flat">Cerrar Detalle</a>
    </div>
  </div>
</div>