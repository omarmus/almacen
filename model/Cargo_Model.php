<?php 
/**
* 
*/
class Cargo_Model
{
	
	function __construct()
	{
		require_once('core/Conectar.php');
		$aux=new Conectar;
		$this->db=$aux->ConexionPDO();
	}
	public function crearnuevocargo($nomcargo,$area_id)
	{
		$values = array('nomcargo' => $nomcargo, 'area_id' => $area_id);
		$query = $this->db->insertInto('cargo')->values($values);

		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}
		else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function listarcargos(){
		$query = $this->db->from('cargo')
						  ->LeftJoin('area ON area.id = cargo.area_id')
						  ->select(array('area.descripcion'))
						  ->orderBy('id')
						  ->fetchAll();
		return json_encode($query);

	}
	public function eliminarcargo($id){
		$query = $this->db->deleteFrom('cargo')->where('id', $id);

		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}
		else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function editarcargo($id,$nomcargo,$area_id){
		$values = array('nomcargo' => $nomcargo, 'area_id' => $area_id);
		$query = $this->db->update('cargo')->set($values)->where('id', $id);

		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}
		else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function buscaruncargo($id){
		$query = $this->db->from('cargo')
						  ->LeftJoin('area ON area.id = cargo.area_id')
						  ->select(array('area.descripcion'))
						  ->where('cargo.id', $id)
						  ->limit(1)->fetch();
		return json_encode(array($query));
	}
	
}
?>