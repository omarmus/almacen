<?php
/**
*
*/
class Reportes_Model
{

	function __construct()
	{
		require_once('core/Conectar.php');
		$aux=new Conectar;
		$this->db=$aux->ConexionPDO();
		date_default_timezone_set("America/La_Paz");
		$this->created_at = Date(DATE_ATOM,time());
		//public $nomfile = '';
		session_start();

	}
	/***INICIO***DESDE AQUI SON LOS DATOS DE REPORTES DE FORMULARIO DE SOLICITUD DE MATERIALES */
	public function crearnombrepdf(){
		$query = $this->db->from('pedido')
						  ->select('MAX(pedido.id) AS num')
						  ->where('estado_pedido_id',1)
						  ->limit(1)->fetch();
		$maxcod = $query->num;
		$fecha = date("YmdH:i:s");
		$nomfile = $maxcod.'-'.$_SESSION['fun_id'].$fecha;
		return $nomfile;
	}
	public function construirtablapdf(){
		$query1 = $this->db->from('pedido')
						  ->select('MAX(pedido.id) AS num')
						  ->where('estado_pedido_id',1)
						  ->limit(1)->fetch();
		$maxcod = $query1->num;
		$query2 = $this->db->from('area')
						   ->LeftJoin('cargo ON cargo.area_id = area.id')
						   ->LeftJoin('funcionario ON funcionario.cargo_id = cargo.id')
						   ->select('area.id AS idarea')
						   ->where('funcionario.id',$_SESSION['fun_id'])
						   ->limit(1)->fetch();
		$idarea = $query2->idarea;
		$query = $this->db->from('material')
						  ->LeftJoin('categoria ON categoria.id = material.categoria_id')
						  ->LeftJoin('transaccion ON transaccion.material_id = material.id')
						  ->LeftJoin('cant_aut ON cant_aut.material_id = material.id')
						  ->select('cant_aut.ca,categoria.tipo,transaccion.q')
						  ->where('transaccion.pedido_id',$maxcod)
						  ->where('cant_aut.area.id',$idarea)
						  ->fetchAll();
		$prueba = array();
		$prueba = $query;
		$cont1 = count($prueba);
		//$a = $prueba[0]->codigo;
		//return $a;
		$html="";
			$html=$html.'<div align="center">
			<br /><table border="1"  align="center">
			<tr bgcolor="#96939333" style="font-family:arial;">
				<th width="37px"><font color="#FFFFFF"><strong>ITEM</strong></font></th>
				<th width="70px"><font color="#FFFFFF"><strong>CODIGO</strong></font></th>
				<th width="70px"><font color="#FFFFFF"><strong>TIPO</strong></font></th>
				<th width="240px"><font color="#FFFFFF"><strong>MATERIAL SUMINISTRO</strong></font></th>
				<th width="70px"><font color="#FFFFFF"><strong>UNIDAD</strong></font></th>
				<th width="85px"><font color="#FFFFFF"><strong>CANTIDAD SOLICITADA</strong></font></th>
				<th width="85px"><font color="#FFFFFF"><strong>CANTIDAD AUTORIZADA</strong></font></th>
			</tr>';
			$c = 1;
			for ($i=0; $i <= $cont1-1; $i++) {
				$html=$html.'<tr>';
				$html=$html.'<td>';
				$html=$html.$c;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->codigo;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->tipo;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->nom_material;
				$html=$html.' '.$prueba[$i]->descripcion;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->presentacion;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->q;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->ca;
				$html=$html.'</td></tr>';
				$c++;
			}
			$html=$html.'</table></div>';
		return ($html);
	}
	public function datosfuncionario(){
		$query = $this->db->from('pedido')
						  ->select('MAX(pedido.id) AS num')
						  ->where('estado_pedido_id',1)
						  ->limit(1)->fetch();
		$maxcod = $query->num;
		$query2 = $this->db->from('funcionario')
						   ->LeftJoin('cargo ON cargo.id = funcionario.cargo_id')
						   ->LeftJoin('area ON area.id = cargo.area_id')
						   ->LeftJoin('pedido ON pedido.funcionario_id = funcionario.id')
						   ->select('cargo.nomcargo,area.descripcion,pedido.fecha_creacion')
						   ->where('funcionario.id',$_SESSION['fun_id'])
						   ->where('pedido.id',$maxcod)
						   ->limit(1)->fetch();
		$fun = array();
		$fun = $query2;
		return ($fun);
	}
	public function datosdirfinanciero(){
		$query = $this->db->from('funcionario')
						   ->LeftJoin('cargo ON cargo.id = funcionario.cargo_id')
						   ->where('cargo.nomcargo','DIRECTOR ADMINISTRATIVO-FINANCIERO')
						   ->limit(1)->fetch();
		$dirfin = array();
		$dirfin = $query;
		return ($dirfin);
	}
	public function generarPDFpedido(){

$medidas = array(280,217);
$pdf = new MYPDF('P', 'mm', $medidas, true, 'UTF-8', true);
$pdf->SetTitle('Formulario de pedido');
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// -------------------------desde aqui creamos pdf--------------------------------
$fun1 = $this->datosfuncionario();
/*transformamos el datetime y reformateamos*/
$date = date_create($fun1->fecha_creacion);
$fecha = date_format($date,'d-m-Y');

$pdf->SetFont('times', 'B', 15);
$pdf->AddPage();
$pdf->SetY(32);
$txt = <<<EOD
FORMULARIO DE SOLICITUD DE MATERIALES
EOD;
$pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
//$pdf->Ln(10);
$pdf->SetXY(37.2, 39.5);
$pdf->SetFont('helvetica', '', 12);
$txt = <<<EOD
CI.:
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('Courier', '', 12);
$pdf->SetXY(47, 39.5);
$txt = <<<EOD
$fun1->ci $fun1->exp
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('helvetica', '', 12);
$txt = <<<EOD
SOLICITANTE:
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('Courier', '', 12);
$pdf->SetXY(47, 44.5);
$txt = <<<EOD
$fun1->nombres $fun1->appaterno $fun1->apmaterno
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('helvetica', '', 12);
$pdf->SetX(27.2);
$txt = <<<EOD
CARGO:
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('Courier', '', 12);
$pdf->SetXY(47, 49.5);
$txt = <<<EOD
$fun1->nomcargo
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('helvetica', '', 12);
$txt = <<<EOD
DESTINO DEL MATERIAL:
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('Courier', '', 12);
$pdf->SetXY(75, 54.5);
$txt = <<<EOD
$fun1->descripcion
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('helvetica', '', 12);
$pdf->SetXY(145, 39.5);
$txt = <<<EOD
FECHA:
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('Courier', '', 12);
$pdf->SetXY(167, 39.5);
$txt = <<<EOD
$fecha
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->Ln(12);
$pdf->SetFont('Courier', '', 11);
$html = $this->construirtablapdf();
$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$dirfina = $this->datosdirfinanciero();
$pdf->lastPage();
$pdf->SetY(-63);
$pdf->SetFont('Courier', '', 8);
$html ='<div><table border="1">
<tr>
<td><table><tr><td>Solicitado por:</td>
</tr>
<tr>
<td>Lic. '.$fun1->nombres.' '.$fun1->appaterno.' '.$fun1->apmaterno.'</td>
</tr>
<tr>
<td>Firma:</td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td>Sello personal</td>
</tr>
</table></td>
<td><table>
<tr>
<td>Inmediato Superior:</td>
</tr>
<tr>
<td>Lic. </td>
</tr>
<tr>
<td>Firma:</td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td>Sello personal</td>
</tr>
</table></td>
<td><table>
<tr>
<td>Autorizado por:</td>
</tr>
<tr>
<td align="center">Lic. '.$dirfina->nombres.' '.$dirfina->appaterno.' '.$dirfina->apmaterno.'</td>
</tr>
<tr>
<td align="center">Direccion Administrativa Financiera</td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td>Sello</td>
</tr>
</table></td>
</tr>
</table></div>';
$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$nomfile = $this->crearnombrepdf();
//$ruta = $carpeta.'/'.$nomfile;
$pdf->Output($nomfile.'.pdf', 'I');
	}
/***FIN***ASTA AQUI SON LOS DATOS DE REPORTES DE FORMULARIO DE SOLICITUD DE MATERIALES */
/***INICIO***DESDE AQUI SON LOS DATOS DE REPORTES DE FORMULARIO DE ENTREGA DE MATERIALES */
	public function crearnombrepdfentrega($id){
		$query = $this->db->from('pedido')
						  ->select('funcionario_id AS idfun')
						  ->where('estado_pedido_id',2)
						  ->where('id',$id)
						  ->limit(1)->fetch();
		$idfun = $query->idfun;
		$fecha = date("YmdH:i:s");
		$nomfile = $id.'-'.$idfun.$fecha;
		return $nomfile;
	}
	public function datosfuncentrega($id){
		$query = $this->db->from('pedido')
						  ->select('funcionario_id AS idfun')
						  ->where('estado_pedido_id',2)
						  ->where('id',$id)
						  ->limit(1)->fetch();
		$idfun = $query->idfun;
		$query2 = $this->db->from('funcionario')
						   ->LeftJoin('cargo ON cargo.id = funcionario.cargo_id')
						   ->LeftJoin('area ON area.id = cargo.area_id')
						   ->LeftJoin('pedido ON pedido.funcionario_id = funcionario.id')
						   ->select('cargo.nomcargo,area.descripcion,pedido.fecha_creacion,pedido.fecha_entrega')
						   ->where('funcionario.id',$idfun)
						   ->where('pedido.id',$id)
						   ->limit(1)->fetch();
		$fun = array();
		$fun = $query2;
		return ($fun);
	}
	public function construirtablapdfentrega($id){
		$query1 = $this->db->from('pedido')
						  ->select('funcionario_id AS idfun')
						  ->where('estado_pedido_id',2)
						  ->where('id',$id)
						  ->limit(1)->fetch();
		$idfun = $query1->idfun;
		$query2 = $this->db->from('area')
						   ->LeftJoin('cargo ON cargo.area_id = area.id')
						   ->LeftJoin('funcionario ON funcionario.cargo_id = cargo.id')
						   ->select('area.id AS idarea')
						   ->where('funcionario.id',$idfun)
						   ->limit(1)->fetch();
		$idarea = $query2->idarea;
		$query = $this->db->from('material')
						  ->LeftJoin('categoria ON categoria.id = material.categoria_id')
						  ->LeftJoin('transaccion ON transaccion.material_id = material.id')
						  ->LeftJoin('cant_aut ON cant_aut.material_id = material.id')
						  ->select('cant_aut.ca,categoria.tipo,transaccion.q,transaccion.qe,transaccion.obs')
						  ->where('transaccion.pedido_id',$id)
						  ->where('cant_aut.area.id',$idarea)
						  ->fetchAll();
		$prueba = array();
		$prueba = $query;
		$cont1 = count($prueba);
		//$a = $prueba[0]->codigo;
		//return $a;
		$html="";
			$html=$html.'<div align="center">
			<br /><table border="1"  align="center">
			<tr bgcolor="#96939333" style="font-family:arial;">
				<th width="37px"><font color="#FFFFFF"><strong>ITEM</strong></font></th>
				<th width="70px"><font color="#FFFFFF"><strong>CODIGO</strong></font></th>
				<th width="240px"><font color="#FFFFFF"><strong>MATERIAL SUMINISTRO</strong></font></th>
				<th width="70px"><font color="#FFFFFF"><strong>UNIDAD</strong></font></th>
				<th width="80px"><font color="#FFFFFF"><strong>SOLICITADA</strong></font></th>
				<th width="80px"><font color="#FFFFFF"><strong>ENTREGADA</strong></font></th>
				<th width="85px"><font color="#FFFFFF" style=""><strong>OBSERVACION</strong></font></th>
			</tr>';
			$c = 1;
			for ($i=0; $i <= $cont1-1; $i++) {
				$html=$html.'<tr>';
				$html=$html.'<td>';
				$html=$html.$c;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->codigo;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->nom_material;
				$html=$html.' '.$prueba[$i]->descripcion;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->presentacion;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->q;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->qe;
				$html=$html.'</td><td style="font-size: 8rem;text-align: left;">';
				$html=$html.$prueba[$i]->obs;
				$html=$html.'</td></tr>';
				$c++;
			}
			$html=$html.'</table></div>';
		return ($html);
	}
	public function generarPDFentrega($id){
		$medidas = array(280,217);
$pdf = new MYPDF('P', 'mm', $medidas, true, 'UTF-8', true);
$pdf->SetTitle('Formulario de entrega');
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}
$fun1 = $this->datosfuncentrega($id);
/*transformamos el datetime y reformateamos*/
$date = date_create($fun1->fecha_entrega);
$fecha = date_format($date,'d-m-Y');

$pdf->SetFont('times', 'B', 15);
$pdf->AddPage();
$pdf->SetY(32);
$txt = <<<EOD
FORMULARIO DE ENTREGA DE MATERIALES
EOD;
$pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
//$pdf->Ln(10);
$pdf->SetXY(37.2, 39.5);
$pdf->SetFont('helvetica', '', 12);
$txt = <<<EOD
CI.:
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('Courier', '', 12);
$pdf->SetXY(47, 39.5);
$txt = <<<EOD
$fun1->ci $fun1->exp
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('helvetica', '', 12);
$txt = <<<EOD
SOLICITANTE:
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('Courier', '', 12);
$pdf->SetXY(47, 44.5);
$txt = <<<EOD
$fun1->nombres $fun1->appaterno $fun1->apmaterno
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('helvetica', '', 12);
$pdf->SetX(27.2);
$txt = <<<EOD
CARGO:
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('Courier', '', 12);
$pdf->SetXY(47, 49.5);
$txt = <<<EOD
$fun1->nomcargo
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('helvetica', '', 12);
$txt = <<<EOD
DESTINO DEL MATERIAL:
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('Courier', '', 12);
$pdf->SetXY(75, 54.5);
$txt = <<<EOD
$fun1->descripcion
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('helvetica', '', 12);
$pdf->SetXY(145, 39.5);
$txt = <<<EOD
FECHA:
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('Courier', '', 12);
$pdf->SetXY(167, 39.5);
$txt = <<<EOD
$fecha
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->Ln(12);
$pdf->SetFont('Courier', '', 10);
$html = $this->construirtablapdfentrega($id);
$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);

$pdf->SetY(-58);

$pdf->SetFont('Courier', '', 8);
$html='<table align="center">
<tr>
<td>...........................<br>ESFMTHEA<br>Responsable de Almacenes<br><strong>ENTREGUE CONFORME</strong></td>
<td>...........................<br>Unidad solicitante<br><strong>RECIBI CONFORME</strong></td>
</tr>
<tr>
<td height="40px"></td>
<td height="40px"></td>
</tr>
<tr>
<td>............................................<br><strong>Vo.Bo.Responsable de Bienes y Servicios</strong></td>
<td>............................................<br><strong>Vo.Bo.Dirección Administrativa Financiera</strong></td>
</tr>
</table>';
$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$pdf->lastPage();
		$nomfile = $this->crearnombrepdfentrega($id);
		$pdf->Output($nomfile.'.pdf', 'I');
	}
/***FIN***ASTA AQUI SON LOS DATOS DE REPORTES DE FORMULARIO DE ENTREGA DE MATERIALES */
/***INICIO***DESDE AQUI SON LOS DATOS DE REPORTES DE FORMULARIO DE INGRESO DE BIENES Y SERVICOS */
	public function crearnombrepdfingresos($id){
		$query = $this->db->from('pedido')
						  ->select('funcionario_id AS idfun')
						  ->where('id',$id)
						  ->limit(1)->fetch();
		$idfun = $query->idfun;
		$fecha = date("YmdH:i:s");
		$nomfile = $id.'-'.$idfun.$fecha;
		return $nomfile;
	}
	public function datosdelproveedor($id,$idprov){
		$query = $this->db->from('proveedor')
						  ->LeftJoin('pedido ON pedido.proveedor_id = proveedor.id')
						  ->select('pedido.id,pedido.Nro_factura,pedido.fuente, pedido.fecha_creacion AS fechapedido')
						  ->where('proveedor.id',$idprov)
						  ->where('pedido.id',$id)
						  ->limit(1)->fetch();
		$prov = array();
		$prov = $query;
		return ($prov);
	}
	public function construirtablapdfingreso($id){
		$query = $this->db->from('transaccion')
						  ->LeftJoin('pedido ON pedido.id = transaccion.pedido_id')
						  ->LeftJoin('material ON material.id = transaccion.material_id')
						  ->select(array('material.codigo,material.nom_material,material.descripcion,material.presentacion,pedido.Nro_factura,pedido.fecha_creacion AS fechped, pedido.fuente,pedido.partida_presup'))
						  ->where('transaccion.transaccion_tipo_id',1)
						  ->where('precio_unitario > ?',0)
						  ->where('pedido_id',$id)
						  //->where('qe',null)
						  ->fetchAll();
		$prueba = array();
		$prueba = $query;
		$cont1 = count($prueba);
		//$a = $prueba[0]->codigo;
		//return $a;
		$html="";
			$html=$html.'<div align="center">
			<br /><table border="1"  align="center">
			<tr bgcolor="#96939333" style="font-family:arial;">
				<th width="37px" rowspan="2"><font color="#FFFFFF"><strong>ITEM</strong></font></th>
				<th width="70px" rowspan="2"><font color="#FFFFFF"><strong>PARTIDA PRESUP.</strong></font></th>
				<th width="240px" rowspan="2"><font color="#FFFFFF"><strong>DESCRIPCIÓN</strong></font></th>
				<th width="70px" rowspan="2"><font color="#FFFFFF"><strong>UNIDAD</strong></font></th>
				<th width="80px" rowspan="2"><font color="#FFFFFF"><strong>CANTIDAD</strong></font></th>
				<th width="158px" colspan="2"><font color="#FFFFFF"><strong>IMPORTE</strong></font></th>
			</tr>
			<tr bgcolor="#96939333" style="font-family:arial;">
				<th><font color="#FFFFFF"><strong>PRECIO<br>UNITARIO<br>(Bs.)</strong></font></th>
				<th><font color="#FFFFFF"><strong>TOTAL<br>(Bs.)</strong></font></th>
			</tr>';
			$c = 1;
			$t = 0;
			for ($i=0; $i <= $cont1-1; $i++) {
				$html=$html.'<tr>';
				$html=$html.'<td>';
				$html=$html.$c;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->partida_presup;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->nom_material;
				$html=$html.' '.$prueba[$i]->descripcion;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->presentacion;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->q;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->precio_unitario;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->q * $prueba[$i]->precio_unitario;
				$html=$html.'</td></tr>';
				$c++;
				$t = $t + $prueba[$i]->q * $prueba[$i]->precio_unitario;
			}
			$html=$html.'<tr>
				<td colspan="6"><strong>TOTAL</strong></td>
				<td>'.$t.'</td>
			</tr>';
			$html=$html.'</table></div>';
		return ($html);
	}
	public function generarPDFingresos($id,$idprov){
		$medidas = array(280,217);
$pdf = new MYPDF('P', 'mm', $medidas, true, 'UTF-8', true);
$pdf->SetTitle('Formulario de Ingreso de Bienes y Materiales');
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}
$pdf->SetFont('times', 'B', 15);
$pdf->AddPage();
$pdf->SetY(32);
$txt = <<<EOD
FORMULARIO DE INGRESO DE BIENES
Y MATERIALES
EOD;
$pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
//$pdf->SetXY(37.2, 39.5);
$pdf->ln(4);
$prov1 = $this->datosdelproveedor($id,$idprov);
$date1 = date_create($prov1->fechapedido);
$fechapedido = date_format($date1,'d-m-Y');
$pdf->SetFont('helvetica', '', 11);
$html='<table border="1">
<tr>
	<td> ALMACEN: <a style="text-decoration:none; font-family:Courier; color:black;">ACTIVO FUNGIBLE</a></td>
	<td> DIRECCIÓN: <a style="text-decoration:none; font-family:Courier; color:black;">'.$prov1->direccion.'</a></td>
</tr>
<tr>
	<td> NOMBRE DE LA EMPRESA: <a style="text-decoration:none; font-family:Courier; color:black;">'.$prov1->empresa.'</a></td>
	<td> TELEFÓNO: <a style="text-decoration:none; font-family:Courier; color:black;">'.$prov1->telefono.'</a></td>
</tr>
<tr>
	<td> NOMBRE DEL PROPIETARIO: <a style="text-decoration:none; font-family:Courier; color:black;">'.$prov1->nombres.' '.$prov1->apellidos.'</a></td>
	<td> UNIDAD SOLICITANTE: <a style="text-decoration:none; font-family:Courier; color:black;">ALMACENES-INVENTARIO</a></td>
</tr>
<tr>
	<td> NRO DE FACTURA: <a style="text-decoration:none; font-family:Courier; color:black;">'.$prov1->Nro_factura.'</a></td>
	<td> FECHA: <a style="text-decoration:none; font-family:Courier; color:black;">'.$fechapedido.'</a></td>
</tr>
<tr>
	<td> NIT.: <a style="text-decoration:none; font-family:Courier; color:black;">'.$prov1->Nit.'</a></td>
	<td> FUENTE: <a style="text-decoration:none; font-family:Courier; color:black;">'.$prov1->fuente.'</a></td>
</tr>
</table>';
$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);

$pdf->SetFont('Courier', '', 10);
$html = $this->construirtablapdfingreso($id);
$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);

$pdf->SetY(-40);

$pdf->SetFont('Courier', '', 8);
$html='<table align="center">
<tr>
<td>...........................<br><strong>ENTREGUE CONFORME</strong></td>
<td>...........................<br><strong>RECIBI CONFORME</strong></td>
</tr>
</table>';
$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$pdf->lastPage();
$nomfile = $this->crearnombrepdfingresos($id);
		$pdf->Output($nomfile.'.pdf', 'I');
	}
/***FIN***HASTA AQUI SON LOS DATOS DE REPORTES DE FORMULARIO DE INGRESO DE BIENES Y SERVICOS */
}
require_once('core/TCPDF/tcpdf.php');
class MYPDF extends TCPDF {

	//Page header
	public function Header() {
		$this->Image('assets/img/logo.jpg', 10, 10, 29, '', 'JPG', '', 'T', false, 400, '', false, false, 0, false, false, false);
		// Set font
		$this->SetFont('helvetica', '', 12);
		// Title
		$this->Cell(0, 15, 'ESCUELA SUPERIOR DE FORMACIÓN DE MAESTROS TECNOLÓGICO HUMANÍSTICO DE EL ALTO', 0, 1, 'C', 0, '', 1);
		$this->SetY(15);
		$this->Cell(0, 15, 'DIRECCIÓN ADMINISTRATIVA FINANCIERA', 0, 1, 'C', 0, '', 1);
		$this->SetY(20);
		//$this->Cell(0, 15, 'ALMACENES E INVENTARIO', 0, false, 'C', 0, '', 0, false, 'M', 'M');
		$this->Cell(0, 15, 'ALMACENES E INVENTARIO', 0, 1, 'C', 0, '', 1);
	}
	public function Footer() {
		$this->SetY(-15);
		$this->SetFont('helvetica', 'I', 10);
		$this->Cell(0, 10, ''.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}
?>