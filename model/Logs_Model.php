<?php
/**
*
*/
class Logs_Model
{

	function __construct()
	{
		require_once('core/Conectar.php');
		$aux=new Conectar;
		$this->db=$aux->ConexionPDO();
	}
	public function listarlogs()
	{
		$query = $this->db->from('logs')
									->limit(100)
									->orderBy('id DESC')
									->fetchAll();
		return json_encode($query);
	}
}
?>