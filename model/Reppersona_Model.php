<?php
date_default_timezone_set('America/La_Paz');
/**
*
*/
class Reppersona_Model
{

	function __construct()
	{
		require_once('core/Conectar.php');
		$aux=new Conectar;
		$this->db=$aux->ConexionPDO();
	}
	public function datosfuncionarios()
	{
		$query = $this->db->from('funcionario')
						  ->where('id > ?',0)
						  ->fetchAll();
		return json_encode($query);
	}
	public function detalleun_fun($idfun)
	{
		$query = $this->db->from('funcionario')
						  ->LeftJoin('cargo ON cargo.id = funcionario.cargo_id')
						  ->select('cargo.nomcargo')
						  ->where('funcionario.id',$idfun)
						  ->limit(1)->fetch();
		return ($query);
	}
	public function buscarDatos($fechaini, $fechafin,$idfun)
	{
		$query = $this->db->from('pedido')
						  ->LeftJoin('transaccion ON transaccion.pedido_id = pedido.id')
						  ->LeftJoin('material ON material.id = transaccion.material_id')
						  ->select('codigo,nom_material,descripcion,presentacion,pedido.fecha_entrega,pedido.estado_pedido_id,q,qe')
						  ->where('pedido.funcionario_id',$idfun)
						  ->where('pedido.transaccion_tipo_id',2)
						  ->where('pedido.estado_pedido_id',2)
						  ->where('date(pedido.fecha_entrega) >= ?',date("Y-m-d", strtotime($fechaini)))
						  ->where('date(pedido.fecha_entrega) <= ?',date("Y-m-d", strtotime($fechafin)))
						  ->fetchAll();
		return json_encode($query);
	}
	public function construirtablareporte($fechaini, $fechafin, $idfun)
	{
		$query = $this->db->from('pedido')
						  ->LeftJoin('transaccion ON transaccion.pedido_id = pedido.id')
						  ->LeftJoin('material ON material.id = transaccion.material_id')
						  ->select('codigo,nom_material,descripcion,presentacion,pedido.fecha_entrega,pedido.estado_pedido_id,q,qe')
						  ->where('pedido.funcionario_id',$idfun)
						  ->where('pedido.transaccion_tipo_id',2)
						  ->where('pedido.estado_pedido_id',2)
						  ->where('date(pedido.fecha_entrega) >= ?',date("Y-m-d", strtotime($fechaini)))
						  ->where('date(pedido.fecha_entrega) <= ?',date("Y-m-d", strtotime($fechafin)))
						  ->fetchAll();
		$prueba = array();
		$prueba = $query;
		$cont1 = count($prueba);
		$html="";
		$html=$html.'<div align="center">
			<br /><table border="1"  align="center">
			<tr bgcolor="#96939333" style="font-family:arial;">
				<th width="37px"><font color="#FFFFFF"><strong>ITEM</strong></font></th>
				<th width="70px"><font color="#FFFFFF"><strong>CODIGO</strong></font></th>
				<th width="240px"><font color="#FFFFFF"><strong>MATERIAL SUMINISTRO</strong></font></th>
				<th width="70px"><font color="#FFFFFF"><strong>UNIDAD</strong></font></th>
				<th width="85px"><font color="#FFFFFF"><strong>CANTIDAD SOLICITADA</strong></font></th>
				<th width="85px"><font color="#FFFFFF"><strong>CANTIDAD ENTREGADA</strong></font></th>
				<th width="85px"><font color="#FFFFFF"><strong>FECHA DE ENTREGA</strong></font></th>
			</tr>';
			$c = 1;
			for ($i=0; $i <= $cont1-1; $i++) {
				$date = date_create($prueba[$i]->fecha_entrega);
				$fecha = date_format($date,'d-m-Y');
				$html=$html.'<tr>';
				$html=$html.'<td>';
				$html=$html.$c;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->codigo;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->nom_material;
				$html=$html.' '.$prueba[$i]->descripcion;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->presentacion;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->q;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->qe;
				$html=$html.'</td><td>';
				$html=$html.$fecha;
				$html=$html.'</td></tr>';
				$c++;
			}
		$html=$html.'</table></div>';
		return ($html);
	}
	public function pdf($fechaini,$fechafin,$idfun)
	{
$medidas = array(280,217);
$pdf = new MYPDF('P', 'mm', $medidas, true, 'UTF-8', true);
$pdf->SetTitle('Reporte General Por Funcionario');
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}
// -------------------------desde aqui creamos pdf--------------------------------
$fun1 = $this->detalleun_fun($idfun);
date_default_timezone_set("America/La_Paz");
$this->created_at = Date(DATE_ATOM,time());
$date = date_create($this->created_at);
$fecha = date_format($date,'d/m/Y H:s');

$pdf->AddPage();
$pdf->SetY(25);
setlocale(LC_TIME, "Spanish");
$pdf->SetFont('helvetica', '', 10);
$nueva_fecha1 = date("d-m-Y", strtotime($fechaini));
$nueva_fecha2 = date("d-m-Y", strtotime($fechafin));
$mes_anyo1 = strftime("%d de %B del %Y", strtotime($nueva_fecha1));
$mes_anyo2 = strftime("%d de %B del %Y", strtotime($nueva_fecha2));
$pdf->Cell(0, 15, '(DEL '.$mes_anyo1.' AL '.$mes_anyo2.')', 0, 1, 'C', 0, '', 1);
$pdf->SetFont('times', 'B', 15);
$pdf->SetY(37);
$txt = <<<EOD
REPORTE DE MATERIALES ENTREGADOS
EOD;
$pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
$pdf->SetXY(37.2, 44);
$pdf->SetFont('helvetica', '', 12);
$txt = <<<EOD
CI.:
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('Courier', '', 12);
$pdf->SetXY(47, 44);
$txt = <<<EOD
$fun1->ci $fun1->exp
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('helvetica', '', 12);
$txt = <<<EOD
SOLICITANTE:
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('Courier', '', 12);
$pdf->SetXY(47, 49);
$txt = <<<EOD
$fun1->nombres $fun1->appaterno $fun1->apmaterno
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('helvetica', '', 12);
$pdf->SetX(27.2);
$txt = <<<EOD
CARGO:
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('Courier', '', 12);
$pdf->SetXY(47, 54);
$txt = <<<EOD
$fun1->nomcargo
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('helvetica', '', 10);
$pdf->SetXY(130, 45);
$txt = <<<EOD
FECHA REPORTE:
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('Courier', '', 10);
$pdf->SetXY(165, 45);
$txt = <<<EOD
$fecha
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->Ln(12);
$pdf->SetFont('Courier', '', 11);
$pdf->SetXY(13,56);
$html = $this->construirtablareporte($fechaini, $fechafin, $idfun);
$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);

		$pdf->Output('hola.pdf', 'I');

	}
}
require_once('core/TCPDF/tcpdf.php');
class MYPDF extends TCPDF {

	//Page header
	public function Header() {
		$this->Image('assets/img/logo.jpg', 10, 10, 29, '', 'JPG', '', 'T', false, 400, '', false, false, 0, false, false, false);
		// Set font
		$this->SetFont('helvetica', '', 12);
		// Title
		$this->Cell(0, 15, 'ESCUELA SUPERIOR DE FORMACIÓN DE MAESTROS TECNOLÓGICO HUMANÍSTICO DE EL ALTO', 0, 1, 'C', 0, '', 1);
		$this->SetY(15);
		$this->Cell(0, 15, 'DIRECCIÓN ADMINISTRATIVA FINANCIERA', 0, 1, 'C', 0, '', 1);
		$this->SetY(20);
		//$this->Cell(0, 15, 'ALMACENES E INVENTARIO', 0, false, 'C', 0, '', 0, false, 'M', 'M');
		$this->Cell(0, 15, 'ALMACENES E INVENTARIO', 0, 1, 'C', 0, '', 1);
	}
	public function Footer() {
		$this->SetY(-15);
		$this->SetFont('helvetica', 'I', 10);
		$this->Cell(0, 10, ''.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}
?>
