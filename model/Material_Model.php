<?php 
/**
* 
*/
class Material_Model
{
	
	function __construct()
	{
		require_once('core/Conectar.php');
		$aux=new Conectar;
		$this->db=$aux->ConexionPDO();
		$this->db1=$aux->Conexion_PDO();
		date_default_timezone_set("America/La_Paz");
		$this->created_at = Date(DATE_ATOM,time());
	}
	public function crearmaterial($matcod,$mattipo,$nommat,$desmat,$minmat/*,$invinicial*/,$presmat,$matactivo)
	{
		try
        {
        	$this->db1->beginTransaction();
        
			$this->db1->exec("insert into material(id,codigo,nom_material,descripcion,min_inventario,presentacion,activo,categoria_id,fecha_creacion) values (null,$matcod,'$nommat','$desmat','$minmat','$presmat','$matactivo',$mattipo,'$this->created_at')");
			/*$lastId = $this->db1->lastInsertId();

			$this->db1->exec("insert into transaccion(id,material_id,q,transaccion_tipo_id ,pedido_id,fecha_creacion) values (null,$lastId,$invinicial,1,null,'$this->created_at')");
*/
        	$this->db1->commit();

        	$ok='1';
			return json_encode(array($ok));
		}
		catch(PDOException $e)
        {
			die($e->getMessage());
			$this->db1->rollBack();
        }
	}
	public function listarmaterial(){
		$query = $this->db->from('material')
						  ->LeftJoin('categoria ON categoria.id = material.categoria_id')
						  ->select(array('tipo'))
						  ->fetchAll();
		return json_encode($query);
	}
	public function eliminarmaterial($id){
		/*try {
			$this->db1->beginTransaction();
			$this->db1->exec("DELETE FROM transaccion where material_id = $id");
			$this->db1->exec("DELETE FROM material where id = $id");
			$this->db1->commit();
			$ok='1';
			return json_encode(array($ok));
		} catch (PDOException $e) {
			die($e->getMessage());
			$this->db1->rollBack();
		}*/
		$query = $this->db->deleteFrom('material')->where('id', $id);

		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}
		else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function editarmate($id,$matcod,$mattipo,$nommat,$desmat,$minmat,$presmat,$matactivo){
		$values = array('codigo' => $matcod, 'nom_material' => $nommat, 'descripcion' => $desmat, 'min_inventario' => $minmat, 'presentacion' => $presmat, 'activo' => $matactivo, 'categoria_id' => $mattipo,'fecha_creacion' => $this->created_at);

		$query = $this->db->update('material')->set($values)->where('id', $id);

		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}
		else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function buscarunmate($id){
		$query = $this->db->from('material')
						  ->LeftJoin('categoria ON categoria.id = material.categoria_id')
						  ->select(array('tipo','nombre_cat'))
						  ->where('material.id', $id)
						  ->limit(1)->fetch();
		return json_encode(array($query));
	}
	
}
?>