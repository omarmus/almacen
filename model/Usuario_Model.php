<?php
/**
*
*/
class Usuario_Model
{

	function __construct()
	{
		require_once('core/Conectar.php');
		$aux=new Conectar;
		$this->db=$aux->ConexionPDO();
	}
	public function listarusuario()
	{
		$query = $this->db->from('usuario')
						  ->where('usuario.id > ?', 0)
						  ->LeftJoin('funcionario ON funcionario.id = usuario.funcionario_id')
						  ->LeftJoin('rol ON rol.id = usuario.rol_id')
						  ->select(array('nombres, ci ,rol_usuario'))
						  ->orderBy('id')
						  ->fetchAll();
		return json_encode($query);
	}
	public function buscarcifun(){

		$query = $this->db->from('usuario')
						  ->LeftJoin('funcionario ON funcionario.id = usuario.funcionario_id')
						  ->select(array('nombres, ci'))
						  ->fetchAll();

		return json_encode($query);
	}
	public function listatodosci(){
		$query = $this->db->from('funcionario')
							->select(array('id,nombres, ci'))
						  	->fetchAll();
		return json_encode($query);
	}
	public function crearusuario($fun_id,$nomusuario,$password1,$rol_id,$useractivo){

		$aux1 = $this->db->from('usuario')
						  ->where('usuario.nombre', $nomusuario)
						  ->limit(1)->fetch();
		if (isset($aux1->nombre)) {
			echo "ya existe".' '.$nomusuario;
		}else{
			$passw = sha1($password1);
			$values = array('nombre' => $nomusuario, 'password' => $passw, 'estado' => $useractivo, 'funcionario_id' => $fun_id, 'rol_id' => $rol_id);
			$query = $this->db->insertInto('usuario')->values($values);

			if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
			}
			else{
			/*echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";*/
        		echo "Nose se Registro!";
			}
		}
	}
	public function eliminarusuario($id){
		$query = $this->db->deleteFrom('usuario')->where('id', $id);

		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function buscarunusuario($id){
		$query = $this->db->from('usuario')
							->LeftJoin('funcionario ON funcionario.id = usuario.funcionario_id')
							->select(array('nombres, ci'))
							->where('usuario.id', $id)
						  	->limit(1)->fetch();
		return json_encode(array($query));
	}
	public function editarusuario($id,$fun_id,$nomusuario,$password1,$rol_id,$useractivo,$editapassword){


		if ($editapassword == true) {
			$passw = sha1($password1);
		}else{
			$passw = $password1;
		}
		$aux1 = $this->db->from('usuario')
						  ->where('usuario.nombre', $nomusuario)
						  ->limit(1)->fetch();
		if (isset($aux1->nombre)){
			$aux2 = $this->db->from('usuario')
						  ->where('usuario.nombre',$nomusuario)
						  ->where('usuario.id',$id)
						  ->limit(1)->fetch();
			if (isset($aux2->nombre)) {
				$values = array('nombre' => $nomusuario, 'password' => $passw, 'estado' => $useractivo, 'funcionario_id' => $fun_id, 'rol_id' => $rol_id);
				$query = $this->db->update('usuario')->set($values)->where('id', $id);

				if($query->execute()){
					$ok='1';
					return json_encode(array($ok));
				}else{
        			echo "Nose se Registro!";
				}
			}else{
				echo "ya existe".' '.$nomusuario;
			}
		}else{

			$values = array('nombre' => $nomusuario, 'password' => $passw, 'estado' => $useractivo, 'funcionario_id' => $fun_id, 'rol_id' => $rol_id);
			$query = $this->db->update('usuario')->set($values)->where('id', $id);

			if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
			}
			else{
        		echo "Nose se Registro!";
			}

		}
	}
	public function editarpass($id, $password1) {
		$passw = sha1($password1);

		$values = array('password' => $passw);
		$query = $this->db->update('usuario')->set($values)->where('id', $id);

		if($query->execute()){
			$ok = '1';
			return json_encode(array($ok));
		} else {
			echo "No se pudo cambiar la contraseña!";
		}
	}
}
