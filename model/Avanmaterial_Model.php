<?php 
/**
* 
*/
class Avanmaterial_Model
{
	
	function __construct()
	{
		require_once('core/Conectar.php');
		$aux=new Conectar;
		$this->db=$aux->ConexionPDO();
		$this->db1=$aux->Conexion_PDO();
		date_default_timezone_set("America/La_Paz");
		$this->created_at = Date(DATE_ATOM,time());
	}
	public function buscarareadefun($id){
		$query = $this->db->from('cargo')
						  ->LeftJoin('area ON area.id = cargo.area_id')
						  ->LeftJoin('funcionario ON funcionario.cargo_id = cargo.id')
						  ->select(array('nomcargo','area.descripcion','cargo.area_id'))
						  ->where('funcionario.id', $id)
						  ->limit(1)->fetch();
		return json_encode(array($query));
	}
	public function listadetallada($id){
		$idmat = "";
		$gral = $this->listarcan_taut($id);
		$todos =  array();
		$cont = count($gral);
		for ($i=0; $i <= $cont-1; $i++) {
			$idmat = $gral[$i]->material_id;
			$aux = $this->detalleunmaterial($idmat);
			array_push($todos, array('id'=>$gral[$i]->id,'ca'=>$gral[$i]->ca,'area_id'=>$gral[$i]->area_id,'material_id'=>$gral[$i]->material_id,'fecha_creacion'=>$gral[$i]->fecha_creacion,'descripcion'=>$gral[$i]->descripcion,'presentacion'=>$gral[$i]->presentacion,'nom_material'=>$gral[$i]->nom_material,'codigo'=>$gral[$i]->codigo,'q'=>$aux));
		}
		return json_encode($todos);
		/*return json_encode($gral);*/
	}
	public function listarcan_taut($id){
		$query = $this->db->from('cant_aut')
						  ->LeftJoin('material ON material.id = cant_aut.material_id')
						  /*->LeftJoin('transaccion ON transaccion.material_id = material.id')*/
						  ->select(array('descripcion,nom_material,codigo,presentacion'))
						  ->where('cant_aut.area_id', $id) 
						  /*->where('transaccion.pedido_id', null)*/
						  ->orderBy('material.id')
						  ->fetchAll();
		return $query;
	}
	public function detalleunmaterial($id){
		$in = $this->totalentradas($id);
		$out = $this->totalsalidas($id);
		$total = $in - $out;
		return json_encode($total);
	}
	public function totalentradas($id){
		$query = $this->db->from('transaccion')
						  ->select('q')
						  ->where('material_id',$id)
						  ->where('transaccion_tipo_id',1)
						  ->fetchAll();
		$ins = array();
		$ins = $query;
		$t = 0; 
		$arrlength = count($ins);
		for ($x = 0; $x <= $arrlength-1; $x++) {
    		$t = $t + $ins[$x]->q;
		} 
		return $t;
	}
	public function totalsalidas($id){
		$query = $this->db->from('transaccion')
						  ->LeftJoin('pedido ON pedido.id = transaccion.pedido_id')
						  ->select('qe')
						  ->where('material_id',$id)
						  ->where('transaccion.transaccion_tipo_id',2)
						  ->where('estado_pedido_id',2)
						  ->fetchAll();
		$uots = array();
		$uots = $query;
		$t = 0; 
		$arrlength = count($uots);
		for ($x = 0; $x <= $arrlength-1; $x++) {
    		$t = $t + $uots[$x]->qe;
		} 
		return $t;
	}
	/*public function listarcantaut($id){
		$query = $this->db->from('cant_aut')
						  ->LeftJoin('material ON material.id = cant_aut.material_id')
						  ->LeftJoin('transaccion ON transaccion.material_id = material.id')
						  ->select(array('descripcion,nom_material,codigo,q'))
						  ->where('cant_aut.area_id', $id)
						  ->where('transaccion.pedido_id', null)
						  ->orderBy('material.id')
						  ->fetchAll();
		return json_encode($query);
	}*/
	public function buscarunmate($id,$idarea){
		$query = $this->db->from('material')
						  ->LeftJoin('cant_aut ON cant_aut.material_id = material.id')
						  ->select(array('ca','area_id'))
						  ->where('material.id', $id)
						  ->where('cant_aut.area_id', $idarea)
						  ->limit(1)->fetch();
		return json_encode(array($query));
	}
	public function crearpedido($trans,$user_id,$fun_id){
		/*var_dump($trans); para mostrar la estructura de un array que recivimos desde el controlador*/
		//echo($trans[0]->array1[0]->codigo); 
		$cont1 = count($trans[0]->array1);
		//var_dump($trans);
		try
        {
        	$this->db1->beginTransaction();
        
			$this->db1->exec("insert into pedido(id,estado_pedido_id,funcionario_id,proveedor_id,usuario_id,transaccion_tipo_id,fecha_creacion) values (null,1,$fun_id,null,$user_id,2,'$this->created_at')");
			$lastId = $this->db1->lastInsertId();
			for ($i=0; $i <= $cont1-1; $i++) {
				$idmat = htmlspecialchars(strip_tags($trans[0]->array1[$i]->material_id)); 
				$q = htmlspecialchars(strip_tags($trans[0]->array1[$i]->ca)); 
				$this->db1->exec("insert into transaccion(id,material_id,q,transaccion_tipo_id ,pedido_id,fecha_creacion) values (null,$idmat,$q,2,$lastId,'$this->created_at')");
			}
        	$this->db1->commit();

        	$ok='1';
			return json_encode(array($ok));
		}
		catch(PDOException $e)
        {
			die($e->getMessage());
			$this->db1->rollBack();
        }
	}
}
?>