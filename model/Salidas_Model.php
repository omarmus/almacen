<?php 
/**
* 
*/
class Salidas_Model
{
	
	function __construct()
	{
		require_once('core/Conectar.php');
		$aux=new Conectar;
		$this->db=$aux->ConexionPDO();
		$this->db1=$aux->Conexion_PDO();
		date_default_timezone_set("America/La_Paz");
		$this->created_at = Date(DATE_ATOM,time());
	}
	public function listarSalidas(){
		$query = $this->db->from('pedido')
						  ->LeftJoin('funcionario ON funcionario.id = pedido.funcionario_id')
						  ->LeftJoin('cargo ON cargo.id = funcionario.cargo_id')
						  ->LeftJoin('area ON area.id = cargo.area_id')
						  ->select(array('nombres,appaterno,apmaterno,cargo.id AS idcar,cargo.nomcargo,area.id AS areaid,area.descripcion'))
						  ->where('pedido.transaccion_tipo_id',2)
						  ->where('estado_pedido_id',2)
						  ->fetchAll();
		return json_encode($query);
	}
	public function detalleunasalida($id){
		$query = $this->db->from('transaccion')
						  ->LeftJoin('pedido ON pedido.id = transaccion.pedido_id')
						  ->LeftJoin('material ON material.id = transaccion.material_id')
						  ->select(array('material.codigo,material.nom_material,material.descripcion,material.presentacion,pedido.fecha_entrega AS fechped'))
						  ->where('transaccion.transaccion_tipo_id',2)
						  ->where('pedido.estado_pedido_id',2)
						  ->where('pedido_id',$id)
						  //->where('qe',null)
						  ->fetchAll();
		return json_encode($query);
	}
}
?>