<?php 
/**
* 
*/
class Solmaterialfun_Model
{
	
	function __construct()
	{
		require_once('core/Conectar.php');
		$aux=new Conectar;
		$this->db=$aux->ConexionPDO();
		$this->db1=$aux->Conexion_PDO();
		date_default_timezone_set("America/La_Paz");
		$this->created_at = Date(DATE_ATOM,time());
	}
	public function listarFuncionario($idfun){
		
		$query = $this->db->from('funcionario')
						  ->LeftJoin('usuario ON usuario.funcionario_id = funcionario.id')
						  ->LeftJoin('cargo ON cargo.id = funcionario.cargo_id')
						  ->LeftJoin('area ON area.id = cargo.area_id')
						  ->select(array('cargo.nomcargo,area.id AS areaid,area.descripcion,usuario.id AS usid'))
						  ->where('funcionario.id > 0')
						  ->where('funcionario.id != ?',$idfun)
						  ->fetchAll();
		return json_encode($query);
	}
	public function datosfuncionario($idfun){
		$query = $this->db->from('pedido')
						  ->select('MAX(pedido.id) AS num')
						  ->where('estado_pedido_id',1)
						  ->limit(1)->fetch();
		$maxcod = $query->num;
		$query2 = $this->db->from('funcionario')
						   ->LeftJoin('cargo ON cargo.id = funcionario.cargo_id')
						   ->LeftJoin('area ON area.id = cargo.area_id')
						   ->LeftJoin('pedido ON pedido.funcionario_id = funcionario.id')
						   ->select('cargo.nomcargo,area.descripcion,pedido.fecha_creacion')
						   ->where('funcionario.id',$idfun)
						   ->where('pedido.id',$maxcod)
						   ->limit(1)->fetch();
		$fun = array();
		$fun = $query2;
		return ($fun);
	}
	public function construirtablapdf($idfun){
		$query1 = $this->db->from('pedido')
						  ->select('MAX(pedido.id) AS num')
						  ->where('estado_pedido_id',1)
						  ->limit(1)->fetch();
		$maxcod = $query1->num;
		$query2 = $this->db->from('area')
						   ->LeftJoin('cargo ON cargo.area_id = area.id')
						   ->LeftJoin('funcionario ON funcionario.cargo_id = cargo.id')
						   ->select('area.id AS idarea')
						   ->where('funcionario.id',$idfun)
						   ->limit(1)->fetch();
		$idarea = $query2->idarea;				  
		$query = $this->db->from('material')
						  ->LeftJoin('categoria ON categoria.id = material.categoria_id')
						  ->LeftJoin('transaccion ON transaccion.material_id = material.id')
						  ->LeftJoin('cant_aut ON cant_aut.material_id = material.id')
						  ->select('cant_aut.ca,categoria.tipo,transaccion.q')
						  ->where('transaccion.pedido_id',$maxcod)
						  ->where('cant_aut.area.id',$idarea)
						  ->fetchAll();
		$prueba = array();
		$prueba = $query;
		$cont1 = count($prueba);
		//$a = $prueba[0]->codigo;
		//return $a;
		$html="";
			$html=$html.'<div align="center">
			<br /><table border="1"  align="center">
			<tr bgcolor="#96939333" style="font-family:arial;">
				<th width="37px"><font color="#FFFFFF"><strong>ITEM</strong></font></th>
				<th width="70px"><font color="#FFFFFF"><strong>CODIGO</strong></font></th>
				<th width="70px"><font color="#FFFFFF"><strong>TIPO</strong></font></th>
				<th width="240px"><font color="#FFFFFF"><strong>MATERIAL SUMINISTRO</strong></font></th>
				<th width="70px"><font color="#FFFFFF"><strong>UNIDAD</strong></font></th>
				<th width="85px"><font color="#FFFFFF"><strong>CANTIDAD SOLICITADA</strong></font></th>
				<th width="85px"><font color="#FFFFFF"><strong>CANTIDAD AUTORIZADA</strong></font></th>
			</tr>';
			$c = 1;
			for ($i=0; $i <= $cont1-1; $i++) { 
				$html=$html.'<tr>';
				$html=$html.'<td>';
				$html=$html.$c;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->codigo;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->tipo;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->nom_material;
				$html=$html.' '.$prueba[$i]->descripcion;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->presentacion;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->q;
				$html=$html.'</td><td>';
				$html=$html.$prueba[$i]->ca;
				$html=$html.'</td></tr>';
				$c++;	
			}
			$html=$html.'</table></div>';
		return ($html);
	}
	public function datosdirfinanciero(){
		$query = $this->db->from('funcionario')
						   ->LeftJoin('cargo ON cargo.id = funcionario.cargo_id')
						   ->where('cargo.nomcargo','DIRECTOR ADMINISTRATIVO-FINANCIERO')
						   ->limit(1)->fetch();
		$dirfin = array();
		$dirfin = $query;
		return ($dirfin);
	}
	public function crearnombrepdf($idfun){
		$query = $this->db->from('pedido')
						  ->select('MAX(pedido.id) AS num')
						  ->where('estado_pedido_id',1)
						  ->limit(1)->fetch();
		$maxcod = $query->num;
		$fecha = date("YmdH:i:s");	
		$nomfile = $maxcod.'-'.$idfun.$fecha;
		return $nomfile;
	}
	public function generarPDFpedido($idfun){
$medidas = array(280,217);
$pdf = new MYPDF('P', 'mm', $medidas, true, 'UTF-8', true);
$pdf->SetTitle('Formulario de pedido');
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// -------------------------desde aqui creamos pdf--------------------------------
$fun1 = $this->datosfuncionario($idfun);

$date = date_create($fun1->fecha_creacion);
$fecha = date_format($date,'d-m-Y');

$pdf->SetFont('times', 'B', 15);
$pdf->AddPage();
$pdf->SetY(32);
$txt = <<<EOD
FORMULARIO DE SOLICITUD DE MATERIALES
EOD;
$pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
//$pdf->Ln(10);
$pdf->SetXY(37.2, 39.5);
$pdf->SetFont('helvetica', '', 12);
$txt = <<<EOD
CI.:
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('Courier', '', 12);
$pdf->SetXY(47, 39.5);
$txt = <<<EOD
$fun1->ci $fun1->exp
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('helvetica', '', 12);
$txt = <<<EOD
SOLICITANTE:
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('Courier', '', 12);
$pdf->SetXY(47, 44.5);
$txt = <<<EOD
$fun1->nombres $fun1->appaterno $fun1->apmaterno
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('helvetica', '', 12);
$pdf->SetX(27.2);
$txt = <<<EOD
CARGO:
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('Courier', '', 12);
$pdf->SetXY(47, 49.5);
$txt = <<<EOD
$fun1->nomcargo
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('helvetica', '', 12);
$txt = <<<EOD
DESTINO DEL MATERIAL:
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('Courier', '', 12);
$pdf->SetXY(75, 54.5);
$txt = <<<EOD
$fun1->descripcion
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('helvetica', '', 12);
$pdf->SetXY(145, 39.5);
$txt = <<<EOD
FECHA:
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetFont('Courier', '', 12);
$pdf->SetXY(167, 39.5);
$txt = <<<EOD
$fecha
EOD;
$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);
$pdf->Ln(12);
$pdf->SetFont('Courier', '', 11);
$html = $this->construirtablapdf($idfun);
$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$dirfina = $this->datosdirfinanciero();
$pdf->lastPage();
$pdf->SetY(-63);
$pdf->SetFont('Courier', '', 8);
$html ='<div><table border="1">
<tr>
<td><table><tr><td>Solicitado por:</td>
</tr>
<tr>
<td>Lic. '.$fun1->nombres.' '.$fun1->appaterno.' '.$fun1->apmaterno.'</td>
</tr>
<tr>
<td>Firma:</td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td>Sello personal</td>
</tr>
</table></td>
<td><table>
<tr>
<td>Inmediato Superior:</td>
</tr>
<tr>
<td>Lic. </td>
</tr>
<tr>
<td>Firma:</td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td>Sello personal</td>
</tr>
</table></td>
<td><table>
<tr>
<td>Autorizado por:</td>
</tr>
<tr>
<td align="center">Lic. '.$dirfina->nombres.' '.$dirfina->appaterno.' '.$dirfina->apmaterno.'</td>
</tr>
<tr>
<td align="center">Direccion Administrativa Financiera</td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td>Sello</td>
</tr>
</table></td>
</tr>
</table></div>';
$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$nomfile = $this->crearnombrepdf($idfun);

$pdf->Output($nomfile.'.pdf', 'I');
	}
}
require_once('core/TCPDF/tcpdf.php');
class MYPDF extends TCPDF {

	//Page header
	public function Header() {
		$this->Image('assets/img/logo.jpg', 10, 10, 29, '', 'JPG', '', 'T', false, 400, '', false, false, 0, false, false, false);
		// Set font
		$this->SetFont('helvetica', '', 12);
		// Title
		$this->Cell(0, 15, 'ESCUELA SUPERIOR DE FORMACIÓN DE MAESTROS TECNOLÓGICO HUMANÍSTICO DE EL ALTO', 0, 1, 'C', 0, '', 1);
		$this->SetY(15);
		$this->Cell(0, 15, 'DIRECCIÓN ADMINISTRATIVA FINANCIERA', 0, 1, 'C', 0, '', 1);
		$this->SetY(20);
		//$this->Cell(0, 15, 'ALMACENES E INVENTARIO', 0, false, 'C', 0, '', 0, false, 'M', 'M');
		$this->Cell(0, 15, 'ALMACENES E INVENTARIO', 0, 1, 'C', 0, '', 1);
	}
	public function Footer() {
		$this->SetY(-15);
		$this->SetFont('helvetica', 'I', 10);
		$this->Cell(0, 10, ''.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}
?>