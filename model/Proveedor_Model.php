<?php 
/**
* 
*/
class Proveedor_Model
{
	
	function __construct()
	{
		require_once('core/Conectar.php');
		$aux=new Conectar;
		$this->db=$aux->ConexionPDO();
		date_default_timezone_set("America/La_Paz");
		$this->created_at = Date(DATE_ATOM,time());
	}
	public function listarproveedor()
	{
		$query = $this->db->from('proveedor')
						  ->fetchAll();
		return json_encode($query);
	} 
	public function crearproveedor($nit,$emp,$nom,$ap,$dir,$fono,$email){
		$values = array('Nit' => $nit, 'empresa'=> $emp,'nombres' => $nom, 'apellidos' => $ap, 'direccion' => $dir, 'telefono' => $fono, 'correo' => $email, 'fecha_creacion' => $this->created_at);
		$query = $this->db->insertInto('proveedor')->values($values);

		if($query->execute()){
			$ok='1';
			return json_encode(array($ok));
		}
		else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function buscarunproveedor($id){
		$query = $this->db->from('proveedor')
							->where('proveedor.id', $id)
						  	->limit(1)->fetch();
		return json_encode(array($query));
	}
	public function eliminarproveedor($id){
		$query = $this->db->deleteFrom('proveedor')->where('id', $id);

		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function editarproveedor($id,$nit,$emp,$nom,$ap,$dir,$fono,$email){
		$values = array('Nit' => $nit, 'empresa'=>$emp,'nombres' => $nom, 'apellidos' => $ap, 'direccion' => $dir, 'telefono' => $fono, 'correo' => $email, 'fecha_creacion' => $this->created_at);
		$query = $this->db->update('proveedor')->set($values)->where('id', $id);
		if($query->execute()){
			$ok='1';
			return json_encode(array($ok));
		}else{
        	echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
}
?>