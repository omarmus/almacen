<?php 
/**
* 
*/
class RegEntrada_Model
{
	
	function __construct()
	{
		require_once('core/Conectar.php');
		$aux=new Conectar;
		$this->db=$aux->ConexionPDO();
		$this->db1=$aux->Conexion_PDO();
		date_default_timezone_set("America/La_Paz");
		$this->created_at = Date(DATE_ATOM,time());
	}
	public function crearingreso($trans,$user_id,$fun_id){
		//var_dump($trans);
		/*var_dump($trans); para mostrar la estructura de un array que recivimos desde el controlador*/
		//echo($trans[0]->array1[0]->codigo); 
		$cont1 = count($trans[0]->array1);
		//var_dump($trans);
		try
        {
        	$this->db1->beginTransaction();
        	$idprov = htmlspecialchars(strip_tags($trans[0]->array1[0]->id_prov));
        	$nrofac = htmlspecialchars(strip_tags($trans[0]->array1[0]->nrofact));
        	$partida_presup = htmlspecialchars(strip_tags($trans[0]->array1[0]->partida_presup));
        	$fuente = htmlspecialchars(strip_tags($trans[0]->array1[0]->fuente));
			$this->db1->exec("insert into pedido(id,estado_pedido_id,funcionario_id,proveedor_id,usuario_id,transaccion_tipo_id,Nro_factura,partida_presup,fuente,fecha_creacion,fecha_entrega) values (null,null,$fun_id,$idprov,$user_id,1,$nrofac,$partida_presup,'$fuente','$this->created_at',null)");
			$lastId = $this->db1->lastInsertId();
			$lastId1 = $lastId;
			for ($i=0; $i <= $cont1-1; $i++) {
				$idmat = htmlspecialchars(strip_tags($trans[0]->array1[$i]->id)); 
				$q = htmlspecialchars(strip_tags($trans[0]->array1[$i]->q)); 
				$pu = htmlspecialchars(strip_tags($trans[0]->array1[$i]->pu)); 
				$this->db1->exec("insert into transaccion(id,material_id,precio_unitario,q,qe,transaccion_tipo_id ,pedido_id,obs,fecha_creacion) values (null,$idmat,$pu,$q,null,1,$lastId,null,'$this->created_at')");
			}
        	$this->db1->commit();
        	$ok='1';
			return json_encode(array($ok,$lastId1,$idprov));
		}
		catch(PDOException $e)
        {
			die($e->getMessage());
			$this->db1->rollBack();
        }
	}
	public function listarIngresos(){
		$query = $this->db->from('transaccion')
						  ->LeftJoin('pedido ON pedido.id = transaccion.pedido_id')
						  ->LeftJoin('material ON material.id = transaccion.material_id')
						  ->select(array('material.codigo,material.nom_material,material.descripcion,material.presentacion,pedido.Nro_factura,pedido.fecha_creacion AS fechped, pedido.fuente'))
						  ->where('transaccion.transaccion_tipo_id',1)
						  ->where('precio_unitario > ?',0)
						  //->where('pedido_id != null',null)
						  //->where('qe',null)
						  ->fetchAll();
		return json_encode($query);
	}
	public function listarpedidoIngreso(){
		$query = $this->db->from('pedido')
						  ->LeftJoin('proveedor ON proveedor.id = pedido.proveedor_id')
						  ->select(array('Nit,empresa,nombres,apellidos'))
						  ->where('pedido.transaccion_tipo_id',1)
						  ->where('estado_pedido_id',null)
						  ->where('Nro_factura > ?',0)
						  ->fetchAll();
		return json_encode($query);
	}
	public function detalleuningreso($id){
		$query = $this->db->from('transaccion')
						  ->LeftJoin('pedido ON pedido.id = transaccion.pedido_id')
						  ->LeftJoin('material ON material.id = transaccion.material_id')
						  ->select(array('material.codigo,material.nom_material,material.descripcion,material.presentacion,pedido.Nro_factura,pedido.fecha_creacion AS fechped, pedido.fuente'))
						  ->where('transaccion.transaccion_tipo_id',1)
						  ->where('precio_unitario > ?',0)
						  ->where('pedido_id',$id)
						  //->where('qe',null)
						  ->fetchAll();
		return json_encode($query);
	}
}
?>