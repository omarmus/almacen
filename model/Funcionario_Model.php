<?php 
/**
* 
*/
class Funcionario_Model
{
	
	function __construct()
	{
		require_once('core/Conectar.php');
		$aux=new Conectar;
		$this->db=$aux->ConexionPDO();
	}
	public function listarfuncionario()
	{
		$query = $this->db->from('funcionario')
						  ->LeftJoin('cargo ON cargo.id = funcionario.cargo_id')
						  ->select(array('nomcargo'))
						  ->where('funcionario.id > ?',0)
						  ->orderBy('id')
						  ->fetchAll();
		return json_encode($query);	
	}	
	public function listarcargo()
	{
		$query = $this->db->from('cargo')
						  ->fetchAll();
		return json_encode($query);
	}
	public function registrafuncionario($ci,$exp,$nombres,$appaterno,$apmaterno,$sexo,$direccion,$telefono,$correo,$selectcargo)
	{
		$values = array('ci' => $ci, 'exp' => $exp,'nombres' => $nombres, 'appaterno' => $appaterno, 'apmaterno' => $apmaterno,  'sexo' => $sexo, 'direccion' => $direccion, 'telefono' => $telefono, 'correo' => $correo,'cargo_id'=>$selectcargo);
		$query = $this->db->insertInto('funcionario')->values($values);

		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}
		else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function eliminarfuncionario($id){
		$query = $this->db->deleteFrom('funcionario')->where('id', $id);

		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}
		else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function buscarunfuncionario($id){
		$query = $this->db->from('funcionario')
						  ->LeftJoin('cargo ON cargo.id = funcionario.cargo_id')
						  ->select(array('nomcargo'))
						  ->where('funcionario.id', $id)
						  ->limit(1)->fetch();
		return json_encode(array($query));
	}
	public function editarfuncionario($id,$ci,$exp,$nombres,$appaterno,$apmaterno,$direccion,$telefono,$correo,$sexo,$cargo_id){
		$values = array('ci' => $ci, 'exp' => $exp, 'nombres' => $nombres, 'appaterno' => $appaterno, 'apmaterno' => $apmaterno,  'sexo' => $sexo, 'direccion' => $direccion, 'telefono' => $telefono, 'correo' => $correo,'cargo_id'=>$cargo_id);
		$query = $this->db->update('funcionario')->set($values)->where('id', $id);

		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}
		else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
}
?>