<?php 
/**
* 
*/
class Login_Model
{
	
	function __construct()
	{
		require_once('core/Conectar.php');
		$aux=new Conectar;
		$this->db=$aux->ConexionPDO();
	}
	public function session($user, $pass)
	{
		$query = $this->db->from('usuario')
						  ->LeftJoin('funcionario ON funcionario.id = usuario.funcionario_id')
						  ->LeftJoin('cargo ON cargo.id = funcionario.cargo_id')
						  //->LeftJoin('area ON area.id = cargo.area_id')
						  ->LeftJoin('rol ON rol.id = usuario.rol_id')
						  ->select(array('nombres','appaterno','apmaterno','rol_usuario','area_id'))
						  ->where('usuario.nombre', $user)
						  ->where('usuario.estado',1)
						  ->limit(1)->fetch();
		if(isset($query->nombre)){
			if($query->password==sha1($pass)){
				session_start();
				$_SESSION['id']=$query->id;
				$_SESSION['fun_id']=$query->funcionario_id;
				$_SESSION['name_complet']=$query->nombres.' '.$query->appaterno.' '.$query->apmaterno;
				$_SESSION['id_area']=$query->area_id;
				if ($query->rol_usuario == "administrador") {
					$acc="index.php?view=admin&action=menuadmin";
				}
				if($query->rol_usuario == "avanzado"){
				$acc="index.php?view=avanzado&action=menuavanzado";
				}
				$ok='1';
				return json_encode(array($acc, $ok));
				
			}else{
				return '0';
			}
		}else{
			return '0';
		}
	}
}
?>