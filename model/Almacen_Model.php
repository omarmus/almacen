<?php
/**
*
*/
class Almacen_Model
{

	function __construct()
	{
		require_once('core/Conectar.php');
		$aux=new Conectar;
		$this->db=$aux->ConexionPDO();
		$this->db1=$aux->Conexion_PDO();
		date_default_timezone_set("America/La_Paz");
		$this->created_at = Date(DATE_ATOM,time());
	}
	public function listarinventario($disponible, $cero) {
		$idmat = "";
		$gral = $this->detalleAlmacen();
		$todos =  array();
		$cont = count($gral);
		for ($i=0; $i <= $cont-1; $i++) {
			$idmat = $gral[$i]->id;
			$aux = $this->detalleunmaterial($idmat);
			if ($disponible) {
				if ($aux > 0) {
					array_push($todos, array('id'=>$gral[$i]->id,'codigo'=>$gral[$i]->codigo,'nom_material'=>$gral[$i]->nom_material,'descripcion'=>$gral[$i]->descripcion,'min_inventario'=>$gral[$i]->min_inventario,'activo'=>$gral[$i]->activo,'fecha_creacion'=>$gral[$i]->fecha_creacion,'tot'=>$aux));
				}
			} else if ($cero) {
				if ($aux == 0) {
					array_push($todos, array('id'=>$gral[$i]->id,'codigo'=>$gral[$i]->codigo,'nom_material'=>$gral[$i]->nom_material,'descripcion'=>$gral[$i]->descripcion,'min_inventario'=>$gral[$i]->min_inventario,'activo'=>$gral[$i]->activo,'fecha_creacion'=>$gral[$i]->fecha_creacion,'tot'=>$aux));
				}
			} else {
				array_push($todos, array('id'=>$gral[$i]->id,'codigo'=>$gral[$i]->codigo,'nom_material'=>$gral[$i]->nom_material,'descripcion'=>$gral[$i]->descripcion,'min_inventario'=>$gral[$i]->min_inventario,'activo'=>$gral[$i]->activo,'fecha_creacion'=>$gral[$i]->fecha_creacion,'tot'=>$aux));
			}
		}
		return json_encode($todos);
	}
	public function detalleAlmacen(){
		$query = $this->db->from('material')
              ->fetchAll();
        return $query;
	}
	public function detalleunmaterial($id){
		$in = $this->totalentradas($id, null, null, null);
		$out = $this->totalsalidas($id, null, null, null);
		$total = $in - $out;
		return json_encode($total);
	}
	public function historial($id, $tipo, $mes, $gestion){
		$in = $this->totalentradas($id, $tipo, $mes, $gestion);
		$out = $this->totalsalidas($id, $tipo, $mes, $gestion);
		$total = $in - $out;
		return json_encode(array($in,$total,$out));
	}
	public function totalentradas($id, $tipo, $mes, $gestion) {
		$query = $this->db->from('transaccion')
						  ->select('q')
						  ->where('material_id',$id)
							->where('transaccion_tipo_id',1);

		if (!is_null($tipo)) {
			if ($tipo == 'MES' && $mes != '-') {
				$gestion = date('Y');
				$lastday = date('t',strtotime($mes.'/01/'.$gestion));
				$ini = date("Y-m-d",strtotime($mes.'/01/'.$gestion));
				$fin = date("Y-m-d",strtotime($mes.'/'.$lastday.'/'.$gestion));

				// var_dump('ENTRADA', $tipo, $ini, $fin);

				$query = $query->where('fecha_creacion >= ?', $ini)
											 ->where('fecha_creacion <= ?', $fin);
			} else if ($tipo == 'GESTION' && $gestion != '-') {
				$ini = date("Y-m-d",strtotime('01/01/'.$gestion));
				$fin = date("Y-m-d",strtotime('12/31/'.$gestion));

				// var_dump('ENTRADA', $tipo, $ini, $fin);

				$query = $query->where('fecha_creacion >= ?', $ini)
											 ->where('fecha_creacion <= ?', $fin);
			}
		}

		$query = $query->fetchAll();
		$ins = array();
		$ins = $query;
		$t = 0;
		$arrlength = count($ins);
		for ($x = 0; $x <= $arrlength-1; $x++) {
    		$t = $t + $ins[$x]->q;
		}
		return $t;
	}
	public function totalsalidas($id, $tipo, $mes, $gestion){
		$query = $this->db->from('transaccion')
						  ->LeftJoin('pedido ON pedido.id = transaccion.pedido_id')
						  ->select('qe')
						  ->where('material_id',$id)
						  ->where('transaccion.transaccion_tipo_id',2)
							->where('estado_pedido_id',2);

		if (!is_null($tipo)) {
			if ($tipo == 'MES' && $mes != '-') {
				$gestion = date('Y');
				$lastday = date('t',strtotime($mes.'/01/'.$gestion));
				$ini = date("Y-m-d",strtotime($mes.'/01/'.$gestion));
				$fin = date("Y-m-d",strtotime($mes.'/'.$lastday.'/'.$gestion));

				// var_dump('SALIDA', $tipo, $ini, $fin);

				$query = $query->where('transaccion.fecha_creacion >= ?', $ini)
												->where('transaccion.fecha_creacion <= ?', $fin);
			} else if ($tipo == 'GESTION' && $gestion != '-') {
				$ini = date("Y-m-d",strtotime('01/01/'.$gestion));
				$fin = date("Y-m-d",strtotime('12/31/'.$gestion));

				// var_dump('SALIDA', $tipo, $ini, $fin);

				$query = $query->where('transaccion.fecha_creacion >= ?', $ini)
												->where('transaccion.fecha_creacion <= ?', $fin);
			}
		}

		$query = $query->fetchAll();
		$uots = array();
		$uots = $query;
		$t = 0;
		$arrlength = count($uots);
		for ($x = 0; $x <= $arrlength-1; $x++) {
    		$t = $t + $uots[$x]->qe;
		}
		return $t;
	}

	public function construirtablareporte($disponible)
	{
		$gral = json_decode($this->listarinventario($disponible, !$disponible));

		$cont1 = count($gral);
		$html="";
		$html=$html.'<div align="center">
			<br />
			<table border="0"  align="center">
				<tr bgcolor="#96939333" style="font-family:arial;">
					<th width="100px" border="1"><font color="#FFFFFF"><strong>ID</strong></font></th>
					<th width="120px" border="1"><font color="#FFFFFF"><strong>CODIGO</strong></font></th>
					<th width="400px" border="1"><font color="#FFFFFF"><strong>NOMBRE DEL MATERIAL</strong></font></th>
					<th width="100px" border="1"><font color="#FFFFFF"><strong>DISPONIBLE</strong></font></th>
				</tr>';
		// var_dump($gral);
		for ($i=0; $i <= $cont1-1; $i++) {
			$html=$html.'<tr>';
			$html=$html.'<td border="1">';
			$html=$html.$gral[$i]->id;
			$html=$html.'</td><td border="1">';
			$html=$html.$gral[$i]->codigo;
			$html=$html.'</td><td border="1">';
			$html=$html.$gral[$i]->nom_material;
			$html=$html.'</td><td border="1">';
			$html=$html.$gral[$i]->tot;
			$html=$html.'</td></tr>';
		}
		$html=$html.'</table></div>';
		return ($html);
	}

	public function pdf($disponible, $nombre) {
		$medidas = array(280,217);
		$pdf = new MYPDF('P', 'mm', $medidas, true, 'UTF-8', true);
		$pdf->SetTitle('Reporte Materiales disponibles');
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}
		$pdf->AddPage();
		$pdf->SetY(22);
		setlocale(LC_TIME, "Spanish");
		$pdf->SetFont('helvetica', '', 10);
		// $nueva_fecha1 = $fechaini;
		// $nueva_fecha2 = $fechafin;
		// $mes_anyo1 = strftime("%d de %B del %Y", strtotime($nueva_fecha1));
		// $mes_anyo2 = strftime("%d de %B del %Y", strtotime($nueva_fecha2));
		$pdf->Cell(0, 20, 'REPORTE DE MATERIALES DISPONIBLES', 0, 1, 'C', 0, '', 1);
		$pdf->SetFont('helvetica', '', 8);
		$pdf->SetY(29);
		$pdf->Cell(0, 15, 'Fecha de reporte: ' . date('d/m/Y H:s') . ' - Generado por:' . $nombre, 0, 1, 'C', 0, '', 1);
		$pdf->SetFont('helvetica', '', 6);
		$pdf->SetXY(5,37);
		$html = $this->construirtablareporte($disponible);
		/*print_r($html);
		die();*/
		$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
		$pdf->lastPage();
		$pdf->SetY(22);
		$pdf->Output('reporte-materiales.pdf', 'I');
	}
}

require_once('core/TCPDF/tcpdf.php');
class MYPDF extends TCPDF {

	// Page header
	public function Header() {
		$this->Image('assets/img/logo.jpg', 10, 10, 29, '', 'JPG', '', 'T', false, 400, '', false, false, 0, false, false, false);
		// Set font
		$this->SetFont('helvetica', '', 12);
		// Title
		$this->Cell(0, 15, 'ESCUELA SUPERIOR DE FORMACIÓN DE MAESTROS TECNOLÓGICO HUMANÍSTICO DE EL ALTO', 0, 1, 'C', 0, '', 1);
		$this->SetY(15);
		$this->Cell(0, 15, 'DIRECCIÓN ADMINISTRATIVA FINANCIERA', 0, 1, 'C', 0, '', 1);
		$this->SetY(20);
		//$this->Cell(0, 15, 'ALMACENES E INVENTARIO', 0, false, 'C', 0, '', 0, false, 'M', 'M');
		$this->Cell(0, 15, 'ALMACENES E INVENTARIO', 0, 1, 'C', 0, '', 1);
	}
	public function Footer() {
		$this->SetY(-15);
		$this->SetFont('helvetica', 'I', 10);
		$this->Cell(0, 10, ''.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}
?>