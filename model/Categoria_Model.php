<?php 
/**
* 
*/
class Categoria_Model
{
	
	function __construct()
	{
		require_once('core/Conectar.php');
		$aux=new Conectar;
		$this->db=$aux->ConexionPDO();
		date_default_timezone_set("America/La_Paz");
		$this->created_at = Date(DATE_ATOM,time());
	}
	public function crearcategoria($tipo, $cate)
	{
		$values = array('tipo' => $tipo, 'nombre_cat' => $cate, 'fecha_creacion' => $this->created_at);
		$query = $this->db->insertInto('categoria')->values($values);

		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}
		else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function listarcategorias(){
		$query = $this->db->from('categoria')
						  ->fetchAll();
		return json_encode($query);
	}
	public function eliminarcategoria($id){
		$query = $this->db->deleteFrom('categoria')->where('id', $id);

		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}
		else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function editarcate($id,$tipo,$nombre_cat){
		$values = array('tipo' => $tipo, 'nombre_cat' => $nombre_cat);
		$query = $this->db->update('categoria')->set($values)->where('id', $id);

		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}
		else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function buscarunacate($id){
		$query = $this->db->from('categoria')
						  ->where('id', $id)
						  ->limit(1)->fetch();
		return json_encode(array($query));
	}
	
}
?>