<?php
date_default_timezone_set('America/La_Paz');
/**
*
*/
class Estadistica_Model
{

	function __construct()
	{
		require_once('core/Conectar.php');
		$aux=new Conectar;
		$this->db=$aux->ConexionPDO();
	}
	public function buscarDatos($fechaini, $fechafin)
	{
		$idmat = "";
		$gral = $this->datosGenerales($fechaini, $fechafin);
		$todos =  array();
		$keys = array();
		$cont = count($gral);
		for ($i=0; $i <= $cont-1; $i++) {
			$idmat = $gral[$i]->material_id;
			$aux = $this->detalleunmaterial($idmat,$fechaini, $fechafin);
			/*array_push($keys, $idmat);*/
			if (in_array("$idmat", $keys)) {
				array_push($todos, array('id'=>$gral[$i]->material_id,'codigo'=>$gral[$i]->codigo,'tipo'=>$gral[$i]->tipo,'nom_material'=>$gral[$i]->nom_material,'descripcion'=>$gral[$i]->descripcion,'presentacion'=>$gral[$i]->presentacion,'q'=>$gral[$i]->q,'precio_unitario'=>$gral[$i]->precio_unitario,'imp_totEntra'=>$gral[$i]->q*$gral[$i]->precio_unitario,'nombre'=>$gral[$i]->nombre,'fecha_creacion'=>$gral[$i]->fecha_creacion,'salidas'=>0));
				array_push($keys, $idmat);
			}else{
				array_push($todos, array('id'=>$gral[$i]->material_id,'codigo'=>$gral[$i]->codigo,'tipo'=>$gral[$i]->tipo,'nom_material'=>$gral[$i]->nom_material,'descripcion'=>$gral[$i]->descripcion,'presentacion'=>$gral[$i]->presentacion,'q'=>$gral[$i]->q,'precio_unitario'=>$gral[$i]->precio_unitario,'imp_totEntra'=>$gral[$i]->q*$gral[$i]->precio_unitario,'nombre'=>$gral[$i]->nombre,'fecha_creacion'=>$gral[$i]->fecha_creacion,'salidas'=>$aux));
				array_push($keys, $idmat);
			}
		}
		return json_encode($todos);
	}
	public function datosGenerales($fechaini, $fechafin)
	{
		//$value = date($fechaini);
		$query = $this->db->from('transaccion')
						  ->LeftJoin('transaccion_tipo ON transaccion_tipo.id = transaccion.transaccion_tipo_id')
						  ->LeftJoin('pedido ON pedido.id = transaccion.pedido_id')
						  ->LeftJoin('material ON material.id = transaccion.material_id')
						  ->LeftJoin('categoria ON categoria.id = material.categoria_id')
						  ->select('codigo,nom_material,descripcion,presentacion,tipo,nombre,pedido.fecha_entrega,pedido.estado_pedido_id')
						  ->where('date(transaccion.fecha_creacion) >= ?',date("Y-m-d", strtotime($fechaini)))
						  ->where('date(transaccion.fecha_creacion) <= ?',date("Y-m-d", strtotime($fechafin)))
						  ->where('transaccion.transaccion_tipo_id',1)
						  /*->orderBy('transaccion.id ASC')*/
						  ->orderBy('material.id ASC,transaccion.id ASC')
						  //->groupBy('material.id')
						  /*->where(array(('date(fecha_creacion) >= ?', date($fechaini)),('date(fecha_creacion) <= ?', date($fechafin))))*/
						  /*->where('date(fecha_creacion)' < date($fechafin)) 'date(fecha_creacion)' >= date($fechaini), */
						  ->fetchAll();
		/*return json_encode($query);*/
		return $query;
		/*SELECT * FROM `pedido` WHERE date(fecha_creacion) > '2018-08-28T04:00:00.000Z'*/
		//echo($fechaini);
	}
	public function detalleunmaterial($idmat,$fechaini, $fechafin)
	{
		$out = $this->totalsalidas($idmat,$fechaini, $fechafin);
		return json_encode($out);
	}
	public function totalsalidas($id,$fechaini, $fechafin){
		$query = $this->db->from('transaccion')
						  ->LeftJoin('pedido ON pedido.id = transaccion.pedido_id')
						  ->select('qe')
						  ->where('material_id',$id)
						  ->where('transaccion.transaccion_tipo_id',2)
						  ->where('estado_pedido_id',2)
						  ->where('date(transaccion.fecha_creacion) >= ?',date("Y-m-d", strtotime($fechaini)))
						  ->where('date(transaccion.fecha_creacion) <= ?',date("Y-m-d", strtotime($fechafin)))
						  ->fetchAll();
		$uots = array();
		$uots = $query;
		$t = 0;
		$arrlength = count($uots);
		for ($x = 0; $x <= $arrlength-1; $x++) {
    		$t = $t + $uots[$x]->qe;
		}
		return $t;
	}
	public function construirtablareporte($fechaini, $fechafin)
	{
		$idmat = "";
		$gral = $this->datosGenerales($fechaini, $fechafin);
		$todos =  array();
		$keys = array();
		$cont = count($gral);
		for ($i=0; $i <= $cont-1; $i++) {
			$idmat = $gral[$i]->material_id;
			$aux = $this->detalleunmaterial($idmat,$fechaini, $fechafin);
			if (in_array("$idmat", $keys)) {
				array_push($todos, array('id'=>$gral[$i]->material_id,'codigo'=>$gral[$i]->codigo,'tipo'=>$gral[$i]->tipo,'nom_material'=>$gral[$i]->nom_material,'descripcion'=>$gral[$i]->descripcion,'presentacion'=>$gral[$i]->presentacion,'q'=>$gral[$i]->q,'precio_unitario'=>$gral[$i]->precio_unitario,'imp_totEntra'=>$gral[$i]->q*$gral[$i]->precio_unitario,'nombre'=>$gral[$i]->nombre,'fecha_creacion'=>$gral[$i]->fecha_creacion,'salidas'=>""));
				array_push($keys, $idmat);
			}else{
				array_push($todos, array('id'=>$gral[$i]->material_id,'codigo'=>$gral[$i]->codigo,'tipo'=>$gral[$i]->tipo,'nom_material'=>$gral[$i]->nom_material,'descripcion'=>$gral[$i]->descripcion,'presentacion'=>$gral[$i]->presentacion,'q'=>$gral[$i]->q,'precio_unitario'=>$gral[$i]->precio_unitario,'imp_totEntra'=>$gral[$i]->q*$gral[$i]->precio_unitario,'nombre'=>$gral[$i]->nombre,'fecha_creacion'=>$gral[$i]->fecha_creacion,'salidas'=>$aux));
				array_push($keys, $idmat);
			}
		}

		$prueba = array();

		$prueba = $todos;
		$cont1 = count($prueba);
		$fecha1 = date_create($fechaini);
		$fech1 = date_format($fecha1,'d-m-Y');
		$fecha2 = date_create($fechafin);
		$fech2 = date_format($fecha2,'d-m-Y');
		$html="";
		$html=$html.'<div align="center">
			<br /><table border="0"  align="center">
			<tr bgcolor="#96939333" style="font-family:arial;">
				<th width="100px" colspan="2" bgcolor="#FFFFFF" border="0"></th>
				<th width="210px" bgcolor="#FFFFFF" border="0"></th>
				<th width="40px" bgcolor="#FFFFFF" border="0"></th>
				<th width="150px" colspan="3" border="1"><font color="#FFFFFF"><strong>SALDO AL '.$fech1.'</strong></font></th>
				<th width="150px" colspan="3" border="1"><font color="#FFFFFF"><strong>ENTRADA</strong></font></th>
				<th width="150px" colspan="3" border="1"><font color="#FFFFFF"><strong>SALIDA</strong></font></th>
				<th width="150px" colspan="3" border="1"><font color="#FFFFFF"><strong>SALDO AL '.$fech2.'</strong></font></th>
			</tr>
			<tr bgcolor="#96939333" style="font-family:arial;">
				<th border="1"><font color="#FFFFFF"><strong>CODIGO</strong></font></th>
				<th border="1"><font color="#FFFFFF"><strong>TIPO</strong></font></th>
				<th border="1"><font color="#FFFFFF"><strong>MATERIAL SUMINISTRO</strong></font></th>
				<th border="1"><font color="#FFFFFF"><strong>UNIDAD</strong></font></th>
				<th border="1"><font color="#FFFFFF"><strong>CANTIDAD</strong></font></th>
				<th border="1"><font color="#FFFFFF"><strong>PU</strong></font></th>
				<th border="1"><font color="#FFFFFF"><strong>IMPORTE TOTAL</strong></font></th>
				<th border="1"><font color="#FFFFFF"><strong>CANTIDAD</strong></font></th>
				<th border="1"><font color="#FFFFFF"><strong>PU</strong></font></th>
				<th border="1"><font color="#FFFFFF"><strong>IMPORTE TOTAL</strong></font></th>
				<th border="1"><font color="#FFFFFF"><strong>CANTIDAD</strong></font></th>
				<th border="1"><font color="#FFFFFF"><strong>PU</strong></font></th>
				<th border="1"><font color="#FFFFFF"><strong>IMPORTE TOTAL</strong></font></th>
				<th border="1"><font color="#FFFFFF"><strong>CANTIDAD</strong></font></th>
				<th border="1"><font color="#FFFFFF"><strong>PU</strong></font></th>
				<th border="1"><font color="#FFFFFF"><strong>IMPORTE TOTAL</strong></font></th>
			</tr>';
			$te = 0;
			$ts = 0;
			$tt = 0;
			for ($i=0; $i <= $cont1-1; $i++) {
				$html=$html.'<tr>';
				$html=$html.'<td border="1">';
				$html=$html.$prueba[$i]['codigo'];
				$html=$html.'</td><td border="1">';
				$html=$html.$prueba[$i]['tipo'];
				$html=$html.'</td><td border="1">';
				$html=$html.$prueba[$i]['nom_material'];
				$html=$html.' '.$prueba[$i]['descripcion'];
				$html=$html.'</td><td border="1">';
				$html=$html.$prueba[$i]['presentacion'];
				$html=$html.'</td><td border="1">';
				$html=$html.$prueba[$i]['q'];
				$html=$html.'</td><td border="1">';
				$html=$html.$prueba[$i]['precio_unitario'];
				$html=$html.'</td><td border="1">';
				$html=$html.$prueba[$i]['q']*$prueba[$i]['precio_unitario'];
				$te = $te + $prueba[$i]['q']*$prueba[$i]['precio_unitario'];
				$html=$html.'</td><td border="1">';
				$html=$html.'</td><td border="1">';
				$html=$html.'</td><td border="1">';
				$html=$html.'</td><td border="1">';
				$html=$html.$prueba[$i]['salidas'];
				$html=$html.'</td><td border="1">';
				$html=$html.$prueba[$i]['precio_unitario'];
				$html=$html.'</td><td border="1">';
				$html=$html.$prueba[$i]['salidas']*$prueba[$i]['precio_unitario'];
				$ts = $ts + $prueba[$i]['salidas']*$prueba[$i]['precio_unitario'];
				$html=$html.'</td><td border="1">';
				if ($prueba[$i]['salidas'] > 0) {
					$q = $prueba[$i]['q'] - $prueba[$i]['salidas'];
					$html=$html.$q;
				}else{
					$html=$html.$prueba[$i]['q'];
				}
				$html=$html.'</td><td border="1">';
				$html=$html.$prueba[$i]['precio_unitario'];
				$html=$html.'</td><td border="1">';
				if ($prueba[$i]['salidas'] > 0) {
					$q = ($prueba[$i]['q'] - $prueba[$i]['salidas'])*$prueba[$i]['precio_unitario'];
					$html=$html.$q;
					$tt = $tt + $q;
				}else{
					$html=$html.$prueba[$i]['q']*$prueba[$i]['precio_unitario'];
					$tt = $tt + $prueba[$i]['q']*$prueba[$i]['precio_unitario'];
				}
				$html=$html.'</td></tr>';
			}
			$html=$html.'<tr>';
			$html=$html.'<td border="1" colspan="4" bgcolor="#96939333"><font color="#FFFFFF"><strong>TOTAL</strong></font>';
			$html=$html.'</td><td border="1">';
			$html=$html.'</td><td border="1">';
			$html=$html.'</td><td border="1">';
			$html=$html.$te;
			$html=$html.'</td><td border="1">';
			$html=$html.'</td><td border="1">';
			$html=$html.'</td><td border="1">';
			$html=$html.'</td><td border="1">';
			$html=$html.'</td><td border="1">';
			$html=$html.'</td><td border="1">';
			$html=$html.$ts;
			$html=$html.'</td><td border="1">';
			$html=$html.'</td><td border="1">';
			$html=$html.'</td><td border="1">';
			$html=$html.$tt;
			$html=$html.'</td></tr>';
		$html=$html.'</table></div>';
		return ($html);
	}
	public function pdf($fechaini, $fechafin)
	{
		//echo($fechaini);

		$medidas = array(280,217);
$pdf = new MYPDF('L', 'mm', $medidas, true, 'UTF-8', true);
$pdf->SetTitle('Reporte General');
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}
$pdf->AddPage();
$pdf->SetY(22);
setlocale(LC_TIME, "Spanish");
$pdf->SetFont('helvetica', '', 10);
$nueva_fecha1 = $fechaini;
$nueva_fecha2 = $fechafin;
$mes_anyo1 = strftime("%d de %B del %Y", strtotime($nueva_fecha1));
$mes_anyo2 = strftime("%d de %B del %Y", strtotime($nueva_fecha2));
$pdf->Cell(0, 15, '(DEL '.$mes_anyo1.' AL '.$mes_anyo2.')', 0, 1, 'C', 0, '', 1);
$pdf->SetFont('helvetica', '', 8);
$pdf->SetY(27);
$pdf->Cell(0, 15, 'Fecha de reporte: ' . date('d/m/Y H:s'), 0, 1, 'C', 0, '', 1);
$pdf->SetFont('helvetica', '', 6);
$pdf->SetXY(5,35);
$html = $this->construirtablareporte($fechaini, $fechafin);
/*print_r($html);
die();*/
$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$pdf->lastPage();
$pdf->SetY(22);
		$pdf->Output('hola.pdf', 'I');
	}
}
require_once('core/TCPDF/tcpdf.php');
class MYPDF extends TCPDF {

	//Page header
	public function Header() {
		$this->Image('assets/img/logo.jpg', 5, 5, 25, '', 'JPG', '', 'T', false, 400, '', false, false, 0, false, false, false);
		// Set font
		$this->SetFont('helvetica', '', 10);
		// Title
		$this->SetY(2);
		$this->Cell(0, 15, 'ESCUELA SUPERIOR DE FORMACIÓN DE MAESTROS TECNOLÓGICO HUMANÍSTICO DE EL ALTO', 0, 1, 'C', 0, '', 1);
		$this->SetY(6);
		$this->Cell(0, 15, 'DIRECCIÓN ADMINISTRATIVA FINANCIERA', 0, 1, 'C', 0, '', 1);
		$this->SetY(12);
		//$this->Cell(0, 15, 'ALMACENES E INVENTARIO', 0, false, 'C', 0, '', 0, false, 'M', 'M');
		$this->Cell(0, 15, 'UNIDAD DE BIENES Y SERVICIOS', 0, 1, 'C', 0, '', 1);
		$this->SetY(18);
		$this->Cell(0, 15, 'INVENTARIO DE ALMACENES', 0, 1, 'C', 0, '', 1);
	}
	public function Footer() {
		$this->SetY(-15);
		$this->SetFont('helvetica', 'I', 10);
		$this->Cell(0, 10, ''.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}
?>