<?php 
/**
* 
*/
class Cantidadautorizada_Model
{
	
	function __construct()
	{
		require_once('core/Conectar.php');
		$aux=new Conectar;
		$this->db=$aux->ConexionPDO();
		date_default_timezone_set("America/La_Paz");
		$this->created_at = Date(DATE_ATOM,time());
	}
	public function buscarmatasig($id){
		$query = $this->db->from('cant_aut')
						  ->LeftJoin('material ON material.id = cant_aut.material_id')
						  ->where('cant_aut.area_id', $id)
						  ->fetchAll();
		return json_encode($query);
	}
	public function listarcantaut($id){
		$query = $this->db->from('cant_aut')
						  ->LeftJoin('material ON material.id = cant_aut.material_id')
						  ->select(array('descripcion,nom_material,codigo'))
						  ->where('cant_aut.area_id', $id)
						  ->fetchAll();
		return json_encode($query);
	}
	public function crearcantidadautorizada($area_id,$mat_id,$ca){
		$values = array('ca' => $ca, 'area_id' => $area_id, 'material_id' => $mat_id, 'fecha_creacion' => $this->created_at);
		$query = $this->db->insertInto('cant_aut')->values($values);
		if($query->execute()){
			$ok='1';
			return json_encode(array($ok));
		}
		else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function buscarunaca($id){
		$query = $this->db->from('cant_aut')
						  ->LeftJoin('material ON material.id = cant_aut.material_id')
						  ->select(array('nom_material,codigo,descripcion'))
						  ->where('cant_aut.id', $id)
						  ->fetchAll();
		return json_encode($query);
	}
	public function editarca($id,$cod_area,$cod_mat,$ca){
		$values = array('ca' => $ca, 'area_id' => $cod_area, 'material_id' => $cod_mat, 'fecha_creacion' => $this->created_at);
		$query = $this->db->update('cant_aut')->set($values)->where('id', $id);
		if($query->execute()){
			$ok='1';
			return json_encode(array($ok));
		}else{
        	echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function eliminarca($id){
		$query = $this->db->deleteFrom('cant_aut')->where('id', $id);

		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
}
?>