<?php 
/**
* 
*/
class Admin_Model
{
	
	function __construct()
	{
		require_once('core/Conectar.php');
		$aux=new Conectar;
		$this->db=$aux->ConexionPDO();
		$this->db1=$aux->Conexion_PDO();
		date_default_timezone_set("America/La_Paz");
		$this->created_at = Date(DATE_ATOM,time());
	}
	public function listarpendientes(){
		$query = $this->db->from('pedido')
						  ->select(array('COUNT(estado_pedido_id) AS num'))
						  ->where('estado_pedido_id',1)
						  ->fetchAll();
					  
		return json_encode($query);
	}
	public function listartodopendientes(){
		$query = $this->db->from('pedido')
						  ->LeftJoin('funcionario ON funcionario.id = pedido.funcionario_id')
						  ->LeftJoin('cargo ON cargo.id = funcionario.cargo_id')
						  ->LeftJoin('area ON area.id = cargo.area_id')
						  ->select(array('nomcargo, nombres, appaterno, apmaterno, area.id AS aid,area.descripcion'))
						  ->where('pedido.estado_pedido_id',1)
						  ->fetchAll();
		return json_encode($query);
	}
	public function generarlistadetallada($id,$aid){
		$idmat = "";
		$gral = $this->detalledeunpedido($id,$aid);
		$todos =  new ArrayObject();
		$cont = count($gral);
		for ($i=0; $i < $cont; $i++) { 
			$idmat = $gral[$i]->id;
			$aux = $this->detalleunmaterial($idmat);
			$todos->append(array('id'=>$gral[$i]->id,'codigo'=>$gral[$i]->codigo,'nom_material'=>$gral[$i]->nom_material,'descripcion'=>$gral[$i]->descripcion,'min_inventario'=>$gral[$i]->min_inventario,'presentacion'=>$gral[$i]->presentacion,'activo'=>$gral[$i]->activo,'categoria_id'=>$gral[$i]->categoria_id,'fecha_creacion'=>$gral[$i]->fecha_creacion,'ca'=>$gral[$i]->ca,'tipo'=>$gral[$i]->tipo,'q'=>$gral[$i]->q,'qe'=>$gral[$i]->qe,'pedido_id'=>$gral[$i]->pedido_id,'obs'=>$gral[$i]->obs,'tot'=>$aux,'tid'=>$gral[$i]->tid));
		}
		//echo var_dump($gral);
		return json_encode($todos);
	}
	public function detalledeunpedido($id,$aid){
		$query = $this->db->from('material')
              ->LeftJoin('categoria ON categoria.id = material.categoria_id')
              ->LeftJoin('transaccion ON transaccion.material_id = material.id')
              ->LeftJoin('cant_aut ON cant_aut.material_id = material.id')
              ->select(array('cant_aut.ca,categoria.tipo,transaccion.q,transaccion.qe,transaccion.pedido_id,transaccion.obs,transaccion.id AS tid'))
              ->where('transaccion.pedido_id',$id)
              ->where('cant_aut.area_id',$aid)
              //->orderBy('material.id')
              ->fetchAll();
        return $query;
	}
	public function detalledeunpedidoEntregar($id){
		$query = $this->db->from('material')
              ->LeftJoin('categoria ON categoria.id = material.categoria_id')
              ->LeftJoin('transaccion ON transaccion.material_id = material.id')
              ->LeftJoin('cant_aut ON cant_aut.material_id = material.id')
              ->select(array('cant_aut.ca,categoria.tipo,transaccion.q,transaccion.qe,transaccion.pedido_id,transaccion.obs,transaccion.id AS tid'))
              ->where('transaccion.pedido_id',$id)
              //->orderBy('material.id')
              ->fetchAll();
        return json_encode($query);
	}
	public function detalleunmaterial($id){
		$in = $this->totalentradas($id);
		$out = $this->totalsalidas($id);
		$total = $in - $out;
		return json_encode($total);
	}
	public function totalentradas($id){
		$query = $this->db->from('transaccion')
						  ->select('q')
						  ->where('material_id',$id)
						  ->where('transaccion_tipo_id',1)
						  ->fetchAll();
		$ins = array();
		$ins = $query;
		$t = 0; 
		$arrlength = count($ins);
		for ($x = 0; $x <= $arrlength-1; $x++) {
    		$t = $t + $ins[$x]->q;
		} 
		return $t;
	}
	public function totalsalidas($id){
		$query = $this->db->from('transaccion')
						  ->LeftJoin('pedido ON pedido.id = transaccion.pedido_id')
						  ->select('qe')
						  ->where('material_id',$id)
						  ->where('transaccion.transaccion_tipo_id',2)
						  ->where('estado_pedido_id',2)
						  ->fetchAll();
		$uots = array();
		$uots = $query;
		$t = 0; 
		$arrlength = count($uots);
		for ($x = 0; $x <= $arrlength-1; $x++) {
    		$t = $t + $uots[$x]->qe;
		} 
		return $t;
	}
	public function cambioMatPedido($tid,$ce,$obs){
		$values = array('qe' => $ce, 'obs'=>$obs);
		$query = $this->db->update('transaccion')->set($values)->where('id', $tid);
		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function nocambioMatPedido($tid,$ce){
		$values = array('qe' => $ce);
		$query = $this->db->update('transaccion')->set($values)->where('id', $tid);
		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function estadoPedido($id){
		$values = array('estado_pedido_id' => 2,'fecha_entrega'=>$this->created_at);
		$query = $this->db->update('pedido')->set($values)->where('id', $id);
		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
}
?>