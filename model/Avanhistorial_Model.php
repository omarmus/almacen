<?php 
/**
* 
*/
class Avanhistorial_Model
{
	
	function __construct()
	{
		require_once('core/Conectar.php');
		$aux=new Conectar;
		$this->db=$aux->ConexionPDO();
	}
	public function listarhistorial($idar,$idus)
	{
		$query = $this->db->from('pedido')
						  ->LeftJoin('funcionario ON funcionario.id = pedido.funcionario_id')
						  ->LeftJoin('cargo ON cargo.id = funcionario.cargo_id')
						  ->LeftJoin('area ON area.id = cargo.area_id')
						  ->select(array('nomcargo, nombres, appaterno, apmaterno, area.id AS aid,area.descripcion'))
						  ->where('pedido.usuario_id',$idus)
						  ->where('area.id',$idar)
						  ->fetchAll();
		return json_encode($query);
	}
	/*public function crearnuevaarea($nomarea)
	{
		$values = array('descripcion' => $nomarea);
		$query = $this->db->insertInto('area')->values($values);

		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}
		else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function listarareas(){
		$query = $this->db->from('area')
						  ->fetchAll();
		return json_encode($query);
	}
	public function eliminararea($id){
		$query = $this->db->deleteFrom('area')->where('id', $id);

		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}
		else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function editararea($id,$descripcion){
		$values = array('descripcion' => $descripcion);
		$query = $this->db->update('area')->set($values)->where('id', $id);

		if($query->execute()){
				$ok='1';
				return json_encode(array($ok));
		}
		else{
			echo "<pre>";
            print_r($exec->errorInfo());
        	echo "</pre>";
		}
	}
	public function buscarunarea($id){
		$query = $this->db->from('area')
						  ->where('id', $id)
						  ->limit(1)->fetch();
		return json_encode(array($query));
	}*/
	
}
?>