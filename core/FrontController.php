<?php
//FUNCIONES PARA EL CONTROLADOR FRONTAL
function cargarControlador($controller){
    $controlador=$controller.'_Controller';
    $file_controlador='controller/'.$controlador.'.php';
    require_once($file_controlador);
    $controllerObj=new $controlador();
    return $controllerObj;
}
function cargarAccion($controllerObj,$action){
    $accion=$action;
    $controllerObj->$accion();
}
function lanzarAccion($controllerObj){
    if(isset($_GET["action"]) && method_exists($controllerObj, $_GET["action"])){
        cargarAccion($controllerObj, $_GET["action"]);
        //si existe un get y el metodo entonces carga la accion
    }else{
        cargarAccion($controllerObj, ACCION_DEFAULT);
        //si no existe se asigna la accion default
    }
}
?>
