<?php
/*
Clase que conectara a la base de datos con todo  los datos de congiguracion para la conexion a la base de datos.
*/
class Conectar
{
    private $driver;
    private $host, $user, $pass, $database, $charset;
    /* Asignar valores de conexion*/
    public function __construct() {
        $db_config = require_once 'config/database.php';
        $this->driver=$db_config["driver"];
        $this->host=$db_config["host"];
        $this->user=$db_config["user"];
        $this->pass=$db_config["pass"];
        $this->database=$db_config["database"];
        $this->charset=$db_config["charset"];
    }
    /*Inicializacion con fluentPDO pasando los datos de conexion*/
    public function ConexionPDO(){
        try
        {
            require_once "core/FluentPDO/FluentPDO.php"; 
            $pdo = new PDO($this->driver.":host=".$this->host.";dbname=".$this->database.";charset=".$this->charset, $this->user, $this->pass);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);         
            return new FluentPDO($pdo);
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }  
    }
    public function Conexion_PDO(){
        try
        {
            $pdo = new PDO($this->driver.":host=".$this->host.";dbname=".$this->database.";charset=".$this->charset, $this->user, $this->pass);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);         
            return $pdo;
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }  
    }
}
?>