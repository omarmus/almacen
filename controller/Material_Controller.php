<?php 
/**
* 
*/
class Material_Controller
{
	
	function __construct()
	{	
		session_start();
		if(!isset($_SESSION['id'])){
			session_unset();
			session_destroy();
			header("location:index.php");
			exit();
		}

		$file="Material_Model";
		require_once "model/".$file.".php";
		$this->model=new $file();
	}
	public function crearmaterial(){

		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$matcod = htmlspecialchars(strip_tags($data->matcod));
		$mattipo = htmlspecialchars(strip_tags($data->mattipo));
		$nommat = htmlspecialchars(strip_tags($data->nommat));
		$desmat = htmlspecialchars(strip_tags($data->desmat));
		$minmat = htmlspecialchars(strip_tags($data->minmat));
		/*$invinicial = htmlspecialchars(strip_tags($data->invinicial));*/
		$presmat = htmlspecialchars(strip_tags($data->presmat));
		$matactivo= htmlspecialchars(strip_tags($data->matactivo));

		if($matcod!='' && $mattipo!='' && $nommat!='' && $desmat!='' && $minmat!='' /*&& $invinicial!=''*/ && $presmat!='' && $matactivo!='' )
		{
		 echo $this->model->crearmaterial($matcod,$mattipo,$nommat,$desmat,$minmat/*,$invinicial*/,$presmat,$matactivo);
		}
	}
	public function listarmaterial(){
		echo $this->model->listarmaterial();
	}
	public function eliminarmaterial(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$id = htmlspecialchars(strip_tags($data->id));
		if($id != ''){
				echo $this->model->eliminarmaterial($id);
		}
	}
	public function editarmate(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

			$id = htmlspecialchars(strip_tags($data->id));;
			$matcod = htmlspecialchars(strip_tags($data->matcod));
			$mattipo = htmlspecialchars(strip_tags($data->mattipo));
			$nommat = htmlspecialchars(strip_tags($data->nommat));
			$desmat = htmlspecialchars(strip_tags($data->desmat));
			$minmat = htmlspecialchars(strip_tags($data->minmat));
			//$invinicial = htmlspecialchars(strip_tags($data->invinicial));
			$presmat = htmlspecialchars(strip_tags($data->presmat));
			$matactivo= htmlspecialchars(strip_tags($data->matactivo));
			 
			if($id != '' && $matcod!='' && $mattipo!='' && $nommat!='' && $desmat!='' && $minmat!=''  && $presmat!='' && $matactivo!=''){
				echo $this->model->editarmate($id,$matcod,$mattipo,$nommat,$desmat,$minmat,$presmat,$matactivo);
			}
	}
	public function buscarunmate(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$id = htmlspecialchars(strip_tags($data->id));
		if($id != ''){
				echo $this->model->buscarunmate($id);
		}
	}	

}
?>