<?php 
/**
* 
*/
class Solmaterialfun_Controller
{
	
	function __construct()
	{	
		session_start();
		if(!isset($_SESSION['id'])){
			session_unset();
			session_destroy();
			header("location:index.php");
			exit();
		}

		$file="Solmaterialfun_Model";
		require_once "model/".$file.".php";
		$this->model=new $file();
	}
	public function listarFuncionario(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$idfun = htmlspecialchars(strip_tags($data->idfun));

		if($idfun!='')
		{
			echo $this->model->listarFuncionario($idfun);
		}
		
	}
	public function generarPDFpedido(){
		require_once('core/TCPDF/tcpdf.php');
		$idfun = $_GET['funid'];
		
		echo $this->model->generarPDFpedido($idfun);
	}
}
?>