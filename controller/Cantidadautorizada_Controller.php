<?php 
/**
* 
*/
class Cantidadautorizada_Controller
{
	
	function __construct()
	{	
		session_start();
		if(!isset($_SESSION['id'])){
			session_unset();
			session_destroy();
			header("location:index.php");
			exit();
		}

		$file="Cantidadautorizada_Model";
		require_once "model/".$file.".php";
		$this->model=new $file();
	}
	public function listarcantaut(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$id = htmlspecialchars(strip_tags($data->id));

		if($id!='')
		{
			echo $this->model->listarcantaut($id);
			//echo "string";
		}
	}
	public function buscarmatasig(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$id = htmlspecialchars(strip_tags($data->id));
		if ($id!='') {
			echo $this->model->buscarmatasig($id);
		}
	}
	public function crearcantidadautorizada(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$area_id = htmlspecialchars(strip_tags($data->area_id));
		$mat_id = htmlspecialchars(strip_tags($data->mat_id));
		$ca = htmlspecialchars(strip_tags($data->cant_aut));
		if ($area_id!='' && $mat_id!='' && $ca!='') {
			echo $this->model->crearcantidadautorizada($area_id,$mat_id,$ca);
		}
	}
	public function buscarunaca(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$id = htmlspecialchars(strip_tags($data->id));
		if ($id!='') {
			echo $this->model->buscarunaca($id);
		}
	}
	public function editarca(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$id = htmlspecialchars(strip_tags($data->id));
		$cod_area = htmlspecialchars(strip_tags($data->cod_area));
		$cod_mat = htmlspecialchars(strip_tags($data->cod_mat));
		$ca = htmlspecialchars(strip_tags($data->ca));
		if ($id!='' && $cod_area!='' && $cod_mat!='' && $ca!='') {
			echo $this->model->editarca($id,$cod_area,$cod_mat,$ca);
		}
	}
	public function eliminarca(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$id = htmlspecialchars(strip_tags($data->id));
		if ($id!='') {
			echo $this->model->eliminarca($id);
		}
	}
}
?>