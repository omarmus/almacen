<?php
/**
*
*/
class Logs_Controller
{

	function __construct()
	{
		session_start();
		if(!isset($_SESSION['id'])){
			session_unset();
			session_destroy();
			header("location:index.php");
			exit();
		}

		$file="Logs_Model";
		require_once "model/".$file.".php";
		$this->model=new $file();
	}
	public function listarlogs()
	{
		echo $this->model->listarlogs();
	}
	public function buscarcifun(){
		echo $this->model->buscarcifun();
	}
	public function listatodosci(){
		echo $this->model->listatodosci();
	}
	public function crearusuario(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$fun_id = htmlspecialchars(strip_tags($data->fun_id));
		$nomusuario = htmlspecialchars(strip_tags($data->nomuser));
		$password1 = htmlspecialchars(strip_tags($data->passw1));
		$rol_id = htmlspecialchars(strip_tags($data->rol_id));
		$useractivo = htmlspecialchars(strip_tags($data->useractivo));

		if($fun_id!='' && $nomusuario!='' && $password1!='' && $rol_id!=''){
			echo $this->model->crearusuario($fun_id,$nomusuario,$password1,$rol_id,$useractivo);
		}else{
			echo "No se Registro!!";
		}
	}
	public function eliminarusuario(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$id = htmlspecialchars(strip_tags($data->id));
		if($id != ''){
				echo $this->model->eliminarusuario($id);
		}
	}
	public function buscarunusuario(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$id = htmlspecialchars(strip_tags($data->id));
		if($id != ''){
				echo $this->model->buscarunusuario($id);
		}
	}
	public function editarusuario(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$id = htmlspecialchars(strip_tags($data->id));
		$fun_id = htmlspecialchars(strip_tags($data->fun_id));
		$nomusuario = htmlspecialchars(strip_tags($data->nomuser));
		$password1 = htmlspecialchars(strip_tags($data->passw1));
		$rol_id = htmlspecialchars(strip_tags($data->rol_id));
		$useractivo = htmlspecialchars(strip_tags($data->activo));
		$editapassword = htmlspecialchars(strip_tags($data->editapassword));

		if($id != '' && $fun_id!='' && $nomusuario!='' && $password1!='' && $rol_id!='' && $useractivo!='' && $editapassword !=''){
			echo $this->model->editarusuario($id,$fun_id,$nomusuario,$password1,$rol_id,$useractivo,$editapassword);
		}else{
			echo "error";
		}
	}
}
?>