<?php 
/**
* 
*/
class funcionario_Controller
{
	
	function __construct()
	{	
		session_start();
		if(!isset($_SESSION['id'])){
			session_unset();
			session_destroy();
			header("location:index.php");
			exit();
		}

		$file="Funcionario_Model";
		require_once "model/".$file.".php";
		$this->model=new $file();

		

	}
	public function listarfuncionario()
	{
		
		echo $this->model->listarfuncionario();
	}
	public function listarcargo(){
		
		echo $this->model->listarcargo();
	}
	public function registrarfuncionario(){

		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$ci = htmlspecialchars(strip_tags($data->ci));
		$exp = htmlspecialchars(strip_tags($data->exp));
		$nombres = htmlspecialchars(strip_tags($data->nombres));
		$appaterno = htmlspecialchars(strip_tags($data->appaterno));
		$apmaterno = htmlspecialchars(strip_tags($data->apmaterno));
		$selectcargo = htmlspecialchars(strip_tags($data->selectcargo));
		$direccion = htmlspecialchars(strip_tags($data->direccion));
		$telefono = htmlspecialchars(strip_tags($data->telefono));
		$correo = htmlspecialchars(strip_tags($data->correo));
		$sexo = htmlspecialchars(strip_tags($data->sexo));
		if($ci!='' && $exp!='' && $nombres!='' && $appaterno!='' && $apmaterno!='' && $selectcargo!='' && $direccion != '' && $telefono != '' && $correo != '' && $sexo != '' )
			{
		
				echo $this->model->registrafuncionario($ci,$exp,$nombres,$appaterno,$apmaterno,$sexo,$direccion,$telefono,$correo,$selectcargo);
			}
	}
	public function eliminarfuncionario(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$id = htmlspecialchars(strip_tags($data->id));
		if($id != ''){
				echo $this->model->eliminarfuncionario($id);
		}
	}
	public function buscarunfuncionario(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$id = htmlspecialchars(strip_tags($data->id));
		if($id != ''){
				echo $this->model->buscarunfuncionario($id);
		}
	}
	public function editarfuncionario(){

		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$id = htmlspecialchars(strip_tags($data->id));
		$ci = htmlspecialchars(strip_tags($data->ci));
		$exp = htmlspecialchars(strip_tags($data->exp));
		$nombres = htmlspecialchars(strip_tags($data->nombres));
		$appaterno = htmlspecialchars(strip_tags($data->appaterno));
		$apmaterno = htmlspecialchars(strip_tags($data->apmaterno));
		$direccion = htmlspecialchars(strip_tags($data->direccion));
		$telefono = htmlspecialchars(strip_tags($data->telefono));
		$correo = htmlspecialchars(strip_tags($data->correo));
		$sexo = htmlspecialchars(strip_tags($data->sexo));
		$cargo_id = htmlspecialchars(strip_tags($data->cargo_id));

		if($id!= '' && $ci!='' && $exp!='' && $nombres!='' && $appaterno!='' && $apmaterno!='' && $direccion != '' && $telefono != '' && $correo != '' && $sexo != '' && $cargo_id!=''){
				echo $this->model->editarfuncionario($id,$ci,$exp,$nombres,$appaterno,$apmaterno,$direccion,$telefono,$correo,$sexo,$cargo_id);
			}
	}
}
?>