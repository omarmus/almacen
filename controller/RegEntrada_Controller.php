<?php 
/**
* 
*/
class RegEntrada_Controller
{
	
	function __construct()
	{	
		session_start();
		if(!isset($_SESSION['id'])){
			session_unset();
			session_destroy();
			header("location:index.php");
			exit();
		}

		$file="RegEntrada_Model";
		require_once "model/".$file.".php";
		$this->model=new $file();
	}
	public function crearingreso(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$user_id = htmlspecialchars(strip_tags($data->user_id));
		$fun_id = htmlspecialchars(strip_tags($data->fun_id));
		$trans = array($data->trans);

		if($user_id != '' && $fun_id != ''){
				echo $this->model->crearingreso($trans,$user_id,$fun_id);
		}
	}
	public function listarIngresos(){
		//echo("asta aqui llega");
		echo $this->model->listarIngresos();
	}
	public function listarpedidoIngreso(){
		echo $this->model->listarpedidoIngreso();
	}
	public function detalleuningreso(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$id = htmlspecialchars(strip_tags($data->id));
		if ($id != '') {
			echo $this->model->detalleuningreso($id);
		}
	}
}
?>