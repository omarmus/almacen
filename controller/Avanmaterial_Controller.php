<?php 
/**
* 
*/
class Avanmaterial_Controller
{
	
	function __construct()
	{	
		session_start();
		if(!isset($_SESSION['id'])){
			session_unset();
			session_destroy();
			header("location:index.php");
			exit();
		}

		$file="Avanmaterial_Model";
		require_once "model/".$file.".php";
		$this->model=new $file();
	}
	public function buscarareadefun(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$id = htmlspecialchars(strip_tags($data->id));
		if($id != ''){
				echo $this->model->buscarareadefun($id);
		}
	}
	/*public function listarcantaut(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$id = htmlspecialchars(strip_tags($data->id));

		if($id!='')
		{
			echo $this->model->listarcantaut($id);
		}
	}*/
	public function listadetallada(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$id = htmlspecialchars(strip_tags($data->id));

		if($id!='')
		{
			echo $this->model->listadetallada($id);
		}
	}
	public function buscarunmate(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$id = htmlspecialchars(strip_tags($data->id));
		$idarea = htmlspecialchars(strip_tags($data->idarea));
		if($id != ''){
				echo $this->model->buscarunmate($id,$idarea);
		}
	}
	public function crearpedido(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$user_id = htmlspecialchars(strip_tags($data->user_id));
		$fun_id = htmlspecialchars(strip_tags($data->fun_id));
		$trans = array($data->trans);

		if($user_id != '' && $fun_id != ''){
				echo $this->model->crearpedido($trans,$user_id,$fun_id);
		}
	}
}
?>