<?php
/**
*
*/
class Admin_Controller
{
	function __construct()
	{
		$file="Admin_Model";
		require_once "model/".$file.".php";
		$this->model=new $file();
	}
	public function menuadmin()
	{
		session_start();
		if(!isset($_SESSION['id'])){
			session_destroy();
			header("location:index.php");
			exit();
		}
		require_once("views/admin/admin.php");
	}

	public function logout()
	{
		if(!isset($_SESSION['id'])) {
			session_unset();
			session_destroy();
			header("Location:index.php");
		}else {
			echo "Operación incorrecta.";
		}
	}
	public function listarpendientes(){
		echo $this->model->listarpendientes();
	}
	public function listartodopendientes(){
		echo $this->model->listartodopendientes();
	}
	public function generarlistadetallada(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$id = htmlspecialchars(strip_tags($data->id));
		$aid = htmlspecialchars(strip_tags($data->aid));
		if($id != '' && $aid != ''){
				echo $this->model->generarlistadetallada($id,$aid);
		}
	}
	public function detalleunpedido(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$id = htmlspecialchars(strip_tags($data->id));
		if($id != ''){
				echo $this->model->detalledeunpedidoEntregar($id);
		}
	}
	public function cambioMatPedido(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$tid = htmlspecialchars(strip_tags($data->tid));
		$ce = htmlspecialchars(strip_tags($data->ce));
		$obs = htmlspecialchars(strip_tags($data->obs));
		if($tid != ''&& $ce !='' && $obs !=''){
				echo $this->model->cambioMatPedido($tid,$ce,$obs);
		}
	}
	public function nocambioMatPedido(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$tid = htmlspecialchars(strip_tags($data->tid));
		$ce = htmlspecialchars(strip_tags($data->ce));
		if($tid != ''&& $ce !=''){
				echo $this->model->nocambioMatPedido($tid,$ce);
		}
	}
	public function estadoPedido(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$id = htmlspecialchars(strip_tags($data->id));
		if($id != ''){
				echo $this->model->estadoPedido($id);
		}
	}
}
?>