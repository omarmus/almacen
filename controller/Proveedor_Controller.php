<?php 
/**
* 
*/
class Proveedor_Controller
{
	
	function __construct()
	{	
		session_start();
		if(!isset($_SESSION['id'])){
			session_unset();
			session_destroy();
			header("location:index.php");
			exit();
		}

		$file="Proveedor_Model";
		require_once "model/".$file.".php";
		$this->model=new $file();	
	}
	public function listarproveedor()
	{
		echo $this->model->listarproveedor();	
	}
	public function crearproveedor(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$nit = htmlspecialchars(strip_tags($data->nit));
		$emp = htmlspecialchars(strip_tags($data->emp));
		$nom = htmlspecialchars(strip_tags($data->nom));
		$ap = htmlspecialchars(strip_tags($data->ap));
		$dir = htmlspecialchars(strip_tags($data->dir));
		$fono = htmlspecialchars(strip_tags($data->fono));
		$email = htmlspecialchars(strip_tags($data->email));

		if($nit!='' && $nom!='' && $ap!='' && $dir!='' && $fono!='' && $email!=''){
			echo $this->model->crearproveedor($nit,$emp,$nom,$ap,$dir,$fono,$email);
		}
	}
	public function eliminarproveedor(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$id = htmlspecialchars(strip_tags($data->id));
		if($id != ''){
				echo $this->model->eliminarproveedor($id);
		}
	}
	public function buscarunproveedor(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$id = htmlspecialchars(strip_tags($data->id));
		if($id != ''){
				echo $this->model->buscarunproveedor($id);
		}
	}
	public function editarproveedor(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$id = htmlspecialchars(strip_tags($data->id));
		$nit = htmlspecialchars(strip_tags($data->nit));
		$emp = htmlspecialchars(strip_tags($data->emp));
		$nom = htmlspecialchars(strip_tags($data->nom));
		$ap = htmlspecialchars(strip_tags($data->ap));
		$dir = htmlspecialchars(strip_tags($data->dir));
		$fono = htmlspecialchars(strip_tags($data->fono));
		$email = htmlspecialchars(strip_tags($data->email));

		if($id!='' && $nit!='' && $nom!='' && $ap!='' && $dir!='' && $fono!='' && $email!=''){
			echo $this->model->editarproveedor($id,$nit,$emp,$nom,$ap,$dir,$fono,$email);
		}
	}
}
?>