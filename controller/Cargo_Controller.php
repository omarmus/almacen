<?php 
/**
* 
*/
class Cargo_Controller
{
	
	function __construct()
	{	
		session_start();
		if(!isset($_SESSION['id'])){
			session_unset();
			session_destroy();
			header("location:index.php");
			exit();
		}

		$file="Cargo_Model";
		require_once "model/".$file.".php";
		$this->model=new $file();

		

	}
	public function crearnuevocargo(){

		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$nomcargo = htmlspecialchars(strip_tags($data->nomcargo));
		$nomarea = htmlspecialchars(strip_tags($data->nomarea));
		$area_id = preg_replace('/[^0-9]/','',$nomarea);
		
		if($nomcargo!='' && $nomarea !='')
		{
			echo $this->model->crearnuevocargo($nomcargo,$area_id);
		}
	}
	public function listarcargos(){
		echo $this->model->listarcargos();
	}
	public function eliminarcargo(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$id = htmlspecialchars(strip_tags($data->id));
		if($id != ''){
				echo $this->model->eliminarcargo($id);
		}
	}
	public function buscaruncargo(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$id = htmlspecialchars(strip_tags($data->id));
		if($id != ''){
				echo $this->model->buscaruncargo($id);
		}
	}
	public function editarcargo(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

			$id = htmlspecialchars(strip_tags($data->id));;
			$nomcargo = htmlspecialchars(strip_tags($data->nomcargo));
			$area_id = htmlspecialchars(strip_tags($data->area_id));
			 
			if($id != '' && $nomcargo!='' && $area_id!=''){
				echo $this->model->editarcargo($id,$nomcargo,$area_id);
			}
	}
}
?>