<?php 
/**
* 
*/
class Reppersona_Controller
{
	
	function __construct()
	{	
		session_start();
		if(!isset($_SESSION['id'])){
			session_unset();
			session_destroy();
			header("location:index.php");
			exit();
		}

		$file="Reppersona_Model";
		require_once "model/".$file.".php";
		$this->model=new $file();
	}
	public function datosfuncionarios()
	{
		
		echo $this->model->datosfuncionarios();
		
	}
	public function buscarDatos(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$fechaini = htmlspecialchars(strip_tags($data->inicial));
		$fechafin = htmlspecialchars(strip_tags($data->final));
		$idfun = htmlspecialchars(strip_tags($data->idfun));
		if($fechaini != '' && $fechafin!='' && $idfun!=''){
				echo $this->model->buscarDatos($fechaini, $fechafin,$idfun);
		}
	}
	public function pdf()
	{
		require_once('core/TCPDF/tcpdf.php');
		$date1 = htmlspecialchars(strip_tags($_GET['inicial']));
		$date2 = htmlspecialchars(strip_tags($_GET['final']));

		$idfun = htmlspecialchars(strip_tags($_GET['idfun']));
		$fechaini = date("Y-m-d", strtotime($date1));
		$fechafin = date("Y-m-d", strtotime($date2));
		echo $this->model->pdf($fechaini,$fechafin,$idfun);
	}
	/*public function excel()
	{
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$fechaini = htmlspecialchars(strip_tags($data->inicial));;
		$fechafin = htmlspecialchars(strip_tags($data->final));
		if($fechaini != '' && $fechafin!=''){
				echo $this->model->excel($fechaini, $fechafin);
		}else{
			echo "error de fechas";
		}
	}*/
}
?>