<?php 
/**
* 
*/
class Avanzado_Controller
{
	
	public function menuavanzado()
	{
		session_start();
		if(!isset($_SESSION['id'])){
			session_destroy();
			header("location:index.php");
			exit();
		}
		require_once("views/avanzado/avanzado.php");
	}

}
?>