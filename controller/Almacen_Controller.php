<?php
/**
*
*/
class Almacen_Controller
{

	function __construct()
	{
		$file="Almacen_Model";
		require_once "model/".$file.".php";
		$this->model=new $file();
	}
	public function listarinventario () {
		echo $this->model->listarinventario(false, false);
	}
	public function listardisponible () {
		echo $this->model->listarinventario(true, false);
	}
	public function listarnodisponible () {
		echo $this->model->listarinventario(false, true);
	}
	public function historial(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$id = htmlspecialchars(strip_tags($data->id));
		$tipo = htmlspecialchars(strip_tags($data->tipo));
		$mes = htmlspecialchars(strip_tags($data->mes));
		$gestion = htmlspecialchars(strip_tags($data->gestion));
		if($id != ''){
				echo $this->model->historial($id, $tipo, $mes, $gestion);
		}
	}
	public function detalleunmaterial(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$id = htmlspecialchars(strip_tags($data->id));
		if($id != ''){
				echo $this->model->detalleunmaterial($id);
		}
	}
	public function pdf()
	{
		$no = htmlspecialchars(strip_tags( $_GET['material']));
		$nombre = htmlspecialchars(strip_tags( $_GET['nombre']));
		echo $this->model->pdf($no == 'si', $nombre);
	}
}
?>