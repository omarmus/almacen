<?php 
/**
* 
*/
class Salidas_Controller
{
	
	function __construct()
	{	
		session_start();
		if(!isset($_SESSION['id'])){
			session_unset();
			session_destroy();
			header("location:index.php");
			exit();
		}

		$file="Salidas_Model";
		require_once "model/".$file.".php";
		$this->model=new $file();
	}
	public function listarSalidas(){
		echo $this->model->listarSalidas();
	}
	public function detalleunasalida(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$id = htmlspecialchars(strip_tags($data->id));
		if ($id != '') {
			echo $this->model->detalleunasalida($id);
		}
	}
}
?>