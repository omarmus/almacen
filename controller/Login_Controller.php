<?php 
/**
* 
*/
class Login_Controller
{
	
	function __construct()
	{
		$file="Login_Model";
		require_once "model/".$file.".php";
		$this->model=new $file();
	}
	public function session()
	{
		$loginvar = file_get_contents('php://input');
		$data = json_decode($loginvar);
		$user=addslashes(htmlspecialchars($data->user));
		$pass=addslashes(htmlspecialchars($data->pass));
		if ($user!='' && $pass!='') {
			
			echo $this->model->session($user, $pass);

		}
	}
	
}
 ?>