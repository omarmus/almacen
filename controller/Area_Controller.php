<?php 
/**
* 
*/
class Area_Controller
{
	
	function __construct()
	{	
		session_start();
		if(!isset($_SESSION['id'])){
			session_unset();
			session_destroy();
			header("location:index.php");
			exit();
		}

		$file="Area_Model";
		require_once "model/".$file.".php";
		$this->model=new $file();

		

	}
	public function crearnuevaarea(){

		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$nomarea = htmlspecialchars(strip_tags($data->nomarea));

		if($nomarea!='')
		{
			echo $this->model->crearnuevaarea($nomarea);
		}
	}
	public function listarareas(){
		echo $this->model->listarareas();
	}
	public function eliminararea(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$id = htmlspecialchars(strip_tags($data->id));
		if($id != ''){
				echo $this->model->eliminararea($id);
		}
	}
	public function editararea(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

			$id = htmlspecialchars(strip_tags($data->id));;
			$descripcion = htmlspecialchars(strip_tags($data->descripcion));
			 
			if($id != '' && $descripcion!=''){
				echo $this->model->editararea($id,$descripcion);
			}
	}
	public function buscarunarea(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$id = htmlspecialchars(strip_tags($data->id));
		if($id != ''){
				echo $this->model->buscarunarea($id);
		}
	}	

}
?>