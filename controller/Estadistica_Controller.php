<?php 
/**
* 
*/
class Estadistica_Controller
{
	
	function __construct()
	{	
		session_start();
		if(!isset($_SESSION['id'])){
			session_unset();
			session_destroy();
			header("location:index.php");
			exit();
		}

		$file="Estadistica_Model";
		require_once "model/".$file.".php";
		$this->model=new $file();
		//date_default_timezone_set("America/La_Paz");
	}
	public function buscarDatos(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$fechaini = htmlspecialchars(strip_tags($data->inicial));;
		$fechafin = htmlspecialchars(strip_tags($data->final));
		if($fechaini != '' && $fechafin!=''){
				echo $this->model->buscarDatos($fechaini, $fechafin);
		}else{
			echo "error de fechas";
		}
	}
	public function pdf()
	{
		require_once('core/TCPDF/tcpdf.php');
		$date1 = htmlspecialchars(strip_tags( $_GET['inicial']));
		$date2 = htmlspecialchars(strip_tags($_GET['final']));


		//$fecha1 = date_create($date1);
		//$fechaini = date_format($date1,'Y-m-d');
		//$fecha2 = date_create($date2);
		//$fechafin = date_format($fecha2,'Y-m-d');
		$fechaini = date("Y-m-d", strtotime($date1));
		$fechafin = date("Y-m-d", strtotime($date2));
		echo $this->model->pdf($fechaini,$fechafin);
		//echo($fechaini);
	}
	public function excel()
	{
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$fechaini = htmlspecialchars(strip_tags($data->inicial));;
		$fechafin = htmlspecialchars(strip_tags($data->final));
		if($fechaini != '' && $fechafin!=''){
				echo $this->model->excel($fechaini, $fechafin);
		}else{
			echo "error de fechas";
		}
	}
}
?>