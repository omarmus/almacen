<?php 
/**
* 
*/
class Categoria_Controller
{
	
	function __construct()
	{	
		session_start();
		if(!isset($_SESSION['id'])){
			session_unset();
			session_destroy();
			header("location:index.php");
			exit();
		}

		$file="Categoria_Model";
		require_once "model/".$file.".php";
		$this->model=new $file();
	}
	public function crearcategoria(){

		$variables = file_get_contents('php://input');
		$data = json_decode($variables);
		$tipo = htmlspecialchars(strip_tags($data->tipo));
		$cate = htmlspecialchars(strip_tags($data->cate));

		if($tipo!='' && $cate!='')
		{
			echo $this->model->crearcategoria($tipo, $cate);
		}
	}
	public function listarcategorias(){
		echo $this->model->listarcategorias();
	}
	public function eliminarcategoria(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$id = htmlspecialchars(strip_tags($data->id));
		if($id != ''){
				echo $this->model->eliminarcategoria($id);
		}
	}
	public function editarcate(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

			$id = htmlspecialchars(strip_tags($data->id));;
			$tipo = htmlspecialchars(strip_tags($data->tipo));
			$nombre_cat = htmlspecialchars(strip_tags($data->nomcat));
			 
			if($id != '' && $tipo!='' && $nombre_cat != ''){
				echo $this->model->editarcate($id,$tipo,$nombre_cat);
			}
	}
	public function buscarunacate(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$id = htmlspecialchars(strip_tags($data->id));
		if($id != ''){
				echo $this->model->buscarunacate($id);
		}
	}	

}
?>