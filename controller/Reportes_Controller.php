<?php 
/**
* 
*/
class Reportes_Controller
{
	
	function __construct()
	{	
		$file="Reportes_Model";
		require_once "model/".$file.".php";
		$this->model=new $file();
	}
	public function recibedatosPDF(){
		$variables = file_get_contents('php://input');
		$data = json_decode($variables);

		$id_usuario = htmlspecialchars(strip_tags($data->id_usuario));
		$id_fun = htmlspecialchars(strip_tags($data->id_fun));
		$idarea = htmlspecialchars(strip_tags($data->idarea));

		echo $this->model->recibedatosPDF($id_usuario,$id_fun,$idarea);
	}
	public function generarPDFpedido(){
		require_once('core/TCPDF/tcpdf.php');
		echo $this->model->generarPDFpedido();
	}
	public function generarPDFentrega(){
		require_once('core/TCPDF/tcpdf.php');
		$id = $_GET['id'];
		echo $this->model->generarPDFentrega($id);
	}
	public function generarPDFingresos(){
		/*require_once('core/TCPDF/tcpdf.php');
		$id = $_GET['id'];
		$idprov = $_GET['idprov'];
		echo $this->model->generarPDFingresos($id,$idprov);*/
		require_once('core/TCPDF/tcpdf.php');
		$id = htmlspecialchars(strip_tags($_GET['id']));
		$idprov = htmlspecialchars(strip_tags($_GET['idprov']));
		echo $this->model->generarPDFingresos($id,$idprov);
	}
}
?>